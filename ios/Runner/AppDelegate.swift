import UIKit
import Flutter
import GoogleMaps
import NaverThirdPartyLogin

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    if #available(iOS 10.0, *) {
      UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
    }
      
    GMSServices.provideAPIKey("AIzaSyAJrSk2SrjMvHkSe0FB74e9-wUrMz29N7E")
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)

  }
    override func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
      
      if (url.absoluteString.contains("thirdPartyLoginResult")) {
          return NaverThirdPartyLoginConnection.getSharedInstance().application(app, open: url, options: options)
      }

      return super.application(app, open: url, options: options)
      
    }

}
