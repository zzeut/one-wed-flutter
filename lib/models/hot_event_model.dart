import 'package:onewed_app/models/resize_image_model.dart';

class HotEvent {

  int? hotEventNo;
  String? hotEventTitle;
  String? content;
  ResizeImage? src;
  int? width;
  int? height;
  int? size;
  String? originFileName;
  String? extension;
  String? startDate;
  String? endDate;

  HotEvent({
    this.hotEventNo,
    this.hotEventTitle,
    this.content,
    this.src,
    this.width,
    this.height,
    this.size,
    this.originFileName,
    this.extension,
    this.startDate,
    this.endDate
  });

  factory HotEvent.fromJson(Map<String, dynamic> json){
    return HotEvent(
        hotEventNo: json['hot_event_no'],
        hotEventTitle: json['hot_event_title'],
        content: json['content'],
        src: json['src'] == null ? null : ResizeImage.fromJson(json['src']),
        width: json['width'],
        height: json['height'],
        size: json['size'],
        originFileName: json['origin_file_name'],
        extension: json['extension'],
        startDate: json['start_date'],
        endDate: json['end_date']
    );
  }
}
