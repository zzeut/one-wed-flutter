import 'package:json_annotation/json_annotation.dart';

part 'oauth_model.g.dart';

@JsonSerializable()
class Oauth {
  @JsonKey(name: 'token_type', defaultValue: '')
  String? tokenType;
  @JsonKey(name: 'expires_in')
  int? expiresIn;
  @JsonKey(name: 'access_token', defaultValue: '')
  String? accessToken;
  @JsonKey(name: 'refresh_token', defaultValue: '')
  String? refreshToken;

  Oauth({
    this.tokenType,
    this.expiresIn,
    this.accessToken,
    this.refreshToken,
  });

  factory Oauth.fromJson(Map<String, dynamic> json) => _$OauthFromJson(json);

  Map<String, dynamic> toJson() => _$OauthToJson(this);
}
