import 'package:onewed_app/models/company_model.dart';
import 'package:onewed_app/models/schedule_model.dart';
import 'package:onewed_app/models/user_model.dart';

class Spending{
  int? spendingNo;
  int? scheduleNo;
  int? userNo;
  int? writerNo;
  int? companyNo;

  int? spendingMoney;
  String? createdAt;

  User? user;
  Company? company;
  Schedule? schedule;


  Spending({
    this.spendingNo,
    this.scheduleNo,
    this.userNo,
    this.writerNo,
    this.companyNo,
    
    this.spendingMoney,
    this.createdAt,
    
    this.user,
    this.company,
    this.schedule
  });
  
  factory Spending.fromJson(Map<String, dynamic> json){
    return Spending(
      spendingNo: json['spending_no'],
      scheduleNo: json['schedule_no'],
      userNo: json['user_no'],
      writerNo: json['writer_no'],
      companyNo: json['company_no'],
      
      spendingMoney: json['spending_money'],
      createdAt: json['created_at'],
      
      user: json['user'] == null ? null : User.fromJson(json['user']),
      company: json['company'] == null ? null : Company.fromJson(json['company']),
      schedule: json['schedule'] == null ? null : Schedule.fromJson(json['schedule'])
    ); 
  }
}