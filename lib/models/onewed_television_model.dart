import 'package:onewed_app/models/resize_image_model.dart';

class OnewedTelevision{
  int? onewedTelevisionNo;
  String? url;
  ResizeImage? src;
  int? width;
  int? height;
  int? size;
  String? originFileName;
  String? extension;

  OnewedTelevision({
    this.onewedTelevisionNo,
    this.url,
    this.src,
    this.width,
    this.height,
    this.size,
    this.originFileName,
    this.extension,
  });

  factory OnewedTelevision.fromJson(Map<String, dynamic> json){
    return OnewedTelevision(
      onewedTelevisionNo: json['onewed_television_no'],
      url: json['url'],
      src: json['src'] == null ? null : ResizeImage.fromJson(json['src']),
      width: json['width'],
      height: json['height'],
      size: json['size'],
      originFileName: json['origin_file_name'],
      extension: json['extension'],
    );
  }

}