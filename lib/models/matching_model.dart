class Matching{
  int? matchingNo;
  int? userNo;
  int? plannerNo;
  int? status;
  String? createdAt;

  Matching({
    this.matchingNo,
    this.userNo,
    this.plannerNo,
    this.status,
    this.createdAt
  });

  factory Matching.fromJson(Map<String, dynamic> json){
    return Matching(
      matchingNo: json['matching_no'],
      userNo: json['user_no'],
      plannerNo: json['planner_no'],
      status: json['status'],
      createdAt: json['created_at']
    );
  }
}