import 'package:onewed_app/models/resize_image_model.dart';

class CompanyCarouselImage{
  int? companyCarouselImageNo;
  int? companyNo;
  ResizeImage? src;
  int? width;
  int? height;
  int? size;
  String? originFileName;
  String? extension;
  int? order;



  CompanyCarouselImage({
    this.companyCarouselImageNo,
    this.companyNo,
    this.src,
    this.width,
    this.height,
    this.size,
    this.originFileName,
    this.extension,
    this.order
  });


  factory CompanyCarouselImage.fromJson(Map<String, dynamic> json){
    return CompanyCarouselImage(
      companyCarouselImageNo: json['company_carousel_image_no'],
      companyNo: json['company_no'],
      src: json['src'] == null ? null : ResizeImage.fromJson(json['src']),
      width: json['width'],
      height: json['height'],
      size: json['size'],
      originFileName: json['origin_file_name'],
      extension: json['extension'],
      order: json['order']
    );
  }

}