class Notice{
  int? noticeNo;
  String? noticeId;
  int? userNo;
  String? noticeTitle;
  String? noticeContent;
  String? createdAt;
  bool? isOpen;

  Notice({
    this.noticeNo,
    this.noticeId,
    this.userNo,
    this.noticeTitle,
    this.noticeContent,
    this.createdAt,
    this.isOpen
  });

  factory Notice.fromJson(Map<String, dynamic> json){
    return Notice(
      noticeNo: json['notice_no'],
      noticeId: json['notice_id'],
      userNo: json['user_no'],
      noticeTitle: json['notice_title'],
      noticeContent: json['notice_content'],
      createdAt: json['created_at'],
      isOpen: false
    );
  }
}