import 'package:onewed_app/models/resize_image_model.dart';
import 'package:onewed_app/models/user_model.dart';

class Message {
  final int? messageNo;
  final String? threadId;
  final int? userNo;
  final Map? body;
  final String? createdAt;
  final String? updatedAt;
  final bool? isMine;
  final User? user;




  Message({
    this.messageNo,
    this.threadId,
    this.userNo,
    this.body,
    this.createdAt,
    this.updatedAt,
    this.isMine,
    this.user
  });

  factory Message.fromJson(Map<String, dynamic> json){
    List<User> _users = [];
    if (json['users'] != null ) {
      json['users'].forEach((element) {
        _users.add(User.fromJson(element));
      });
    }

    return Message(
        messageNo: json['message_no'],
        threadId: json['thread_id'],
        userNo: json['user_no'],
        body: json['body'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        isMine: json['is_mine'],
        user: json['user'] == null ? null : User.fromJson(json['user'])
    );
  }
}
