import 'package:onewed_app/models/sum_category_spending_money_model.dart';

class TotalSpending{
  int? userNo;
  int? budgetMoney;
  int? sumSpendingMoney;
  int? spendingPercentage;
  List<SumCategorySpendingMoney>? categorySpendingMoney;

  TotalSpending({
    this.userNo,
    this.budgetMoney,
    this.spendingPercentage,
    this.sumSpendingMoney,
    this.categorySpendingMoney,
  });

  factory TotalSpending.fromJson(Map<String, dynamic> json){
    List<SumCategorySpendingMoney> _categorySpendingMoney = [];

    if(json['sum_category_spending_moneys']!= null){
      json['sum_category_spending_moneys'].forEach((element) => _categorySpendingMoney.add(SumCategorySpendingMoney.fromJson(element)));
    }

    return TotalSpending(
      userNo: json['user_no'],
      budgetMoney: json['budget_money'],
      sumSpendingMoney: json['sum_spending_money'],
      spendingPercentage: json['spending_percentage'],
      categorySpendingMoney: _categorySpendingMoney
    );
  }

}