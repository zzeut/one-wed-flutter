class Thread{
  String? threadId;
  String? subject;

  Thread({
    this.threadId,
    this.subject
  });

  factory Thread.fromJson(Map<String, dynamic> json){
    return Thread(
      threadId: json['thread_id'],
      subject: json['subject']
    );
  }

}