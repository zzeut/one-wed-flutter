class Faq{
  int? faqNo;
  String? question;
  String? answer;
  bool? isOpen;

  Faq({
    this.faqNo,
    this.question,
    this.answer,
    this.isOpen
  });

  factory Faq.fromJson(Map<String, dynamic> json){
    return Faq(
      faqNo: json['faq_no'],
      question: json['faq_question'],
      answer: json['faq_answer'],
      isOpen: false
    );
  }
}