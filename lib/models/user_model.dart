import 'package:easy_localization/easy_localization.dart';
import 'package:onewed_app/models/area_code_model.dart';
import 'package:onewed_app/models/resize_image_model.dart';

class User {
  final int? userNo;
  final String? userId;
  final String? name;
  final String? email;
  final String? firstPhone;
  final String? secondPhone;
  final String? lastPhone;
  String? gender;
  final AreaCode? areaCode;
  final ResizeImage? profilePath;
  final String? weddingCeremonyYear;
  final String? weddingCeremonyMonth;
  final String? weddingCeremonyDate;
  final bool? isMarketingPush;
  final bool? isSchedulePush;
  final int? budgetMoney;
  final int? likeCount;
  final int? scheduleCount;
  final int? weddingCeremonyDDay;
  int? level;
  String? weddingCeremonyDayString;
  int? writerSchedule;



  User({
    this.userNo,
    this.userId,
    this.name,
    this.email,
    this.firstPhone,
    this.secondPhone,
    this.lastPhone,
    this.gender,
    this.areaCode,
    this.profilePath,
    this.weddingCeremonyYear,
    this.weddingCeremonyMonth,
    this.weddingCeremonyDate,
    this.isMarketingPush,
    this.isSchedulePush,
    this.budgetMoney,
    this.likeCount,
    this.scheduleCount,
    this.weddingCeremonyDDay,
    this.weddingCeremonyDayString,
    this.level,
    this.writerSchedule
  });

  factory User.fromJson(Map<String, dynamic> json){
    int? weddingCeremonyDDay;
    String weddingCeremonyDayString = "웨딩일정이 없습니다.";

    if(json['wedding_ceremony_year'] != null){
      DateTime now = DateFormat("yyyy-MM-dd").parse("${DateTime.now()}");

      DateTime weddingCeremonyDate = DateFormat("yyyy-MM-dd").parse("${json['wedding_ceremony_year']}-${json['wedding_ceremony_month']}-${json['wedding_ceremony_date']}");
      weddingCeremonyDDay = now.difference(weddingCeremonyDate).inDays;


      if(weddingCeremonyDDay > 0){
        weddingCeremonyDayString = "D+$weddingCeremonyDDay";
      }else if(weddingCeremonyDDay == 0){
        weddingCeremonyDayString = "D-Day";
      }else{
        weddingCeremonyDayString = "D$weddingCeremonyDDay";
      }

    }

    return User(
      userNo: json['user_no'],
      userId: json['uuid'],
      name: json['name'],
      email: json['email'],
      firstPhone: json['first_phone'],
      secondPhone: json['second_phone'],
      lastPhone: json['last_phone'],
      gender: json['gender'],
      areaCode: json['area_code'] == null ? null : AreaCode.fromJson(json['area_code']),
      profilePath: json['profile_path'] == null ? null : ResizeImage.fromJson(json['profile_path']),
      weddingCeremonyYear: json['wedding_ceremony_year'],
      weddingCeremonyMonth: json['wedding_ceremony_month'],
      weddingCeremonyDate: json['wedding_ceremony_date'],
      isMarketingPush: json['is_marketing_push'],
      isSchedulePush: json['is_schedule_push'],
      budgetMoney: json['budget_money'],
      likeCount: json['like_count'],
      scheduleCount: json['schedule_count'],
      weddingCeremonyDDay: weddingCeremonyDDay,
      weddingCeremonyDayString: weddingCeremonyDayString,
      level: json['level'],
      writerSchedule: json['writer_schedule']
    );
  }
}
