import 'package:onewed_app/models/company_model.dart';

class CompanyLike{
  int? companyLikeNo;
  int? userNo;
  int? companyNo;
  String? deletedAt;
  Company? company;

  CompanyLike({
    this.companyLikeNo,
    this.userNo,
    this.companyNo,
    this.deletedAt,
    this.company
    
  });

  factory CompanyLike.fromJson(Map<String, dynamic> json){
    return CompanyLike(
      companyLikeNo: json['company_like_no'],
      userNo: json['user_no'],
      companyNo: json['company_no'],
      deletedAt: json['deleted_at'],
      company: json['company'] == null ? null : Company.fromJson(json['company'])
    );
  }
}