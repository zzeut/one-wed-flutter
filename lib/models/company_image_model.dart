import 'package:onewed_app/models/company_image_category_model.dart';
import 'package:onewed_app/models/resize_image_model.dart';

class CompanyImage{
  int? companyImageNo;
  int? companyNo;
  int? companyImageCategoryNo;
  ResizeImage? src;
  int? width;
  int? height;
  int? size;
  String? originFileName;
  String? extension;
  int? order;
  CompanyImageCategory? companyImageCategory;

  CompanyImage({
    this.companyImageNo,
    this.companyNo,
    this.companyImageCategoryNo,
    this.src,
    this.width,
    this.height,
    this.size,
    this.originFileName,
    this.extension,
    this.order,
    this.companyImageCategory

  });

  factory CompanyImage.fromJson(Map<String, dynamic> json){
    return CompanyImage(
      companyImageNo: json['company_image_no'],
      companyNo: json['company_no'],
      companyImageCategoryNo: json['company_image_category_no'],
      src: json['src'] == null ? null : ResizeImage.fromJson(json['src']),
      width: json['width'],
      height: json['height'],
      size: json['size'],
      originFileName: json['origin_file_name'],
      extension: json['extension'],
      order: json['order'],
      companyImageCategory: json['company_image_category'] == null ? null : CompanyImageCategory.fromJson(json['company_image_category'])
    );
  }

}