import 'package:easy_localization/easy_localization.dart';
import 'package:onewed_app/models/company_model.dart';
import 'package:onewed_app/models/user_model.dart';

class Schedule {
  int? scheduleNo;
  String? scheduleId;
  int? writerNo;
  int? userNo;
  int? companyNo;
  String? scheduleName;
  String? scheduleDate;
  String? startTime;
  String? endTime;
  String? memo;
  Company? company;
  int? startTimeDuration;
  int? endTimeDuration;
  int? state;
  User? planner;

  Schedule(
      {
        this.scheduleNo,
        this.scheduleId,
        this.writerNo,
        this.userNo,
        this.companyNo,
        this.scheduleName,
        this.scheduleDate,
        this.startTime,
        this.endTime,
        this.memo,
        this.company,
        this.startTimeDuration,
        this.endTimeDuration,
        this.state,
        this.planner
      });

  factory Schedule.fromJson(Map<String, dynamic> json) {
    DateTime now = DateTime.now();
    now = DateFormat("yyyy-MM-dd").parse("$now");
    DateTime scheduleStartDate = DateFormat("yyyy-MM-dd").parse("${json['schedule_date']}");
    DateTime scheduleEndDate = DateFormat("yyyy-MM-dd").parse("${json['schedule_date']}");



    int state = 0;
    if(now.difference(scheduleStartDate) < Duration.zero){
      //진행대기
      state = 1;
    }else if(now.difference(scheduleStartDate) >= Duration.zero && now.difference(scheduleEndDate) <= Duration.zero){
      //진행중
      state = 2;
    }else if(now.difference(scheduleEndDate) > Duration.zero){
      //진행완료
      state = 3;
    }




    return Schedule(
        scheduleNo: json['schedule_no'],
        scheduleId: json['schedule_id'],
        writerNo: json['writer_no'],
        userNo: json['user_no'],
        companyNo: json['company_no'],
        scheduleName: json['schedule_name'],
        scheduleDate: json['schedule_date'],
        startTime: json['start_time'],
        endTime: json['end_time'],
        memo: json['memo'],
        company: json['company'] == null ? null : Company.fromJson(json['company']),
        startTimeDuration: now.difference(scheduleStartDate).inDays,
        endTimeDuration: now.difference(scheduleEndDate).inDays,
        state: state,
      planner: json['planner'] == null ? null : User.fromJson(json['planner'])

    );

  }
}
