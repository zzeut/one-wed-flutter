class Term{
  int? termNo;
  String? termTitle;
  String? termContent;
  String? type;

  Term({
    this.termNo,
    this.termTitle,
    this.termContent,
    this.type
  });

  factory Term.fromJson(Map<String, dynamic> json){
    return Term(
      termNo: json['term_no'],
      termTitle: json['terms_title'],
      termContent: json['terms_content'],
      type: json['type']
    );
  }
}