import 'package:flutter/material.dart';
import 'package:onewed_app/models/company_model.dart';
import 'package:onewed_app/models/company_review_image_model.dart';
import 'package:onewed_app/models/schedule_model.dart';
import 'package:onewed_app/models/user_model.dart';

class CompanyReview{
  int? companyReviewNo;
  String? companyReviewId;
  int? scheduleNo;
  int? companyNo;
  int? userNo;
  int? score;
  String? content;
  String? createdAt;
  List<CompanyReviewImage>? companyReviewImageList;
  Schedule? schedule;
  Company? company;
  User? user;

  CompanyReview({
    this.companyReviewNo,
    this.companyReviewId,
    this.scheduleNo,
    this.companyNo,
    this.userNo,
    this.score,
    this.content,
    this.createdAt,
    this.companyReviewImageList,
    this.schedule,
    this.company,
    this.user
  });

  factory CompanyReview.fromJson(Map<String, dynamic> json){
    List<CompanyReviewImage> companyReviewImageList = [];
    if(json['company_review_images'] != null){
      json['company_review_images'].forEach((element) => companyReviewImageList.add(CompanyReviewImage.fromJson(element)));
    }


    return CompanyReview(
      companyReviewNo: json['company_review_no'],
      companyReviewId: json['company_review_id'],
      scheduleNo: json['schedule_no'],
      companyNo: json['company_no'],
      userNo: json['user_no'],
      score: json['score'],
      content: json['content'],
      createdAt: json['created_at'],
      companyReviewImageList:companyReviewImageList,
      schedule: json['schedule'] == null ? null : Schedule.fromJson(json['schedule']),
      company: json['company'] == null ? null : Company.fromJson(json['company']),
      user: json['user'] == null ? null : User.fromJson(json['user'])
    ); 
  }



}