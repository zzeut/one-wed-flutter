class onboarding{
  String? content;
  String? title;
  String? imageUrl;

  onboarding({
    this.content,
    this.title,
    this.imageUrl
  });

  factory onboarding.fromJson(Map<dynamic, dynamic> json){
    return onboarding(
      content: json['content'],
      title: json['title'],
      imageUrl: json['image_url']
    );
  }
}