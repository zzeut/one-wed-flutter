import 'package:onewed_app/models/resize_image_model.dart';

class Event {

  int? eventNo;
  String? eventId;
  String? eventTitle;
  String? content;
  ResizeImage? src;
  int? width;
  int? height;
  int? size;
  String? originFileName;
  String? extension;
  String? startDate;
  String? endDate;

  Event({
    this.eventNo,
    this.eventId,
    this.eventTitle,
    this.content,
    this.src,
    this.width,
    this.height,
    this.size,
    this.originFileName,
    this.extension,
    this.startDate,
    this.endDate
  });

  factory Event.fromJson(Map<String, dynamic> json){
    return Event(
      eventNo: json['event_no'],
      eventId: json['event_id'],
      eventTitle: json['event_title'],
      content: json['content'],
      src: json['src'] == null ? null : ResizeImage.fromJson(json['src']),
      width: json['width'],
      height: json['height'],
      size: json['size'],
      originFileName: json['origin_file_name'],
      extension: json['extension'],
      startDate: json['start_date'],
      endDate: json['end_date']
    );
  }
}
