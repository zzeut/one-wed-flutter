class InquiryAnswer{
  int? answerNo;
  String? answerId;
  int? userNo;
  int? questionNo;
  String? content;
  String? createdAt;


  InquiryAnswer({
    this.answerNo,
    this.answerId,
    this.userNo,
    this.questionNo,
    this.content,
    this.createdAt
  });

  factory InquiryAnswer.fromJson(Map<String, dynamic> json){
    return InquiryAnswer(
      answerNo: json['answer_no'],
      answerId: json['answer_id'],
      userNo: json['user_no'],
      questionNo: json['question_no'],
      content: json['answer_content'],
      createdAt: json['created_at']
    );
  }
}