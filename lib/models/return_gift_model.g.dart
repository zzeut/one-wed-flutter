// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'return_gift_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReturnGift _$ReturnGiftFromJson(Map<String, dynamic> json) {
  return ReturnGift(
    returnGiftId: json['return_gift_id'] as String? ?? '',
    createdAt: json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String),
    updatedAt: json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String),
    order: json['order'] as String? ?? '',
    company: json['company'] as String? ?? '',
    product: json['product'] as String? ?? '',
    address: json['address'] as String? ?? '',
    rating: (json['rating'] as num?)?.toDouble(),
    isLike: json['is_like'] as bool? ?? false,
    returnGiftPhotos: (json['return_gift_photos'] as List<dynamic>?)
            ?.map((e) => ReturnGiftPhoto.fromJson(e as Map<String, dynamic>))
            .toList() ??
        [],
    returnGiftComments: (json['return_gift_comments'] as List<dynamic>?)
            ?.map((e) => ReturnGiftComment.fromJson(e as Map<String, dynamic>))
            .toList() ??
        [],
    orderTitle: json['orderTitle'] as String?,
  );
}

Map<String, dynamic> _$ReturnGiftToJson(ReturnGift instance) =>
    <String, dynamic>{
      'return_gift_id': instance.returnGiftId,
      'created_at': instance.createdAt?.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'order': instance.order,
      'company': instance.company,
      'product': instance.product,
      'address': instance.address,
      'rating': instance.rating,
      'is_like': instance.isLike,
      'return_gift_photos': instance.returnGiftPhotos,
      'return_gift_comments': instance.returnGiftComments,
      'orderTitle': instance.orderTitle,
    };

ReturnGiftPhoto _$ReturnGiftPhotoFromJson(Map<String, dynamic> json) {
  return ReturnGiftPhoto(
    returnGiftPhotoId: json['return_gift_photo_id'] as String? ?? '',
    createdAt: json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String),
    updatedAt: json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String),
    imageUrl: json['image_url'] as String? ?? '',
  );
}

Map<String, dynamic> _$ReturnGiftPhotoToJson(ReturnGiftPhoto instance) =>
    <String, dynamic>{
      'return_gift_photo_id': instance.returnGiftPhotoId,
      'created_at': instance.createdAt?.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'image_url': instance.imageUrl,
    };

ReturnGiftComment _$ReturnGiftCommentFromJson(Map<String, dynamic> json) {
  return ReturnGiftComment(
    returnGiftCommentId: json['return_gift_comment_id'] as String? ?? '',
    createdAt: json['created_at'] == null
        ? null
        : DateTime.parse(json['created_at'] as String),
    updatedAt: json['updated_at'] == null
        ? null
        : DateTime.parse(json['updated_at'] as String),
    title: json['title'] as String? ?? '',
  );
}

Map<String, dynamic> _$ReturnGiftCommentToJson(ReturnGiftComment instance) =>
    <String, dynamic>{
      'return_gift_comment_id': instance.returnGiftCommentId,
      'created_at': instance.createdAt?.toIso8601String(),
      'updated_at': instance.updatedAt?.toIso8601String(),
      'title': instance.title,
    };
