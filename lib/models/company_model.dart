import 'dart:ffi';

import 'package:onewed_app/models/area_code_model.dart';
import 'package:onewed_app/models/category_model.dart';
import 'package:onewed_app/models/company_carousel_image_model.dart';
import 'package:onewed_app/models/company_image_category_model.dart';
import 'package:onewed_app/models/company_information_model.dart';
import 'package:onewed_app/models/company_representative_image_model.dart';
import 'package:onewed_app/models/planner_model.dart';
import 'package:onewed_app/models/resize_image_model.dart';

class Company{
  int? companyNo;
  String? companyId;
  String? companyName;
  int? categoryNo;
  ResizeImage? representativeImageSrc;
  CompanyInformation? companyInformation;
  AreaCode? areaCode;
  double? lat;
  double? lng;
  String? zipCode;
  String? address;
  String? reviewScore;
  int? reviewCount;
  int? likeCount;
  String? description;
  bool? companyLikeExists;
  CompanyRepresentativeImage? companyRepresentativeImage;
  List<CompanyCarouselImage>? companyCarouselImage;
  Categories? category;
  List<CompanyImageCategory>? imageCategoryList;
  String? firstPhone;
  String? secondPhone;
  String? lastPhone;
  List<Planner>? planners;
  Planner? planner;

  Company({
    this.companyNo,
    this.companyId,
    this.companyName,
    this.categoryNo,
    this.representativeImageSrc,
    this.companyInformation,
    this.areaCode,
    this.lat,
    this.lng,
    this.zipCode,
    this.address,
    this.reviewScore,
    this.reviewCount,
    this.likeCount,
    this.description,
    this.companyLikeExists,
    this.companyRepresentativeImage,
    this.companyCarouselImage,
    this.category,
    this.imageCategoryList,
    this.firstPhone,
    this.secondPhone,
    this.lastPhone,
    this.planners,
    this.planner
  });


  factory Company.fromJson(Map<String, dynamic> json){
    List<CompanyCarouselImage> companyCarouselImages = [];
    if(json['company_carousel_images']!=null){
      json['company_carousel_images'].forEach((element) => companyCarouselImages.add(CompanyCarouselImage.fromJson(element)));
    }

    List<CompanyImageCategory> companyImageCategoryList = [];
    if(json['company_image_categories'] != null){
      json['company_image_categories'].forEach((element) => companyImageCategoryList.add(CompanyImageCategory.fromJson(element)));
    }

    List<Planner> plannerList = [];
    if(json['planners'] != null){
      json['planners'].forEach((element) => plannerList.add(Planner.fromJson(element)));
    }


    return Company(
      companyNo: json['company_no'],
      companyId: json['company_id'],
      companyName: json['company_name'],
      categoryNo: json['category_no'],
      representativeImageSrc: json['representative_image_src'] == null ? null : ResizeImage.fromJson(json['representative_image_src']),
      companyInformation: json['company_information'] == null ? null : CompanyInformation.fromJson(json['company_information']),
      areaCode: json['area_code'] == null ? null : AreaCode.fromJson(json['area_code']),
      lat: json['lat'],
      lng: json['lng'],
      zipCode: json['zip_code'],
      address: json['address'],
      reviewCount: json['review_count'],
      reviewScore: json['review_score'],
      likeCount: json['like_count'],
      description: json['description'],
      companyLikeExists: json['company_like_exists'],
      companyRepresentativeImage: json['company_representative_image'] == null ? null : CompanyRepresentativeImage.fromJson(json['company_representative_image']),
      companyCarouselImage: companyCarouselImages,
      category: json['category'] == null ? null : Categories.fromJson(json['category']),
      imageCategoryList: companyImageCategoryList,
      firstPhone: json['first_phone'],
      secondPhone: json['second_phone'],
      lastPhone: json['last_phone'],
      planners: plannerList,
      planner: json['planner'] == null ? null : Planner.fromJson(json['planner'])
    );
  }


}