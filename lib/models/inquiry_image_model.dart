import 'package:onewed_app/models/resize_image_model.dart';

class InquiryImage{
  int? questionImageNo;
  int? questionNo;
  ResizeImage? src;
  int? width;
  int? height;
  int? size;
  String? originFileName;
  String? extension;

  InquiryImage({
    this.questionImageNo,
    this.questionNo,
    this.src,
    this.width,
    this.height,
    this.size,
    this.originFileName,
    this.extension
  });

  factory InquiryImage.fromJson(Map<String, dynamic> json){
    return InquiryImage(
      questionImageNo: json['question_image_no'],
      questionNo: json['question_no'],
      src: json['src'] == null ? null : ResizeImage.fromJson(json['src']),
      width: json['width'],
      height: json['height'],
      size: json['size'],
      originFileName: json['origin_file_name'],
      extension: json['extension']
    );
  }
}