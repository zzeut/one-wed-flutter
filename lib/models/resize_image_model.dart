class ResizeImage {
  final String s;
  final String sm;
  final String m;
  final String lm;
  final String l;
  final String o;

  ResizeImage({
    required this.s,
    required this.sm,
    required this.m,
    required this.lm,
    required this.l,
    required this.o
  });

  factory ResizeImage.fromJson(Map<String, dynamic> json) {
    return ResizeImage(
        s: json['s'],
        sm: json['s_m'],
        m: json['m'],
        lm: json['l_m'],
        l: json['l'],
        o: json['o']
    );
  }
}