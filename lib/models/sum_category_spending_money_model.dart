class SumCategorySpendingMoney{
  int? categoryNo;
  String? categoryName;
  int? spendingMoney;

  SumCategorySpendingMoney({
    this.categoryNo,
    this.categoryName,
    this.spendingMoney
  });

  factory SumCategorySpendingMoney.fromJson(Map<String, dynamic> json){
    return SumCategorySpendingMoney(
      categoryNo: json['category_no'],
      categoryName: json['category_name'],
      spendingMoney: json['sum_category_spending_money']
    );
  }

}