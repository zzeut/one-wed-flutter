class CompanyInformation{
  String? reception;
  String? holeCount;
  String? numberOfCapacity;
  String? numberOfParkingLots;
  String? honeymoonCountry;

  CompanyInformation({
    this.reception,
    this.holeCount,
    this.numberOfCapacity,
    this.numberOfParkingLots,
    this.honeymoonCountry
  });

  factory CompanyInformation.fromJson(Map<String, dynamic> json){
    return CompanyInformation(
      reception: json['reception'],
      holeCount: json['hole_count'].toString(),
      numberOfCapacity: json['number_of_capacity'].toString(),
      numberOfParkingLots: json['number_of_parking_lots'].toString(),
      honeymoonCountry: json['honeymoon_country']
    );
  }
}