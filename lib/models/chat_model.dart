import 'package:onewed_app/models/message_model.dart';
import 'package:onewed_app/models/resize_image_model.dart';
import 'package:onewed_app/models/user_model.dart';

class Chat {
  final int? participantNo;
  final String? threadId;
  final int? userNo;
  final bool? muted;
  final String? lastRead;
  final String? createdAt;
  final String? updatedAt;
  final String? deletedAt;
  final List<User>? users;
  int? newMessageCount;
  final Message? lastMessage;

  Chat({
    this.participantNo,
    this.threadId,
    this.userNo,
    this.muted,
    this.lastRead,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.users,
    this.newMessageCount,
    this.lastMessage
  });

  factory Chat.fromJson(Map<String, dynamic> json){
    List<User> _users = [];

    if (json['users'] != null ) {

      json['users'].forEach((element) {
        if(element!=null){
          _users.add(User.fromJson(element));
        }

      });

    }

    return Chat(
      participantNo: json['participant_no'],
      threadId: json['thread_id'],
      userNo: json['user_no'],
      muted: json['muted'],
      lastRead: json['last_read'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
      deletedAt: json['deleted_at'],
      users: _users,
      newMessageCount: json['new_message_count'],
      lastMessage: json['last_message'] == null ? null : Message.fromJson(json['last_message'])
    );
  }
}
