import 'package:onewed_app/models/resize_image_model.dart';

class Categories{
  int? categoryNo;
  String? categoryName;
  String? categoryContent;
  ResizeImage? src;
  int? parentCategoryNo;
  List<Categories>? childCategories;
  ResizeImage? backgroundSrc;
  int? type;

  Categories({
    this.categoryNo,
    this.categoryName,
    this.categoryContent,
    this.src,
    this.parentCategoryNo,
    this.childCategories,
    this.backgroundSrc,
    this.type
  });

  factory Categories.fromJson(Map<String, dynamic> json){
    List<Categories> _categoryList = [];
    if(json['child_categories'] != null){
      json['child_categories'].forEach((element) => {
        _categoryList.add(Categories.fromJson(element))
      });
    }


    return Categories(
      categoryNo: json['category_no'],
      categoryName: json['category_name'],
      categoryContent: json['category_content'],
      src: json['src'] == null ? null : ResizeImage.fromJson(json['src']),
      parentCategoryNo: json['parent_category_no'],
      childCategories: _categoryList,
      backgroundSrc: json['background_src'] == null ? null : ResizeImage.fromJson(json['background_src']),
      type: json['type']
    );
  }
}