import 'package:onewed_app/models/category_model.dart';
import 'package:onewed_app/models/inquiry_answer_model.dart';
import 'package:onewed_app/models/inquiry_image_model.dart';
import 'package:onewed_app/models/user_model.dart';

class Inquiry{
  int? questionNo;
  String? questionId;
  int? userNo;
  int? categoryNo;
  String? content;
  String? createdAt;
  bool? isOpen;
  
  User? user;
  InquiryAnswer? inquiryAnswer;
  Categories? category;
  InquiryImage? inquiryImage;

  Inquiry({
    this.questionNo,
    this.questionId,
    this.userNo,
    this.categoryNo,
    this.content,
    this.createdAt,
    this.isOpen,
    
    this.user,
    this.inquiryAnswer,
    this.category,
    this.inquiryImage
  });

  factory Inquiry.fromJson(Map<String, dynamic> json){
    return Inquiry(
      questionNo: json['question_no'],
      questionId: json['question_id'],
      userNo: json['user_no'],
      categoryNo: json['category_no'],
      content: json['question_content'],
      createdAt: json['created_at'],

      isOpen: false,
      
      user: json['user'] == null ? null : User.fromJson(json['user']),
      inquiryAnswer: json['one_on_one_answer'] == null ? null : InquiryAnswer.fromJson(json['one_on_one_answer']),
      category: json['category'] == null ? null : Categories.fromJson(json['category']),
      inquiryImage: json['one_on_one_question_image'] == null ? null : InquiryImage.fromJson(json['one_on_one_question_image'])
    );
  }
}