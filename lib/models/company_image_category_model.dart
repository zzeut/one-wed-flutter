class CompanyImageCategory{
  int? companyImageCategoryNo;
  int? categoryNo;
  String? categoryName;

  CompanyImageCategory({
    this.companyImageCategoryNo,
    this.categoryNo,
    this.categoryName
  });

  factory CompanyImageCategory.fromJson(Map<String, dynamic> json){
    return CompanyImageCategory(
      companyImageCategoryNo: json['company_image_category_no'],
      categoryNo: json['category_no'],
      categoryName: json['category_name']
    );
  }

}