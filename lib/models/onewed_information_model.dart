class OnewedInformation{
  String? phoneNumber;
  String? businessDay;
  String? businessTime;

  OnewedInformation({
    this.phoneNumber,
    this.businessDay,
    this.businessTime
  });

  factory OnewedInformation.fromJson(Map<String, dynamic> json){
    return OnewedInformation(
      phoneNumber: json['phone_number_info'],
      businessDay: json['business_day_info'],
      businessTime: json['business_time_info']
    );
  }

}