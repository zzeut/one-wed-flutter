import 'package:onewed_app/models/company_model.dart';
import 'package:onewed_app/models/event_model.dart';
import 'package:onewed_app/models/resize_image_model.dart';

class RecommendWedding{
  int? recommendWeddingInformationNo;
  int? companyNo;
  int? eventNo;
  int? order;
  ResizeImage? src;
  int? width;
  int? height;
  int? size;
  String? originFileName;
  String? extension;
  Company? company;
  Event? event;

  RecommendWedding({
    this.recommendWeddingInformationNo,
    this.companyNo,
    this.eventNo,
    this.order,
    this.src,
    this.width,
    this.height,
    this.size,
    this.originFileName,
    this.extension,
    this.company,
    this.event
  });

  factory RecommendWedding.fromJson(Map<String, dynamic> json){
    return RecommendWedding(
      recommendWeddingInformationNo: json['recommended_wedding_information_no'],
      companyNo: json['company_no'],
      eventNo: json['event_no'],
      order: json['order'],
      src: json['src'] == null ? null : ResizeImage.fromJson(json['src']),
      width: json['width'],
      height: json['height'],
      size: json['size'],
      originFileName: json['origin_file_name'],
      extension: json['extension'],
      company: json['company'] == null ? null : Company.fromJson(json['company']),
      event: json['event'] == null ? null : Event.fromJson(json['event'])
    );
  }
}