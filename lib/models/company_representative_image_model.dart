import 'package:onewed_app/models/resize_image_model.dart';

class CompanyRepresentativeImage{
  int? companyRepresentativeImageNo;
  int? companyNo;
  ResizeImage? src;
  int? width;
  int? height;
  int? size;
  String? originFileName;
  String? extension;




  CompanyRepresentativeImage({
    this.companyRepresentativeImageNo,
    this.companyNo,
    this.src,
    this.width,
    this.height,
    this.size,
    this.originFileName,
    this.extension
  });

  factory CompanyRepresentativeImage.fromJson(Map<String, dynamic> json){
    return CompanyRepresentativeImage(
        companyRepresentativeImageNo: json['company_representative_image_no'],
        companyNo: json['company_no'],
        src: json['src'] == null ? null : ResizeImage.fromJson(json['src']),
        width: json['width'],
        height: json['height'],
        size: json['size'],
        originFileName: json['origin_file_name'],
        extension: json['extension']
    );
  }
}