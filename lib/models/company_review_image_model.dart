import 'package:onewed_app/models/resize_image_model.dart';

class CompanyReviewImage{
  int? companyReviewImageNo;
  int? companyReviewNo;
  ResizeImage? src;
  int? width;
  int? height;
  int? size;
  String? originFileName;
  String? extension;


  CompanyReviewImage({
    this.companyReviewImageNo,
    this.companyReviewNo,
    this.src,
    this.width,
    this.height,
    this.size,
    this.originFileName,
    this.extension
  });

  factory CompanyReviewImage.fromJson(Map<String, dynamic> json){
    return CompanyReviewImage(
      companyReviewImageNo: json['company_review_image_no'],
      companyReviewNo: json['company_review_no'],
      src: json['src'] == null ? null : ResizeImage.fromJson(json['src']),
      width: json['width'],
      height: json['height'],
      size: json['size'],
      originFileName: json['origin_file_name'],
      extension: json['extension']
    );
  }
}