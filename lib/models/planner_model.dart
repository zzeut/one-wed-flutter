class Planner{
  int? companyPlannerNo;
  int? companyNo;
  int? userNo;
  int? level;

  Planner({
    this.companyPlannerNo,
    this.companyNo,
    this.userNo,
    this.level
  });

  factory Planner.fromJson(Map<String, dynamic> json){
    return Planner(
      companyPlannerNo: json['company_planner_no'],
      companyNo: json['company_no'],
      userNo: json['user_no'],
      level: json['level']
    );
  }
}