class CheckItem{
  int? checkItemNo;
  int? checkNo;
  int? order;
  String? content;

  CheckItem({
    this.checkItemNo,
    this.checkNo,
    this.order,
    this.content
  });

  factory CheckItem.fromJson(Map<String, dynamic> json){
    return CheckItem(
      checkItemNo: json['check_item_no'],
      checkNo: json['check_no'],
      order: json['order'],
      content: json['content']
    );
  }
}