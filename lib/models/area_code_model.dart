class AreaCode{
  int? code;
  String? areaName;
  String? groupCode;
  String? region1DepthName;
  String? region2DepthName;
  String? region3DepthName;

  AreaCode({
    this.code,
    this.areaName,
    this.groupCode,
    this.region1DepthName,
    this.region2DepthName,
    this.region3DepthName
  });

  factory AreaCode.fromJson(Map<String, dynamic> json){
    return AreaCode(
      code: json['code'],
      areaName: json['area_name'],
      groupCode: json['group_code'],
      region1DepthName: json['region_1depth_name'],
      region2DepthName: json['region_2depth_name'],
      region3DepthName: json['region_3depth_name']
    );
  }

}