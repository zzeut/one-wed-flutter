
import 'package:onewed_app/models/check_item_model.dart';

class Check {
  int? checkNo;
  int? checkCategoryNo;
  int? order;
  String? title;
  bool? userCheckExists;
  List<CheckItem>? checkItems;

  Check({
    this.checkNo,
    this.checkCategoryNo,
    this.order,
    this.title,
    this.userCheckExists,
    this.checkItems
  });

  factory Check.fromJson(Map <String, dynamic> json){
    List<CheckItem> _checkItem = [];
    if(json['check_items'] != null){
      json['check_items'].forEach((element) => {
        _checkItem.add(CheckItem.fromJson(element))
      });
    }

    return Check(
      checkNo: json['check_no'],
      checkCategoryNo: json['check_category_no'],
      order: json['order'],
      title: json['title'],
      userCheckExists: json['user_check_exists'],
      checkItems: _checkItem
    );
  }
}
