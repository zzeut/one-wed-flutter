class FcmToken{
  int? fcmTokenNo;
  int? userNo;
  String? fcmToken;
  String? machineType;

  FcmToken({
    this.fcmTokenNo,
    this.userNo,
    this.fcmToken,
    this.machineType
  });

  factory FcmToken.fromJson(Map<String, dynamic> json){
    return FcmToken(
      fcmTokenNo: json['fcm_token_no'],
      userNo: json['user_no'],
      fcmToken: json['fcm_token'],
      machineType: json['machine_type']
    );
  }
}