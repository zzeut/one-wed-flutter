class CheckCategory{
  int? checkCategoryNo;
  int? order;
  String? checkCategoryName;

  CheckCategory({
    this.checkCategoryNo,
    this.order,
    this.checkCategoryName
  });

  factory CheckCategory.fromJson(Map<String, dynamic> json){
    return CheckCategory(
      checkCategoryNo: json['check_category_no'],
      order: json['order'],
      checkCategoryName: json['check_category_name']
    );
  }
}