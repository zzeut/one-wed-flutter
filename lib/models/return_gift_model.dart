import 'package:json_annotation/json_annotation.dart';

part 'return_gift_model.g.dart';

@JsonSerializable()
class ReturnGift {
  @JsonKey(name: 'return_gift_id', defaultValue: '')
  String? returnGiftId;
  @JsonKey(name: 'created_at')
  DateTime? createdAt;
  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;
  @JsonKey(name: 'order', defaultValue: '')
  String? order;
  @JsonKey(name: 'company', defaultValue: '')
  String? company;
  @JsonKey(name: 'product', defaultValue: '')
  String? product;
  @JsonKey(name: 'address', defaultValue: '')
  String? address;
  @JsonKey(name: 'rating')
  double? rating;
  @JsonKey(name: 'is_like', defaultValue: false)
  bool? isLike;
  @JsonKey(name: 'return_gift_photos', defaultValue: [])
  List<ReturnGiftPhoto>? returnGiftPhotos;
  @JsonKey(name: 'return_gift_comments', defaultValue: [])
  List<ReturnGiftComment>? returnGiftComments;
  String? orderTitle;

  ReturnGift({
    this.returnGiftId,
    this.createdAt,
    this.updatedAt,
    this.order,
    this.company,
    this.product,
    this.address,
    this.rating,
    this.isLike,
    this.returnGiftPhotos,
    this.returnGiftComments,
    this.orderTitle = '',
  });

  factory ReturnGift.fromJson(Map<String, dynamic> json) =>
      _$ReturnGiftFromJson(json);

  Map<String, dynamic> toJson() => _$ReturnGiftToJson(this);

  List<ReturnGift> getOrderTitles() {
    // TODO
    return [
      ReturnGift(
        order: 'rl',
        orderTitle: '기본순',
      ),
      ReturnGift(
        order: 'ak',
        orderTitle: '찜 많은 순',
      ),
      ReturnGift(
        order: 'wj',
        orderTitle: '찜 적은 순',
      ),
      ReturnGift(
        order: 'sh',
        orderTitle: '별점 높은 순',
      ),
      ReturnGift(
        order: 'sk',
        orderTitle: '별점 낮은 순',
      ),
    ];
  }
}

@JsonSerializable()
class ReturnGiftPhoto {
  @JsonKey(name: 'return_gift_photo_id', defaultValue: '')
  String? returnGiftPhotoId;
  @JsonKey(name: 'created_at')
  DateTime? createdAt;
  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;
  @JsonKey(name: 'image_url', defaultValue: '')
  String? imageUrl;

  ReturnGiftPhoto({
    this.returnGiftPhotoId,
    this.createdAt,
    this.updatedAt,
    this.imageUrl,
  });

  factory ReturnGiftPhoto.fromJson(Map<String, dynamic> json) =>
      _$ReturnGiftPhotoFromJson(json);

  Map<String, dynamic> toJson() => _$ReturnGiftPhotoToJson(this);
}

@JsonSerializable()
class ReturnGiftComment {
  @JsonKey(name: 'return_gift_comment_id', defaultValue: '')
  String? returnGiftCommentId;
  @JsonKey(name: 'created_at')
  DateTime? createdAt;
  @JsonKey(name: 'updated_at')
  DateTime? updatedAt;
  @JsonKey(name: 'title', defaultValue: '')
  String? title;

  ReturnGiftComment({
    this.returnGiftCommentId,
    this.createdAt,
    this.updatedAt,
    this.title,
  });

  factory ReturnGiftComment.fromJson(Map<String, dynamic> json) =>
      _$ReturnGiftCommentFromJson(json);

  Map<String, dynamic> toJson() => _$ReturnGiftCommentToJson(this);
}
