import 'package:flutter/material.dart';

class ColorTheme {
  static Color grayPlaceholder = Color.fromRGBO(205, 210, 216, 1.0);
  static Color black = Color.fromRGBO(31, 31, 36, 1.0);
  static Color primaryBlue = Color(0xff005bab);
  static Color primaryOrange = Color(0xffec6c00);
  static Color primaryYellow = Color(0xfff5ab00);
  static Color defaultBlack = Color.fromRGBO(56, 56, 56, 1.0);
  static Color grayDefaultText = Color.fromRGBO(130, 130, 130, 1.0);
  static Color grayDefaultTitle = Color.fromRGBO(99, 99, 99, 1.0);
  static Color backgroundColor = Colors.white;
  static Color primaryColor = Color(0xffF18F8F);


  static Color darkDark = Color.fromRGBO(30, 30, 30, 1.0);
  static Color darkDarkBlack = Color.fromRGBO(48, 48, 48, 1.0);
  static Color darkBlack = Color.fromRGBO(34, 34, 34, 1.0);



  
  static Color facebookBackground = Color.fromRGBO(25, 119, 243, 1.0);
  static Color kakaoBackground = Color.fromRGBO(61, 31, 32, 1.0);
  static Color naverBackground = Color.fromRGBO(0, 195, 0, 1.0);
  static Color googleBackground = Color.fromRGBO(231, 96, 85, 1.0);
  static Color appleBackground  = Color.fromRGBO(0, 0, 0, 1.0);


  static Color grayLevel1 = Color.fromRGBO(236, 236, 236, 1.0);
  static Color darkGray = Color.fromRGBO(112, 112, 112, 1.0);
  static Color lightGray = Color.fromRGBO(211, 211, 211, 1.0);



  static Color darkLight = Color.fromRGBO(167, 167, 167, 1.0);

  static Color lightDark = Color.fromRGBO(161, 161, 161, 1.0);
  static Color lightLight = Color.fromRGBO(249, 249, 249, 1.0);

  static Color invalid = Color.fromRGBO(255, 71, 82, 1.0);

}
