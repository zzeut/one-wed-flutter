import 'package:flutter/material.dart';
import 'package:onewed_app/themes/color_theme.dart';

class TextStyleTheme {
  static TextStyle common = TextStyle();
  static TextStyle subtitle = common.copyWith(
      color: Color.fromRGBO(31, 31, 36, 1.0),
      fontSize: 18,
      fontWeight: FontWeight.w700);

  //black


  //white


  static TextStyle whiteS14W400 = common.copyWith(
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.w400
  );
  static TextStyle whiteS16W400 = common.copyWith(
    color: Colors.white,
    fontSize: 16,
    fontWeight: FontWeight.w400
  );
  static TextStyle whiteS14W700 = common.copyWith(
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.w700
  );
  static TextStyle whiteS16W700 = common.copyWith(
    color: Colors.white,
    fontSize: 16,
    fontWeight: FontWeight.w700
  );
  static TextStyle whiteS18W700 = common.copyWith(
    color: Colors.white,
    fontSize: 18,
    fontWeight: FontWeight.w700
  );


  // new
  static TextStyle blackS14W400 = common.copyWith(
      color: ColorTheme.black,
      fontSize: 14,
      fontWeight: FontWeight.w400
  );
  static TextStyle blackS16W400 = common.copyWith(
      color: ColorTheme.black,
      fontSize: 16,
      fontWeight: FontWeight.w400
  );
  static TextStyle blackS18W400 = common.copyWith(
      color: ColorTheme.black,
      fontSize: 18,
      fontWeight: FontWeight.w400
  );

  static TextStyle blackS16W500 = common.copyWith(
      color: ColorTheme.black,
      fontSize: 16,
      fontWeight: FontWeight.w500
  );
  static TextStyle blackS18W500 = common.copyWith(
      color: ColorTheme.black,
      fontSize: 18,
      fontWeight: FontWeight.w500
  );

  static TextStyle blackS14W700 = common.copyWith(
      color: ColorTheme.black,
      fontSize: 14,
      fontWeight: FontWeight.w700
  );
  static TextStyle blackS16W700 = common.copyWith(
      color: ColorTheme.black,
      fontSize: 16,
      fontWeight: FontWeight.w700
  );
  static TextStyle blackS18W700 = common.copyWith(
      color: ColorTheme.black,
      fontSize: 18,
      fontWeight: FontWeight.w700
  );

  // primary

  static TextStyle primaryS14W400 = common.copyWith(
    color: ColorTheme.primaryColor,
    fontSize: 14,
    fontWeight: FontWeight.w400
  );
  static TextStyle primaryS14W700 = common.copyWith(
    color: ColorTheme.primaryColor,
    fontSize: 14,
    fontWeight: FontWeight.w700
  );
  static TextStyle primaryS16W700 = common.copyWith(
    color: ColorTheme.primaryColor,
    fontSize: 16,
    fontWeight: FontWeight.w700
  );

  static TextStyle primaryS18W700 = common.copyWith(
    color: ColorTheme.primaryColor,
    fontSize: 18,
    fontWeight: FontWeight.w700
  );
  static TextStyle primaryS20W700 = common.copyWith(
    color: ColorTheme.primaryColor,
    fontSize: 20,
    fontWeight: FontWeight.w700
  );
  static TextStyle primaryS24W700 = common.copyWith(
    color: ColorTheme.primaryColor,
    fontSize: 24,
    fontWeight: FontWeight.w700
  );
  static TextStyle primaryS32W700 = common.copyWith(
    color: ColorTheme.primaryColor,
    fontSize: 32,
    fontWeight: FontWeight.w700
  );

  //dark dark
  static TextStyle darkDarkS14W400 = common.copyWith(
      color: ColorTheme.darkDarkBlack,
      fontSize: 14,
      fontWeight: FontWeight.w400
  );
  static TextStyle darkDarkS16W400 = common.copyWith(
      color: ColorTheme.darkDarkBlack,
      fontSize: 16,
      fontWeight: FontWeight.w400
  );
  static TextStyle darkDarkS18W400 = common.copyWith(
      color: ColorTheme.darkDarkBlack,
      fontSize: 18,
      fontWeight: FontWeight.w400
  );

  static TextStyle darkDarkS16W700 = common.copyWith(
      color: ColorTheme.darkDarkBlack,
      fontSize: 16,
      fontWeight: FontWeight.w700
  );
  static TextStyle darkDarkS18W700 = common.copyWith(
      color: ColorTheme.darkDarkBlack,
      fontSize: 18,
      fontWeight: FontWeight.w700
  );



  //dark black
  static TextStyle darkBlackS12W400 = common.copyWith(
      color: ColorTheme.darkBlack,
      fontSize: 12,
      fontWeight: FontWeight.w400
  );
  static TextStyle darkBlackS16W400 = common.copyWith(
      color: ColorTheme.darkBlack,
      fontSize: 16,
      fontWeight: FontWeight.w400
  );

  static TextStyle darkBlackS16W700 = common.copyWith(
      color: ColorTheme.darkBlack,
      fontSize: 16,
      fontWeight: FontWeight.w700
  );
  static TextStyle darkBlackS18W700 = common.copyWith(
      color: ColorTheme.darkBlack,
      fontSize: 18,
      fontWeight: FontWeight.w700
  );

  //dark gray
  static TextStyle darkGrayS14W400 = common.copyWith(
      color: ColorTheme.darkGray,
      fontSize: 14,
      fontWeight: FontWeight.w400
  );
  static TextStyle darkGrayS16W400 = common.copyWith(
      color: ColorTheme.darkGray,
      fontSize: 16,
      fontWeight: FontWeight.w400
  );

  // dark light
  static TextStyle darkLightS12W400 =common.copyWith(
      color: ColorTheme.darkLight,
      fontSize: 12,
      fontWeight: FontWeight.w400
  );
  static TextStyle darkLightS14W400 =common.copyWith(
      color: ColorTheme.darkLight,
      fontSize: 14,
      fontWeight: FontWeight.w400
  );
  static TextStyle darkLightS14W700 =common.copyWith(
      color: ColorTheme.darkLight,
      fontSize: 14,
      fontWeight: FontWeight.w700
  );
  static TextStyle darkLightS16W400 =common.copyWith(
      color: ColorTheme.darkLight,
      fontSize: 16,
      fontWeight: FontWeight.w400
  );
  static TextStyle darkLightS16W700 =common.copyWith(
      color: ColorTheme.darkLight,
      fontSize: 16,
      fontWeight: FontWeight.w700
  );



  static TextStyle lightDarkS14W400 = common.copyWith(
      color: ColorTheme.lightDark,
      fontSize: 14,
      fontWeight: FontWeight.w400
  );
  static TextStyle lightDarkS16W400 = common.copyWith(
      color: ColorTheme.lightDark,
      fontSize: 16,
      fontWeight: FontWeight.w400
  );

  static TextStyle lightDarkS18W700 = common.copyWith(
      color: ColorTheme.lightDark,
      fontSize: 16,
      fontWeight: FontWeight.w400
  );

  //invalid
  static TextStyle invalidS14W400 = common.copyWith(
      color: ColorTheme.invalid,
      fontSize: 14,
      fontWeight: FontWeight.w400
  );

}
