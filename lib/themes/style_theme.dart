import 'package:flutter/material.dart';

class StyleTheme {
  static BoxShadow boxShadow = BoxShadow(
    color: Color(0xff954F4F).withOpacity(0.12),
    offset: Offset(0, 4),
    blurRadius: 6,
  );

  static BoxDecoration boxDecoration = BoxDecoration(
    color: Color(0xffFEF9F9),
    borderRadius: BorderRadius.circular(12),
    boxShadow: [StyleTheme.boxShadow],
  );
}
