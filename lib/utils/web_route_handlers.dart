import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/routes/home/page/home_page.dart';
import 'package:onewed_app/routes/sign/page/sign_in_page.dart';
import 'package:onewed_app/routes/tab/tab_page.dart';

var signInHandler = Handler(
  handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
    return SignInPage();
  },
);

var tabHandler = Handler(
  handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
    return TabPage();
  },
);

var homeHandler = Handler(
  handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
    return HomePage();
  },
);
