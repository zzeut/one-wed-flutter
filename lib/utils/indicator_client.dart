import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:onewed_app/themes/color_theme.dart';

class IndicatorClient {
  IndicatorClient._initialize() {
    print('IndicatorClient Initialize');
    EasyLoading.instance.userInteractions = false;

    EasyLoading.instance.maskType = EasyLoadingMaskType.none;
    EasyLoading.instance.loadingStyle = EasyLoadingStyle.custom;
    //
    EasyLoading.instance.backgroundColor = Colors.transparent;
    EasyLoading.instance.indicatorColor = ColorTheme.primaryColor;
    EasyLoading.instance.textColor = Colors.transparent;
    EasyLoading.instance.progressColor = Colors.transparent;
    EasyLoading.instance.boxShadow = <BoxShadow>[];

  }

  static final IndicatorClient _instance = IndicatorClient._initialize();

  static IndicatorClient get shared => _instance;

  show() {
    EasyLoading.show();
  }

  hide() {
    EasyLoading.dismiss();
  }
}
