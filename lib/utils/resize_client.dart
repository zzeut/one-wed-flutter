import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:image/image.dart' as IMG;

Future<Uint8List> resizeFuture(Uint8List bytes) {
  return compute(resize, bytes);
}

Uint8List resize(Uint8List bytes) {
  int maxSize = 1024;

  IMG.Image? img = IMG.decodeImage(bytes);
  if (img != null) {
    if (img.width >= maxSize || img.height >= maxSize) {
      double widthRatio = img.width / img.height;
      double heightRatio = img.height / img.width;
      int width = img.width > img.height ? maxSize : (widthRatio * maxSize).round();
      int height = img.width > img.height ? (heightRatio * maxSize).round() : maxSize;

      IMG.Image resized = IMG.copyResize(img, width: width, height: height);
      Uint8List resizedData = Uint8List.fromList(IMG.encodeJpg(resized));
      return resizedData;
    } else {
      return bytes;
    }
  }
  return bytes;

}