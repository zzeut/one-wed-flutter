import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/utils/web_route_handlers.dart';

class Routes {
  static String signIn = "/sign_in";

  static String tab = "/tab";

  static String home = "/home";

  static void configureRoutes(FluroRouter router) {
    router.notFoundHandler = Handler(
      handlerFunc: (BuildContext? context, Map<String, List<String>> params) {
        return;
      },
    );

    router.define(signIn, handler: signInHandler);

    router.define(tab, handler: tabHandler);

    router.define(home, handler: homeHandler);
  }
}
