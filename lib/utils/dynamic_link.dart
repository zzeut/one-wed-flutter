import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';

class DynamicLink{
  static final DynamicLink _manager = DynamicLink.internal();

  factory DynamicLink(){
    return _manager;
  }

  DynamicLink.internal(){
    //초기화 코드 입력
  }

  Future<bool> isKakaotalkInstalled() async{
    bool installed = await isKakaotalkInstalled();
    return installed;
  }


  Future<String> buildDynamicLink(String path) async{
    String url = "https://onewed.page.link";

    final DynamicLinkParameters parameters = DynamicLinkParameters(
        uriPrefix: url,
        link: Uri.parse('$url/$path'),
        androidParameters: AndroidParameters(
            packageName: 'com.onewed.app'
        ),
        iosParameters: IosParameters(
          bundleId: 'com.onewed.app',
          appStoreId: '1591882918'
        )
    );

    ShortDynamicLink dynamicUrl = await parameters.buildShortLink();
    print(dynamicUrl.shortUrl);
    return dynamicUrl.shortUrl.toString();
  }
}