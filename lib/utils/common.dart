import 'dart:io';
import 'package:intl/intl.dart';
import 'package:onewed_app/themes/color_theme.dart';

String customDateTime(DateTime dateTime) {
  return DateFormat('yyyy.MM.dd(E) HH:mm').format(dateTime.toLocal());
}

String customNumber(double price) {
  return NumberFormat.currency(
    locale: Platform.localeName,
    name: '',
    decimalDigits: 0,
  ).format(price.toInt());
}

String chatDateTime(String formattedString) {
  DateTime dateTime = DateTime.parse(formattedString);
  DateTime now = DateTime.now();

  Duration duration = now.difference(dateTime);
  if (duration.inDays < 1) {
    return messageDateTime(formattedString);
  }
  return "${dateTime.year.toString().substring(2)}.${dateTime.month}.${dateTime.day}";
}
String messageDateTime(String formattedString) {

  DateTime notificationDate = DateFormat("yyyy-MM-dd HH:mm:ss").parse(formattedString);

  var hour = notificationDate.hour;
  String criteria = '오전';
  if(hour > 12){
    criteria = '오후';
  }

  hour = hour % 12 == 0 ? 12 : hour % 12;
  var min = notificationDate.minute < 10 ? "0${notificationDate.minute}" : notificationDate.minute;

  return '${criteria} ${hour}:${min}';



/*  DateTime dateTime = DateTime.parse(formattedString);
  String meridiemString = "${dateTime.hour < 12 ? '오전' : '오후'}";
  int hour = dateTime.hour;
  if (dateTime.hour > 12) {
    hour = hour - 12 ;
  }
  String hourString = "${hour < 10 ? '0' : ''}${hour}";
  String minuteString = "${dateTime.minute < 10 ? '0' : ''}${dateTime.minute}";
  return "${meridiemString} ${hourString}:${minuteString}";*/
}
