import 'dart:async';
import 'dart:developer';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:retry/retry.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:onewed_app/networking/response.dart';

class NetworkComponent {
  NetworkService _networkService = NetworkService();
  static final NetworkComponent _instance = NetworkComponent._internal();

  factory NetworkComponent() {
    return _instance;
  }

  NetworkComponent._internal() {
    //클래스가 최초 생성될때 1회 발생
    //초기화 코드
    _networkService = NetworkService();
  }

  NetworkService get shared => _networkService;
}

class NetworkService {
  Future<Response> request(String method, String path, var data) {
    log(method);
    String? scheme = dotenv.env['BASE_URL_SCHEME'];
    String? baseUrl = dotenv.env['BASE_URL_HOST'];
    var endPoint = Uri(scheme: scheme, host: baseUrl, path: '/api/v1$path');

    String? accessToken;
    String? refreshToken;

    Future<SharedPreferences> preferenceInstance =
        SharedPreferences.getInstance();

    return preferenceInstance.then((preference) {
      accessToken = preference.getString('accessToken');
      refreshToken = preference.getString('refreshToken');
      String clientId = "${dotenv.env["CLINT_ID"]}";
      String clientSecret = "${dotenv.env["CLIENT_SECRET"]}";


      var headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'client_id': clientId,
        'client_secret': clientSecret
      };

      if (accessToken != null) {
        print(accessToken!);
        headers['Authorization'] = 'Bearer $accessToken';
      }

      var request;


      switch (method) {
        case 'get':
          if (data != null) {
            var endPoint = Uri(queryParameters: data, scheme: scheme, host: baseUrl, path: '/api/v1$path');
            request = http.get(endPoint, headers: headers);
            // print(endPoint);
          } else {
            request = http.get(endPoint, headers: headers);
            // print(endPoint);
          }
          break;
        case 'post':
          var body = json.encode(data ?? Map());
          print(body);
          request = http.post(endPoint, headers: headers, body: body);
          break;
        case 'put':
          var body = json.encode(data ?? Map());
          request = http.post(endPoint, headers: headers, body: body);
          break;
        case 'delete':
          if (data == null) {
            data = {'_method': 'delete'};
          } else {
            data['_method'] = 'delete';
          }
          var body = json.encode(data ?? Map());
          request = http.post(endPoint, headers: headers, body: body);
          break;
      }

      return retry(() => request,
              retryIf: (e) => e is SocketException || e is TimeoutException)
          .then((response) {
        log(response.statusCode.toString());
        if (response.statusCode >= 200 && response.statusCode < 300) {
          return Future.value(
              Response.success(response.statusCode, response.body));
        } else if (response.statusCode == 401) {
          if (accessToken != null && refreshToken != null && path != '/oauth') {
            return refreshAccessToken(method, path, data);
          } else {
            return Future.error(
                Response.failure(response.statusCode, response.body));
          }
        } else {
          return Future.error(
              Response.failure(response.statusCode, response.body));
        }
      });
    });
  }

  Future<Response> requestMultipart(String method, String path, var data, Map<String, Uint8List> files) {

    String? scheme = dotenv.env['BASE_URL_SCHEME'];
    String? baseUrlHost = dotenv.env['BASE_URL_HOST'];

    String? accessToken;
    String? refreshToken;


    Future<SharedPreferences> preferenceInstance = SharedPreferences.getInstance();
    return preferenceInstance.then((preference) {
      accessToken = preference.getString('accessToken');
      refreshToken = preference.getString('refreshToken');
      var headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'client_id': '${dotenv.env['CLIENT_ID']}',
        'client_secret': '${dotenv.env['CLIENT_SECRET']}'
      };

      if (accessToken != null) {
        log(accessToken!);
        headers['Authorization'] = 'Bearer $accessToken';
      }

      var endPoint = Uri(scheme: scheme, host: baseUrlHost, path: '/api/v1$path');
      var request = http.MultipartRequest(method, endPoint);
      headers.entries.forEach((element) {
        request.headers[element.key] = element.value;
      });
      data.entries.forEach((element) {
        request.fields[element.key] = element.value;
      });
      var futures = files.entries.map((element) {
        http.MultipartFile file = http.MultipartFile.fromBytes(
            element.key, element.value,
            filename: 'photo.jpg');
        request.files.add(file);
        return Future.value();
      });
      return Future.wait(futures).then((_) {
        return request.send();
      }).then((result) {
        return http.Response.fromStream(result);
      }).then((response)  {
        print(response.body);
        if (response.statusCode >= 200 && response.statusCode < 300) {
          return Future.value(
              Response.success(response.statusCode, response.body));
        } else {
          return Future.error(
              Response.failure(response.statusCode, response.body));
        }
      });
    });
  }

  Future<Response> refreshAccessToken(
      String method, String path, var data) async {
    Future<SharedPreferences> preferenceInstance =
        SharedPreferences.getInstance();

    return preferenceInstance.then((preference) {
      String? refreshToken = preference.getString("refreshToken");

      return request("post", '/oauth', {
        "refresh_token": refreshToken,
        "grant_type": "refresh_token"
      }).then((response) {
        if (response.statusCode >= 200 && response.statusCode < 300) {
          preference.setString("accessToken", response.data['access_token']);
          preference.setString("refreshToken", response.data['refresh_token']);
          return request(method, path, data);
        } else {
          preference.remove('accessToken');
          preference.remove('refreshToken');
          return Future.error(
              Response.failure(response.statusCode, response.data.toString()));
        }
      });
    });
  }
}
