import 'dart:convert';

class Response {
  var statusCode;
  var success;
  var data;
  var error;

  Response();

  factory Response.empty() {
    var response = Response();
    response.success = false;
    return response;
  }

  factory Response.success(int statusCode, String body) {
    var response = Response();
    response.success = true;
    response.statusCode = statusCode;
    response.data = json.decode(body);
    return response;
  }
  factory Response.failure(int statusCode, String body) {
    var response = Response();
    response.success = false;
    response.statusCode = statusCode;
    response.error = json.decode(body);
    return response;
  }
}
