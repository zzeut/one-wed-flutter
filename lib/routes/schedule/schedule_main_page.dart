import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/components/schedule/calendar_schedule_list_component.dart';
import 'package:onewed_app/components/schedule/schedule_list_component.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/event_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/event_provider.dart';
import 'package:onewed_app/providers/schdule_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/event/page/event_detail_page.dart';
import 'package:onewed_app/routes/event/widget/event_widget.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/widgets/error.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:onewed_app/widgets/tab_bar.dart';
import 'package:provider/provider.dart';

class ScheduleMainPage extends StatelessWidget {
  static const route = '';
  bool? isBack;
  ScheduleMainPage({Key? key, this.isBack}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ScheduleProvider>(
      create: (context) => ScheduleProvider(),
      child: _View(this.isBack),
    );
  }
}

class _View extends BaseRoute {
  bool? isBack;
  _View(this.isBack);

  _ViewState createState() => _ViewState(this.isBack);
}

class _ViewState extends BaseRouteState with SingleTickerProviderStateMixin {
  bool? isBack;
  _ViewState(this.isBack);

  int _index = 0;
  int tabIndex = 0;
  late TabController _tabController;

  UserProvider? _userProvider;
  User? me;

  String weddingCeremonyString = "";


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
  }

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();


    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {  
            return [
              SliverAppBar(
                leading: isBack!= null && isBack! ? GestureDetector(
                  onTap: () => Navigator.of(context).pop(),
                  child:  Image.asset(iconArrowLeft),
                ) : Container(),
                pinned: true,
                floating: true,
                expandedHeight: 250,
                collapsedHeight: 60,
                title: Text('스케줄', style: TextStyle( color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold,),),
                centerTitle: true,
                flexibleSpace: FlexibleSpaceBar(
                  background: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 16),
                        decoration: BoxDecoration(
                            color: Color(0xffFFFCFC)
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              width: 110,
                              height: 110,
                              child: Image.asset("assets/images/schedule/image_main.png", fit: BoxFit.fill,),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("${me!.weddingCeremonyDayString}", style: TextStyleTheme.primaryS20W700,),
                                SizedBox(height: 4,),
                                Text("${me!.name}님의 행복한 결혼까지", style: TextStyleTheme.darkGrayS16W400,),
                                Text("원웨드가 도와드리겠습니다.", style: TextStyleTheme.darkGrayS16W400,),
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                bottom: PreferredSize(
                  preferredSize: Size.fromHeight(60),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white
                    ),
                    child: Column(
                      children: [
                        Container(
                          height: 1,
                          color: Color(0xffECECEC),
                        ),
                        TabBar(
                            controller: _tabController,
                            indicatorColor: ColorTheme.primaryColor,
                            indicator: UnderlineTabIndicator(
                              borderSide: BorderSide(width: 2,color: ColorTheme.primaryColor),
                              insets: EdgeInsets.symmetric(horizontal:16.0),
                            ),
                            labelColor: ColorTheme.primaryColor,
                            unselectedLabelColor: Colors.black,
                            labelStyle: TextStyle(
                                fontWeight: FontWeight.w700
                            ),
                            unselectedLabelStyle: TextStyle(
                                fontWeight: FontWeight.w400
                            ),

                            tabs: [
                              Container(
                                height: 58,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("리스트", style: TextStyle(fontSize: 18,),)
                                  ],
                                ),
                              ),
                              Container(
                                height: 58,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("캘린더", style: TextStyle(fontSize: 18,),)
                                  ],
                                ),
                              )
                            ]
                        ),

                        Container(
                          height: 1,
                          color: Color(0xffECECEC),
                        ),
                      ],
                    ),
                  )
                ),
              )
            ];
          },
          body: TabBarView(
            controller: _tabController,
            children: [
              ScheduleListComponent(),
              CalendarScheduleListComponent(),
            ],
          ),

        ),
      ),
    );


  }
}
