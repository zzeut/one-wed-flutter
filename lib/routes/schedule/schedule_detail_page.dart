import 'dart:async';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/dateTime.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/schedule_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/broadcast_provider.dart';
import 'package:onewed_app/providers/match_provider.dart';
import 'package:onewed_app/providers/schdule_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/chat/chat_detail_page.dart';
import 'package:onewed_app/routes/chat/chat_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

// ignore: must_be_immutable
class ScheduleDetailPage extends StatelessWidget {
  int scheduleNo;

  ScheduleDetailPage({Key? key, required this.scheduleNo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ScheduleProvider>(
            create: (context) => ScheduleProvider()),
        ChangeNotifierProvider<MatchProvider>(
            create: (context) => MatchProvider()),
        ChangeNotifierProvider<BroadCastProvider>(
            create: (context) => BroadCastProvider()),
      ],
      child: View(this.scheduleNo),
    );

    return ChangeNotifierProvider<ScheduleProvider>(
      create: (context) => ScheduleProvider(),
      child: View(this.scheduleNo),
    );
  }
}

class View extends BaseRoute {
  int scheduleNo;

  View(this.scheduleNo);

  @override
  _ViewState createState() => _ViewState(this.scheduleNo);
}

class _ViewState extends BaseRouteState {
  int scheduleNo;

  _ViewState(this.scheduleNo);

  ScheduleProvider? _scheduleProvider;
  Schedule? _schedule;

  MatchProvider? _matchProvider;
  UserProvider? _userProvider;
  User? me;

  BroadCastProvider? _broadCastProvider;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);
    _scheduleProvider!.selectScheduleDetail(this.scheduleNo);
  }

  @override
  Widget build(BuildContext context) {
    _scheduleProvider = Provider.of<ScheduleProvider>(context);
    _schedule = _scheduleProvider!.getSchedule();

    _matchProvider = Provider.of<MatchProvider>(context);

    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();

    _broadCastProvider = Provider.of<BroadCastProvider>(context);

    if (_schedule == null) {
      return Container();
    }
    String scheduleDate = '';
    if (_schedule != null) {
      switch (_schedule!.state) {
        case 1:
          scheduleDate = "D - ${_schedule!.startTimeDuration!.toInt().abs()}";
          break;
        case 2:
          scheduleDate = "D - Day";
          break;
        case 3:
          scheduleDate = "D + ${_schedule!.startTimeDuration!.toInt().abs()}";
          break;
      }
    }

    Completer<GoogleMapController> _controller = Completer();

    return PlatformScaffold(
        title: "스케줄 상세정보",
        body: SafeArea(
          bottom: false,
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
            child: ListView(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 15),
                  decoration: BoxDecoration(
                      color: ColorTheme.primaryColor,
                      borderRadius: BorderRadius.all(Radius.circular(12)),
                      boxShadow: [
                        BoxShadow(
                          color: Color(0xff954F4F).withOpacity(0.12),
                          blurRadius: 6,
                          offset: Offset(0, 4.0),
                        )
                      ]),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 24,
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            child: Center(
                              child: Text(
                                "$scheduleDate",
                                style: TextStyleTheme.primaryS14W700,
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Text(
                        "${Date.scheduleMonthDateDay(_schedule!.scheduleDate.toString())}",
                        style: TextStyleTheme.whiteS14W400,
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        "${_schedule!.scheduleName}",
                        style: TextStyleTheme.whiteS18W700,
                      ),
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        "${Date.timeHourMinCol(_schedule!.startTime.toString())} - ${Date.timeHourMinCol(_schedule!.endTime.toString())}",
                        style: TextStyleTheme.whiteS14W400,
                      ),
                      SizedBox(
                        height: 24,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 40,
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            decoration: BoxDecoration(
                              color: _schedule!.state == 1
                                  ? Colors.white
                                  : Colors.transparent,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                            ),
                            child: Center(
                              child: Text(
                                "진행 대기",
                                style: _schedule!.state == 1
                                    ? TextStyleTheme.primaryS14W700
                                    : TextStyleTheme.whiteS14W400,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Image.asset(
                            "assets/icons/icon-ellipse.png",
                            width: 20,
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Container(
                            height: 40,
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            decoration: BoxDecoration(
                              color: _schedule!.state == 2
                                  ? Colors.white
                                  : Colors.transparent,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                            ),
                            child: Center(
                              child: Text(
                                "진행중",
                                style: _schedule!.state == 2
                                    ? TextStyleTheme.primaryS14W700
                                    : TextStyleTheme.whiteS14W400,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Image.asset(
                            "assets/icons/icon-ellipse.png",
                            width: 20,
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Container(
                            height: 40,
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            decoration: BoxDecoration(
                              color: _schedule!.state == 3
                                  ? Colors.white
                                  : Colors.transparent,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                            ),
                            child: Center(
                              child: Text(
                                "완료",
                                style: _schedule!.state == 3
                                    ? TextStyleTheme.primaryS14W700
                                    : TextStyleTheme.whiteS14W400,
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Container(
                      width: 88,
                      child: Text(
                        "카테고리",
                        style: TextStyleTheme.darkGrayS16W400,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        child: Text(
                      "${_schedule!.company!.category!.categoryName}",
                      style: TextStyleTheme.darkDarkS16W400,
                    ))
                  ],
                ),
                if (_schedule!.company!.firstPhone != null)
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 4),
                        width: 88,
                        child: Text(
                          "대표번호",
                          style: TextStyleTheme.darkGrayS16W400,
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                          child: Text(
                        "${_schedule!.company!.firstPhone}-${_schedule!.company!.secondPhone}-${_schedule!.company!.lastPhone}",
                        style: TextStyleTheme.darkDarkS16W400,
                      ))
                    ],
                  ),
                SizedBox(
                  height: 4,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 88,
                      child: Text(
                        "주소",
                        style: TextStyleTheme.darkGrayS16W400,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${_schedule!.company!.address}",
                          style: TextStyleTheme.darkDarkS16W400,
                        ),
                        CustomContainer(
                          onPressed: () {
                            Clipboard.setData(ClipboardData(
                                    text:
                                        _schedule!.company!.address.toString()))
                                .then((value) => BotToast.showText(
                                    text: "주소가 클립보드에 복사되었습니다."));
                          },
                          margin: EdgeInsets.only(top: 4),
                          width: 80,
                          height: 32,
                          borderColor: Color(0xffA7A7A7),
                          borderWidth: 1,
                          borderRadius: [8, 8, 8, 8],
                          child: Center(
                            child: Text("주소공유"),
                          ),
                        )
                      ],
                    ))
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 160,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: GoogleMap(
                      mapType: MapType.normal,
                      initialCameraPosition: CameraPosition(
                          bearing: 0,
                          target: LatLng(_schedule!.company!.lat!.toDouble(),
                              _schedule!.company!.lng!.toDouble()),
                          tilt: 0,
                          zoom: 17),
                      markers: Set.from([
                        Marker(
                          markerId: MarkerId("1"),
                          draggable: false,
                          position: LatLng(_schedule!.company!.lat!.toDouble(),
                              _schedule!.company!.lng!.toDouble()),
                        )
                      ]),
                      myLocationButtonEnabled: false,
                      onMapCreated: (GoogleMapController controller) {
                        _controller.complete(controller);
                      },
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                if (_schedule!.memo != null)
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        border: Border.all(width: 1, color: Color(0xff303030))),
                    child: Text(
                      "${_schedule!.memo}",
                      style: TextStyleTheme.blackS16W400,
                    ),
                  ),
                SizedBox(
                  height: 51,
                ),
                Row(
                  children: [
                    if(_schedule!.planner!.firstPhone != null)
                    Expanded(
                      child: CustomContainer(
                        margin: EdgeInsets.only(right: 10),
                        onPressed: showCallAlert,
                        height: 60,
                        borderRadius: [12, 12, 12, 12],
                        borderWidth: 1,
                        borderColor: ColorTheme.primaryColor,
                        child: Center(
                          child: Text(
                            "전화 문의",
                            style: TextStyleTheme.primaryS16W700,
                          ),
                        ),
                    )),
                    Expanded(
                        child: CustomContainer(
                      onPressed: () {
                        scheduleInquiry();
                      },
                      height: 60,
                      borderRadius: [12, 12, 12, 12],
                      backgroundColor: ColorTheme.primaryColor,
                      child: Center(
                        child: Text(
                          "스케줄 문의",
                          style: TextStyleTheme.whiteS16W700,
                        ),
                      ),
                    ))
                  ],
                )
              ],
            ),
          ),
        ));
  }

  void scheduleInquiry() {
    indicator.show();
    Map<String, String> data = {
      "user_no": "${me!.userNo}",
      "status": "1"
    };

    _matchProvider!.selectSelectMating(data).then((value){
      if(value != null && value.isNotEmpty){

        this.navigator!.pushRoute(ChatPage(isBack: true,));
      }else{
        _matchProvider!.insertMatching(me!.userNo!.toInt(), null).then((response){
          BotToast.showText(text: "담당 플래너가 컨택중입니다. 최대 2-3일 정도소요됩니다.");
        });
      }
    });

/*    _matchProvider!.checkMatching(me!.userNo!.toInt(), _schedule!.writerNo!.toInt()).then((response) {
      if(response == null){
        indicator.hide();
        BotToast.showText(text: "담당 플래너가 컨택중입니다. 최대 2-3일 정도소요됩니다.");

      }else{
        _broadCastProvider!.selectChat("${response.threadId}").then((res) {
          indicator.hide();
          if (res != null) {
            this.navigator!.pushRoute(ChatDetailPage(
                res.threadId!.toString(), res.users![0].name ?? ''));
          } else {
            BotToast.showText(text: "담당 플래너가 컨택중입니다. 최대 2-3일 정도소요됩니다.");
          }


        });
      }

    }).catchError((onError) {
      // 매칭 정보 추가
      addMatching();
    });*/
  }

  void addMatching() {
    print('add matching');
    _matchProvider!.insertMatching(me!.userNo!.toInt(), _schedule!.writerNo!.toInt()).then((response) {

      indicator.hide();
      BotToast.showText(text: "스케줄 문의 신청이 완료되었습니다. 최대 2-3일 정도소요됩니다.");
    }).catchError((onError){
      indicator.hide();
      BotToast.showText(text: "오류가 발생했습니다. 잠시 후 시도해주세요");
    });
  }

  showCallAlert() {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              insetPadding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
              clipBehavior: Clip.antiAliasWithSaveLayer,
              contentPadding:
                  EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(12.0))),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      CustomContainer(
                        onPressed: (){
                          Navigator.pop(context);
                        },
                        child: Image.asset("assets/icons/icon_cancel_darkgray_32.png", width: 24,),
                      )
                    ],
                  ),
                  Container(
                    width: 80,
                    height: 80,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: profileWidget(),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text("${_schedule!.planner!.name}", style: TextStyleTheme.blackS16W700),
                  Text("${_schedule!.planner!.firstPhone}-${_schedule!.planner!.secondPhone}-${_schedule!.planner!.lastPhone}",style: TextStyleTheme.darkGrayS16W400,),
                  SizedBox(
                    height: 20,
                  ),
                  CustomContainer(
                    width: double.infinity,
                    height: 60,
                    onPressed: () async {

                      launch("tel://${_schedule!.planner!.firstPhone}${_schedule!.planner!.secondPhone}${_schedule!.planner!.lastPhone}");
                    },
                    backgroundColor: ColorTheme.primaryColor,
                    borderRadius: [6, 6, 6, 6],
                    child: Center(
                      child: Text(
                        "전화걸기",
                        style: TextStyleTheme.whiteS16W700,
                      ),
                    ),
                  )
                ],
              ),
            ));
  }

  Widget profileWidget() {
    if (_schedule!.planner!.profilePath == null) {
      if (_schedule!.planner!.gender == "1") {
        return Image(image: AssetImage("assets/images/profile_man.png"));
      } else if (_schedule!.planner!.gender == "2") {
        return Image(image: AssetImage("assets/images/profile_woman.png"));
      }
    } else {
      return Image.network(_schedule!.planner!.profilePath!.o, fit: BoxFit.cover,);
    }

    return Image(image: AssetImage("assets/images/profile_default.png"));
  }
}
