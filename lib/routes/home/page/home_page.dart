import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/dateTime.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/images.dart';
import 'package:onewed_app/models/category_model.dart';
import 'package:onewed_app/models/event_model.dart';
import 'package:onewed_app/models/hot_event_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/category_provider.dart';
import 'package:onewed_app/providers/event_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/check/page/check_page.dart';
import 'package:onewed_app/routes/company/company_page.dart';
import 'package:onewed_app/routes/event/page/event_page.dart';
import 'package:onewed_app/routes/home/widget/home_widget.dart';
import 'package:onewed_app/routes/return_gift/page/return_gift_page.dart';
import 'package:onewed_app/routes/review/review_page.dart';
import 'package:onewed_app/routes/review/review_write_list_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/style_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/widgets/error.dart';
import 'package:onewed_app/widgets/image.dart';
import 'package:onewed_app/widgets/inkwell.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:onewed_app/widgets/page_indicator.dart';
import 'package:onewed_app/widgets/size.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatelessWidget {
  static const route = '';

  @override
  Widget build(BuildContext context) {

    return MultiProvider(
      providers: [
        ChangeNotifierProvider<EventProvider>(create: (context) => EventProvider()),
        ChangeNotifierProvider<CategoryProvider>(create: (context) => CategoryProvider()),

      ],
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {


  EventProvider? _eventProvider;
  HotEvent? hotEvent;

  UserProvider? _userProvider;
  User? me;




  @override
  void afterFirstLayout(BuildContext context){
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    _eventProvider!.selectHotEvent();

  }



  @override
  Widget build(BuildContext context) {
    _eventProvider = Provider.of<EventProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();


    return PlatformScaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Image.asset(imageLogo),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(1),
          child: Container(
            height: 1,
            color: Color(0xffECECEC),
          ),
        ),
      ),
      body: CustomScrollView(
        slivers: [
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Container(
                  margin: EdgeInsets.only(top: 25, left: 20, right: 20),
                  child: Row(
                    children: [
                      Image.asset(imageMain),
                      Container(
                        margin: EdgeInsets.only(left: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if(me!.level == 1)
                              Text("${me!.weddingCeremonyDayString}", style: TextStyleTheme.primaryS20W700,),

                            Container(
                              margin: EdgeInsets.only(top: 4),
                              child: Text(
                                me!.level == 1 ? '${me!.name ?? '사용자'}님의 예식까지\n원웨드가 도와드리겠습니다.' : '안녕하세요 \n${me!.name} 플래너님 ',
                                style: TextStyle(
                                  color: Color(0xff707070),
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 15, left: 16, right: 16),
                  child: Wrap(
                    spacing: 20,
                    children: [
                      HomeButton(
                        onTap: () {
                          // TODO
                          this.navigator!.pushRoute(CompanyPage());
                        },
                        icon: iconWeddingshop,
                        title: '웨딩샵',
                      ),
                      HomeButton(
                        onTap: () {
                          this.navigator!.pushRoute(ReturnGiftPage());
                        },
                        icon: iconPresent,
                        title: '답례품',
                      ),
                      HomeButton(
                        onTap: () {
                          this.navigator!.pushRoute(
                                CheckPage(),
                              );
                        },
                        icon: iconChecklist,
                        title: '웨딩체크',
                      ),
                      HomeButton(
                        onTap: () {
                          // TODO
                          this.navigator!.pushRoute(ReviewPage());
                        },
                        icon: iconReview,
                        title: '웨딩리뷰',
                      ),
                    ],
                  ),
                ),
                HomeTitleCard(
                  title: 'HOT 이벤트',
                ),
                Consumer<EventProvider>(builder: (context, eventProvider, child ) {
                  hotEvent = eventProvider.getHotEvent();

                  if (eventProvider.isLoading) {
                    return CustomLoading(
                      margin: EdgeInsets.only(top: 4),
                    );
                  }

                  if (eventProvider.isError) {
                    // TODO
                    return ErrorCard(
                      margin: EdgeInsets.only(top: 4),
                    );
                  }

                  return Container(
                    margin: EdgeInsets.only(top: 4),
                    height: 200,
                    child: eventWidget(hotEvent!),
                  );
                },
                ),
                HomeTitleCard(
                  title: '웨딩카페',
                ),
                Container(
                  margin:
                      EdgeInsets.only(top: 4, left: 16, right: 16, bottom: 60),
                  decoration: StyleTheme.boxDecoration,
                  child: CustomInkWell(
                    onTap: () {
                      // TODO
                      launch("https://cafe.naver.com/weddingincheon");
                    },
                    padding: EdgeInsets.only(
                        top: 12, left: 16, right: 16, bottom: 12),
                    borderRadius: BorderRadius.circular(12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.asset(iconMainCafe),
                            Container(
                              margin: EdgeInsets.only(top: 12),
                              child: Text(
                                '카페에서 다양한 소식들을 확인해 보세요.',
                                style: TextStyle(
                                  color: Color(0xff303030),
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Image.asset(iconArrowRight),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget eventWidget(HotEvent event){
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16),
      child: GestureDetector(
        onTap: () {
          this.navigator!.pushRoute(
            EventPage(),
          );
        },
        child: Stack(
          children: [
            CustomImageCard(
              width: customWidth(context),
              height: 200,
              imageUrl: '${event.src!.m}',
              boxFit: BoxFit.fill,
              boxShadow: [StyleTheme.boxShadow],
              borderRadius: BorderRadius.circular(12),
            ),
            /*Positioned(
              left: 16,
              right: 16,
              bottom: 32,
              child: Column(
                crossAxisAlignment:
                CrossAxisAlignment.start,
                children: [
                  Text(
                    "${event.hotEventTitle}",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 4),
                    child: Text(
                      "${event.content}",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            ),*/
          ],
        ),
      ),
    );

  }
}
