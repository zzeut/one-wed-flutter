import 'package:flutter/material.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/style_theme.dart';
import 'package:onewed_app/widgets/inkwell.dart';
import 'package:onewed_app/widgets/size.dart';

class HomeButton extends StatelessWidget {
  final Function() onTap;
  final String icon;
  final String title;

  const HomeButton({
    Key? key,
    required this.onTap,
    required this.icon,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ((customWidth(context) - 92) / 4).floorToDouble(),
      height: 100,
      decoration: StyleTheme.boxDecoration,
      child: CustomInkWell(
        onTap: onTap,
        borderRadius: BorderRadius.circular(12),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(icon),
            Container(
              margin: EdgeInsets.only(top: 12),
              child: Text(
                '$title',
                style: TextStyle(
                  color: Color(0xff303030),
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class HomeTitleCard extends StatelessWidget {
  final String title;

  const HomeTitleCard({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 40, left: 16),
      child: Text(
        '$title',
        style: TextStyle(
          color: ColorTheme.primaryColor,
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
