import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/routes/sign/page/certification_webview_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CertificationPage extends StatelessWidget{
  static const route = '/sign/certification';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      title: '휴대폰 인증',
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child:  Column(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Container(
                        width: 100,
                        height: 164,
                        decoration: BoxDecoration(
                          color: ColorTheme.grayLevel1,
                          borderRadius: BorderRadius.all(Radius.circular(11))
                        ),

                        child: Image(
                          image: AssetImage('assets/icons/icon-lock.png'),
                          width: 48,
                          height: 64,
                        ),
                      ),
                    ),
                    SizedBox(height: 38,),
                    Center(
                      child: Text("휴대폰 인증이 필요합니다.", style: TextStyleTheme.darkBlackS16W700,),
                    ),
                    SizedBox(height: 8,),

                    Center(
                      child: Text("원웨드의 서비스사용을 위해서 휴대폰 인증을 해주세요.", style: TextStyleTheme.darkGrayS14W400,),
                    ),
                    SizedBox(height: 4,),

                    Center(
                      child: Text("상품문의, 1:1톡 등 다양한 서비스를 이용하실 수 있습니다!", style: TextStyleTheme.darkGrayS14W400,),
                    ),
                    SizedBox(height: 100,),
                  ],
                )
              ),
              CustomContainer(
                onPressed: (){
                  this.navigator!.pushRoute(
                      CertificationWebViewPage()
                  );
                },
                backgroundColor: ColorTheme.primaryColor,
                height: 60,
                borderRadius: [12, 12, 12,12],
                child: Center(
                  child: Text("휴대폰 인증", style: TextStyleTheme.whiteS16W700,),
                ),
              )
            ],
          ),
        ),
      )
    );
  }


}
