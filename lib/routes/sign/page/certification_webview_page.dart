import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/tab/tab_page.dart';
import 'package:provider/provider.dart';

class CertificationWebViewPage extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return View();
  }

}

class View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  
  UserProvider? _userProvider;
  File? file;

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    
    return PlatformScaffold(
        body: SafeArea(
            child: InAppWebView(
              initialUrlRequest: URLRequest(url: Uri.parse("${dotenv.env['PASS_CERTIFICATION_URL']}")),
              // initialUrlRequest: URLRequest(url: Uri.parse("http://192.168.0.13:8080/certification")),
              initialOptions: InAppWebViewGroupOptions(
                crossPlatform: InAppWebViewOptions(
                  useShouldOverrideUrlLoading: true,
                  mediaPlaybackRequiresUserGesture: true,
                ),

//                장유정
              ),
              onWebViewCreated: (InAppWebViewController controller) {

                controller.addJavaScriptHandler(handlerName: "impUid", callback: (value) {
                  // Here you receive all the arguments from the JavaScript side
                  // that is a List<dynamic>
                  this.modifyMe(value[0]);

                  // return args.reduce((curr, next) => curr + next);
                });
              },
            )
        )
    );
  }


  void modifyMe(String impUid){

    Map map = {
      "imp_uid": impUid,
      "_method":"PUT"
    };


    _userProvider!.modifyMe(map, file).then((value){
      if(value!=null){
        _userProvider!.selectMe('*').then((user){
          this.navigator!.replaceRoute(TabPage());
          BotToast.showText(text: "반갑습니다${user!.name}님. 회원가입이 완료 됐습니다!");
        });



      }else{
        this.navigator!.popRoute(null);
        BotToast.showText(text: "오류가 발생했습니다. 잠시 후 시도해주세요");
      }

    }).catchError((onError){
      print(onError);
      this.navigator!.popRoute(null);
      BotToast.showText(text: "오류가 발생했습니다. 잠시 후 시도해주세요");
    });
  }
}
