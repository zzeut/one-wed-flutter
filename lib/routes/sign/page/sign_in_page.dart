import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_naver_login/flutter_naver_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:kakao_flutter_sdk/all.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/providers/push_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/chat/chat_page.dart';
import 'package:onewed_app/routes/sign/page/certification_page.dart';
import 'package:onewed_app/routes/tab/tab_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:provider/provider.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';



class SignInPage extends StatelessWidget {
  static const route = '';

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<PushProvider>(create: (context) => PushProvider(), child: _View(),);
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  bool _isLoading = false;


  UserProvider? _userProvider;
  PushProvider? _pushProvider;


  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);
  }


  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    _pushProvider = Provider.of<PushProvider>(context);

    return CustomModal(
      isLoading: _isLoading,
      child: PlatformScaffold(
        body: SafeArea(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: ListView(
              children: [
                SizedBox(height: 80,),
                Center(
                  child: Image(
                    image: AssetImage('assets/images/image_logo.png'),
                    width: 190,
                    height: 50,
                    fit: BoxFit.cover,
                  ),
                ),
                SizedBox(height: 14,),
                Center(
                  child: Text("빛나는 날을 위한 웨딩 플래닝 서비스", style: TextStyleTheme.darkDarkS14W400,),
                ),
                SizedBox(height: 26,),
                Center(
                  child: Image(
                    image: AssetImage('assets/images/image_main_2.png'),
                    fit: BoxFit.cover,
                    width: 200,
                    height: 200,
                  ),
                ),
                SizedBox(height: 50,),
                Center(
                  child: Text("소셜로그인으로 다양한 웨딩 정보를 받아보세요!", style: TextStyleTheme.darkDarkS14W400,),
                ),
                SizedBox(height: 20,),
                GestureDetector(
                  onTap: (){
                    facebookLogin();
                  },
                  child: Container(
                    height: 50,
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                      color: ColorTheme.facebookBackground,
                      borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          child: Image(
                            image: AssetImage('assets/icons/icon_facebook.png'),
                            width: 24,
                            height: 24,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Expanded(
                            child: Center(
                              child: Text('facebook으로 로그인', style: TextStyleTheme.whiteS16W700,),
                            )
                        )
                      ],

                    ),
                  ),
                ),
                SizedBox(height: 15,),
                GestureDetector(
                  onTap: (){
                    this.kakaoLogin();
                  },
                  child: Container(
                    height: 50,
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                      color: ColorTheme.kakaoBackground,
                      borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          child: Image(
                            image: AssetImage('assets/icons/icon_kakao.png'),
                            width: 24,
                            height: 24,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Expanded(
                            child: Center(
                              child: Text('Kakao로 로그인', style: TextStyleTheme.whiteS16W700,),
                            )
                        )
                      ],

                    ),
                  ),
                ),
                SizedBox(height: 15,),
                GestureDetector(
                  onTap: (){
                    naverLogin();
                  },
                  child: Container(
                    height: 50,
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                      color: ColorTheme.naverBackground,
                      borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          child: Image(
                            image: AssetImage('assets/icons/icon_naver.png'),
                            width: 24,
                            height: 24,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Expanded(
                            child: Center(
                              child: Text('Naver로 로그인', style: TextStyleTheme.whiteS16W700,),
                            )
                        )
                      ],

                    ),
                  ),
                ),
                SizedBox(height: 15,),
                Visibility(
                  visible: foundation.defaultTargetPlatform == foundation.TargetPlatform.iOS,
                  child: GestureDetector(
                    onTap: appleLogin,
                    child: Container(
                      height: 50,
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      decoration: BoxDecoration(
                          color: ColorTheme.appleBackground,
                          borderRadius: BorderRadius.all(Radius.circular(10))
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            child: Image(
                              image: AssetImage('assets/icons/icon_apple.png'),
                              width: 24,
                              height: 24,
                              fit: BoxFit.cover,
                            ),
                          ),
                          Expanded(
                              child: Center(
                                child: Text('Apple로 로그인', style: TextStyleTheme.whiteS16W700,),
                              )
                          )
                        ],

                      ),
                    ),
                  )
                ),
                Visibility(
                  visible: foundation.defaultTargetPlatform == foundation.TargetPlatform.android,
                  child: GestureDetector(
                    onTap: (){
                      googleLogin();
                    },
                    child: Container(
                      height: 50,
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      decoration: BoxDecoration(
                          color: ColorTheme.googleBackground,
                          borderRadius: BorderRadius.all(Radius.circular(10))
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            child: Image(
                              image: AssetImage('assets/icons/icon_google.png'),
                              width: 24,
                              height: 24,
                              fit: BoxFit.cover,
                            ),
                          ),
                          Expanded(
                              child: Center(
                                child: Text('google로 로그인', style: TextStyleTheme.whiteS16W700,),
                              )
                          )
                        ],

                      ),
                    ),
                  )
                ),
                SizedBox(height: 30,),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void socialLogin(String type, String token) async{
    Map<String, String> map = {
      "token": token
    };
    indicator.show();
    _userProvider!.socialLogin(type, map).then((response){
      if(response!.statusCode == 200){
        getMe();
      }else{
        indicator.hide();
      }

    }).catchError((e){

      BotToast.showText(text: '소셜로그인 중 오류가 발생했습니다 잠시 후 다시 시도해주세요');
      indicator.hide();
      print(e);
    });

}

void getMe(){

  _userProvider!.selectMe('*').then((response) async {
    final value = await FirebaseMessaging.instance.getToken();
    if(value != null){

      String machineType = "0";
      if(foundation.defaultTargetPlatform == foundation.TargetPlatform.iOS){
        machineType = "2";
      }else if(foundation.defaultTargetPlatform == foundation.TargetPlatform.android){
        machineType = "1";
      }
      _pushProvider!.insertFcmToken(value, machineType).then((value) => {print(value)});
    }

    indicator.hide();
    navigator!.resetRoute(TabPage());
    /*if(response!.name == null){
      navigator!.pushRoute(CertificationPage());
    }else{

      if(response.level == 9){
        navigator!.resetRoute(ChatPage());
      }else{

      }

    }*/
  }).catchError((e){
    print(e);
    BotToast.showText(text: '유저 정보를 불러오는 중 오류가 발생했습니다 잠시 후 다시 시도해주세요');
    indicator.hide();
  });
}

  void kakaoLogin() async{
    try{
      final installed = await isKakaoTalkInstalled();
      final authCode = installed ? await AuthCodeClient.instance.requestWithTalk() : await AuthCodeClient.instance.request();

      AccessTokenResponse token = await AuthApi.instance.issueAccessToken(authCode);
      TokenManager.instance.setToken(token);

      print(token.accessToken);

      this.socialLogin('kakao', token.accessToken);
    }catch (e){
      print('==================Kakao Login error==================');
      print(e);
    }

  }

  void facebookLogin() async{
    try{

      final LoginResult result = await FacebookAuth.instance.login();
      if(result.status == LoginStatus.success){

        final AccessToken? accessToken = result.accessToken;

        this.socialLogin('facebook', accessToken!.token);
      }else{
        print(result.status);
      }

    }catch(e){
      print('==================facebook Login error==================');
      print(e);
    }
  }

  void googleLogin() async{
    try{
      GoogleSignIn googleSignIn = GoogleSignIn(
        scopes: [
          'email',
        ],);
      GoogleSignInAccount? account = await googleSignIn.signIn();
      GoogleSignInAuthentication auth = await account!.authentication;

      this.socialLogin('google', auth.accessToken.toString());
    }catch(e){
      print('==================google Login error==================');
      print(e);
    }
  }

  void naverLogin() async{

    final NaverLoginResult result = await FlutterNaverLogin.logIn();
    NaverAccessToken res = await FlutterNaverLogin.currentAccessToken;
    try {
      this.socialLogin('naver', res.accessToken);
    } catch(e) {

      print('==================naver Login error==================');
      print(e);
    }
  }

  void appleLogin() async{
    try{
      final credential = await SignInWithApple.getAppleIDCredential(scopes: [
        AppleIDAuthorizationScopes.email
      ]);

      this.socialLogin('apple', "${credential.identityToken}");
    }catch(error){

      print('==================apple Login error==================');
      print(error);
    }




  }
}
