import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/components/dateTime.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/components/review/company_reveiw_item_component.dart';
import 'package:onewed_app/models/company_review_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/company_review_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/review/review_detail_page.dart';
import 'package:onewed_app/routes/review/review_image_detail_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ReviewListPage extends StatelessWidget{
  static const route = '/mypage/review';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return _View();
  }

}

class _View extends BaseRoute {
  @override
  __ViewState createState() => __ViewState();
}

class __ViewState extends BaseRouteState {

  CompanyReviewProvider? _companyReviewProvider;
  List<CompanyReview> _companyReviewList = [];

  UserProvider? _userProvider;
  User? me;

  RefreshController refreshController = new RefreshController(initialRefresh: false);

  Map<String, String> sort = {
    "sort[by]": "created_at",
    "sort[order]": "desc"
  };

  int page = 0;
  bool hasMore = true;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    onRefresh();
  }


  @override
  Widget build(BuildContext context) {
    _companyReviewProvider = Provider.of<CompanyReviewProvider>(context);
    _companyReviewList = _companyReviewProvider!.getCompanyReviewList();

    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();

    return PlatformScaffold(

      title: "리뷰내역",

      body: _companyReviewList.length == 0 ? Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 10, left: 8, right: 8, ),
            child: Row(
              children: [
                CustomContainer(
                  onPressed: (){
                    sort = {
                      "sort[by]": "created_at",
                      "sort[order]": "desc"
                    };

                    setState(() {

                    });
                  },
                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 12),
                  child: Text("최근작성순", style: sort["sort[by]"] == 'created_at' ? TextStyleTheme.primaryS16W700: TextStyleTheme.darkLightS16W400 ,),

                ),
                CustomContainer(
                  onPressed: (){
                    sort = {
                      "sort[by]": "schedule_date",
                      "sort[order]": "desc"
                    };
                    setState(() {

                    });
                  },
                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 12),
                  child: Text("최근방문순", style: sort["sort[by]"] == 'schedule_date' ? TextStyleTheme.primaryS16W700: TextStyleTheme.darkLightS16W400 ,),
                )
              ],
            ),
          ),
          Expanded(child: Container(
            height: 500,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(image: AssetImage("assets/images/empty-review-image.png"), width: 80, height: 80, fit: BoxFit.cover,),
                SizedBox(height: 30,),
                Text("작성하신 리뷰가 없습니다", style: TextStyleTheme.darkBlackS18W700,),
                SizedBox(height: 8,),
                Text("원웨드를 통해 업체를 이용하신 후", style: TextStyleTheme.darkGrayS16W400,),
                Text("고객님의 소중한 리뷰를 남겨주세요.", style: TextStyleTheme.darkGrayS16W400,),
              ],
            ),
          ))
        ],
      ) : RefreshConfiguration(
        child: SmartRefresher(
          controller: refreshController,
          onRefresh: this.onRefresh,
          onLoading: this.onLoad,
          enablePullUp: true,
          header: CustomSmartRefresher.customHeader(),
          footer: CustomSmartRefresher.customFooter(),
          child: ListView(
            children: [
              Container(
                padding: EdgeInsets.only(top: 10, left: 8, right: 8, ),
                child: Row(
                  children: [
                    CustomContainer(
                      onPressed: (){
                        sort = {
                          "sort[by]": "created_at",
                          "sort[order]": "desc"
                        };

                        onRefresh();
                      },
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 12),
                      child: Text("최근작성순", style: sort["sort[by]"] == 'created_at' ? TextStyleTheme.primaryS16W700: TextStyleTheme.darkLightS16W400 ,),

                    ),
                    CustomContainer(
                      onPressed: (){
                        sort = {
                          "sort[by]": "schedule_date",
                          "sort[order]": "desc"
                        };
                        onRefresh();
                      },
                      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 12),
                      child: Text("최근방문순", style: sort["sort[by]"] == 'schedule_date' ? TextStyleTheme.primaryS16W700: TextStyleTheme.darkLightS16W400 ,),
                    )
                  ],
                ),
              ),
              Consumer<CompanyReviewProvider>(builder: (context, companyReviewModel, child){
                _companyReviewList = companyReviewModel.getCompanyReviewList();
                return companyReviewListWidget(_companyReviewList);
              })

            ],
          ),
        ),
      )

    );

  }

  onRefresh(){
    page = 0;
    hasMore = true;

    this.onLoad();
    refreshController.refreshCompleted();
  }

  onLoad(){

    if(hasMore){
      page = page +1;
      Map<String, String> params = {
        "page" : "$page",
        "user_no": "${me!.userNo}",
        "more_field": "*"
      };
      params.addAll(sort);
      _companyReviewProvider!.selectCompanyReviewList(page, params).then((response){

        if(response!.length == 0){
          hasMore = false;
        }

        setState(() {

        });
        refreshController.loadComplete();

      }).catchError((onError){
      });

      refreshController.loadComplete();
    }
    refreshController.loadComplete();
  }



  Widget companyReviewListWidget(List<CompanyReview> companyReviewList){
    List<Widget> list = [];
    companyReviewList.forEach((element) {list.add(CompanyReviewItemComponent(
      companyReview: element,
      reviewImageDetail: (index) {
         this.navigator!.pushRoute(ReviewImageDetailPage(reviewImageList: element.companyReviewImageList!, index: index,));
      },
      reviewDetail: (){
        this.navigator!.pushRoute(ReviewDetailPage(companyReviewNo: element.companyReviewNo!.toInt()));
      },
    ));});

    return Column(
      children: list,
    );
  }


}

