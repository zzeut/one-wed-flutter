import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/company/company_page.dart';
import 'package:onewed_app/routes/inquiry/inquiry_list_page.dart';
import 'package:onewed_app/routes/inquiry/write_inquiry_page.dart';
import 'package:onewed_app/routes/me/budget_page.dart';
import 'package:onewed_app/routes/me/like_list_page.dart';
import 'package:onewed_app/routes/me/review_list_page.dart';
import 'package:onewed_app/routes/notice/notice_page.dart';
import 'package:onewed_app/routes/schedule/schedule_main_page.dart';
import 'package:onewed_app/routes/service/service_page.dart';
import 'package:onewed_app/routes/setting/setting_page.dart';
import 'package:onewed_app/routes/term/term_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class MyPage extends StatelessWidget{
  static const route = '/mypage';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View();
  }
}

class _View extends BaseRoute {
  @override
  __ViewState createState() => __ViewState();
}

class __ViewState extends BaseRouteState {
  UserProvider? _userProvider;
  User? me;

  RefreshController refreshController = new RefreshController(initialRefresh: false);
  
  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();

    return PlatformScaffold(
      title: '마이페이지',
      leading: Container(),
      body: SafeArea(
        child: RefreshConfiguration(
          child: SmartRefresher(
            controller: refreshController,
            onRefresh: this.onRefresh,
            enablePullDown: true,
            header: CustomSmartRefresher.customHeader(),
            child: ListView(
              children: [
                Consumer<UserProvider>(builder: (context, userModel, child){
                  me = userModel.getMe();
                  return Column(
                    children: [
                      SizedBox(height: 20,),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: Row(
                          children: [
                            Container(
                              height: 52,
                              width: 52,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: profileWidget(),
                              ),
                            ),
                            SizedBox(width: 16,),
                            Expanded(child:
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("${me!.level == 1 ? me!.weddingCeremonyDayString : ""}", style: TextStyleTheme.primaryS20W700,),
                                SizedBox(height: 4,),
                                Text("안녕하세요. ${me!.name ?? '사용자'}님", style: TextStyle(
                                    color: ColorTheme.darkDarkBlack,
                                    fontSize: 18
                                ),)
                              ],
                            )
                            ),
                            CustomContainer(
                              onPressed: (){
                                this.navigator!.pushRoute(SettingPage());
                              },
                              width: 52,
                              height: 52,
                              borderColor: Color.fromRGBO(211, 211, 211, 1.0),
                              borderWidth: 1,
                              borderRadius: [10, 10, 10, 10],
                              child: Image(
                                image: AssetImage("assets/icons/icon_setting.png"),
                                width: 18,
                                height: 18,
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 40,),
                      GestureDetector(
                        onTap: (){
                          if(me!.level != 1){
                            BotToast.showText(text: "플래너는 이용할 수 없는 기능입니다");
                          }else{
                            if(me!.budgetMoney == null || me!.budgetMoney == 0){
                              showBudgetAlert();
                            }else{
                              this.navigator!.pushRoute(BudgetPage());
                            }
                          }

                        },
                        child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 16),
                          padding: EdgeInsets.only(top: 16, bottom: 16, left: 20, right: 15),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(12)),
                              border: Border.all(
                                  color:ColorTheme.primaryColor
                              ),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xff954f4f).withOpacity(0.12),
                                    offset: Offset(0, 4),
                                    blurRadius: 6
                                )
                              ]
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Image(
                                            image: AssetImage("assets/icons/icon_wallet_32.png"),
                                          ),
                                          SizedBox(width: 11,),
                                          Text("예산", style: TextStyle(
                                              color: ColorTheme.darkDarkBlack,
                                              fontSize: 18,
                                              fontWeight: FontWeight.w700
                                          ),)
                                        ],
                                      ),
                                      SizedBox(height: 8,),
                                      if(me!.level == 1)
                                      me!.budgetMoney == null || me!.budgetMoney == 0 ?
                                        Text("예산설정을 위해 플래너와 상담해주세요.", style: TextStyle(
                                          color: ColorTheme.darkLight,
                                          fontSize: 18,
                                        ),): Text("${me!.budgetMoney}", style: TextStyleTheme.primaryS18W700,)
                                    ],
                                  )
                              ),
                              Container(
                                width: 10,
                                height: 20,
                                child: Image(
                                  image: AssetImage("assets/icons/icon_arrow_right.png"),
                                  fit: BoxFit.cover,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 20,),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 16),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: (){
                                    this.navigator!.pushRoute(LikeListPage());
                                  },
                                  child: Container(
                                      padding: EdgeInsets.only(top: 16, bottom: 16, left: 20, right: 15),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.all(Radius.circular(12)),
                                          border: Border.all(
                                              color:ColorTheme.primaryColor
                                          ),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Color(0xff954f4f).withOpacity(0.12),
                                                offset: Offset(0, 4),
                                                blurRadius: 6
                                            )
                                          ]
                                      ),

                                      child: Row(
                                        children: [
                                          Expanded(
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      Image(image: AssetImage("assets/icons/icon_ike.png"),),
                                                      SizedBox(width: 11,),
                                                      Text("찜", style: TextStyle(
                                                          color: ColorTheme.darkDarkBlack,
                                                          fontSize: 18,
                                                          fontWeight: FontWeight.w700
                                                      ),)
                                                    ],
                                                  ),
                                                  SizedBox(height: 8,),
                                                  Text("${me!.likeCount}개", style: TextStyleTheme.primaryS18W700,)
                                                ],
                                              )
                                          ),
                                          Container(
                                            width: 10,
                                            height: 20,
                                            child: Image(
                                              image: AssetImage("assets/icons/icon_arrow_right.png"),
                                              fit: BoxFit.cover,
                                            ),
                                          )
                                        ],
                                      )
                                  ),
                                )
                            ),
                            SizedBox(width: 20,),
                            Expanded(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: (){
                                    this.navigator!.pushRoute(ScheduleMainPage(isBack: true,));

                                  },
                                  child: Container(
                                      padding: EdgeInsets.only(top: 16, bottom: 16, left: 20, right: 15),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.all(Radius.circular(12)),
                                          border: Border.all(
                                              color:ColorTheme.primaryColor
                                          ),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Color(0xff954f4f).withOpacity(0.12),
                                                offset: Offset(0, 4),
                                                blurRadius: 6
                                            )
                                          ]
                                      ),

                                      child: Row(
                                        children: [
                                          Expanded(
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      Image(image: AssetImage("assets/icons/mypage/icon_schedule.png"),width: 24,),
                                                      SizedBox(width: 11,),
                                                      Text("스케줄", style: TextStyle(
                                                          color: ColorTheme.darkDarkBlack,
                                                          fontSize: 18,
                                                          fontWeight: FontWeight.w700
                                                      ),)
                                                    ],
                                                  ),
                                                  SizedBox(height: 8,),
                                                  Text("${me!.level == 1 ? me!.scheduleCount : me!.writerSchedule}개", style: TextStyleTheme.primaryS18W700,)
                                                ],
                                              )
                                          ),
                                          Container(
                                            width: 10,
                                            height: 20,
                                            child: Image(
                                              image: AssetImage("assets/icons/icon_arrow_right.png"),
                                              fit: BoxFit.cover,
                                            ),
                                          )
                                        ],
                                      )
                                  ),
                                )
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 20,),
                      Container(
                        height: 8,
                        decoration: BoxDecoration(
                            color: ColorTheme.grayLevel1
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          this.navigator!.pushRoute(NoticePage());
                        },
                        child: Container(
                          padding: EdgeInsets.only(top: 20, left: 16, bottom: 19, right: 25),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(width: 1,color: ColorTheme.grayLevel1)
                              )
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                  child: Text("공지사항", style: TextStyleTheme.darkDarkS16W400,)
                              ),
                              Image(image: AssetImage("assets/icons/gray-right-arrow.png"), width: 10,height: 20, fit: BoxFit.cover,)
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          this.navigator!.pushRoute(ServicePage());
                        },
                        child: Container(
                          padding: EdgeInsets.only(top: 20, left: 16, bottom: 19, right: 25),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(width: 1,color: ColorTheme.grayLevel1)
                              )
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                  child: Text("자주 묻는 질문", style: TextStyleTheme.darkDarkS16W400,)
                              ),
                              Image(image: AssetImage("assets/icons/gray-right-arrow.png"), width: 10,height: 20, fit: BoxFit.cover,)
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          this.navigator!.pushRoute(WriteInquiryPage());
                        },
                        child: Container(
                          padding: EdgeInsets.only(top: 20, left: 16, bottom: 19, right: 25),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(width: 1,color: ColorTheme.grayLevel1)
                              )
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                  child: Text("문의하기", style: TextStyleTheme.darkDarkS16W400,)
                              ),
                              Image(image: AssetImage("assets/icons/gray-right-arrow.png"), width: 10,height: 20, fit: BoxFit.cover,)
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          this.navigator!.pushRoute(InquiryListPage());
                        },
                        child: Container(
                          padding: EdgeInsets.only(top: 20, left: 16, bottom: 19, right: 25),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(width: 1,color: ColorTheme.grayLevel1)
                              )
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                  child: Text("문의내역", style: TextStyleTheme.darkDarkS16W400,)
                              ),
                              Image(image: AssetImage("assets/icons/gray-right-arrow.png"), width: 10,height: 20, fit: BoxFit.cover,)
                            ],
                          ),
                        ),
                      ),
                      if(me!.level == 1)
                        GestureDetector(
                          onTap: (){
                            this.navigator!.pushRoute(ReviewListPage());
                          },
                          child: Container(
                            padding: EdgeInsets.only(top: 20, left: 16, bottom: 19, right: 25),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(width: 1,color: ColorTheme.grayLevel1)
                                )
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                    child: Text("리뷰내역", style: TextStyleTheme.darkDarkS16W400,)
                                ),
                                Image(image: AssetImage("assets/icons/gray-right-arrow.png"), width: 10,height: 20, fit: BoxFit.cover,)
                              ],
                            ),
                          ),
                        ),
                      GestureDetector(
                        onTap: (){
                          this.navigator!.pushRoute(TermPage());
                        },
                        child: Container(
                          padding: EdgeInsets.only(top: 20, left: 16, bottom: 19, right: 25),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(width: 1,color: ColorTheme.grayLevel1)
                              )
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                  child: Text("이용약관", style: TextStyleTheme.darkDarkS16W400,)
                              ),
                              Image(image: AssetImage("assets/icons/gray-right-arrow.png"), width: 10,height: 20, fit: BoxFit.cover,)
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 27,)

                    ],
                  );
                })
              ],
            ),
          ),
        )
      )
    );
  }

  void onRefresh(){
    _userProvider!.selectMe('*');
    refreshController.refreshCompleted();
  }

  Widget profileWidget(){
    if(me!.profilePath == null){
      if(me!.gender == "1"){
        return Image(
            image: AssetImage("assets/images/profile_man.png")
        );
      }else if(me!.gender == "2"){
        return Image(
            image: AssetImage("assets/images/profile_woman.png")
        );
      }
    }else{
      return Image.network(me!.profilePath!.o, fit: BoxFit.cover,);
    }


    return Image(
        image: AssetImage("assets/images/profile_default.png")
    );
  }


  void showBudgetAlert(){

    showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          return AlertDialog(
            insetPadding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
            clipBehavior: Clip.antiAliasWithSaveLayer,
            contentPadding: EdgeInsets.only(top: 30, left: 16, right: 16, bottom: 16),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            content: CustomContainer(
              width: MediaQuery.of(context).size.width,
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("예산 설정 안내", style: TextStyleTheme.blackS18W500,),
                    SizedBox(height: 4,),
                    Text("예산 설정을 위해서는 업체 문의를 통한", style: TextStyleTheme.darkDarkS16W400,),
                    SizedBox(height: 4,),
                    Text("플래너와의 상담을 통해 해드립니다.", style: TextStyleTheme.darkDarkS16W400,),
                    SizedBox(height: 20,),
                    Row(
                      children: [
                        Expanded(child: CustomContainer(
                          onPressed: (){
                            Navigator.pop(context);
                          },
                          height: 60,
                          borderWidth: 1,
                          borderColor: ColorTheme.darkDark,
                          borderRadius: [6, 6, 6, 6],
                          child: Center(
                            child: Text("나중에", style: TextStyleTheme.darkDarkS16W400,),
                          ),
                        )),
                        SizedBox(width: 10,),
                        Expanded(child: CustomContainer(
                          onPressed: (){
                            this.navigator!.pushRoute(CompanyPage());
                          },
                          height: 60,
                          backgroundColor: ColorTheme.primaryColor,
                          borderRadius: [6, 6, 6, 6],
                          child: Center(
                            child: Text("웨딩샵 알아보기", style: TextStyleTheme.whiteS16W700,),
                          ),
                        ))
                      ],
                    )
                  ]),
            ),
          );
        }
    );
  }

}
