import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/providers/push_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/sign/page/sign_in_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WithdrawalPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ChangeNotifierProvider<PushProvider>(
      create: (context) => PushProvider(),
      child: View(),
    );
  }
}

class View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {

  UserProvider? _userProvider;
  PushProvider? _pushProvider;

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    _pushProvider = Provider.of<PushProvider>(context);


    return PlatformScaffold(
      title: "회원탈퇴",
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset("assets/images/withdrawal-image.png", width: 80,),
                  SizedBox(height: 30,),
                  Text("고객님, 원웨드를 탈퇴하실 건가요?", style: TextStyleTheme.darkDarkS18W700,),
                  SizedBox(height: 8,),
                  Text("탈퇴 시에는 소중한 회원님의 플래너와의 대화, \n찜, 회원 정보가 모두 사라집니다.", style: TextStyleTheme.darkDarkS16W400, textAlign: TextAlign.center),
                ],
              )
            ),
            Row(
              children: [
                SizedBox(width: 16,),
                CustomContainer(
                  onPressed: (){
                    showWithdrawalConfirm();
                  },
                  borderRadius: [12, 12, 12, 12],
                  borderColor: ColorTheme.darkDark,
                  borderWidth: 1,
                  width: 88,
                  height: 60,
                  child: Center(
                    child: Text("탈퇴", style: TextStyleTheme.darkDarkS16W400,),
                  ),
                ),
                SizedBox(width: 10,),
                Expanded(child: CustomContainer(
                  onPressed: (){
                    this.navigator!.popRoute(null);
                  },
                  borderRadius: [12, 12, 12, 12],
                  backgroundColor: ColorTheme.primaryColor,
                  height: 60,
                  child: Center(
                    child: Text("나중에 할게요", style: TextStyleTheme.whiteS16W700,),
                  ),
                )),
                SizedBox(width: 16,),
              ],
            )
          ],
        ),
      )
    );
  }

  showWithdrawalConfirm(){
    bool isAgree = false;

    showDialog(
        context: context,
        builder: (context) => StatefulBuilder(builder: (buildContent, setState) => AlertDialog(
          insetPadding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12))
          ),
          contentPadding: EdgeInsets.zero,
          content: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(top: 30, bottom: 20, left: 16, right: 16),
                child: Column(
                  children: [
                    Text("원웨드를 정말 탈퇴하시겠습니까?", style: TextStyleTheme.blackS18W700,),
                    SizedBox(height: 4,),
                    Text("탈퇴 시 웨딩 플래닝 서비스를 받아보실 수 없습니다.", style: TextStyleTheme.darkGrayS16W400,),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: ColorTheme.lightLight
                ),
                padding: EdgeInsets.only(top: 20, bottom: 20, left: 16, right: 16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("탈퇴 전 확인 하세요!", style: TextStyleTheme.darkDarkS16W700,),
                    SizedBox(height: 4,),
                    Text("탈퇴하신 계정과 정보는 복구할 수 없습니다.\n원웨드의 서비스를 더 이상 사용하실 수 없습니다.\n재가입시에는 신규회원으로 가입됩니다.", style: TextStyleTheme.darkDarkS14W400,),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 10, bottom: 16, left: 16, right: 16),
                child: Column(
                  children: [
                    CustomContainer(
                      onPressed: (){
                        setState(() => isAgree = !isAgree);
                      },
                      child: Row(
                        children: [
                          Container(
                            width: 20,
                            height: 20,
                            decoration: BoxDecoration(
                              color: isAgree ? ColorTheme.primaryColor :  ColorTheme.grayLevel1,
                              shape: BoxShape.circle
                            ),
                            child: Image.asset("assets/icons/icon_check.png", width: 8, height: 6,),
                          ),
                          SizedBox(width: 14,),
                          Expanded(child: Text("안내사항을 모두 숙지하고 이에 동의합니다.", style: isAgree ? TextStyleTheme.darkDarkS14W400 :  TextStyleTheme.lightDarkS14W400,))
                        ],
                      ),
                    ),
                    SizedBox(height: 20,),

                    Row(
                      children: [
                        SizedBox(width: 16,),
                        CustomContainer(
                          onPressed: (){
                            if(isAgree){
                              withdrawal();

                              Navigator.pop(context);
                            }

                          },
                          borderRadius: [12, 12, 12, 12],
                          borderColor: ColorTheme.darkDark,
                          borderWidth: 1,
                          width: 88,
                          height: 60,
                          child: Center(
                            child: Text("탈퇴", style: TextStyleTheme.darkDarkS16W400,),
                          ),
                        ),
                        SizedBox(width: 10,),
                        Expanded(child: CustomContainer(
                          onPressed: (){
                            Navigator.pop(context);
                          },
                          borderRadius: [12, 12, 12, 12],
                          backgroundColor: ColorTheme.primaryColor,
                          height: 60,
                          child: Center(
                            child: Text("나중에 할게요", style: TextStyleTheme.whiteS16W700,),
                          ),
                        )),
                        SizedBox(width: 16,),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),

        ))
    );
  }

  withdrawal() async{
    _userProvider!.withdrawalMe().then((value){
      if(value!= null){
        showWithdrawalSuccessAlert();
      }
    });
  }

  showWithdrawalSuccessAlert(){
    showDialog(
      barrierDismissible: false,
      context: context, builder: (context) => AlertDialog(

        insetPadding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        contentPadding: EdgeInsets.only(top: 40, left: 16, right: 16, bottom: 16),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0))
        ),

        content: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image.asset("assets/images/3.0x/image_logo.png", width: 200, height: 60,),
            SizedBox(height: 20,),
            Text("원웨드를 사용해주셔서 감사합니다.", style: TextStyleTheme.blackS18W500),
            Text("다음에는 더 훌륭한 서비스로 만나뵙겠습니다.", style: TextStyleTheme.darkDarkS16W400,),
            SizedBox(height: 40,),
            CustomContainer(
              width: double.infinity,
              height: 60,
              onPressed: () async{

                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.remove('accessToken');
                prefs.remove('refreshToken');

                // 탈퇴 유저 fcmtoken
                _pushProvider!.deleteUserFcmToken();

                this.navigator!.popToRootRoute();
                this.navigator!.resetRoute((SignInPage()));
              },
              backgroundColor: ColorTheme.primaryColor,
              borderRadius: [6, 6, 6, 6],
              child: Center(
                child: Text("나중에 만나요", style: TextStyleTheme.whiteS16W700,),
              ),
            )
          ],
        ),

    ));
  }
}
