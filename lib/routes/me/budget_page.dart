import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/components/dateTime.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/const.dart';
import 'package:onewed_app/models/spending_model.dart';
import 'package:onewed_app/models/total_spending_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/spending_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/inquiry/write_inquiry_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class BudgetPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ChangeNotifierProvider(
      create: (context) => SpendingProvider(),
      child: View(),
    );
  }

}

class View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {

  UserProvider? _userProvider;
  User? me;

  SpendingProvider? _spendingProvider;
  TotalSpending? _totalSpending;

  List<Spending> _spendingList = [];

  int page = 0;
  bool hasMore = true;

  RefreshController refreshController = new RefreshController(initialRefresh: false);

  bool isBudgetOpen = false;

  var f = NumberFormat('###,###,###,###');

  SPENDING_FILTER spendingFilter = SPENDING_FILTER.week;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    _spendingProvider!.selectTotalSpending(me!.userNo!.toInt());

    onLoading();
  }


  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();

    _spendingProvider = Provider.of<SpendingProvider>(context);
    _totalSpending = _spendingProvider!.getTotalSpending();

    if(_totalSpending == null){
      return Container();
    }

    return PlatformScaffold(
      title: "예산",
      body: SafeArea(
        bottom: false,
        child: Container(
          padding: EdgeInsets.only(bottom: 20),
          child: Column(
            children: [
              Expanded(
                child: RefreshConfiguration(
                  child: SmartRefresher(
                    enablePullDown: false,
                    enablePullUp: true,
                    onLoading: this.onLoading,
                    footer: CustomSmartRefresher.customFooter(),
                    controller: refreshController,
                    child: ListView(
                      children: [
                        SizedBox(height: 20,),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: Column(
                            children: [
                              Stack(
                                children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 270),
                                    decoration: BoxDecoration(
                                        color: Colors.white,

                                        border: Border.all(
                                          width: 1,
                                          color: ColorTheme.primaryColor,
                                        ),
                                        borderRadius: BorderRadius.all(Radius.circular(12)),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Color(0xff954f4f).withOpacity(0.12),
                                            offset: Offset(0, 4),
                                            blurRadius: 6,
                                          )
                                        ]
                                    ),
                                    child: Stack(
                                      alignment: Alignment.bottomCenter,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(left: 16, right: 16, top: 16, bottom:  isBudgetOpen ? 60: 40),
                                          height:  isBudgetOpen ? null :  140,
                                          child: Card(
                                            elevation: 0,
                                            clipBehavior: Clip.antiAlias,
                                            child: Wrap(
                                              children: [
                                                Column(
                                                  children: _totalSpending!.categorySpendingMoney!.map((element) => Container(
                                                    margin: EdgeInsets.only(bottom: 8),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        Text("${element.categoryName}", style: TextStyleTheme.darkDarkS16W400,),
                                                        Text("-${f.format(element.spendingMoney)}원", style: TextStyleTheme.darkDarkS16W400,)
                                                      ],
                                                    ),
                                                  )).toList(),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                            child: Container(
                                              height: 60,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(Radius.circular(14)),
                                                gradient: LinearGradient(
                                                  colors: [
                                                    Colors.white,
                                                    Colors.white.withOpacity(0.85),
                                                  ],
                                                  begin: Alignment.bottomCenter,
                                                  end: Alignment.topCenter,
                                                ),
                                              ),
                                              child: Center(
                                                child: CustomContainer(
                                                  onPressed: (){
                                                    setState(() {
                                                      isBudgetOpen = !isBudgetOpen;
                                                    });
                                                  },
                                                  padding: EdgeInsets.all(5),
                                                  child:  RotatedBox(
                                                    quarterTurns: isBudgetOpen==true ? 2 : 0,
                                                    child: Image(image: AssetImage("assets/icons/icon_arrow_down_pink.png"), width: 24, height: 24, fit: BoxFit.cover,),
                                                  ),
                                                ),
                                              ),
                                            )
                                        )
                                      ],
                                    ),
                                  ),
                                  Positioned(
                                    child: Container(
                                      padding: EdgeInsets.only(top: 50, left: 16, right: 16, bottom: 16),
                                      decoration: BoxDecoration(
                                          color: ColorTheme.primaryColor,
                                          borderRadius: BorderRadius.all(Radius.circular(12))
                                      ),
                                      child: Column(
                                        children: [
                                          Text("예산대비 지출", style: TextStyleTheme.whiteS16W400,),
                                          SizedBox(height: 16,),
                                          Container(
                                            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius: BorderRadius.all(Radius.circular(24))
                                            ),
                                            child: Text("${_totalSpending!.spendingPercentage}%", style: TextStyleTheme.primaryS32W700,),
                                          ),
                                          SizedBox(height: 16,),
                                          Container(
                                            height: 8,
                                            child: LinearPercentIndicator(
                                              percent: _totalSpending!.spendingPercentage!.toDouble() / 100 > 1 ? 1 : _totalSpending!.spendingPercentage!.toDouble() / 100,
                                              progressColor: Colors.white,
                                              backgroundColor: Colors.white.withOpacity(0.2),
                                              lineHeight: 8,
                                            ),
                                          ),
                                          SizedBox(height: 32,),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Row(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Container(
                                                    width: 6,
                                                    height: 6,
                                                    decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        shape: BoxShape.circle
                                                    ),
                                                  ),
                                                  SizedBox(width: 8,),
                                                  Text("예산", style: TextStyleTheme.whiteS18W700,)
                                                ],
                                              ),
                                              Text("${f.format(_totalSpending!.budgetMoney!.toInt())}원", style: TextStyleTheme.whiteS18W700,)

                                            ],
                                          ),
                                          SizedBox(height: 8,),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Row(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Container(
                                                    width: 6,
                                                    height: 6,
                                                    decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        shape: BoxShape.circle
                                                    ),
                                                  ),
                                                  SizedBox(width: 8,),
                                                  Text("총 지출액", style: TextStyleTheme.whiteS18W700,)
                                                ],
                                              ),
                                              Text("${f.format(_totalSpending!.sumSpendingMoney!.toInt())}원", style: TextStyleTheme.whiteS18W700,)

                                            ],
                                          ),

                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(height: 30,),
                              Container(
                                height: 40,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Expanded(child: ListView(
                                      shrinkWrap: true,
                                      scrollDirection: Axis.horizontal,
                                      children: [

                                        CustomContainer(
                                          onPressed: (){
                                            setState(() {
                                              spendingFilter = SPENDING_FILTER.week;
                                            });

                                            onRefresh();
                                          },
                                          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                          borderRadius: [10, 10, 10, 10],
                                          backgroundColor: spendingFilter == SPENDING_FILTER.week ? ColorTheme.primaryColor : Colors.white,
                                          child: Text("1주일", style: spendingFilter == SPENDING_FILTER.week ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
                                        ),
                                        CustomContainer(
                                          onPressed: (){
                                            setState(() {
                                              spendingFilter = SPENDING_FILTER.month;
                                            });

                                            onRefresh();
                                          },
                                          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                          borderRadius: [10, 10, 10, 10],
                                          backgroundColor: spendingFilter == SPENDING_FILTER.month ? ColorTheme.primaryColor : Colors.white,
                                          child: Text("1개월", style: spendingFilter == SPENDING_FILTER.month ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
                                        ),
                                        CustomContainer(
                                          onPressed: (){
                                            setState(() {
                                              spendingFilter = SPENDING_FILTER.three_month;
                                            });

                                            onRefresh();
                                          },
                                          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                          borderRadius: [10, 10, 10, 10],
                                          backgroundColor: spendingFilter == SPENDING_FILTER.three_month ? ColorTheme.primaryColor : Colors.white,
                                          child: Text("3개월", style: spendingFilter == SPENDING_FILTER.three_month ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
                                        ),
                                        CustomContainer(
                                          onPressed: (){
                                            setState(() {
                                              spendingFilter = SPENDING_FILTER.half_year;
                                            });

                                            onRefresh();
                                          },
                                          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                          borderRadius: [10, 10, 10, 10],
                                          backgroundColor: spendingFilter == SPENDING_FILTER.half_year ? ColorTheme.primaryColor : Colors.white,
                                          child: Text("6개월", style: spendingFilter == SPENDING_FILTER.half_year ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
                                        ),
                                        CustomContainer(
                                          onPressed: (){
                                            setState(() {
                                              spendingFilter = SPENDING_FILTER.all;
                                            });

                                            onRefresh();
                                          },
                                          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                          borderRadius: [10, 10, 10, 10],
                                          backgroundColor: spendingFilter == SPENDING_FILTER.all ? ColorTheme.primaryColor : Colors.white,
                                          child: Text("전체", style: spendingFilter == SPENDING_FILTER.all ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
                                        ),
                                      ],
                                    ))
                                  ],
                                ),
                              ),
                              /*Row(
                                children: [
                                  CustomContainer(
                                    onPressed: (){
                                      setState(() {
                                        spendingFilter = SPENDING_FILTER.week;
                                      });

                                      onRefresh();
                                    },
                                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                    borderRadius: [10, 10, 10, 10],
                                    backgroundColor: spendingFilter == SPENDING_FILTER.week ? ColorTheme.primaryColor : Colors.white,
                                    child: Text("1주일", style: spendingFilter == SPENDING_FILTER.week ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
                                  ),
                                  CustomContainer(
                                    onPressed: (){
                                      setState(() {
                                        spendingFilter = SPENDING_FILTER.month;
                                      });

                                      onRefresh();
                                    },
                                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                    borderRadius: [10, 10, 10, 10],
                                    backgroundColor: spendingFilter == SPENDING_FILTER.month ? ColorTheme.primaryColor : Colors.white,
                                    child: Text("1개월", style: spendingFilter == SPENDING_FILTER.month ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
                                  ),
                                  CustomContainer(
                                    onPressed: (){
                                      setState(() {
                                        spendingFilter = SPENDING_FILTER.three_month;
                                      });

                                      onRefresh();
                                    },
                                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                    borderRadius: [10, 10, 10, 10],
                                    backgroundColor: spendingFilter == SPENDING_FILTER.three_month ? ColorTheme.primaryColor : Colors.white,
                                    child: Text("3개월", style: spendingFilter == SPENDING_FILTER.three_month ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
                                  ),
                                  CustomContainer(
                                    onPressed: (){
                                      setState(() {
                                        spendingFilter = SPENDING_FILTER.half_year;
                                      });

                                      onRefresh();
                                    },
                                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                    borderRadius: [10, 10, 10, 10],
                                    backgroundColor: spendingFilter == SPENDING_FILTER.half_year ? ColorTheme.primaryColor : Colors.white,
                                    child: Text("6개월", style: spendingFilter == SPENDING_FILTER.half_year ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
                                  ),
                                  CustomContainer(
                                    onPressed: (){
                                      setState(() {
                                        spendingFilter = SPENDING_FILTER.all;
                                      });

                                      onRefresh();
                                    },
                                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                                    borderRadius: [10, 10, 10, 10],
                                    backgroundColor: spendingFilter == SPENDING_FILTER.all ? ColorTheme.primaryColor : Colors.white,
                                    child: Text("전체", style: spendingFilter == SPENDING_FILTER.all ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
                                  ),
                                ],
                              ),*/
                              SizedBox(height: 30,),
                            ],
                          ),
                        ),
                        Consumer<SpendingProvider>(builder: (context, spendingModel, child){
                          _spendingList = spendingModel.getSpendingList();

                          if(_spendingList.length == 0){
                            return Column(
                              children: [
                                Image.asset("assets/images/spending-empty-image.png"),
                                SizedBox(height: 30,),
                                Text("예산 및 지출내역이 없습니다.", style: TextStyleTheme.darkDarkS18W700,),
                                SizedBox(height: 8,),
                                Text("문의사항을 통해 예산을 설정 및 수정 하실 수 있습니다.", style: TextStyleTheme.darkDarkS14W400,),

                                SizedBox(height: 32,),
                              ],
                            );
                          }

                          return Container(
                            padding: EdgeInsets.only(bottom: 12),
                            child: Column(
                              children: _spendingList.map((spending) => spendingItemWidget(spending)).toList(),
                            ),
                          );


                        }),
                      ],
                    ),
                  ),
                )
              ),
              Container(
                padding: EdgeInsets.only(top: 20, left: 16, right: 16),
                child: Row(
                  children: [
                    Expanded(
                        child: Container(
                          child: CustomContainer(
                            onPressed: (){
                              this.navigator!.pushRoute(WriteInquiryPage(initialIndex: 1,));
                            },
                            height: 60,
                            borderWidth: 1,
                            borderColor: ColorTheme.primaryColor,
                            borderRadius: [12, 12, 12, 12],
                            child: Center(
                              child: Text("전화 문의", style: TextStyleTheme.primaryS16W700,),
                            ),
                          ),
                        )
                    ),
                    SizedBox(width: 10,),
                    Expanded(
                        child: Container(
                          child: CustomContainer(
                            onPressed: (){
                              this.navigator!.pushRoute(WriteInquiryPage(initialIndex: 0,));
                            },
                            height: 60,
                            backgroundColor: ColorTheme.primaryColor,
                            borderRadius: [12, 12, 12, 12],
                            child: Center(
                              child: Text("1:1 문의", style: TextStyleTheme.whiteS16W700,),
                            ),
                          ),
                        )
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      )
    );
  }

  onRefresh(){
    page = 0;
    hasMore = true;
    onLoading();
  }

  onLoading(){
    if(hasMore){
      page = page+1;
      Map<String, String> data = {
        "user_no": "${me!.userNo}",
        "page": "$page",
        "more_field": "*"
      };

      switch(spendingFilter){
        case SPENDING_FILTER.week:
          data.addAll({"in_date": "7"});
          break;
        case SPENDING_FILTER.month:
          data.addAll({"in_date": "30"});
          break;
        case SPENDING_FILTER.three_month:
          data.addAll({"in_date": "90"});
          break;
        case SPENDING_FILTER.half_year:
          data.addAll({"in_date": "180"});
          break;
      }



      _spendingProvider!.selectSpendingList(page, data).then((response){
        if(response!.length == 0){
          hasMore = false;
        }
        refreshController.loadComplete();
      });

    }else{
      refreshController.loadComplete();
    }
  }

  Widget spendingItemWidget(Spending spending){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 18),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: ColorTheme.grayLevel1,
            width: 1
          )
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              Text("${Date.date(spending.createdAt.toString())}", style: TextStyleTheme.darkLightS14W400,),
              SizedBox(height: 4,),
              Text("${spending.schedule!.scheduleName}", style: TextStyleTheme.darkDarkS16W400,)
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text("${spending.company!.category!.categoryName}", style: TextStyleTheme.darkLightS14W400,),
              SizedBox(height: 4,),
              Text("-${f.format(spending.spendingMoney)}원", style: TextStyleTheme.darkDarkS16W400,)
            ],
          ),
        ],
      ),
    );
  }


}
