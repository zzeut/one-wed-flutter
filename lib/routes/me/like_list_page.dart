import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/me/like_list_component.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/category_model.dart';
import 'package:onewed_app/providers/category_provider.dart';
import 'package:onewed_app/providers/company_like_provider.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:provider/provider.dart';

class LikeListPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return MultiProvider(
      providers: [
        ChangeNotifierProvider<CompanyLikeProvider>(
          create: (context) => CompanyLikeProvider()
        ),

      ],
      child: View(),
    );

  }

}

class View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState  with SingleTickerProviderStateMixin{
  CategoryProvider? _categoryProvider;
  List<Categories> _categoryList = [];



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);


  }



  @override
  Widget build(BuildContext context) {
    _categoryProvider = Provider.of<CategoryProvider>(context);
    _categoryList = _categoryProvider!.getCategoryList();



    return DefaultTabController(
      length: _categoryList.length,
      child: Scaffold(
        appBar: AppBar(

          title:Text('찜', style: TextStyle( color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold,),),
          leading: GestureDetector(
            onTap: () => Navigator.of(context).pop(),
            child: Image.asset(iconArrowLeft),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(60),
            child: Column(
              children: [
                Container(
                  height: 1,
                  color: Color(0xffECECEC),
                ),
                TabBar(
                  isScrollable: true,
                  indicatorColor: ColorTheme.primaryColor,
                  indicator: UnderlineTabIndicator(
                    borderSide: BorderSide(width: 4,color: ColorTheme.primaryColor),
                    insets: EdgeInsets.symmetric(horizontal:16.0),
                  ),
                  labelColor: ColorTheme.primaryColor,
                  unselectedLabelColor: Colors.black,
                  labelStyle: TextStyle(
                      fontWeight: FontWeight.w700
                  ),
                  unselectedLabelStyle: TextStyle(
                      fontWeight: FontWeight.w400
                  ),

                  tabs: _categoryList.map((element) => Container(
                    height: 58,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("${element.categoryName}", style: TextStyle(fontSize: 18,),)
                      ],
                    ),
                  ),).toList()
                ),

                Container(
                  height: 1,
                  color: Color(0xffECECEC),
                ),
              ],
            ),
          ),
        ),
        body: TabBarView(
          children: _categoryList.map((element) => LikeListComponent(category: element)).toList(),
        ),
      )
    );
  }

}
