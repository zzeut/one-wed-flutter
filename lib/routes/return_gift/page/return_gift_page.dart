import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/company/return_gift_component.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/category_model.dart';
import 'package:onewed_app/providers/category_provider.dart';
import 'package:onewed_app/providers/company_provider.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:provider/provider.dart';

class ReturnGiftPage extends StatelessWidget {
  static const route = '';

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CategoryProvider>(create: (context) => CategoryProvider(), child: _View(),);
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  int _index = 0;
  String _order = 'rl';

  CompanyProvider? _companyProvider;
  CategoryProvider? _categoryProvider;
  Categories? category;


  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);


    //답레품 카테고리 가져오기
    _categoryProvider!.selectCategory({
        "type": "2",
        "parent_category": "true",
        "more_field":"*"
    });
  }

  @override
  Widget build(BuildContext context) {
    _companyProvider = Provider.of<CompanyProvider>(context);

    _categoryProvider = Provider.of<CategoryProvider>(context);
    category = _categoryProvider!.getCategory();

    if(category==null){
      return Container();
    }

    return DefaultTabController(
      length: category!.childCategories!.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            '답례품',
            style: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.bold,),
          ),
          leading: GestureDetector(
            onTap: () => Navigator.of(context).pop(),
            child: Image.asset(iconArrowLeft),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(60),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TabBar(
                  indicatorColor: ColorTheme.primaryColor,
                  indicator: UnderlineTabIndicator(
                    borderSide: BorderSide(width: 4 ,color: ColorTheme.primaryColor),
                    insets: EdgeInsets.symmetric(horizontal:16.0),
                  ),
                  labelColor: ColorTheme.primaryColor,
                  unselectedLabelColor: Colors.black,
                  labelStyle: TextStyle(
                      fontWeight: FontWeight.w700
                  ),
                  unselectedLabelStyle: TextStyle(
                      fontWeight: FontWeight.w400
                  ),
                  isScrollable: true,
                  tabs: category!.childCategories!.map((e) =>
                      Container(
                        height: 58,
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("${e.categoryName}", style: TextStyle(
                              fontSize: 18,
                            ),)
                          ],
                        ),
                      )
                  ).toList(),
                ),
                Container(
                  height: 1,
                  color: Color(0xffECECEC),
                )
              ],
            ),
          ),
        ),
        body: TabBarView(
          children: category!.childCategories!.map((e) => ReturnGiftComponent(categoryNo: e.categoryNo!.toInt())).toList(),
        ),
      ),
    );
  }
}
