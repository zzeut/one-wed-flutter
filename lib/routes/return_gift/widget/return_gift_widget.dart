import 'package:flutter/material.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/return_gift_model.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/utils/common.dart';
import 'package:onewed_app/widgets/image.dart';
import 'package:onewed_app/widgets/size.dart';

class ReturnGiftTypeTitle extends StatelessWidget {
  final Function() onTap;
  final String title;
  final bool isActive;

  const ReturnGiftTypeTitle({
    Key? key,
    required this.onTap,
    required this.title,
    required this.isActive,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Center(
        child: Container(
          margin: EdgeInsets.only(left: 16),
          child: Text(
            '$title',
            style: TextStyle(
              color: isActive ? ColorTheme.primaryColor : Color(0xffA7A7A7),
              fontSize: 16,
              fontWeight: isActive ? FontWeight.bold : FontWeight.normal,
            ),
          ),
        ),
      ),
    );
  }
}

class ReturnGiftCard extends StatelessWidget {
  // TODO
  final Function() onTap;
  final Function() onTapHeart;
  final ReturnGift returnGift;

  const ReturnGiftCard({
    Key? key,
    required this.onTap,
    required this.onTapHeart,
    required this.returnGift,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: ((customWidth(context) - 42) / 2).floorToDouble(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            onTap: onTap,
            child: Stack(
              children: [
                CustomImageCard(
                  width: ((customWidth(context) - 42) / 2).floorToDouble(),
                  height: ((customWidth(context) - 42) / 2).floorToDouble(),
                  imageUrl: '${returnGift.returnGiftPhotos![0].imageUrl}',
                  borderRadius: BorderRadius.circular(12),
                ),
                Positioned(
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: Container(
                    height: 48,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(12),
                        bottomRight: Radius.circular(12),
                      ),
                      gradient: LinearGradient(
                        colors: [
                          Color(0xff131313).withOpacity(0.55),
                          Color(0xff929292).withOpacity(0),
                        ],
                        begin: Alignment.bottomCenter,
                        end: Alignment.topCenter,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 9,
                  right: 8,
                  child: GestureDetector(
                    onTap: onTapHeart,
                    child: Image.asset(
                      iconHeartBig,
                      color:
                          returnGift.isLike! ? ColorTheme.primaryColor : null,
                    ),
                  ),
                ),
                Positioned(
                  left: 14,
                  right: 8,
                  bottom: 8,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Image.asset(
                            iconStarSmall,
                            color: Color(0xffFFDB7E),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 6),
                            child: Text(
                              '${returnGift.rating}',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Image.asset(iconComment),
                          Container(
                            margin: EdgeInsets.only(left: 6),
                            child: Text(
                              '${customNumber(returnGift.returnGiftComments!.length.toDouble())}',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 8),
            child: Text(
              '${returnGift.address}',
              style: TextStyle(
                color: Color(0xff707070),
                fontSize: 14,
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 4),
            child: Text(
              '${returnGift.company}/${returnGift.product}',
              style: TextStyle(
                color: Color(0xff303030),
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }
}
