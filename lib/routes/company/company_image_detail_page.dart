import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/company_image_model.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/widgets/page_indicator.dart';
import 'package:photo_view/photo_view.dart';

class CompanyImageDetailPage extends StatelessWidget{
  List<CompanyImage> companyImageList;
  int? index;
  CompanyImageDetailPage({Key? key, required this.companyImageList, this.index}) :super(key: key);


  @override
  Widget build(BuildContext context) {
    return View(this.companyImageList, this.index);
  }

}

class View extends BaseRoute {
  List<CompanyImage> companyImageList;
  int? index;
  View(this.companyImageList, this.index);

  @override
  _ViewState createState() => _ViewState(this.companyImageList, this.index);
}

class _ViewState extends BaseRouteState {
  List<CompanyImage> companyImageList;
  int? index;
  _ViewState(this.companyImageList, this.index);

  PageController pageController = new PageController();


  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    if(this.index != null){
      pageController.jumpToPage(this.index!);
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return PlatformScaffold(
      backgroundColor: Colors.black,
      body: SafeArea(

        child: Column(
          children: [
            Expanded(
              child:PageView(
                controller: pageController,
                children: companyImageList.map((e) => companyImageItemWidget(e)).toList(),
              ),
            ),

            Container(
              padding: EdgeInsets.symmetric(horizontal: 28),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: ColorTheme.darkDark
              ),
              height: 80,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(onPressed: (){
                    this.navigator!.popRoute(null);
                  },
                  icon: Image.asset("assets/icons/3.0x/icon_cancel.png", width: 24,))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget companyImageItemWidget(CompanyImage companyImage){
    return Stack(
      alignment: Alignment.center,
      children: [
        CustomContainer(
          width: double.infinity,
          child: PhotoView(
            imageProvider: NetworkImage(
              "${companyImage.src!.o}",
            ),
            // imageProvider: Image.network("${companyImage.src!.o}", fit: BoxFit.fitWidth,),
          ),
        ),
       /* Positioned(
            bottom: 20,
            child: Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(6))
              ),
              child: Center(
                child: Text("${companyImage.companyImageCategory!.categoryName}", style: TextStyleTheme.darkBlackS16W400,),
              ),
            )
        )*/

      ],
    );
  }
}
