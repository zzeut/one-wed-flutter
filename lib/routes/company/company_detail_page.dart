import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/company/company_image_component.dart';
import 'package:onewed_app/components/company/company_infomation_component.dart';
import 'package:onewed_app/components/company/company_review_list_component.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/models/company_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/broadcast_provider.dart';
import 'package:onewed_app/providers/company_like_provider.dart';
import 'package:onewed_app/providers/company_provider.dart';
import 'package:onewed_app/providers/match_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/chat/chat_detail_page.dart';
import 'package:onewed_app/routes/chat/chat_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/style_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/utils/dynamic_link.dart';
import 'package:onewed_app/widgets/page_indicator.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

class CompanyDetailPage extends StatelessWidget{
  int companyNo;


  CompanyDetailPage({
    Key? key,
    required this.companyNo,
  }) :super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return MultiProvider(
        providers: [
          ChangeNotifierProvider<CompanyLikeProvider>(
            create: (context) => CompanyLikeProvider()
          ),
          ChangeNotifierProvider<MatchProvider>(
              create: (context) => MatchProvider()
          ),
          ChangeNotifierProvider<BroadCastProvider>(
              create: (context) => BroadCastProvider()
          ),
        ],
      child: _View(this.companyNo),
    );

    return ChangeNotifierProvider<CompanyLikeProvider>(
      create: (context) => CompanyLikeProvider(),
      child: _View(this.companyNo),
    );

  }

}

class _View extends BaseRoute {
  int companyNo;
  _View(this.companyNo,);

  @override
  __ViewState createState() => __ViewState(this.companyNo);
}

class __ViewState extends BaseRouteState with SingleTickerProviderStateMixin{
  int companyNo;
  __ViewState(this.companyNo);

  CompanyProvider? _companyProvider;
  Company? company;

  ScrollController _scrollController = new ScrollController();
  PageController _pageController = new PageController(initialPage: 0);

  CompanyLikeProvider? _companyLikeProvider;

  bool isScroll = false;

  UserProvider? _userProvider;
  User? me;

  MatchProvider? _matchProvider;
  BroadCastProvider? _broadCastProvider;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _scrollController.addListener((){

      if(_scrollController.position.pixels > 250){
        setState(() {
          isScroll = true;
        });
      }else{
        setState(() {
          isScroll = false;
        });
      }

    });

  }

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    _companyProvider!.selectCompanyDetail(companyNo.toInt()).then((value){

      setState(() {
        company = value;
      });
    });

  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _companyProvider!.setCompanyDetail(null);
  }


  @override
  Widget build(BuildContext context) {
    _companyProvider = Provider.of<CompanyProvider>(context);
    _companyLikeProvider = Provider.of<CompanyLikeProvider>(context);

    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();

    _matchProvider = Provider.of<MatchProvider>(context);
    _broadCastProvider = Provider.of<BroadCastProvider>(context);

    if(company==null){
      indicator.show();
      return Container();
    }else{
      indicator.hide();
    }

    return DefaultTabController(
        length: company!.category!.type != 2 ? 3 : 2,
        child: Scaffold(
            body: Stack(
              alignment: Alignment.bottomRight,
              children: [
                NestedScrollView(
                  controller: _scrollController,
                  headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                    return [
                      SliverAppBar(
                        collapsedHeight: 60,
                        expandedHeight: MediaQuery.of(context).size.width - 61,
                        floating: false,
                        pinned: true,
                        title: Center(
                          child: Text(
                            "${company!.companyName}",
                            style: isScroll ? TextStyleTheme.blackS18W700 : TextStyleTheme.whiteS18W700,
                          ),
                        ),
                        leading: CustomContainer(
                          onPressed: (){
                            this.navigator!.popRoute(null);
                          },
                          child: isScroll ? Image.asset("assets/icons/icon_arrow_back_gray.png") : Image.asset("assets/icons/icon_arrow_back_white.png"),
                        ),
                        flexibleSpace: FlexibleSpaceBar(
                          centerTitle: true,
                          background: Container(
                            margin: EdgeInsets.only(bottom: 60),
                            child: Stack(
                              children: [
                                PageView(
                                  controller: _pageController,
                                  children: company!.companyCarouselImage!.map((e) => Container(
                                    height: MediaQuery.of(context).size.width - 61,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: NetworkImage(e.src!.o),
                                            fit: BoxFit.fill
                                        )
                                    ),
                                  )).toList(),
                                ),
                                Positioned(
                                  left: 0,
                                  right: 0,
                                  bottom: 8,
                                  child: CustomPageIndicator(
                                    pageController: _pageController,
                                    count: company!.companyCarouselImage!.length == 0 ? 1 : company!.companyCarouselImage!.length,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        actions: [
                          CustomContainer(
                            onPressed: () async{
                              // shareBottomSheet();
                              // DynamicLink().buildDynamicLink('company?company_no=$companyNo');

                              // print(Uri.base.path);
                              String link  = await DynamicLink().buildDynamicLink('company?company_no=$companyNo');
                              Share.share(link);
                              // Share.share("웨딩정보를 공유합니다. ${DynamicLink().buildDynamicLink('company?company_no=$companyNo')}");
                            },
                            child: isScroll ? Image.asset("assets/icons/icon_share_gray.png") : Image.asset("assets/icons/icon_share_white.png"),
                          ),

                          CustomContainer(
                            onPressed: (){
                              _companyLikeProvider!.companyLike(company!.companyNo!.toInt()).then((response){

                                if(response == null || response.deletedAt != null){
                                  company!.companyLikeExists = false;
                                }else{
                                  company!.companyLikeExists = true;
                                }

                                if(company!.category!.parentCategoryNo != null){
                                  _companyProvider!.companyListUpdate(company!.category!.parentCategoryNo!.toInt(), company!);
                                }else{
                                  _companyProvider!.companyListUpdate(company!.categoryNo!.toInt(), company!);
                                }


                                _companyProvider!.searchCompanyListUpdate(company!);
                                setState(() {

                                });

                              });
                            },
                            child: company!.companyLikeExists == true ? Image.asset("assets/icons/icon_haert_fill_40.png"):Image.asset("assets/icons/icon_heart_40.png"),
                          )
                        ],
                        bottom: PreferredSize(
                          preferredSize: Size.fromHeight(60),
                          child:  Container(
                            decoration: BoxDecoration(
                                color: Colors.white
                            ),
                            child: Consumer<CompanyProvider>(builder: (context, companyModel, child){
                              company = companyModel.getCompany();
                              return Column(
                                children: [
                                  TabBar(

                                    indicatorColor: ColorTheme.primaryColor,
                                    indicator: UnderlineTabIndicator(
                                      borderSide: BorderSide(width: 2,color: ColorTheme.primaryColor),
                                      insets: EdgeInsets.symmetric(horizontal:16.0),
                                    ),
                                    labelColor: ColorTheme.primaryColor,
                                    unselectedLabelColor: Colors.black,
                                    labelStyle: TextStyle(
                                        fontWeight: FontWeight.w700
                                    ),
                                    unselectedLabelStyle: TextStyle(
                                        fontWeight: FontWeight.w400
                                    ),
                                    tabs: [
                                      Container(
                                        height: 58,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Text("정보", style: TextStyle(
                                              fontSize: 18,
                                            ),)
                                          ],
                                        ),
                                      ),

                                      Container(
                                        height: 58,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Text("이미지", style: TextStyle(
                                              fontSize: 18,
                                            ),)
                                          ],
                                        ),
                                      ),
                                      if(company!.category!.type != 2)
                                        Container(
                                          height: 58,
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Text("리뷰(${company!.reviewCount})", style: TextStyle(
                                                fontSize: 18,
                                              ),)
                                            ],
                                          ),
                                        ),
                                    ],
                                  ),
                                  Container(
                                    height: 1,
                                    color: Color(0xffECECEC),
                                  ),
                                ],
                              );
                            }),
                          ),
                        ),


                      ),

                    ];
                  },
                  body: TabBarView(
                    children: [
                      CompanyInformationComponent(
                        company: company!,
                        matching: (){
                          scheduleInquiry();
                        },
                        me: me ?? User(),
                      ),
                      CompanyImageComponent(
                        company: company!,
                        matching: (){
                          scheduleInquiry();
                        },
                      ),

                      if(company!.category!.type != 2)
                        CompanyReviewListComponent(companyNo: companyNo),
                    ],
                  ),
                ),
              ],
            )
        )
    );

  }



  void scheduleInquiry(){

    Map<String, String> data = {
      "user_no": "${me!.userNo}",
      "status": "1"
    };
    _matchProvider!.selectSelectMating(data).then((value){
      if(value != null && value.isNotEmpty){
        this.navigator!.pushRoute(ChatPage(isBack: true,));
      }else{
        print(123412342134);
        _matchProvider!.insertMatching(me!.userNo!.toInt(), null).then((response){
          BotToast.showText(text: "담당 플래너가 컨택중입니다. 최대 2-3일 정도소요됩니다.");
        }).catchError((onError){
          BotToast.showText(text: "담당 플래너가 컨택중입니다. 최대 2-3일 정도소요됩니다.");
        });
      }
    });

/*
    _matchProvider!.checkCompanyMatching(me!.userNo!.toInt(), company!.companyNo!.toInt()).then((response){


      _broadCastProvider!.selectChat("${response!.threadId}").then((res){
        if(res!=null){
          this.navigator!.pushRoute(ChatDetailPage(res.threadId!.toString(), res.users![0].name ?? ''));
        }else{
          BotToast.showText(text: "담당 플래너가 컨택중입니다. 최대 2-3일 정도소요됩니다.");
        }
      });

    }).catchError((onError){
      print('check matching error :: ');
      print(onError);
      // 매칭 정보 추가
      addMatching();

    });*/
  }

  void addMatching(){
    _matchProvider!.insertMatching(me!.userNo!.toInt(), company!.planner != null ? company!.planner!.userNo : null).then((response){
      BotToast.showText(text: "스케줄 문의 신청이 완료되었습니다. 최대 2-3일 정도소요됩니다.");
    }).catchError((e){
      BotToast.showText(text: "담당 플래너가 컨택중입니다. 최대 2-3일 정도소요됩니다.");
    });
  }


}
