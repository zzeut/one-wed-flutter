import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/company/company_component.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/category_model.dart';
import 'package:onewed_app/providers/category_provider.dart';
import 'package:onewed_app/routes/search/search_result_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:provider/provider.dart';

class CompanyPage extends StatelessWidget{
  int? index;
  CompanyPage({Key? key, this.index}):super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View(this.index);
  }

}

class _View extends BaseRoute {
  int? index;
  _View(this.index);

  @override
  __ViewState createState() => __ViewState(this.index);
}

class __ViewState extends BaseRouteState  with SingleTickerProviderStateMixin{
  int? index;
  __ViewState(this.index);


  CategoryProvider? _categoryProvider;
  List<Categories> _categoryList = [];

  TextEditingController textEditingController = new TextEditingController();

  TabController? tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    tabController = new TabController(vsync: this, length: _categoryList.length);

  }

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);



    if(this.index != null){

      print("index index index :::::: ");
      print(index);
      DefaultTabController.of(context)!.animateTo(this.index!);
    }

  }

  @override
  Widget build(BuildContext context) {
    _categoryProvider = Provider.of<CategoryProvider>(context);
    _categoryList = _categoryProvider!.getTypeCategoryList(1);

    if(_categoryList == null){
      return PlatformScaffold(
          body: CustomLoading(
            margin: EdgeInsets.zero,
          )
      );
    }


    return DefaultTabController(
      length: _categoryList.length,
      initialIndex: this.index ?? 0,
      child: Scaffold(
          appBar: AppBar(
            toolbarHeight: 61,
            title: Row(
              children: [
                Expanded(
                  child: Container(
                    height: 60,
                    child: TextField(
                      controller: textEditingController,
                      onChanged: (value){
                        setState(() {

                        });
                      },
                      onSubmitted: (value){
                        this.navigator!.pushRoute(SearchResultPage(keyword: value));
                      },
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(12)),
                              borderSide: new BorderSide(
                                  width: 1,
                                  color: ColorTheme.darkDarkBlack
                              )
                          ),
                          hintText: '웨딩 검색어를 입력해주세요',
                          contentPadding: EdgeInsets.only(left: 16, right: 20, top: 20, bottom: 20),
                          suffixIcon: textEditingController.text.isEmpty ?  CustomContainer(
                            onPressed: (){

                            },
                            margin: EdgeInsets.only(right: 5),
                            child: Image(
                              image: AssetImage('assets/icons/icon-textfield_search.png'),
                            ),
                          ) : CustomContainer(
                            onPressed: (){
                              textEditingController.clear();
                              setState(() {

                              });
                            },
                            margin: EdgeInsets.only(right: 5),
                            child: Image(
                              image: AssetImage('assets/icons/icon_cancel_darkgray_32.png'),
                            ),
                          ),
                          enabledBorder:  new OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(12)),
                              borderSide: new BorderSide(
                                  width: 1,
                                  color: ColorTheme.darkDarkBlack
                              )
                          ),
                          focusedBorder:  new OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(12)),
                              borderSide: new BorderSide(
                                  width: 1,
                                  color: ColorTheme.darkDarkBlack
                              )
                          )
                      ),
                    ),
                  ),
                )
              ],
            ),
            leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Image.asset(iconArrowLeft),
            ),
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(60),
              child: Column(
                children: [
                  TabBar(
                    indicatorColor: ColorTheme.primaryColor,
                    indicator: UnderlineTabIndicator(
                      borderSide: BorderSide(width: 2,color: ColorTheme.primaryColor),
                      insets: EdgeInsets.symmetric(horizontal:16.0),
                    ),
                    labelColor: ColorTheme.primaryColor,
                    unselectedLabelColor: Colors.black,
                    labelStyle: TextStyle(
                        fontWeight: FontWeight.w700
                    ),
                    unselectedLabelStyle: TextStyle(
                        fontWeight: FontWeight.w400
                    ),
                    isScrollable: true,
                    tabs: _categoryList.map((e) =>
                        Container(
                          height: 58,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("${e.categoryName}", style: TextStyle(
                                fontSize: 18,
                              ),)
                            ],
                          ),
                        )
                    ).toList(),
                  ),

                  Container(
                    height: 1,
                    color: Color(0xffECECEC),
                  ),
                ],
              ),
            ),
          ),
          body: SafeArea(
            bottom: false,
            child: TabBarView(
              children: _categoryList.map((e) => CompanyComponent(category: e)).toList(),
            ),
          )
      ),
    );

  }
}
