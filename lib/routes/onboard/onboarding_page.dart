import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/on_boarding_model.dart';
import 'package:onewed_app/routes/splash/splash_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/widgets/page_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnboardingPage extends StatelessWidget{
  Function() setFirst;
  List<onboarding> onboardingList;
  OnboardingPage({Key? key, required this.setFirst, required this.onboardingList}):super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return View(this.setFirst, this.onboardingList);
  }

}

class View extends BaseRoute {
  Function() setFirst;
  List<onboarding> onboardingList;
  View(this.setFirst, this.onboardingList);

  @override
  _ViewState createState() => _ViewState(this.setFirst, this.onboardingList);
}

class _ViewState extends BaseRouteState {
  Function() setFirst;
  List<onboarding> onboardingList;
  _ViewState(this.setFirst, this.onboardingList);




  PageController pageController = new PageController();
  int _page = 0;


  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);
    
  }
  
  @override
  Widget build(BuildContext context) {
    if(onboardingList.isEmpty){
      return Container();
    }

    return PlatformScaffold(
        body: SafeArea(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  /*decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black,
                      width: 1
                    )
                  ),*/
                  height: 396,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          flex: 1,
                          child: PageView(
                            onPageChanged: (page){
                              setState(() {
                                _page = page;
                              });
                            },
                            controller: pageController,
                            children: onboardingList.map((e) => onboardingWidget(e)).toList(),
                          )
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 30,),
                CustomPageIndicator(
                  pageController: pageController,
                  count: onboardingList.length,
                  dotColor: ColorTheme.grayLevel1,
                ),
                SizedBox(height: 30,),
                _page == 0 ?
                CustomContainer(
                  onPressed: (){
                    this.setFirst();
                    this.navigator!.replaceRoute(SplashPage());
                  },
                  width: double.infinity,
                  height: 60,
                  child: Center(
                    child: Text("건너뛰기", style: TextStyleTheme.darkGrayS16W400,),
                  ),
                ) :
                CustomContainer(
                  onPressed: (){
                    pageController.animateToPage(_page-1, duration: Duration(milliseconds: 300), curve: Curves.easeInOut);
                  },
                  width: double.infinity,
                  height: 60,
                  child: Center(
                    child: Text("이전", style: TextStyleTheme.darkGrayS16W400,),
                  ),
                ),
                SizedBox(height: 16,),

                _page == onboardingList.length-1 ?
                CustomContainer(
                  onPressed: (){
                    this.setFirst();
                    this.navigator!.replaceRoute(SplashPage());
                  },
                  width: double.infinity,
                  height: 60,
                  borderRadius: [12, 12, 12, 12],
                  backgroundColor: ColorTheme.primaryColor,
                  child: Center(
                    child: Text("원웨드 시작하기", style: TextStyleTheme.whiteS16W700,),
                  ),
                ):
                CustomContainer(
                  onPressed: (){
                    pageController.animateToPage(_page+1, duration: Duration(milliseconds: 300), curve: Curves.easeInOut);
                  },
                  width: double.infinity,
                  height: 60,
                  borderRadius: [12, 12, 12, 12],
                  backgroundColor: ColorTheme.primaryColor,
                  child: Center(
                    child: Text("다음", style: TextStyleTheme.whiteS16W700,),
                  ),
                ),
              ],
            ),
          ),
        )
    );
  }

  Widget onboardingWidget(onboarding onboarding){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 300,
          padding: EdgeInsets.symmetric(horizontal: 20),
          width: double.infinity,
          child: Image.network("${onboarding.imageUrl}",fit: BoxFit.fill,),
        ),
        SizedBox(height: 20,),
        Text("${onboarding.title}", style: TextStyleTheme.blackS18W700,),
        SizedBox(height: 4,),
        Text("${onboarding.content!.toString().replaceAll('\\n', '\n')}", style: TextStyleTheme.darkDarkS16W400,),
      ],
    );
  }

}
