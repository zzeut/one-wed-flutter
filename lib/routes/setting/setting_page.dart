import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/dateTime.dart';
import 'package:onewed_app/components/me/area_picker_component.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/area_code_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/area_code_provider.dart';
import 'package:onewed_app/providers/push_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/me/withdrawal_page.dart';
import 'package:onewed_app/routes/setting/certification_webview_page.dart';
import 'package:onewed_app/routes/sign/page/sign_in_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingPage extends StatelessWidget{
  static const route = '/setting';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<AreaCodeProvider>(create: (context) => AreaCodeProvider()),
          ChangeNotifierProvider<PushProvider>(create: (context) => PushProvider())
        ],
      child: _View(),
    );

  }

}

class _View extends BaseRoute {
  @override
  __ViewState createState() => __ViewState();
}

class __ViewState extends BaseRouteState {
  UserProvider? _userProvider;
  User? me;

  AreaCode? areaCode;
  AreaCode? _areaCode;

  DateTime? weddingCeremonyDate;
  bool? isMarketingPush;
  bool? isSchedulePush;



  ImagePicker picker = new ImagePicker();
  File? file;

  AreaCodeProvider? _areaCodeProvider;
  List<AreaCode> _areaCodeList = [];

  PushProvider? _pushProvider;

  TextEditingController emailTextEditingController = new TextEditingController();

  TextEditingController firstPhoneEditController = new TextEditingController();
  TextEditingController secondPhoneEditController = new TextEditingController();
  TextEditingController lastPhoneEditController = new TextEditingController();


  TextEditingController nameEditController = new TextEditingController();



  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    modifyMe();
  }


  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);


    if(me!.weddingCeremonyYear != null){
      weddingCeremonyDate = DateFormat('yyyy-MM-dd').parse("${me!.weddingCeremonyYear}-${me!.weddingCeremonyMonth}-${me!.weddingCeremonyDate}");
    }

    if(me!.areaCode != null){
      areaCode = me!.areaCode;
      _areaCode = me!.areaCode;
    }

    _areaCodeProvider!.selectAreaCodeList({
      "region_2depth_name": "",
      "region_3depth_name": ""
    });

    if(me!.isMarketingPush != null){
      isMarketingPush = me!.isMarketingPush==true;
    }
    if(me!.isSchedulePush != null){
      isSchedulePush = me!.isSchedulePush==true;
    }

    emailTextEditingController.text = me!.email ?? '';

    if(me!.name != null){
      nameEditController.text = "${me!.name}";
    }

    if(me!.firstPhone != null){
      firstPhoneEditController.text = "${me!.firstPhone}";
    }
    if(me!.secondPhone != null){
      secondPhoneEditController.text = "${me!.secondPhone}";
    }
    if(me!.lastPhone != null){
      lastPhoneEditController.text = "${me!.lastPhone}";
    }

    setState(() {

    });

  }

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();

    _areaCodeProvider = Provider.of<AreaCodeProvider>(context);
    _areaCodeList = _areaCodeProvider!.getAreaCodeList();

    _pushProvider = Provider.of<PushProvider>(context);


    return PlatformScaffold(
      title: '프로필',
      onTap: (){
        this.modifyMe();
      },
      body: SafeArea(
        bottom: false,
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: 20),
              height: 120,
              width: 120,
              child: Center(
                child: CustomContainer(
                  onPressed: (){
                    imagePicker();
                  },
                  child: Stack(
                    alignment: Alignment.bottomRight,
                    children: [
                      Container(
                        height: 120,
                        width: 120,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: profileWidget(),
                        ),
                      ),
                      Positioned(
                          child: Container(
                            width: 48,
                            height: 48,
                            margin: EdgeInsets.only(right: 8, bottom: 8),
                            decoration: BoxDecoration(
                              color: ColorTheme.primaryColor,
                              borderRadius: BorderRadius.all(Radius.circular(12)),
                            ),
                            child: Image(image: AssetImage("assets/icons/icon-camera.png"),width: 24,height: 27,),
                          )
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("이름", style: TextStyleTheme.darkBlackS16W700,),
                  SizedBox(height: 4,),
                  TextField(
                    controller: nameEditController,
                    style: TextStyleTheme.blackS16W400,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            borderSide: new BorderSide(
                                width: 1,
                                color: ColorTheme.darkDarkBlack
                            )
                        ),

                        contentPadding: EdgeInsets.only(left: 16, right: 20, top: 20, bottom: 20),
                        enabledBorder:  new OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            borderSide: new BorderSide(
                                width: 1,
                                color: ColorTheme.darkDarkBlack
                            )
                        ),
                        focusedBorder:  new OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            borderSide: new BorderSide(
                                width: 1,
                                color: ColorTheme.darkDarkBlack
                            )
                        )
                    ),
                  ),
                  SizedBox(height: 20,),
                  Text("이메일", style: TextStyleTheme.darkBlackS16W700,),
                  SizedBox(height: 4,),
                  TextFormField(
                    controller: emailTextEditingController,
                    style: TextStyleTheme.blackS16W400,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          borderSide: new BorderSide(
                              width: 1,
                              color: ColorTheme.darkDarkBlack
                          )
                      ),
                      contentPadding: EdgeInsets.only(left: 16, right: 20, top: 20, bottom: 20),
                      enabledBorder:  new OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          borderSide: new BorderSide(
                              width: 1,
                              color: ColorTheme.darkDarkBlack
                          )
                      ),
                      focusedBorder:  new OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          borderSide: new BorderSide(
                              width: 1,
                              color: ColorTheme.darkDarkBlack
                          )
                      ),
                      hintText: "이메일을 입력해주세요",
                      hintStyle: TextStyleTheme.darkLightS16W400,

                    ),
                  ),
                  SizedBox(height: 20,),
                  Text("휴대폰번호", style: TextStyleTheme.darkBlackS16W700,),
                  SizedBox(height: 4,),
                  Row(
                    children: [
                      Expanded(
                        child: TextFormField(
                          controller: firstPhoneEditController,
                          style: TextStyleTheme.blackS16W400,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: ColorTheme.darkDarkBlack
                                  )
                              ),
                              /*filled: true,
                          fillColor: ColorTheme.grayLevel1,*/
                              contentPadding: EdgeInsets.only(left: 16, right: 20, top: 20, bottom: 20),
                              enabledBorder:  new OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: ColorTheme.darkDarkBlack
                                  )
                              ),
                              focusedBorder:  new OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: ColorTheme.darkDarkBlack
                                  )
                              )
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 6, right: 6),
                        width: 4,
                        height: 1,
                        decoration: BoxDecoration(
                          color: Colors.black
                        ),
                      ),
                      Expanded(
                        child: TextFormField(
                          style: TextStyleTheme.blackS16W400,
                          controller: secondPhoneEditController,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: ColorTheme.darkDarkBlack
                                  )
                              ),
                              /*filled: true,
                        fillColor: ColorTheme.grayLevel1,*/
                              contentPadding: EdgeInsets.only(left: 16, right: 20, top: 20, bottom: 20),
                              enabledBorder:  new OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: ColorTheme.darkDarkBlack
                                  )
                              ),
                              focusedBorder:  new OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: Color.fromRGBO(167, 167, 167, 1.0)
                                  )
                              )
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 6, right: 6),
                        width: 4,
                        height: 1,
                        decoration: BoxDecoration(
                            color: Colors.black
                        ),
                      ),
                      Expanded(
                        child: TextFormField(
                          controller: lastPhoneEditController,
                          style: TextStyleTheme.blackS16W400,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: ColorTheme.darkDarkBlack
                                  )
                              ),
                              /*filled: true,
                        fillColor: ColorTheme.grayLevel1,*/
                              contentPadding: EdgeInsets.only(left: 16, right: 20, top: 20, bottom: 20),
                              enabledBorder:  new OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: ColorTheme.darkDarkBlack
                                  )
                              ),
                              focusedBorder:  new OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: Color.fromRGBO(167, 167, 167, 1.0)
                                  )
                              )
                          ),
                        ),
                      ),
                    ],
                  ),
                  /*Row(
                    children: [
                      Expanded(
                        child: TextFormField(
                          initialValue: me!.firstPhone != null ? "${me!.firstPhone}-${me!.secondPhone}-${me!.lastPhone}" : '',
                          readOnly: true,
                          style: TextStyleTheme.darkLightS16W400,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: Color.fromRGBO(167, 167, 167, 1.0)
                                  )
                              ),
                              filled: true,
                              fillColor: ColorTheme.grayLevel1,
                              contentPadding: EdgeInsets.only(left: 16, right: 20, top: 20, bottom: 20),
                              enabledBorder:  new OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: Color.fromRGBO(167, 167, 167, 1.0)
                                  )
                              ),
                              focusedBorder:  new OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: Color.fromRGBO(167, 167, 167, 1.0)
                                  )
                              )
                          ),
                        ),
                      ),
                      SizedBox(width: 10,),
                      CustomContainer(
                        onPressed: (){

                          this.navigator!.pushRoute(CertificationWebViewPage()).then((value){
                            _userProvider!.selectMe('*').then((value){
                              setState(() {
                                me = value;
                              });
                            });
                          });
                        },
                        backgroundColor: ColorTheme.primaryColor,
                        height: 60,
                        width: 80,
                        borderRadius: [12, 12, 12, 12],
                        child: Align(
                          alignment: Alignment.center,
                          child: Text("재인증", style: TextStyleTheme.whiteS16W700,),
                        ),
                      )
                    ],
                  ),*/
/*                  if(me!.firstPhone != null)
                    Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            initialValue: me!.firstPhone != null ? "${me!.firstPhone}-${me!.secondPhone}-${me!.lastPhone}" : '',
                            readOnly: true,
                            style: TextStyleTheme.darkLightS16W400,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(12)),
                                    borderSide: new BorderSide(
                                        width: 1,
                                        color: Color.fromRGBO(167, 167, 167, 1.0)
                                    )
                                ),
                                filled: true,
                                fillColor: ColorTheme.grayLevel1,
                                contentPadding: EdgeInsets.only(left: 16, right: 20, top: 20, bottom: 20),
                                enabledBorder:  new OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(12)),
                                    borderSide: new BorderSide(
                                        width: 1,
                                        color: Color.fromRGBO(167, 167, 167, 1.0)
                                    )
                                ),
                                focusedBorder:  new OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(12)),
                                    borderSide: new BorderSide(
                                        width: 1,
                                        color: Color.fromRGBO(167, 167, 167, 1.0)
                                    )
                                )
                            ),
                          ),
                        ),
                        SizedBox(width: 10,),
                        CustomContainer(
                          onPressed: (){

                            this.navigator!.pushRoute(CertificationWebViewPage()).then((value){
                              _userProvider!.selectMe('*').then((value){
                                setState(() {
                                  me = value;
                                });
                              });
                            });
                          },
                          backgroundColor: ColorTheme.primaryColor,
                          height: 60,
                          width: 80,
                          borderRadius: [12, 12, 12, 12],
                          child: Align(
                            alignment: Alignment.center,
                            child: Text("재인증", style: TextStyleTheme.whiteS16W700,),
                          ),
                        )
                      ],
                    )
                  else
                    InkWell(
                      onTap: (){
                        this.navigator!.pushRoute(CertificationWebViewPage()).then((value){
                          _userProvider!.selectMe('*').then((value){
                            setState(() {
                              me = value;
                            });
                          });
                        });
                      },
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                          color: ColorTheme.primaryColor,
                          borderRadius: BorderRadius.all(Radius.circular(12))
                        ),
                        child: Center(
                          child: Text(
                            "휴대폰 인증하기",
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.w700
                            ),
                          ),
                        ),
                      ),
                    ),*/
                  SizedBox(height: 20,),
                  if(me!.level == "1")
                  Column(
                    children: [
                      Text("예식일", style: TextStyleTheme.darkBlackS16W700,),
                      SizedBox(height: 4,),
                      CustomContainer(
                        onPressed: (){
                          datePicker();
                        },
                        height: 60,
                        borderRadius: [12, 12, 12, 12],
                        borderColor: ColorTheme.darkDarkBlack,
                        borderWidth: 1,
                        padding: EdgeInsets.only(left: 16),
                        child: Row(
                          children: [
                            Expanded(
                              child: weddingCeremonyDate == null ?
                              Text('예식일을 선택해 주세요', style: TextStyleTheme.darkLightS16W400,) : Text("${Date.weddingCeremonyDate(weddingCeremonyDate!.toString())}"),
                            ),
                            Image(image: AssetImage("assets/icons/icon_arrow_down_m.png"), width: 60, height: 60,)
                          ],
                        ),
                      ),
                      SizedBox(height: 20,),

                      Text("예식지역", style: TextStyleTheme.darkBlackS16W700,),
                      SizedBox(height: 4,),

                      CustomContainer(
                        onPressed: (){
                          areaPicker();
                        },
                        height: 60,
                        borderRadius: [12, 12, 12, 12],
                        borderColor: ColorTheme.darkDarkBlack,
                        borderWidth: 1,
                        padding: EdgeInsets.only(left: 16),
                        child: Row(
                          children: [
                            Expanded(
                              child: _areaCode == null ?
                              Text('예식 지역을 선택해 주세요', style: TextStyleTheme.darkLightS16W400,) : Text("${_areaCode!.areaName}"),
                            ),
                            Image(image: AssetImage("assets/icons/icon_arrow_down_m.png"), width: 60, height: 60,)
                          ],
                        ),
                      ),
                      SizedBox(height: 20,),
                    ],
                  ),
                  Text("${me!.level == 1 ? '신랑신부' : "성별"}", style: TextStyleTheme.darkBlackS16W700,),
                  SizedBox(height: 4,),
                  Row(
                    children: [
                      Expanded(
                        child: CustomContainer(
                          onPressed: (){
                            if(me!.gender == "1"){
                              me!.gender = null;
                            }else{
                              me!.gender = "1";
                            }
                            setState(() {

                            });
                          },
                          height: 60,
                          borderRadius: [12, 12, 12, 12],
                          backgroundColor: me!.gender == "1" ? ColorTheme.primaryColor : ColorTheme.grayLevel1,
                          child: Center(
                            child: Text("${me!.level == 1 ? '신랑' : "남성"}", style: me!.gender == "1" ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkLightS16W700),
                          ),
                        )
                      ),
                      SizedBox(width: 10,),
                      Expanded(
                          child: CustomContainer(
                            onPressed: (){
                              if(me!.gender == "2"){
                                me!.gender = null;
                              }else{
                                me!.gender = "2";
                              }
                              setState(() {

                              });

                            },
                            height: 60,
                            borderRadius: [12, 12, 12, 12],
                            backgroundColor: me!.gender == "2" ? ColorTheme.primaryColor : ColorTheme.grayLevel1,
                            child: Center(
                              child: Text("${me!.level == 1 ? '신부' : "여성"}", style: me!.gender == "2" ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkLightS16W700),
                            ),
                          )
                      )
                    ],
                  ),
                  SizedBox(height: 20,),
                ],
              ),
            ),

            Container(
              height: 8,
              decoration: BoxDecoration(
                  color: ColorTheme.grayLevel1
              ),
            ),
            SizedBox(height: 20,),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Text("알림설정", style: TextStyleTheme.darkDarkS16W700,),
            ),
            SizedBox(height: 10,),
            Container(
              padding: EdgeInsets.only(left: 16, right: 16, bottom: 20, top: 20),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 1,color: ColorTheme.grayLevel1)
                )
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Text("마케팅 PUSH 알림", style: TextStyleTheme.darkDarkS16W400,)
                  ),
                  CupertinoSwitch(
                    activeColor: ColorTheme.primaryColor,
                    trackColor: ColorTheme.grayLevel1,
                    value: isMarketingPush == true,
                    onChanged: (value){
                      setState(() {
                        isMarketingPush = value;
                      });
                    }
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 16, right: 16, bottom: 20, top: 20),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 1,color: ColorTheme.grayLevel1)
                )
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Text("일정 PUSH 알림", style: TextStyleTheme.darkDarkS16W400,)
                  ),
                  CupertinoSwitch(
                    activeColor: ColorTheme.primaryColor,
                    trackColor: ColorTheme.grayLevel1,
                    value: isSchedulePush == true,
                    onChanged: (value){
                      setState(() {
                        isSchedulePush = value;
                      });
                    }
                  )
                ],
              ),
            ),

            Container(
              height: 8,
              decoration: BoxDecoration(
                  color: ColorTheme.grayLevel1
              ),
            ),

            GestureDetector(
              onTap: (){
                showLogoutAlert();
              },
              child: Container(
                padding: EdgeInsets.only(left: 16, right: 25, bottom: 20, top: 20),
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(width: 1,color: ColorTheme.grayLevel1)
                    )
                ),
                child: Row(
                  children: [
                    Expanded(
                        child: Text("로그아웃", style: TextStyleTheme.darkDarkS16W400,)
                    ),
                    Image(image: AssetImage("assets/icons/gray-right-arrow.png"), width: 10,height: 20, fit: BoxFit.cover,)
                  ],
                ),
              ),
            ),
           GestureDetector(
             onTap: (){
               this.navigator!.pushRoute(WithdrawalPage());
             },
             child:  Container(
               padding: EdgeInsets.only(left: 16, right: 25, bottom: 20, top: 20),
               decoration: BoxDecoration(
                   border: Border(
                       bottom: BorderSide(width: 1,color: ColorTheme.grayLevel1)
                   )
               ),
               child: Row(
                 children: [
                   Expanded(
                       child: Text("회원탈퇴", style: TextStyleTheme.darkDarkS16W400,)
                   ),
                   Image(image: AssetImage("assets/icons/gray-right-arrow.png"), width: 10,height: 20, fit: BoxFit.cover,)
                 ],
               ),
             ),
           ),

          ],
        ),
      )
    );
  }

  Widget profileWidget(){
    if(file != null){
      return Image.file(file!, fit: BoxFit.cover,);
    }

    if(me!.profilePath == null){
      if(me!.gender == "1"){
        return Image(
            image: AssetImage("assets/images/profile_man.png")
        );
      }else if(me!.gender == "2"){
        return Image(
            image: AssetImage("assets/images/profile_woman.png")
        );
      }
    }else{
      return Image.network(me!.profilePath!.o, fit: BoxFit.cover,);
    }


    return Image(
        image: AssetImage("assets/images/profile_default.png")
    );
  }

  datePicker(){

    DateTime? dateTime;

    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          insetPadding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          contentPadding:
          EdgeInsets.only(left: 19, right: 17, top: 13, bottom: 22),
          content: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(

              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("예식일", style: TextStyleTheme.darkBlackS18W700,),

                SizedBox(height: 20,),
                Container(
                  height: 190,
                  child: CupertinoDatePicker(
                    initialDateTime: weddingCeremonyDate ?? DateTime.now(),
                    onDateTimeChanged: (DateTime newdate) {
                      dateTime = newdate;
                    },
                    use24hFormat: true,
                    minuteInterval: 1,
                    mode: CupertinoDatePickerMode.date,
                  ),
                ),
                SizedBox(height: 40,),
                CustomContainer(
                  onPressed: (){

                    if(dateTime!=null){
                      weddingCeremonyDate = dateTime;
                    }
                    setState(() {

                    });

                    Navigator.of(context).pop();
                  },
                  backgroundColor: ColorTheme.primaryColor,
                  height: 60,
                  borderRadius: [16, 16, 16, 16],
                  child: Center(
                    child: Text("선택 완료", style: TextStyleTheme.whiteS16W700,),
                  ),
                )
              ],
            ),
          ),

        );
      },
    );
  }

  void imagePicker() async{
    XFile? image = await picker.pickImage(source: ImageSource.gallery);

    if(image!=null){
      file = File(image.path);
      setState(() {

      });
    }
  }



  areaPicker(){

    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          insetPadding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          contentPadding: EdgeInsets.only(left: 19, right: 17, top: 13, bottom: 22),
          content: StatefulBuilder(
            builder: (BuildContext context, StateSetter dialogSetState){
              return Container(
                width: MediaQuery.of(context).size.width,
                child: Column(

                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Stack(
                      alignment: Alignment.center,
                      children: [
                        Center(
                          child: Text("지역선택", style: TextStyleTheme.darkBlackS18W700,),
                        ),
                        Positioned(
                            right: 0,
                            top: 0,
                            child: CustomContainer(
                              onPressed: (){
                                areaCode = _areaCode;
                                Navigator.of(context).pop();
                              },
                              child: Image.asset("assets/icons/icon_cancel_gy.png"),
                            )
                        )
                      ],
                    ),


                    SizedBox(height: 20,),
                    Expanded(
                        child: GridView.count(
                          crossAxisCount: 2,
                          childAspectRatio: ((MediaQuery.of(context).size.width - 42) / 2) / 60,

                          crossAxisSpacing: 10,
                          mainAxisSpacing: 10,
                          children: areaWidgetList(_areaCodeList, (AreaCode? value){
                            dialogSetState(() => areaCode = value);
                          }),
                        )
                    ),
                    SizedBox(height: 20,),
                    CustomContainer(
                      onPressed: (){
                        setState((){
                          _areaCode = areaCode;
                        });
                        Navigator.of(context).pop();
                      },
                      backgroundColor: ColorTheme.primaryColor,
                      height: 60,
                      borderRadius: [16, 16, 16, 16],
                      child: Center(
                        child: Text("선택 완료", style: TextStyleTheme.whiteS16W700,),
                      ),
                    )
                  ],
                ),
              );
            },
          ),

        );
      },
    ).then((value) {
      if(value == null){
        areaCode = _areaCode;
      }
    });
  }

  List<Widget> areaWidgetList(List<AreaCode> areaCodeList, Function(AreaCode?) onPressed){

    List<Widget> list = [];

    list.add(CustomContainer(
      onPressed: (){
        onPressed(null);
      },
      height: 60,
      borderWidth: 1,
      borderColor: areaCode == null ? ColorTheme.primaryColor : ColorTheme.darkDark,
      borderRadius: [6, 6, 6, 6],
      child: Center(
        child: Text("전체", style: areaCode == null ?  TextStyleTheme.primaryS16W700 : TextStyleTheme.darkDarkS16W400,),
      ),
    ));

    areaCodeList.forEach((element) => list.add(CustomContainer(
      onPressed: (){
        onPressed(element);
      },
      height: 60,
      borderWidth: 1,
      borderColor: areaCode == element ? ColorTheme.primaryColor : ColorTheme.darkDark,
      borderRadius: [6, 6, 6, 6],
      child: Center(
        child: Text("${element.areaName}", style: areaCode == element ?  TextStyleTheme.primaryS16W700 : TextStyleTheme.darkDarkS16W400,),
      ),
    )));


    return list;
  }

  void modifyMe(){


    if( emailTextEditingController.text.isNotEmpty &&!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(emailTextEditingController.text)){
      BotToast.showText(text: "이메일 형식이 올바르지않습니다");
    }else{

      Map map = {
        "is_marketing_push": "${isMarketingPush == true ? 1 : 0}",
        "is_schedule_push": "${isSchedulePush == true ? 1 : 0}",
        "_method":"put",
        "gender": "${me!.gender ?? ''}"
      };

      if(emailTextEditingController.text.isNotEmpty){
        map.addAll({"email": "${emailTextEditingController.text}",});
      }

      if(weddingCeremonyDate!=null){
        map.addAll({
          "wedding_ceremony_year": "${weddingCeremonyDate!.year}",
          "wedding_ceremony_month": "${weddingCeremonyDate!.month}",
          "wedding_ceremony_date": "${weddingCeremonyDate!.day}",
        });
      }

      if(_areaCode != null){
        map.addAll({"area_code":"${ _areaCode!.code}"});
      }

      if(nameEditController.text.trim().isNotEmpty){
        map['name'] = nameEditController.text.trim();
      }

      if(firstPhoneEditController.text.trim().isNotEmpty && secondPhoneEditController.text.trim().isNotEmpty  && lastPhoneEditController.text.trim().isNotEmpty ){
        map['first_phone'] = firstPhoneEditController.text.trim();
        map['second_phone'] = secondPhoneEditController.text.trim();
        map['last_phone'] = lastPhoneEditController.text.trim();
      }

      _userProvider!.modifyMe(map, file).then((value){
        _userProvider!.selectMe('*').then((value){
          this.navigator!.popRoute(null);
        });
      });
    }

  }

  void showLogoutAlert(){

    showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          return AlertDialog(
            insetPadding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
            clipBehavior: Clip.antiAliasWithSaveLayer,
            contentPadding: EdgeInsets.only(top: 30, left: 16, right: 16, bottom: 16),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            content: CustomContainer(
              width: MediaQuery.of(context).size.width,
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("로그아웃 하시겠습니까?", style: TextStyleTheme.blackS18W500,),
                    SizedBox(height: 4,),
                    Text("소셜로그인으로 언제든지  로그인하실 수 있습니다.", style: TextStyleTheme.darkDarkS14W400,),
                    SizedBox(height: 20,),
                    Row(
                      children: [
                        Expanded(child: CustomContainer(
                          onPressed: (){
                            Navigator.pop(context);
                          },
                          height: 60,
                          backgroundColor: ColorTheme.primaryColor,
                          borderRadius: [6, 6, 6, 6],
                          child: Center(
                            child: Text("계속 사용할게요", style: TextStyleTheme.whiteS16W700,),

                          ),
                        )),
                        SizedBox(width: 10,),
                        Expanded(child: CustomContainer(
                          onPressed: (){
                            logout();
                          },
                          height: 60,
                          borderWidth: 1,
                          borderColor: ColorTheme.darkDark,
                          borderRadius: [6, 6, 6, 6],
                          child: Center(
                            child: Text("로그아웃", style: TextStyleTheme.darkDarkS16W400,),
                          ),
                        ))
                      ],
                    )
                  ]),
            ),
          );
        }
    );
  }

  void logout() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('accessToken');
    prefs.remove('refreshToken');

    // 유저 push token 삭제
    _pushProvider!.deleteUserFcmToken();

    this.navigator!.popToRootRoute();
    this.navigator!.resetRoute((SignInPage()));
  }


}
