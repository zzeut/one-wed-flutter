import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/platform_bottom_tab_bar.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/broadcast_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/chat/chat_detail_page.dart';
import 'package:onewed_app/routes/chat/chat_page.dart';
import 'package:onewed_app/routes/company/company_detail_page.dart';
import 'package:onewed_app/routes/event/page/event_detail_page.dart';
import 'package:onewed_app/routes/home/page/home_page.dart';
import 'package:onewed_app/routes/inquiry/inquiry_list_page.dart';
import 'package:onewed_app/routes/me/my_page.dart';
import 'package:onewed_app/routes/review/review_detail_page.dart';
import 'package:onewed_app/routes/schedule/schedule_detail_page.dart';
import 'package:onewed_app/routes/search/search_page.dart';
import 'package:onewed_app/routes/schedule/schedule_main_page.dart';
import 'package:flutter/foundation.dart';
import 'package:onewed_app/routes/setting/setting_page.dart';
import 'dart:convert';

import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';


class TabPage extends StatelessWidget {
  static const String route = '';
  int? currentIndex;
  TabPage({Key? key, this.currentIndex}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return _View(this.currentIndex);
  }
}

class _View extends BaseRoute {
  int? currentIndex;
  _View(this.currentIndex);

  _ViewState createState() => _ViewState(this.currentIndex);
}

class _ViewState extends BaseRouteState {
  int? currentIndex;
  _ViewState(this.currentIndex);

  bool _isProfile = false;

  UserProvider? _userProvider;
  User? me;


  BroadCastProvider? _broadCastProvider;

  /// Create a [AndroidNotificationChannel] for heads up notifications
  late AndroidNotificationChannel channel;

  /// Initialize the [FlutterLocalNotificationsPlugin] package.
  late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  @override
  void initState(){
    // TODO: implement initState
    super.initState();
    this.initFcm();
    this.profileWriteAlert();


    Timer timer = new Timer(new Duration(seconds: 1), () {
      initDynamicLinks();
    });


    // with WidgetsBindingObserver와 함께 foreground 동적링크 추가
    WidgetsBinding.instance!.addObserver(this);

  }

  void initFcm() async {
    super.initState();
    FirebaseMessaging.instance.getInitialMessage().then((RemoteMessage? message) {
      if (message != null) {
        selectNotification(json.encode(message.data));
      }
    });
    if (!kIsWeb) {

      channel = const AndroidNotificationChannel(
        'high_importance_channel', // id
        'High Importance Notifications', // title
        description: 'This channel is used for important notifications.', // description
        importance: Importance.high,

      );

      flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();


      const AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('ic_stat_name');
      final IOSInitializationSettings initializationSettingsIOS = IOSInitializationSettings(onDidReceiveLocalNotification: onDidReceiveLocalNotification);
      final InitializationSettings initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
      );
      await flutterLocalNotificationsPlugin.initialize(initializationSettings,
          onSelectNotification: selectNotification);

      /// Create an Android Notification Channel.
      ///
      /// We use this channel in the `AndroidManifest.xml` file to override the
      /// default FCM channel to enable heads up notifications.
      await flutterLocalNotificationsPlugin
          .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannel(channel);

      /// Update the iOS foreground notification presentation options to allow
      /// heads up notifications.
      await FirebaseMessaging.instance
          .setForegroundNotificationPresentationOptions(
        alert: true,
        badge: false,
        sound: true,
      );
    }

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print('Got a message whilst in the foreground!');
      print('Message data: ${message.data}');

      _broadCastProvider!.selectChatList(1);

      if (message.notification != null) {
        print('Message also contained a notification: ${message.notification}');
      }

      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null && !kIsWeb) {
        flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channelDescription: channel.description,
              // TODO add a proper drawable resource to android, for now using
              //      one that already exists in example app.
              icon: 'ic_stat_name',
              color: ColorTheme.primaryColor
            ),
            iOS: IOSNotificationDetails(

            )
          ),
          payload: json.encode(message.data),

        );
      }

    });

    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print('A new onMessageOpenedApp event was published!');
      selectNotification(json.encode(message.data));

    });
  }

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();


    _broadCastProvider = Provider.of<BroadCastProvider>(context, listen: false);

    return PlatformScaffold(
      onWillPop: this.onWillPop,
      body: PlatformBottomTabBar(
        children: [
          // 홈
          HomePage(),
          // 검색
          SearchPage(),
          // 웨딩챗
          ChatPage(),
          // 스케쥴
          ScheduleMainPage(),
          // 마이페이지
          MyPage(),
        ],
        pathList: [
          // TODO
        ],
        currentIndex: currentIndex ?? 0,
      ),
    );
  }


  void selectNotification(String? payload) async {
    if (payload != null) {
      Map data = json.decode(payload);
      debugPrint('notification payload: $payload');
      print(data);
      switch (data['click_action']) {
        case "inquiry_list_page":
          this.navigator!.pushRoute(InquiryListPage());
          break;
        case "chat_detail_page":
          this.navigator!.pushRoute(ChatDetailPage(data['thread_id'], data['user_name'] ?? ''));
          break;
        case "schedule_detail_page":
          this.navigator!.pushRoute(ScheduleDetailPage(scheduleNo: int.parse(data['schedule_no'])));
          break;
      }

    }
  }

  void onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) async {
    // display a dialog with the notification details, tap ok to go to another page
  }

  void profileWriteAlert() async{

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? isProfileDate = prefs.getString('isProfileDate');

    if(me!.weddingCeremonyYear == null || me!.areaCode == null){
      if(isProfileDate == null){
        showProfileAlert();
      }else{
        DateTime profileDate = DateFormat("yyyy-MM-dd HH:mm:ss").parse(isProfileDate);
        if(profileDate.difference(DateTime.now()).inDays.abs() > 14){
          showProfileAlert();
        }
      }

    }
  }

  void showProfileAlert() async{
    showDialog(
        barrierDismissible: false,
        context: context, builder: (context) => AlertDialog(

      insetPadding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      contentPadding: EdgeInsets.only(top: 40, left: 16, right: 16, bottom: 16),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0))
      ),

      content: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset("assets/images/profile-alert-image.png", width: 120, height: 60,),
          SizedBox(height: 40,),
          Text("프로필을 작성해주세요!", style: TextStyleTheme.blackS18W500),
          SizedBox(height: 4,),
          Text("아직 프로필이 미완성 되었습니다.\n프로필을 완성시켜서 자세한 웨딩정보를 알아보세요.", style: TextStyleTheme.darkGrayS14W400, textAlign: TextAlign.center,),
          SizedBox(height: 20,),
          CustomContainer(
            onPressed: (){
              setProfileDate();
              Navigator.pop(context);
            },
            height: 60,
            child: Center(
              child: Text("나중에", style: TextStyleTheme.darkGrayS16W400,),
            ),
          ),
          SizedBox(height: 16,),
          CustomContainer(
            onPressed: (){
              setProfileDate();
              Navigator.pop(context);
              this.navigator!.pushRoute(SettingPage());
            },
            borderRadius: [6, 6, 6, 6],
            backgroundColor: ColorTheme.primaryColor,
            height: 60,
            child: Center(
              child: Text("작성할게요", style: TextStyleTheme.whiteS16W700,),
            ),
          )
        ],
      ),

    ));
  }


  void setProfileDate() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('isProfileDate', "${DateTime.now()}");
  }



  void initDynamicLinks() async {
    final PendingDynamicLinkData? data = await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri deepLink = data!.link;

    if (deepLink != null && deepLink.queryParameters != null) {
      switch (deepLink.path){
        case "/company":
          var companyNo = deepLink.queryParameters['company_no'];
          this.navigator!.pushRoute(CompanyDetailPage(companyNo: int.parse(companyNo.toString())));
          break;
        case "/event":
          var eventNo = deepLink.queryParameters['event_no'];
          print("eventNo ::::::: $eventNo");
          this.navigator!.pushRoute(EventDetailPage(eventNo: int.parse(eventNo.toString()))).catchError((onError){
            print("on Error :::::: ");
            print(onError);
          });
          break;
        case "/company/review":
          var companyReviewNo = deepLink.queryParameters['company_review_no'];
          this.navigator!.pushRoute(ReviewDetailPage(companyReviewNo: int.parse(companyReviewNo.toString())));
      }
    }


    FirebaseDynamicLinks.instance.onLink(
      onSuccess: (linkData) async {
        final Uri? deepLink = linkData?.link;

        print("deep link :::::::");
        print(deepLink!.path);

        switch (deepLink.path){
          case "/company":
            var companyNo = deepLink.queryParameters['company_no'];
            this.navigator!.pushRoute(CompanyDetailPage(companyNo: int.parse(companyNo.toString())));
            break;
          case "/event":
            var eventNo = deepLink.queryParameters['event_no'];
            print("eventNo ::::::: $eventNo");
            this.navigator!.pushRoute(EventDetailPage(eventNo: int.parse(eventNo.toString()))).catchError((onError){
              print("on Error :::::: ");
              print(onError);
            });
            break;
          case "/company/review":
            var companyReviewNo = deepLink.queryParameters['company_review_no'];
            this.navigator!.pushRoute(ReviewDetailPage(companyReviewNo: int.parse(companyReviewNo.toString())));
        }
        
      },
      onError: (error)  async {
        print('onLinkError');
        print(error.message);
      },
    );

/*

    final Uri deepLink = data!.link;

    print(deepLink);

    if (deepLink != null) {
      // do something
    }*/
  }

}
