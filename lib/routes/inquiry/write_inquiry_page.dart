import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/inquiry/call_inquiry_component.dart';
import 'package:onewed_app/components/inquiry/write_inquiry_component.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/themes/color_theme.dart';

class WriteInquiryPage extends StatelessWidget{
  int?  initialIndex;
  WriteInquiryPage({Key? key, this.initialIndex}):super(key: key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View(this.initialIndex);
  }
}

class _View extends BaseRoute{
  int?  initialIndex;
  _View(this.initialIndex);

  @override
  __ViewState createState() => __ViewState(this.initialIndex);
}

class __ViewState extends BaseRouteState  with SingleTickerProviderStateMixin{
  int?  initialIndex;
  __ViewState(this.initialIndex);

  TabController? _tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(initialIndex);
    _tabController = new TabController(length: 2, vsync: this, initialIndex: initialIndex ?? 0);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text('문의하기', style: TextStyle(
            color: Colors.black,
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        leading: GestureDetector(
          onTap: () => Navigator.of(context).pop(),
          child: Image.asset(iconArrowLeft),
        ),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: Column(
            children: [
              Container(height: 1, color: Color(0xffECECEC),),
              TabBar(
                controller: _tabController,
                  indicatorColor: ColorTheme.primaryColor,
                  indicator: UnderlineTabIndicator(
                    borderSide: BorderSide(width: 4,color: ColorTheme.primaryColor),
                    insets: EdgeInsets.symmetric(horizontal:16.0),
                  ),
                  labelColor: ColorTheme.primaryColor,
                  unselectedLabelColor: Colors.black,
                  labelStyle: TextStyle(
                      fontWeight: FontWeight.w700
                  ),
                  unselectedLabelStyle: TextStyle(
                      fontWeight: FontWeight.w400
                  ),

                  tabs: [
                  Container(
                    height: 58,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("1:1문의", style: TextStyle(
                          fontSize: 18,
                        ),)
                      ],
                    ),
                  ),
                  Container(
                    height: 58,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("전화문의", style: TextStyle(
                          fontSize: 18,
                        ),)
                      ],
                    ),
                  ),

                ]
              ),
              Container(height: 1,color: Color(0xffECECEC),),
            ],
          ),
        ),
      ),
      body: SafeArea(
        child: TabBarView(
          controller: _tabController,
          children: [
            WriteInquiryComponent(),
            CallInquiryComponent(),
          ],
        ),
      )
    );
  }
}
