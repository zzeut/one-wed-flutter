import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/dateTime.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/inquiry_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/inquiry_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/inquiry/edit_inquiry_page.dart';
import 'package:onewed_app/routes/inquiry/write_inquiry_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class InquiryListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return _View();
  }
}

class _View extends BaseRoute {
  @override
  __ViewState createState() => __ViewState();
}

class __ViewState extends BaseRouteState {

  InquiryProvider? _inquiryProvider;
  UserProvider? _userProvider;

  User? me;

  int page = 0;
  bool hasMore = true;



  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);
    onRefresh();
  }

  @override
  Widget build(BuildContext context) {
    _inquiryProvider = Provider.of<InquiryProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);

    me = _userProvider!.getMe();

    List<Inquiry> _inquiryList = [];

    return PlatformScaffold(
        title: "문의 내역",
        body: SafeArea(
          bottom: false,
          child: Consumer<InquiryProvider>(builder: (context, inquiryModel, child){
            _inquiryList = inquiryModel.getInquiryList;

            if(_inquiryList.length == 0){
              return Column(

                children: [
                  Expanded(
                      child:  Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 80,
                            child: Image.asset("assets/images/call_inquiry.png"),
                          ),
                          SizedBox(height: 30,),
                          Text("문의 내역이 없습니다", style: TextStyleTheme.darkDarkS18W700,),
                          SizedBox(height: 4,),
                          Text("문의 사항이 있을 시에는", style: TextStyleTheme.darkGrayS16W400,),
                          Text("전화 문의, 1:1 문의를 이용해주시기 바랍니다.", style: TextStyleTheme.darkGrayS16W400,),
                        ],
                      )
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      children: [
                        Expanded(
                            child: Container(
                              child: CustomContainer(
                                onPressed: (){
                                  this.navigator!.pushRoute(WriteInquiryPage(initialIndex: 1,));
                                },
                                height: 60,
                                borderWidth: 1,
                                borderColor: ColorTheme.primaryColor,
                                borderRadius: [12, 12, 12, 12],
                                child: Center(
                                  child: Text("전화 문의", style: TextStyleTheme.primaryS16W700,),
                                ),
                              ),
                            )
                        ),
                        SizedBox(width: 10,),
                        Expanded(
                            child: Container(
                              child: CustomContainer(
                                onPressed: (){
                                  this.navigator!.pushRoute(WriteInquiryPage(initialIndex: 0,));
                                },
                                height: 60,
                                backgroundColor: ColorTheme.primaryColor,
                                borderRadius: [12, 12, 12, 12],
                                child: Center(
                                  child: Text("1:1 문의", style: TextStyleTheme.whiteS16W700,),
                                ),
                              ),
                            )
                        ),
                      ],
                    ),
                  )


                ],
              );
            }

            List<Widget> list = [];
            _inquiryList.forEach((element) {list.add(inquiryWidget(element));});

            if(hasMore){
              list.add(
                CustomContainer(
                  onPressed: (){
                    onLoading();
                  },
                  margin: EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 16),
                  height: 58,
                  borderWidth: 1,
                  borderColor: ColorTheme.darkDarkBlack,
                  borderRadius: [12, 12, 12, 12],
                  child: Center(
                    child: Text("더보기", style: TextStyleTheme.darkDarkS16W400),
                  ),
                )
              );
            }
            return ListView(
              children: list,
            );
          }),
        ));
  }

  onRefresh(){
    page = 0;
    hasMore = true;
    onLoading();
  }

  onLoading(){
    if(hasMore){
      page++;

      Map<String, String> map = {
        "user_no": "${me!.userNo}",
        "more_field": "*",
        "sort[by]":"created_at",
        "sort[order]":"desc",
        "page": "$page"
      };
      _inquiryProvider!.selectInquiryList(page, map).then((res){
        if(res!.length == 0){
          hasMore = false;
        }
      });
    }
  }

  Widget inquiryWidget(Inquiry inquiry){
    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 16,),
              Align(
                alignment: Alignment.centerLeft,
                child: inquiry.inquiryAnswer == null ?  Text("[답변 대기중]", style: TextStyleTheme.darkLightS14W700,) : Text("[답변 완료]", style: TextStyleTheme.primaryS14W700,),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text("${Date.date(inquiry.createdAt.toString())}", style: TextStyleTheme.darkLightS14W400,),
              ),
              Row(
                children: [
                  Expanded(
                    child: CustomContainer(
                      onPressed: (){
                        inquiry.isOpen = inquiry.isOpen == true ? false : true;
                        _inquiryProvider!.updateInquiryList(inquiry);
                      },
                      child: Text(
                        "${inquiry.content}",
                        style: TextStyleTheme.blackS16W500,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    )
                  ),
                  CustomContainer(
                    onPressed: (){
                      inquiry.isOpen = inquiry.isOpen == true ? false : true;
                      _inquiryProvider!.updateInquiryList(inquiry);
                    },
                    child: RotatedBox(
                      quarterTurns: inquiry.isOpen==true ? 2 : 0,
                      child: Image(image: AssetImage("assets/icons/icon_arrow_down_m.png"), width: 40, height: 40, fit: BoxFit.cover,),
                    )
                  )

                ],
              ),
              SizedBox(height: 16,),
              if(inquiry.isOpen == true)
                inquiryDetailWidget(inquiry)
            ],
          ),
        ),
        Container(
          height: 8,
          decoration: BoxDecoration(
            color: ColorTheme.grayLevel1
          ),
        )
      ],
    );
  }

  Widget inquiryDetailWidget(Inquiry inquiry){

    Widget inquiryBtnWidget = Container(
      margin: EdgeInsets.only(bottom: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          CustomContainer(
            onPressed: (){
              this.navigator!.pushRoute(EditInquiryPage(inquiryNo: inquiry.questionNo!.toInt()));
            },
            height: 48,
            width: 120,
            borderRadius: [12, 12, 12, 12],
            borderWidth: 1,
            borderColor: ColorTheme.darkDarkBlack,
            child: Center(
              child: Text("수정", style: TextStyleTheme.darkDarkS16W400,),
            ),
          ),
          SizedBox(width: 10,),
          CustomContainer(
            onPressed: (){
              deleteInquiryPopup(inquiry);
            },
            height: 48,
            width: 120,
            borderRadius: [12, 12, 12, 12],
            borderWidth: 1,
            borderColor: ColorTheme.darkDarkBlack,
            child: Center(
              child: Text("삭제", style: TextStyleTheme.darkDarkS16W400,),
            ),
          ),
        ],
      ),
    );
    Widget inquiryAnswerWidget = inquiry.inquiryAnswer == null ? Container(): Container(
      padding: EdgeInsets.symmetric(vertical: 16),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            width: 1,
            color: ColorTheme.grayLevel1
          )
        )
      ),
      child: Column(
        children: [
          Align(
              alignment: Alignment.centerLeft,
              child: Text("[답변 내용]", style: TextStyleTheme.blackS14W700,)
          ),
          SizedBox(height: 16,),
          Align(
            alignment: Alignment.centerLeft,
            child: Text("${inquiry.inquiryAnswer!.content}", style: TextStyleTheme.darkDarkS16W400,)
          ),
        ],
      )
    );


    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            width: 69,
            height: 32,
            decoration: BoxDecoration(
                border: Border.all(
                    color: ColorTheme.darkGray,
                    width: 1
                ),
              borderRadius: BorderRadius.all(Radius.circular(20))
            ),
            child: Center(
              child: Text("${inquiry.category!.categoryName}", style: TextStyleTheme.darkGrayS16W400,),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 16),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text("${inquiry.content}", style: TextStyleTheme.darkDarkS16W400,),
          ),
        ),
        if(inquiry.inquiryImage != null)
          Container(
            margin: EdgeInsets.only(bottom: 16),
            height: 160,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: Image.network("${inquiry.inquiryImage!.src!.m}", width: double.infinity, height: 160, fit: BoxFit.fill,),
            ),
          ),
        inquiry.inquiryAnswer == null ?
          inquiryBtnWidget : inquiryAnswerWidget
      ],
    );
  }

  void deleteInquiryPopup(Inquiry inquiry){

    showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          return AlertDialog(
            insetPadding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
            clipBehavior: Clip.antiAliasWithSaveLayer,
            contentPadding: EdgeInsets.only(top: 30, left: 16, right: 16, bottom: 16),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0))),
            content: CustomContainer(
              width: MediaQuery.of(context).size.width,
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("문의를 삭제 하시겠습니끼?", style: TextStyleTheme.blackS18W500,),
                    SizedBox(height: 4,),
                    Text("삭제한 문의는 복구할 수 없습니다.", style: TextStyleTheme.darkDarkS14W400,),
                    SizedBox(height: 20,),
                    Row(
                      children: [
                        Expanded(child: CustomContainer(
                          onPressed: (){
                            Navigator.pop(context);
                          },

                          height: 60,
                          borderWidth: 1,
                          borderColor: ColorTheme.darkDark,
                          borderRadius: [6, 6, 6, 6],
                          child: Center(
                            child: Text("나중에", style: TextStyleTheme.darkDarkS16W400,),
                          ),

                        )),
                        SizedBox(width: 10,),
                        Expanded(child: CustomContainer(
                          onPressed: (){
                            _inquiryProvider!.deleteInquiry(inquiry.questionNo!.toInt()).then((value) {
                              if(value!=null){
                                _inquiryProvider!.deleteUpdateInquiryList(value);
                              }
                              Navigator.pop(context);
                              BotToast.showText(text: "문의가 삭제되었습니다.");
                            });
                          },

                          height: 60,
                          backgroundColor: ColorTheme.primaryColor,
                          borderRadius: [6, 6, 6, 6],
                          child: Center(
                            child: Text("삭제", style: TextStyleTheme.whiteS16W700,),

                          ),
                        ))
                      ],
                    )
                  ]),
            ),
          );
        }
    );
  }

}
