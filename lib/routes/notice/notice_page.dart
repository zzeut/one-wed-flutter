import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/components/dateTime.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/notice_model.dart';
import 'package:onewed_app/providers/notice_provider.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class NoticePage extends StatelessWidget{
  static const route = '/mypage/notice';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ChangeNotifierProvider<NoticeProvider>(
      create: (context) => NoticeProvider(),
      child: _View(),
    );
  }

}
class _View extends BaseRoute {
  @override
  __ViewState createState() => __ViewState();
}

class __ViewState extends BaseRouteState {

  RefreshController refreshController = new RefreshController(initialRefresh: false);

  NoticeProvider? _noticeProvider;
  List<Notice> _noticeList = [];

  int page = 0;
  bool hasMore = true;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    this.onRefresh();
  }

  @override
  Widget build(BuildContext context) {
    _noticeProvider = Provider.of<NoticeProvider>(context);

    return PlatformScaffold(
      title: "공지사항",
      body: SafeArea(
        bottom: false,
        child: RefreshConfiguration(
          child: SmartRefresher(
            controller: refreshController,
            onLoading: this.onLoading,
            onRefresh: this.onRefresh,
            enablePullUp: true,
            footer: CustomSmartRefresher.customFooter(),
            header: CustomSmartRefresher.customHeader(),
            child: ListView(
              children: [
                Consumer<NoticeProvider>(builder: (context, noticeModel, child){
                  _noticeList = noticeModel.noticeList;

                  List<Widget> list = [];
                  _noticeList.forEach((element) {list.add(noticeWidget(element));});
                  return Column(
                    children: list,
                  );

                })
              ],
            ),
          ),
        ),
      )
    );
  }

  onRefresh(){
    page = 0;
    hasMore = true;

    onLoading();
    refreshController.refreshCompleted();
  }

  onLoading(){
    if(hasMore){
      page = page+1;
      _noticeProvider!.selectNotice(page).then((response){
        if(response!.length == 0){
          hasMore=false;
        }
      });

      refreshController.loadComplete();
    }
    refreshController.loadComplete();
  }

  Widget noticeWidget(Notice notice){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.only(left: 16, top: 19, right: 29, bottom: 14),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Text("${Date.date(notice.createdAt.toString())}", style: TextStyleTheme.darkLightS14W400,),
              ),
              SizedBox(height: 5,),
              CustomContainer(
                onPressed: (){
                  if(notice.isOpen == true){
                    notice.isOpen = false;
                  }else{
                    notice.isOpen = true;
                  }
                  _noticeProvider!.updateNoticeList(notice);
                },
                child: Row(
                  children: [
                    Expanded(child: Text("${notice.noticeTitle}", style: TextStyleTheme.blackS16W500, maxLines: 1,)),
                    RotatedBox(
                      quarterTurns: notice.isOpen==true ? 2 : 0,
                      child: Image(image: AssetImage("assets/icons/icon_arrow_down_m.png"), width: 40, height: 40, fit: BoxFit.cover,),
                    )
                  ],
                ),
              ),
              if(notice.isOpen==true)
                noticeContentWidget(notice),
            ],
          ),
        ),
        Container(
          height: 8,
          decoration: BoxDecoration(
              color: ColorTheme.grayLevel1
          ),
        )
      ],
    );
  }

  Widget noticeContentWidget(Notice notice){
    return Column(
      children: [
        SizedBox(height: 14,),
        Html(data: "${notice.noticeContent}"),
        SizedBox(height: 2,)
      ],
    );
  }
}
