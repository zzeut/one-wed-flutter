import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/company/company_item_component.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/const.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/company_model.dart';
import 'package:onewed_app/providers/company_like_provider.dart';
import 'package:onewed_app/providers/company_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/company/company_detail_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/widgets/error.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class SearchResultPage extends StatelessWidget{
  String keyword;
  SearchResultPage({Key? key, required this.keyword}) :super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return ChangeNotifierProvider<CompanyLikeProvider>(
      create: (context) => CompanyLikeProvider(),
      child: View(this.keyword),
    );

  }

}
class View extends BaseRoute {
  String keyword;
  View(this.keyword);


  @override
  _ViewState createState() => _ViewState(this.keyword);
}

class _ViewState extends BaseRouteState {
  String keyword;
  _ViewState(this.keyword);

  COMPANY_SORT companySort = COMPANY_SORT.all;

  int page = 0;
  bool hasMore = true;


  CompanyProvider? _companyProvider;
  List<Company> companyList = [];

  RefreshController refreshController = new RefreshController(initialRefresh: false);
  TextEditingController textEditingController = new TextEditingController();


  CompanyLikeProvider? _companyLikeProvider;
  UserProvider? _userProvider;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    textEditingController.text = this.keyword;
    page = 0;
    hasMore = true;
    this.onLoading();



  }

  @override
  Widget build(BuildContext context) {
    _companyProvider = Provider.of<CompanyProvider>(context);
    _companyLikeProvider = Provider.of<CompanyLikeProvider>(context);

    _userProvider = Provider.of<UserProvider>(context);

    return PlatformScaffold(
      body: SafeArea(
        child: RefreshConfiguration(
          child: SmartRefresher(
            enablePullUp: true,
            enablePullDown: true,
            onRefresh: this.onRefresh,
            onLoading: this.onLoading,
            header: CustomSmartRefresher.customHeader(),
            footer: CustomSmartRefresher.customFooter(),
            controller: refreshController,
            child: CustomScrollView(
              slivers: [
                SliverAppBar(
                  toolbarHeight: 60,
                  title: Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: 60,
                          child: TextField(
                            controller: textEditingController,
                            onChanged: (value){
                              setState(() {

                              });
                            },
                            onSubmitted: (value){
                              onRefresh();
                            },
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(12)),
                                    borderSide: new BorderSide(
                                        width: 1,
                                        color: ColorTheme.darkDarkBlack
                                    )
                                ),
                                hintText: '웨딩 검색어를 입력해주세요',
                                contentPadding: EdgeInsets.only(left: 16, right: 20, top: 20, bottom: 20),
                                suffixIcon: textEditingController.text.isEmpty ?  CustomContainer(
                                  onPressed: (){

                                  },
                                  margin: EdgeInsets.only(right: 5),
                                  child: Image(
                                    image: AssetImage('assets/icons/icon-textfield_search.png'),
                                  ),
                                ) : CustomContainer(
                                  onPressed: (){
                                    textEditingController.clear();
                                    setState(() {

                                    });
                                  },
                                  margin: EdgeInsets.only(right: 5),
                                  child: Image(
                                    image: AssetImage('assets/icons/icon_cancel_darkgray_32.png'),
                                  ),
                                ),
                                enabledBorder:  new OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(12)),
                                    borderSide: new BorderSide(
                                        width: 1,
                                        color: ColorTheme.darkDarkBlack
                                    )
                                ),
                                focusedBorder:  new OutlineInputBorder(
                                    borderRadius: BorderRadius.all(Radius.circular(12)),
                                    borderSide: new BorderSide(
                                        width: 1,
                                        color: ColorTheme.darkDarkBlack
                                    )
                                )
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  leading: GestureDetector(
                    onTap: () => Navigator.of(context).pop(),
                    child: Image.asset(iconArrowLeft),
                  ),
                  bottom: PreferredSize(
                    preferredSize: Size.fromHeight(60),
                    child: Container(
                      margin: EdgeInsets.only(left: 8),
                      height: 48,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                              child: ListView(
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                children:  <Widget>[

                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.all;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("기본순", style: companySort == COMPANY_SORT.all ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.like_desc;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("찜 많은 순", style: companySort == COMPANY_SORT.like_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.like_asc;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("찜 적은 순", style: companySort == COMPANY_SORT.like_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.score_desc;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("별점 높은 순", style: companySort == COMPANY_SORT.score_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.score_asc;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("별점 낮은 순", style: companySort == COMPANY_SORT.score_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                ],
                              )
                          )
                        ],
                      ),
                    ),
                  )
                ),
                SliverPadding(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                  sliver: Consumer<CompanyProvider>(builder: (context, companyModel, child){
                    companyList = companyModel.getSearchCompanyList();
                    if(companyModel.isLoading){
                      return SliverList(delegate: SliverChildListDelegate([
                        CustomLoading(margin: EdgeInsets.only(top: 20))
                      ]));
                    }

                    if(companyModel.isError){
                      return SliverList(delegate: SliverChildListDelegate([
                        ErrorCard(margin: EdgeInsets.only(top: 20))
                      ]));
                    }

                    if(companyList.length == 0){
                      return SliverFillRemaining(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset("assets/images/gray-logo.png", width: 235, height: 60, fit: BoxFit.fill,),
                            SizedBox(height: 30,),
                            Text("해당 검색결과가 없습니다.", style: TextStyleTheme.darkDarkS18W700,),
                            SizedBox(height: 8,),
                            Text("다양한 웨딩 검색어를 입력해주세요", style: TextStyleTheme.darkGrayS16W400,),
                            SizedBox(height: 120,)
                          ],
                        )
                      );
                    }

                    return SliverGrid.count(
                      childAspectRatio: 186/240,
                      crossAxisCount: 2,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 20,
                      children: companyList.map((e) => CompanyItemComponent(
                        company: e,
                        companyLike: (){
                          companyLike(e);
                        },
                        companyDetail: (){
                          this.navigator!.pushRoute(CompanyDetailPage(companyNo: e.companyNo!.toInt()));
                        },

                      )).toList(),
                    );
                  }),
                )
                
              ]
            ),
          ),
        ),
      )
    );
  }

  onRefresh(){
    page = 0;
    hasMore = true;
    onLoading();

    refreshController.refreshCompleted();
  }

  onLoading(){
    if(hasMore){
      page++;

      Map<String, String> map = {
        "page": "$page",
        "more_field": "company_like",
        "company_name": "${textEditingController.text}"
      };

      switch(companySort){
        case COMPANY_SORT.like_desc:
          map['sort[by]'] = "like_count";
          map['sort[order]'] = "desc";
          break;
        case COMPANY_SORT.like_asc:
          map['sort[by]'] = "like_count";
          map['sort[order]'] = "asc";
          break;

        case COMPANY_SORT.score_desc:
          map['sort[by]'] = "review_score";
          map['sort[order]'] = "desc";
          break;
        case COMPANY_SORT.score_asc:
          map['sort[by]'] = "review_score";
          map['sort[order]'] = "asc";
          break;
      }

      _companyProvider!.searchCompany(page, map).then((response){
        if(response!.length ==0 ){
          hasMore = false;
        }
      });

      refreshController.loadComplete();
    }
  }

  void companyLike(Company company){
    _companyLikeProvider!.companyLike(company.companyNo!.toInt()).then((response){
      if(response == null || response.deletedAt != null){
        company.companyLikeExists = false;
      }else{
        company.companyLikeExists = true;
      }

      _userProvider!.selectMe('*');


      if(company.category!.parentCategoryNo != null){
        _companyProvider!.companyListUpdate(company.category!.parentCategoryNo!.toInt(), company);
      }else{
        _companyProvider!.companyListUpdate(company.categoryNo!.toInt(), company);
      }

      _companyProvider!.searchCompanyListUpdate(company);
    });

  }

}
