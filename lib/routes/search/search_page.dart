import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/category_model.dart';
import 'package:onewed_app/models/onewed_television_model.dart';
import 'package:onewed_app/models/recommend_wedding_model.dart';
import 'package:onewed_app/providers/category_provider.dart';
import 'package:onewed_app/providers/onewed_television_provider.dart';
import 'package:onewed_app/providers/recommend_wedding_provider.dart';
import 'package:onewed_app/routes/company/company_detail_page.dart';
import 'package:onewed_app/routes/company/company_page.dart';
import 'package:onewed_app/routes/event/page/event_detail_page.dart';
import 'package:onewed_app/routes/event/page/event_page.dart';
import 'package:onewed_app/routes/search/search_result_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class SearchPage extends StatelessWidget{
  static const route = '/search';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<OnewedTelevisionProvider>(create: (context) => OnewedTelevisionProvider()),
        ChangeNotifierProvider<RecommendWeddingProvider>(create: (context) => RecommendWeddingProvider()),
        // ChangeNotifierProvider<CategoryProvider>(create: (context) => CategoryProvider()),

      ],
      child: _View(),
    );
    return _View();
  }
}

class _View extends BaseRoute {
  @override
  __ViewState createState() => __ViewState();
}

class __ViewState extends BaseRouteState {
  CategoryProvider? _categoryProvider;
  List<Categories> _categoryList = [];

  OnewedTelevisionProvider? _onewedTelevisionProvider;
  OnewedTelevision? _onewedTelevision;

  RecommendWeddingProvider? _recommendWeddingProvider;
  RecommendWedding? _recommendWedding;

  TextEditingController textEditingController = new TextEditingController();

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    _onewedTelevisionProvider!.selectOnewedTelevision();
    _recommendWeddingProvider!.selectRecommendWedding();
  }

  @override
  Widget build(BuildContext context) {
    _categoryProvider = Provider.of<CategoryProvider>(context);
    _categoryList = _categoryProvider!.getTypeCategoryList(1);

    _onewedTelevisionProvider = Provider.of<OnewedTelevisionProvider>(context);
    _onewedTelevision = _onewedTelevisionProvider!.getOnewedTelevision();


    _recommendWeddingProvider = Provider.of<RecommendWeddingProvider>(context);
    _recommendWedding = _recommendWeddingProvider!.getRecommendWedding();

    return PlatformScaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(top: 10, left: 16, right: 16, bottom: 20),
          child: ListView(
            shrinkWrap: true,
            children: [
              Container(
                height: 60,
                child: TextField(
                  controller: textEditingController,
                  onChanged: (value){
                    setState(() {

                    });
                  },
                  onSubmitted: (value){
                    this.navigator!.pushRoute(SearchResultPage(keyword: value));
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          borderSide: new BorderSide(
                              width: 1,
                              color: ColorTheme.darkDarkBlack
                          )
                      ),
                      hintText: '웨딩 검색어를 입력해주세요',
                      contentPadding: EdgeInsets.only(left: 16, right: 20, top: 20, bottom: 20),
                      suffixIcon: textEditingController.text.isEmpty ?  CustomContainer(
                        onPressed: (){

                        },
                        margin: EdgeInsets.only(right: 5),
                        child: Image(
                          image: AssetImage('assets/icons/icon-textfield_search.png'),
                        ),
                      ) : CustomContainer(
                        onPressed: (){
                          textEditingController.clear();
                          setState(() {

                          });
                        },
                        margin: EdgeInsets.only(right: 5),
                        child: Image(
                          image: AssetImage('assets/icons/icon_cancel_darkgray_32.png'),
                        ),
                      ),
                      enabledBorder:  new OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          borderSide: new BorderSide(
                              width: 1,
                              color: ColorTheme.darkDarkBlack
                          )
                      ),
                      focusedBorder:  new OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          borderSide: new BorderSide(
                              width: 1,
                              color: ColorTheme.darkDarkBlack
                          )
                      )
                  ),
                ),
              ),
              SizedBox(height: 30,),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("카테고리", style: TextStyleTheme.primaryS18W700,)
                ],
              ),
              SizedBox(height: 4,),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("카테고리를 통해 쉽게 웨딩정보를 알아보세요.", style: TextStyle(
                      fontSize: 16,
                      color: ColorTheme.darkDarkBlack,
                      fontWeight: FontWeight.w400
                  ),)
                ],
              ),
              SizedBox(height: 20,),
              categoryListWidget(),

              if(_recommendWedding!=null)
                Column(
                  children: [
                    SizedBox(height: 40,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text("추천 웨딩정보", style: TextStyleTheme.primaryS18W700)
                      ],
                    ),
                    SizedBox(height: 4,),
                    Stack(
                      alignment: Alignment.centerRight,
                      children: [
                        CustomContainer(
                          onPressed: (){
                            if(_recommendWedding!.eventNo != null){
                              this.navigator!.pushRoute(EventDetailPage(eventNo: _recommendWedding!.event!.eventNo!.toInt()));
                            }else if(_recommendWedding!.companyNo!=null){
                              this.navigator!.pushRoute(CompanyDetailPage(companyNo: _recommendWedding!.companyNo!));
                            }
                          },
                          width: double.infinity,
                          height: 120,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(12),
                            child: FadeInImage.assetNetwork(
                              placeholder: 'assets/images/skeleton-loader.gif',
                              image: _recommendWedding!.src!.m,
                              fit: BoxFit.fill,
                            ),
                            /*child: Container(
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: NetworkImage("${_recommendWedding!.src!.m}"),
                                  fit: BoxFit.fill
                                )
                            ),
                          ),*/
                          ),

                        ),
                        Container(
                            margin: EdgeInsets.only(right: 15),
                            child: Image(
                              image: AssetImage('assets/icons/white-right-icon-arrow.png'),
                              width: 10,
                              height: 20,
                            )
                        )
                      ],
                    ),
                  ],
                ),
              if(_onewedTelevision!=null)
                Column(
                  children: [
                    SizedBox(height: 40,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text("원웨드 TV", style: TextStyleTheme.primaryS18W700)
                      ],
                    ),
                    SizedBox(height: 4,),
                    CustomContainer(
                      onPressed: () async{
                        await launch(_onewedTelevision!.url.toString());
                      },
                      child: Stack(
                        alignment: Alignment.centerRight,
                        children: [
                          Container(
                            width: double.infinity,
                            height: 120,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(12),
                              child: FadeInImage.assetNetwork(
                                placeholder: 'assets/images/skeleton-loader.gif',
                                image: _onewedTelevision!.src!.m,
                                fit: BoxFit.fill,
                              ),
                            ),

                          ),
                          Container(
                              margin: EdgeInsets.only(right: 15),
                              child: Image(
                                image: AssetImage('assets/icons/white-right-icon-arrow.png'),
                                width: 10,
                                height: 20,
                              )
                          )
                        ],
                      ),
                    ),
                  ],
                )

            ],
          ),
        ),
      )
    );
  }

  Widget categoryListWidget(){
    int columns = 4;

    return StaggeredGridView.countBuilder(
        shrinkWrap: true,
        crossAxisCount: columns,
        crossAxisSpacing: 10,
        mainAxisSpacing: 20,
        itemCount: _categoryList.length,
        itemBuilder: (BuildContext context, int index){
          return GestureDetector(
            onTap: (){
              this.navigator!.pushRoute(CompanyPage(index: index,));
            },
            child: Container(
              // padding: EdgeInsets.only(top: 25, bottom: 8),
              height: 100,
              decoration: BoxDecoration(
                  color: Color.fromRGBO(254, 249, 249, 1.0),
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(149, 78, 78, 0.12),
                      offset: Offset(0, 4.0),
                      blurRadius: 7,
                    )
                  ]
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if(_categoryList[index].src != null)
                    Image(
                      image: NetworkImage(_categoryList[index].src!.o),
                      width: 30,
                      height: 30,
                    ),
                  SizedBox(height: 13,),
                  Text("${_categoryList[index].categoryName}", maxLines: 1, overflow: TextOverflow.ellipsis,)
                ],
              ),
            ),
          );
        },
        staggeredTileBuilder: (int index) {
          return StaggeredTile.fit(1);
        },
      );

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Expanded(child: ListView(
          shrinkWrap: true,
          children: [
            StaggeredGridView.countBuilder(
              shrinkWrap: true,
              crossAxisCount: columns,
              crossAxisSpacing: 10,
              mainAxisSpacing: 20,
              itemCount: _categoryList.length,
              itemBuilder: (BuildContext context, int index){
                return GestureDetector(
                  onTap: (){
                    this.navigator!.pushRoute(CompanyPage(index: index,));
                  },
                  child: Container(
                    // padding: EdgeInsets.only(top: 25, bottom: 8),
                    height: 100,
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(254, 249, 249, 1.0),
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromRGBO(149, 78, 78, 0.12),
                            offset: Offset(0, 4.0),
                            blurRadius: 7,
                          )
                        ]
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        if(_categoryList[index].src != null)
                          Image(
                            image: NetworkImage(_categoryList[index].src!.o),
                            width: 30,
                            height: 30,
                          ),
                        SizedBox(height: 13,),
                        Text("${_categoryList[index].categoryName}", maxLines: 1, overflow: TextOverflow.ellipsis,)
                      ],
                    ),
                  ),
                );
              },
              staggeredTileBuilder: (int index) {
                return StaggeredTile.fit(1);
              },
            )
          ],
        ))
      ],
    );

    return GridView.builder(
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 4,
          mainAxisSpacing: 20,
          crossAxisSpacing: 20,
        ),
        itemCount: _categoryList.length,
        itemBuilder: (context, index){
          return GestureDetector(
            onTap: (){
              this.navigator!.pushRoute(CompanyPage(index: index,));
            },
            child: Container(
              // padding: EdgeInsets.only(top: 25, bottom: 8),
              height: 100,
              decoration: BoxDecoration(
                  color: Color.fromRGBO(254, 249, 249, 1.0),
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(149, 78, 78, 0.12),
                      offset: Offset(0, 4.0),
                      blurRadius: 7,
                    )
                  ]
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if(_categoryList[index].src != null)
                    Image(
                      image: NetworkImage(_categoryList[index].src!.o),
                      width: 30,
                      height: 30,
                    ),
                  SizedBox(height: 13,),
                  Text("${_categoryList[index].categoryName}", maxLines: 1, overflow: TextOverflow.ellipsis,)
                ],
              ),
            ),
          );
        }
    );

    return Container();

  }
}
