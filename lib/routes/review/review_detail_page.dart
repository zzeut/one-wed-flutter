import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/dateTime.dart';
import 'package:onewed_app/components/making_name.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/components/profile_component.dart';
import 'package:onewed_app/components/review/company_review_profile_component.dart';
import 'package:onewed_app/components/review/company_review_start_component.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/company_review_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/company_provider.dart';
import 'package:onewed_app/providers/company_review_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/review/company_review_edit_page.dart';
import 'package:onewed_app/routes/review/review_image_detail_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/utils/dynamic_link.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

class ReviewDetailPage extends StatelessWidget {
  int companyReviewNo;

  ReviewDetailPage({Key? key, required this.companyReviewNo}) :super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return View(this.companyReviewNo);
  }

}

class View extends BaseRoute {
  int companyReviewNo;

  View(this.companyReviewNo);

  @override
  _ViewState createState() => _ViewState(this.companyReviewNo);
}

class _ViewState extends BaseRouteState {
  int companyReviewNo;

  _ViewState(this.companyReviewNo);

  CompanyReviewProvider? _companyReviewProvider;
  CompanyReview? _companyReview;

  UserProvider? _userProvider;
  User? me;

  CompanyProvider? _companyProvider;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);
    _companyReviewProvider!.selectCompanyReview(companyReviewNo);
  }

  @override
  Widget build(BuildContext context) {
    _companyReviewProvider = Provider.of<CompanyReviewProvider>(context);
    _companyReview = _companyReviewProvider!.getCompanyReview();

    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();

    _companyProvider = Provider.of<CompanyProvider>(context);


    if (_companyReviewProvider!.isLoading) {
      return PlatformScaffold(
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomLoading(margin: EdgeInsets.only(top: 20))
            ],
          ),
        ),
      );
    }

    if(_companyReview == null){
      return PlatformScaffold(
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomLoading(margin: EdgeInsets.only(top: 20))
            ],
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "${_companyReview!.company!.companyName}",
          style: TextStyle(
            color: Colors.black,
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        leading: GestureDetector(
          onTap: () => Navigator.of(context).pop(),
          child: Image.asset(iconArrowLeft),
        ),
        actions: [
          CustomContainer(
            onPressed: () async{
              String link  = await DynamicLink().buildDynamicLink('company/review?company_review_no=$companyReviewNo');
              Share.share(link);
            },
            child: Image.asset("assets/icons/icon_share_gray.png"),
          )
        ],
      ),

      body: SafeArea(
        bottom: false,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
          child: ListView(
            children: [
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CompanyReviewProfileComponent(
                        companyReview: _companyReview!),
                    Container(
                        margin: EdgeInsets.only(bottom: 16),
                        padding: EdgeInsets.symmetric(vertical: 16),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: ColorTheme.grayLevel1,
                                    width: 1
                                )
                            )
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(_companyReview!.content!.toString(),
                              style: TextStyleTheme.blackS16W400,),
                            if(_companyReview!.companyReviewImageList!
                                .length > 0)
                              Container(
                                margin: EdgeInsets.only(top: 16),
                                child: reviewImageWidget(_companyReview!),
                              )
                          ],
                        )
                    ),

                    Text("방문날짜 ${Date.scheduleYearMonthDate(
                        _companyReview!.schedule!.scheduleDate!
                            .toString())}",
                      style: TextStyleTheme.darkGrayS14W400,)
                  ]
              ),
              SizedBox(height: 20,),
              if(me!.userNo == _companyReview!.userNo)
                Row(
                  children: [
                    Expanded(
                        child: CustomContainer(
                          onPressed: (){
                            reviewDeleteAlertShow();
                          },
                          borderWidth: 1,
                          borderColor: ColorTheme.darkBlack,
                          height: 60,
                          borderRadius: [12, 12, 12, 12],
                          child: Center(
                            child: Text("삭제", style: TextStyleTheme.darkBlackS16W400,),
                          ),
                        )
                    ),
                    SizedBox(width: 10,),
                    Expanded(
                        child: CustomContainer(
                          onPressed: (){
                            this.navigator!.pushRoute(CompanyReviewEditPage(companyReview: _companyReview!));
                          },
                          borderWidth: 1,
                          borderColor: ColorTheme.darkBlack,
                          height: 60,
                          borderRadius: [12, 12, 12, 12],
                          child: Center(
                            child: Text("수정", style: TextStyleTheme.darkBlackS16W400,),
                          ),
                        )
                    ),
                  ],
                )
            ],
          ),
        ),
      ),
    );
  }


  Widget reviewImageWidget(CompanyReview companyReview) {
    if (companyReview.companyReviewImageList!.length == 1) {
      return CustomContainer(
        onPressed: () {
          this.navigator!.pushRoute(ReviewImageDetailPage(
            reviewImageList: companyReview.companyReviewImageList!,
            index: 0,
          ));
        },
        width: double.infinity,
        height: 160,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(12),
          child: FadeInImage.assetNetwork(
            placeholder: 'assets/images/skeleton-loader.gif',
            image: companyReview.companyReviewImageList![0].src!.o,
            fit: BoxFit.cover,
          ),
        ),
      );
    } else if (companyReview.companyReviewImageList!.length == 2) {
      List<Widget> list = [];

      for(int i=0; i<companyReview.companyReviewImageList!.length; i++){
        list.add(Expanded(
          child: CustomContainer(
            onPressed: () {
              this.navigator!.pushRoute(ReviewImageDetailPage(
                reviewImageList: companyReview.companyReviewImageList!,
                index: i,
              ));
            },
            height: 160,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: FadeInImage.assetNetwork(
                placeholder: 'assets/images/skeleton-loader.gif',
                image: companyReview.companyReviewImageList![i].src!.o,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ));
        
        if(i < companyReview.companyReviewImageList!.length-1 ){
          list.add(SizedBox(width: 10,));
        }
      }
      return Row(
        children: list,
      );
    } else {
      return Container(

        height: 160,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: companyReview.companyReviewImageList!.length,
                  itemBuilder: (BuildContext context, int index) =>
                      CustomContainer(
                          onPressed: () {
                            this.navigator!.pushRoute(ReviewImageDetailPage(
                              reviewImageList: companyReview.companyReviewImageList!,
                              index: index,
                            ));
                          },
                          height: 160,
                          width: 160,
                          margin: EdgeInsets.only(right: index == companyReview.companyReviewImageList!.length? 0 : 10),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(12),
                            child: FadeInImage.assetNetwork(
                              placeholder: 'assets/images/skeleton-loader.gif',
                              image: companyReview.companyReviewImageList![index].src!.o,
                              fit: BoxFit.cover,
                            ),
                          )

                      ),
                ))
          ],
        ),
      );
    }
  }


  reviewDeleteAlertShow() {
    showDialog(
      context: context, builder: (context) =>
        AlertDialog(

            // insetPadding: EdgeInsets.all(10),
            clipBehavior: Clip.antiAliasWithSaveLayer,
            contentPadding: EdgeInsets.only(
                top: 40, left: 16, right: 16, bottom: 16),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0))
            ),

            content: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text("리뷰를 삭제 하시겠습니까?", style: TextStyleTheme.blackS18W500),
                Text("삭제한 리뷰는 복구할 수 없습니다.",
                  style: TextStyleTheme.darkDarkS16W400,),
                SizedBox(height: 20,),
                Row(
                  children: [
                    Expanded(
                      child: CustomContainer(
                        onPressed: (){
                          Navigator.pop(context);
                        },
                        borderWidth: 1,
                        borderColor: ColorTheme.darkBlack,
                        height: 60,
                        borderRadius: [12, 12, 12, 12],
                        child: Center(
                          child: Text("나중에", style: TextStyleTheme.darkBlackS16W400,),
                        )
                      )
                    ),
                    SizedBox(width: 10,),
                    Expanded(
                      child: CustomContainer(
                        onPressed: delReview,
                        borderRadius: [12, 12, 12, 12],
                        backgroundColor: ColorTheme.primaryColor,
                        height: 60,
                        child: Center(
                          child: Text("삭제하기", style: TextStyleTheme.whiteS16W700,),
                        ),
                      )
                    ),
                  ],
                )
              ],)
        ),
    );
  }

  void delReview() async{

    _companyReviewProvider!.deleteCompanyReview(companyReviewNo).then((value){
      if(value!=null){
        BotToast.showText(text: '리뷰가 삭제되었습니다.');
        if(_companyReview!.company!.category!.parentCategoryNo == null){
          _companyReviewProvider!.deleteCategoryCompanyReviewUpdate(_companyReview!.company!.categoryNo!.toInt(), value);
        }else{
          _companyReviewProvider!.deleteCategoryCompanyReviewUpdate(_companyReview!.company!.category!.parentCategoryNo!.toInt(),value);
        }

        _companyReviewProvider!.deleteCompanyReviewUpdate(value);

        if(_companyProvider!.getCompany() != null){
          _companyProvider!.selectCompanyDetail(_companyProvider!.getCompany()!.companyNo!.toInt());
          // _companyReviewProvider!.selectCompanyReviewList(1, {"company_no":"${_companyProvider!.getCompany()!.companyNo}", "more_field": "*"});
        }

        Navigator.pop(context);

        this.navigator!.popRoute(null);
      }
    });
  }
}
