import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/components/dateTime.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/const.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/company_model.dart';
import 'package:onewed_app/models/schedule_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/company_like_provider.dart';
import 'package:onewed_app/providers/company_provider.dart';
import 'package:onewed_app/providers/schdule_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/me/review_list_page.dart';
import 'package:onewed_app/routes/review/review_write_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/widgets/error.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ReviewWriteListPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ChangeNotifierProvider<CompanyLikeProvider>(
      create: (context) => CompanyLikeProvider(),
      child: View()
    ,);
    return View();
  }

}

class View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {

  ScheduleProvider? _scheduleProvider;
  CompanyLikeProvider? _companyLikeProvider;
  CompanyProvider? _companyProvider;

  List<Schedule> _scheduleList = [];

  UserProvider? _userProvider;
  User? me;

  int page = 0;
  bool hasMore = true;

  bool isLike = false;
  SCHEDULE_SORT scheduleSort = SCHEDULE_SORT.created_desc;

  RefreshController refreshController = new RefreshController(initialRefresh: false);

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    onRefresh();
  }

  @override
  Widget build(BuildContext context) {
    _scheduleProvider = Provider.of<ScheduleProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);
    _companyLikeProvider = Provider.of<CompanyLikeProvider>(context);
    _companyProvider = Provider.of<CompanyProvider>(context);

    _scheduleList = _scheduleProvider!.getScheduleList();

    me = _userProvider!.getMe();

    return Scaffold(
      appBar: AppBar(
        title:Text('웨딩리뷰 작성',
          style: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.bold,),
        ),
          leading: GestureDetector(
            onTap: () => Navigator.of(context).pop(),
            child: Image.asset(iconArrowLeft),
          ),

        bottom: PreferredSize(
          preferredSize:Size.fromHeight(59),
          child: Column(
            children: [
              Container(
                height: 1,
                color: Color(0xffECECEC),
              ),
              Column(
                children: [
                  SizedBox(height: 10,),
                  CustomContainer(
                    onPressed: (){
                      isLike = !isLike;
                      onRefresh();
                      setState(() {

                      });
                    },
                    height: 48,
                    padding: EdgeInsets.only(left: 18, right: 8 ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              width: 24,
                              height: 24,
                              decoration: BoxDecoration(
                                  color: isLike ? ColorTheme.primaryColor : ColorTheme.grayLevel1,
                                  shape: BoxShape.circle
                              ),
                              child: Center(
                                child: Image.asset("assets/icons/icon_check.png"),
                              ),
                            ),
                            SizedBox(width: 8,),
                            Text("찜", style: isLike ? TextStyleTheme.darkDarkS16W400 : TextStyleTheme.darkLightS16W400 ,)
                          ],
                        ),
                        Row(
                          children: [

                            CustomContainer(
                              onPressed: (){
                                scheduleSort = SCHEDULE_SORT.created_desc;
                                onRefresh();
                                setState(() {

                                });
                              },
                              padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),

                              child: Text("최근 방문순", style: scheduleSort == SCHEDULE_SORT.created_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400 ,),
                            ),
                            CustomContainer(
                              onPressed: (){
                                scheduleSort = SCHEDULE_SORT.created_asc;
                                onRefresh();
                                setState(() {

                                });
                              },
                              padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),

                              child: Text("오래된 방문순", style: scheduleSort == SCHEDULE_SORT.created_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400 ,),
                            ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              )

            ],
          ),
        ),
      ),

      body: SafeArea(
        child: RefreshConfiguration(
          child: SmartRefresher(
            controller: refreshController,
            onLoading: this.onLoading,
            onRefresh: this.onRefresh,
            enablePullDown: true,
            enablePullUp: true,
            footer: CustomSmartRefresher.customFooter(),
            header: CustomSmartRefresher.customHeader(),
            child: Consumer<ScheduleProvider>(builder: (context, scheduleModel, child){
              _scheduleList = scheduleModel.getReviewScheduleList();

              if(scheduleModel.isLoading){
                return CustomLoading(margin: EdgeInsets.only(top: 20));
              }

              if(scheduleModel.isError){
                return ErrorCard(margin: EdgeInsets.only(top: 20));
              }

              if(_scheduleList.length  == 0){
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("assets/images/review-empty-image.png", width: 74, height: 80, fit: BoxFit.fill,),
                    SizedBox(height: 30,),
                    Text("이용하신 웨딩샵이 없습니다.", style: TextStyleTheme.darkDarkS18W700,),
                    SizedBox(height: 8,),
                    Text("원웨드의 다양한 웨딩샵을 이용하고", style: TextStyleTheme.darkDarkS16W400,),
                    Text("특별한 혜택을 받아보세요.", style: TextStyleTheme.darkDarkS16W400,),

                  ],
                );
              }

              return Padding(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: ListView(
                  children: _scheduleList.map((e) => reviewItemWidget(e)).toList(),
                ),
              );
            }),
          ),
        ),
      )
    );
  }

  onRefresh(){
    page = 0;
    hasMore = true;

    onLoading();
    refreshController.refreshCompleted();
  }

  onLoading(){
    if(hasMore){
      page = page+1;
      Map<String, String> params = {
        "page":"1",
        "user_no": "${me!.userNo}",
        "more_field": "company",
        "state":"done",
        "review_exist": "false",
      };

      switch(scheduleSort){
        case SCHEDULE_SORT.created_asc:
          params.addAll({"sort[by]":"schedule_date", "sort[order]":"asc"});
          break;
        case SCHEDULE_SORT.created_desc:
          params.addAll({"sort[by]":"schedule_date", "sort[order]":"desc"});
          break;
      }

      if(isLike){
        params.addAll({
          "company_like":"true"
        });
      }


      _scheduleProvider!.selectReviewScheduleList(page, params).then((value){
        if(value!.length == 0){
          hasMore = false;
        }
        refreshController.loadComplete();
      });
    }else{
      refreshController.loadComplete();
    }
  }

  Widget reviewItemWidget(Schedule schedule){
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: Row(
        children: [
          Container(
            width: 88,
            height: 88,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: schedule.company!.representativeImageSrc == null ?
              Image.asset("assets/images/default-company.jpeg", fit: BoxFit.cover,)  :
              FadeInImage.assetNetwork(
                  placeholder: 'assets/images/skeleton-loader.gif',
                  image: schedule.company!.representativeImageSrc!.o
              ),
            ),
          ),
          SizedBox(width: 10,),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("${schedule.company!.areaCode!.region1DepthName} ${schedule.company!.areaCode!.region2DepthName}", style: TextStyleTheme.darkGrayS14W400,),
                SizedBox(height: 4,),
                Text("${schedule.company!.companyName}", maxLines: 1, overflow: TextOverflow.ellipsis, style: TextStyleTheme.blackS16W500,),
                SizedBox(height: 4,),
                Text("방문날짜 ${Date.scheduleYearMonthDate(schedule.scheduleDate.toString())}", style: TextStyleTheme.darkGrayS14W400,),

              ],
            )
          ),
          Container(
            height: 88,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                CustomContainer(
                  width: 40,
                  onPressed: (){
                    companyLike(schedule);
                  },
                  child: schedule.company!.companyLikeExists==true ? Image.asset("assets/icons/icon_haert_fill_40.png") : Image.asset("assets/icons/icon_heart_40.png"),
                ),
                CustomContainer(
                  onPressed: (){
                    this.navigator!.pushRoute(ReviewWritePage(schedule: schedule));
                  },
                  borderWidth: 1,
                  borderColor: ColorTheme.primaryColor,
                  width: 88,
                  height: 40,
                  borderRadius: [12, 12, 12, 12],
                  child: Center(
                    child: Text("리뷰쓰기", style: TextStyleTheme.primaryS16W700,),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  companyLike(Schedule schedule){
    indicator.show();
    _companyLikeProvider!.companyLike(schedule.companyNo!.toInt()).then((response) {

      if(response == null || response.deletedAt != null){
        schedule.company!.companyLikeExists = false;
      }else{
        schedule.company!.companyLikeExists = true;
      }

      int index = _scheduleList.indexWhere((element) => element.scheduleNo == schedule.scheduleNo);
      print(schedule.company!.companyLikeExists);
      if(index > -1){
        setState(() {
          _scheduleList[index] = schedule;
        });

      }

      if(_companyProvider!.getCompany()!= null){
        print('company get detail set');

        Company? company = _companyProvider!.getCompany();
        if(response == null || response.deletedAt != null){
          company!.companyLikeExists = false;
        }else{
          company!.companyLikeExists = true;
        }

        _companyProvider!.setCompanyDetail(schedule.company);
      }

      if(schedule.company!.category!.parentCategoryNo != null){
        if(_companyProvider!.getCompanyList(schedule.company!.category!.parentCategoryNo!.toInt()) != null){
          List<Company> companyList = _companyProvider!.getCompanyList(schedule.company!.category!.parentCategoryNo!.toInt());
          int index = companyList.indexWhere((element) => element.companyNo == schedule.companyNo);
          _companyProvider!.companyListUpdate(schedule.company!.category!.parentCategoryNo!.toInt(), companyList[index]);

        }
      }else{
        if(_companyProvider!.getCompanyList(schedule.company!.categoryNo!.toInt()) != null){
          List<Company> companyList = _companyProvider!.getCompanyList(schedule.company!.categoryNo!.toInt());
          int index = companyList.indexWhere((element) => element.companyNo == schedule.companyNo);
          _companyProvider!.companyListUpdate(schedule.company!.categoryNo!.toInt(), companyList[index]);

        }

      }


      _userProvider!.selectMe('*');


      indicator.hide();
    }).catchError((onError){
      indicator.hide();
    });
  }
}
