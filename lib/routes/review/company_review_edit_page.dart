import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/company_review_image_model.dart';
import 'package:onewed_app/models/company_review_model.dart';
import 'package:onewed_app/providers/company_provider.dart';
import 'package:onewed_app/providers/company_review_provider.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';

class CompanyReviewEditPage extends StatelessWidget{
  CompanyReview companyReview;
  CompanyReviewEditPage({Key? key, required this.companyReview}):super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return View(this.companyReview);
  }

}

class View extends BaseRoute {
  CompanyReview companyReview;
  View(this.companyReview);

  @override
  _ViewState createState() => _ViewState(this.companyReview);
}

class _ViewState extends BaseRouteState {
  CompanyReview companyReview;
  _ViewState(this.companyReview);

  TextEditingController textEditingController = new TextEditingController();

  int score = 0;

  ImagePicker picker = ImagePicker();
  List<File> files = [];

  CompanyReviewProvider? _companyReviewProvider;
  List<CompanyReviewImage> reviewImageList = [];
  List<int> delReviewImageNoList = [];

  CompanyProvider? _companyProvider;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    score = companyReview.score!.toInt();
    textEditingController.text = companyReview.content!.toString();
    reviewImageList = companyReview.companyReviewImageList!;
    setState(() {

    });
  }



  @override
  Widget build(BuildContext context) {

    _companyReviewProvider = Provider.of<CompanyReviewProvider>(context);
    _companyProvider = Provider.of<CompanyProvider>(context);

    return PlatformScaffold(
      title: "웨딩리뷰 작성",
      body: SafeArea(
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.only(top: 20, bottom: 16, left: 16, right: 16),
          child: Column(

            children: [
              Text("${companyReview.company!.companyName}", style: TextStyleTheme.blackS18W700,),
              Text("에서의 서비스는 어떠셨나요?", style: TextStyleTheme.darkDarkS18W400,),
              SizedBox(height: 20,),
              reviewScoreWidget(),
              SizedBox(height: 30,),
              Stack(
                alignment: Alignment.bottomRight,
                children: [
                  TextField(
                    controller: textEditingController,
                    maxLines: 8,
                    maxLength: 700,
                    onChanged: (value){
                      setState(() {

                      });
                    },
                    decoration: InputDecoration(
                        counterText: "",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            borderSide: new BorderSide(
                                width: 1,
                                color: ColorTheme.darkDarkBlack
                            )
                        ),
                        hintText: '웨딩 리뷰를 작성해주세요. (최소 5자 이상)',
                        hintStyle: TextStyleTheme.darkLightS16W400,
                        contentPadding: EdgeInsets.only(left: 16, right: 20, top: 20, bottom: 20),
                        enabledBorder:  new OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            borderSide: new BorderSide(
                                width: 1,
                                color: ColorTheme.darkDarkBlack
                            )
                        ),
                        focusedBorder:  new OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            borderSide: new BorderSide(
                                width: 1,
                                color: ColorTheme.darkDarkBlack
                            )
                        )
                    ),

                  ),
                  Container(
                    margin: EdgeInsets.only(right: 8, bottom: 8),
                    child: Text("${textEditingController.text.length}/700", style: TextStyleTheme.darkLightS14W400,),
                  )
                ],
              ),
              SizedBox(height: 10,),
              Align(
                alignment: Alignment.centerLeft,
                child: Text("사진은 4장까지 첨부가 가능합니다.", style: TextStyleTheme.darkGrayS14W400,),
              ),
              SizedBox(height: 20,),
              Container(
                width: double.infinity ,
                height: 88,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(child: ListView(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,

                      children: [
                        if(files.length + reviewImageList.length != 4)
                          CustomContainer(
                            onPressed: (){
                              imagePicker();
                            },
                            width: 88,
                            height: 88,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(12),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: ColorTheme.lightLight,
                                    border: Border.all(
                                        color: ColorTheme.grayLevel1,
                                        width: 1
                                    )
                                ),
                                child: Image.asset("assets/icons/icon_camera_40.png"),
                              ),
                            ),
                          ),
                        Row(
                          children: reviewImageList.map((e) => Stack(
                            alignment: Alignment.topRight,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                width: 88,
                                height: 88,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(12),
                                  child: Image.network(e.src!.o, fit: BoxFit.cover,),
                                ),
                              ),
                              CustomContainer(
                                onPressed: (){
                                  reviewImageList.removeWhere((element) => element.companyReviewImageNo == e.companyReviewImageNo);
                                  delReviewImageNoList.add(e.companyReviewImageNo!.toInt());
                                  setState(() {

                                  });
                                },
                                margin: EdgeInsets.only(top: 4, right: 4),
                                width: 32,
                                height: 32,
                                backgroundColor: Colors.black.withOpacity(0.7),
                                borderRadius: [6, 6, 6, 6],
                                child: Center(
                                  child: Image.asset("assets/icons/3.0x/icon_cancel.png", width: 24, height: 24,),
                                ),
                              )
                            ],
                          )).toList(),
                        ),
                        Row(
                          children: files.map((e) => Stack(
                            alignment: Alignment.topRight,
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                width: 88,
                                height: 88,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(12),
                                  child: Image.file(e, fit: BoxFit.cover,),
                                ),
                              ),
                              CustomContainer(
                                onPressed: (){
                                  files.removeWhere((element) => element == e);
                                  setState(() {

                                  });
                                },
                                margin: EdgeInsets.only(top: 4, right: 4),
                                width: 32,
                                height: 32,
                                backgroundColor: Colors.black.withOpacity(0.7),
                                borderRadius: [6, 6, 6, 6],
                                child: Center(
                                  child: Image.asset("assets/icons/3.0x/icon_cancel.png", width: 24, height: 24,),
                                ),
                              )
                            ],
                          )).toList(),
                        )
                      ],
                    ))
                  ],
                ),
              ),
              SizedBox(height: 20,),
              Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      CustomContainer(
                        onPressed: (){
                          this.navigator!.popRoute(null);
                        },
                        child: Center(
                          child: Text("나중에 수정할게요", style: TextStyleTheme.darkGrayS16W400,),
                        ),
                      ),
                      SizedBox(height: 36,),
                      CustomContainer(
                        onPressed: (){
                          saveReview();
                        },
                        height: 60,
                        width: double.infinity,
                        backgroundColor: ColorTheme.primaryColor,
                        borderRadius: [12, 12, 12, 12],
                        child: Center(
                          child: Text("리뷰 수정", style: TextStyleTheme.whiteS16W700,),
                        ),
                      )

                    ],
                  )
              )
            ],
          ),
        ),
      ),
    );
  }


  Widget reviewScoreWidget(){

    int i = 1;
    int count = 1;

    List<Widget> list = [];
    for(i = 1; i<= score ; i++){
      int setScore = i;
      list.add(CustomContainer(
        onPressed: (){
          if(score == setScore){
            score = 0;
          }else{
            score = setScore;
          }

          setState(() {

          });
        },
        margin: EdgeInsets.only(right:  setScore == 5 ? 0 : 10),
        width: 48,
        height: 48,
        child: Image.asset("assets/icons/icon_star_yw_fill_48.png"),
      ));

      count++;
    }

    for(i = count; i< 6 ; i++){
      int setScore = i;
      list.add(CustomContainer(
        onPressed: (){
          if(score == setScore){
            score = 0;
          }else{
            score = setScore;
          }
          setState(() {

          });
        },
        margin: EdgeInsets.only(right:  setScore == 5 ? 0 : 10),
        width: 48,
        height: 48,
        child: Image.asset("assets/icons/icon_star_yw_48.png"),
      ));
    }


    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: list,
    );
  }

  Future<void> imagePicker() async{
    if(files.length + reviewImageList.length > 4){
      BotToast.showText(text: "이미지는 최대 4장까지 첨부가 가능합니다.");
    }else{
      await picker.pickImage(source: ImageSource.gallery).then((value){
        if(value!=null){
          files.add(File(value.path));
          setState(() {

          });
        }
      });

    }
  }

  void saveReview() async {
    if(score == 0){
      BotToast.showText(text: "별점을 선택해 주세요.");
    }else if(textEditingController.text.length <5){
      BotToast.showText(text: "5자 이상의 웨딩리뷰를 작성해주세요.");
    }else {
      indicator.show();


      Map data = {
        "score": "$score",
        "content": "${textEditingController.text}",
        "_method": "put"
      };


      if(delReviewImageNoList.isNotEmpty){
        delReviewImageNoList.forEach((element) {
          data.addAll({"delete_company_review_image_no[]": "$element"});
        });
      }


      _companyReviewProvider!.modifyCompanyReview(companyReview.companyReviewNo!.toInt(), data, files).then((response){
        if(response != null){
          BotToast.showText(text: "리뷰 수정이 완료되었습니다.");
          _companyReviewProvider!.selectCompanyReview(companyReview.companyReviewNo!.toInt()).then((value) async{

            indicator.hide();

            if(value!=null){
              await _companyReviewProvider!.categoryCompanyReviewListUpdate(companyReview.company!.category!.parentCategoryNo!.toInt(), companyReview);
              await _companyReviewProvider!.companyReviewListUpdate(value);


              if(_companyProvider!.getCompany() != null){
                int companyNo = _companyProvider!.getCompany()!.companyNo!.toInt();
                _companyProvider!.selectCompanyDetail(companyNo);
                Map<String, String> params ={
                  "company_no": "$companyNo",
                  "more_field": "*"
                };
                _companyReviewProvider!.selectCompanyReviewList(1, params);
              }

              this.navigator!.popRoute(null);
            }
          });

        }
      });


/*
      _companyReviewProvider!.insertCompanyReview(schedule.scheduleNo!.toInt(), data, files).then((response){
        indicator.hide();
        if(response != null){
          _scheduleProvider!.removeReviewSchedule(schedule);

          BotToast.showText(text: "리뷰 작성이 완료되었습니다.");
          this.navigator!.popRoute(null);
        }else{
          BotToast.showText(text: "오류가 발생했습니다. 잠시 후 시도해주세요");
          this.navigator!.popRoute(null);
        }
      });*/

    }
  }
}
