import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/components/review/company_review_component.dart';
import 'package:onewed_app/const.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/category_model.dart';
import 'package:onewed_app/providers/category_provider.dart';
import 'package:onewed_app/providers/company_review_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/review/review_write_list_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';

class ReviewPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return View();

  }

}

class View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState {
  CategoryProvider? _categoryProvider;
  List<Categories> _categoryList = [];

  UserProvider? _userProvider;


  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

  }

  bool isOnlyPhoto = false;
  REVIEW_SORT reviewSort = REVIEW_SORT.create_desc;

  @override
  Widget build(BuildContext context) {
    _categoryProvider = Provider.of<CategoryProvider>(context);
    _categoryList = _categoryProvider!.getTypeCategoryList(1);

    _userProvider = Provider.of<UserProvider>(context);

    return DefaultTabController(
      length: _categoryList.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text('웨딩리뷰',style: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.bold,),),
          leading: GestureDetector(
            onTap: () => Navigator.of(context).pop(),
            child: Image.asset(iconArrowLeft),
          ),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(60),
            child: Column(
              children: [
                Container(
                  height: 1,
                  color: Color(0xffECECEC),
                ),
                TabBar(
                  isScrollable: true,
                  indicatorColor: ColorTheme.primaryColor,
                  indicator: UnderlineTabIndicator(
                    borderSide: BorderSide(width: 4,color: ColorTheme.primaryColor),
                    insets: EdgeInsets.symmetric(horizontal:16.0),
                  ),
                  labelColor: ColorTheme.primaryColor,
                  unselectedLabelColor: Colors.black,
                  labelStyle: TextStyle(
                      fontWeight: FontWeight.w700
                  ),
                  unselectedLabelStyle: TextStyle(
                      fontWeight: FontWeight.w400
                  ),
                  tabs: _categoryList.map((category) => Container(
                    height: 58,
                    child: Center(child: Text("${category.categoryName}", style: TextStyle(fontSize: 18,),),),
                    ),).toList()
                ),
                Container(
                  height: 1,
                  color: Color(0xffECECEC),
                ),
              ]
            )
          ),
        ),
        body: Stack(
          alignment: Alignment.bottomRight,
          children: [
            TabBarView(
              children: _categoryList.map((e) => CompanyReviewComponent(category: e,)).toList(),
            ),
            if(_userProvider!.user.level == "1")
              Positioned(
                right: 16,
                bottom: 40,
                child: GestureDetector(
                  onTap: (){
                    this.navigator!.pushRoute(ReviewWriteListPage());
                  },
                  child: Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: ColorTheme.primaryColor
                    ),
                    child: Center(
                        child: Image.asset("assets/icons/icon_review_32.png", fit: BoxFit.fill, width: 32,)
                    ),
                  ),
                )
            )
          ],
        ),
      )
    );
  }
}
