import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/company_review_image_model.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/widgets/page_indicator.dart';

class ReviewImageDetailPage extends StatelessWidget {
  List<CompanyReviewImage> reviewImageList;
  int? index;

  ReviewImageDetailPage({Key? key, required this.reviewImageList, this.index}) :super(key: key);


  @override
  Widget build(BuildContext context) {
    return View(this.reviewImageList, this.index);
  }

}

class View extends BaseRoute {
  List<CompanyReviewImage> reviewImageList;
  int? index;

  View(this.reviewImageList, this.index);

  @override
  _ViewState createState() => _ViewState(this.reviewImageList, this.index);
}

class _ViewState extends BaseRouteState {
  List<CompanyReviewImage> reviewImageList;
  int? index;
  _ViewState(this.reviewImageList, this.index);


  PageController pageController = new PageController();

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);
    if(index != null){
      pageController.jumpToPage(index!);
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return PlatformScaffold(
      backgroundColor: Colors.black,
      body: SafeArea(

        child: Column(
          children: [
            Expanded(
              child:PageView(
                controller: pageController,
                children: reviewImageList.map((e) => reviewImageWidget(e)).toList(),
              ),
            ),

            Container(
              padding: EdgeInsets.symmetric(horizontal: 28),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: ColorTheme.darkDark
              ),
              height: 80,
              child: Stack(
                alignment: Alignment.centerRight,
                children: [
                  CustomPageIndicator(pageController: pageController, count: reviewImageList.length),
                  Positioned(
                    child: IconButton(
                        onPressed: (){
                          this.navigator!.popRoute(null);
                        },
                        icon: Image.asset("assets/icons/3.0x/icon_cancel.png", width: 24,)
                    )
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }



  Widget reviewImageWidget(CompanyReviewImage reviewImage){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(child: CustomContainer(
          // width: double.infinity,
          child: Image.network("${reviewImage.src!.o}", fit: BoxFit.fitWidth,),
        ))

      ],
    );
  }
}
