import 'dart:async';

import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/category_provider.dart';
import 'package:onewed_app/providers/push_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/chat/chat_detail_page.dart';
import 'package:onewed_app/routes/chat/chat_page.dart';
import 'package:onewed_app/routes/company/company_detail_page.dart';
import 'package:onewed_app/routes/event/page/event_detail_page.dart';
import 'package:onewed_app/routes/review/review_detail_page.dart';
import 'package:onewed_app/routes/sign/page/sign_in_page.dart';
import 'package:onewed_app/routes/tab/tab_page.dart';
import 'package:provider/provider.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:shared_preferences/shared_preferences.dart';

// 화면관련 스플래쉬 처리는 해당 dependency 사용
// https://pub.dev/packages/flutter_native_splash

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<PushProvider>(
        create: (context) => PushProvider(),
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState with WidgetsBindingObserver{

  UserProvider? _userProvider;
  bool _isFirst = true;

  PushProvider? _pushProvider;
  CategoryProvider? _categoryProvider;

  User? me;

  @override
  void initState(){
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    // with WidgetsBindingObserver와 함께 background 동적링크 추가
    WidgetsBinding.instance!.removeObserver(this);
  }




  @override
  void afterFirstLayout(BuildContext context)  async{
    super.afterFirstLayout(context);

    FlutterAppBadger.removeBadge();

    indicator.show();

    await _categoryProvider!.selectTypeCategoryList(1,{
      'more_field': '*',
      "parent_category": "true",
      "type": "1"
    });

    await _categoryProvider!.selectTypeCategoryList(2,{
      'more_field': '*',
      "parent_category": "true",
      "type": "2"
    });
    await _categoryProvider!.selectCategoryList({
      'more_field': '*',
      "parent_category": "true",
    });



    _userProvider!.selectMe('*').then((user) {
      indicator.hide();
      if (user!.name == null) {
        this.navigator!.resetRoute(SignInPage());
      } else {

        FirebaseMessaging.instance.getToken().then((value) {
          if(value != null){

            String machineType = "0";
            if(foundation.defaultTargetPlatform == foundation.TargetPlatform.iOS){
              machineType = "2";
            }else if(foundation.defaultTargetPlatform == foundation.TargetPlatform.android){
              machineType = "1";
            }
            _pushProvider!.insertFcmToken(value, machineType).then((value) => {print(value)});
          }
        });

        setState(() {
          me = user;
        });

        this.navigator!.resetRoute(TabPage());

      }
    }).catchError((e) {
      indicator.hide();
      this.navigator!.resetRoute(SignInPage());
    });
    // this.navigator!.resetRoute(TabPage());
    // 오래 걸리는 로직들 수행
  }

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);
    _pushProvider = Provider.of<PushProvider>(context);
    _categoryProvider = Provider.of<CategoryProvider>(context);



    return PlatformScaffold(
      body: Container(),
    );
  }

  void onClickNotification(RemoteMessage message){

    if(message.data != null){
      if(message.data['thread_id'] != null){
        if(me!.level == 9){
          this.navigator!.resetRoute(ChatPage());
        }else{
          this.navigator!.resetRoute(TabPage(currentIndex: 2,));
        }

        this.navigator!.pushRoute(ChatDetailPage(message.data['thread_id'], message.data['user_name']));

      }
    }
  }

}
