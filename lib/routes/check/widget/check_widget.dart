import 'package:flutter/material.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/check_model.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/widgets/inkwell.dart';
import 'package:onewed_app/widgets/size.dart';

class CheckCard extends StatelessWidget {
  final Function() onTap;
  final Check check;

  const CheckCard({
    Key? key,
    required this.onTap,
    required this.check,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20, left: 16, right: 16),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: check.userCheckExists! ? ColorTheme.primaryColor : Color(0xffD3D3D3),
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '${check.title}',
            style: TextStyle(
              color: Colors.black,
              fontSize: 16,
              fontWeight: FontWeight.w500,
            ),
          ),
          SizedBox(height: 8,),
          Column(
            children: List<Widget>.generate(check.checkItems!.length, (index){
              return Row(
                children: [
                  SizedBox(width: 8,),
                  Text("\u2022"),
                  SizedBox(width: 8,),
                  Expanded(
                    child: Text("${check.checkItems![index].content}", style: TextStyle(
                      fontSize: 14,
                      color: ColorTheme.darkDarkBlack,
                      fontWeight: FontWeight.w400,
                    ),)
                  )
                ],
              );
            }),
          ),
          Container(
            margin: EdgeInsets.only(top: 16),
            child: Stack(
              children: [
                Container(
                  width: customWidth(context),
                  height: 60,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      color: check.userCheckExists!
                          ? ColorTheme.primaryColor
                          : Color(0xffD3D3D3),
                    ),
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: CustomInkWell(
                    onTap: onTap,
                    borderRadius: BorderRadius.circular(6),
                    child: Center(
                      child: Text(
                        '체크 완료',
                        style: TextStyle(
                          color: check.userCheckExists!
                              ? ColorTheme.primaryColor
                              : Color(0xffD3D3D3),
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 20,
                  bottom: 0,
                  child: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                      color: check.userCheckExists!
                          ? ColorTheme.primaryColor
                          : Color(0xffECECEC),
                      shape: BoxShape.circle,
                    ),
                    child: Center(
                      child: Image.asset(iconCheck),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
