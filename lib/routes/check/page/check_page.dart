import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/check/check_list_component.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/check_category_model.dart';
import 'package:onewed_app/models/check_model.dart';
import 'package:onewed_app/providers/check_provider.dart';
import 'package:onewed_app/routes/check/widget/check_widget.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/widgets/app_bar.dart';
import 'package:onewed_app/widgets/error.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:onewed_app/widgets/tab_bar.dart';
import 'package:provider/provider.dart';

class CheckPage extends StatelessWidget {
  static const route = '';

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CheckProvider>(
      create: (context) => CheckProvider(),
      child: _View(),
    );
    return _View();
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState with SingleTickerProviderStateMixin {
  int _index = 0;

  CheckProvider? _checkProvider;
  List<CheckCategory> _checkCategoryList = [];

  TabController? _tabController;

  List<Widget> tabs = [];

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    _checkProvider!.selectCheckCategoryList();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    print(tabs.length);
    print(_checkCategoryList.length);

    _tabController = new TabController(length: tabs.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    _checkProvider = Provider.of<CheckProvider>(context);
    _checkCategoryList = _checkProvider!.getCheckCategoryList();

    _checkCategoryList.map((category) {
      tabs.add(Center(
        child: Text('${category.checkCategoryName}'),
      ));
    });

    return DefaultTabController(
        length: _checkCategoryList.length,
        child: new Scaffold(
          appBar: AppBar(
            title: Text('체크리스트',
              style: TextStyle(
                color: Colors.black,
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            leading: GestureDetector(
              onTap: () => Navigator.of(context).pop(),
              child: Image.asset(iconArrowLeft),
            ),
            bottom: new TabBar(
              isScrollable: true,
              indicator: UnderlineTabIndicator(
                borderSide: BorderSide(width: 4, color: ColorTheme.primaryColor),
                insets: EdgeInsets.symmetric(horizontal:16.0),
              ),
              labelColor: ColorTheme.primaryColor,
              unselectedLabelColor: Colors.black,
              labelStyle: TextStyle(
                fontWeight: FontWeight.w700,
              ),
              unselectedLabelStyle: TextStyle(
                fontWeight: FontWeight.w400,
              ),
              tabs:
                List<Widget>.generate(_checkCategoryList.length, (int index) {
                  return Container(
                    height: 55,
                    child: Center(
                      child:
                        Text("${_checkCategoryList[index].checkCategoryName}", style: TextStyle(
                          fontSize: 18
                        )),
                    ),
                  );
              }),
            ),
          ),
          body: SafeArea(
            child: new TabBarView(
                children: List<Widget>.generate(_checkCategoryList.length,
                    (int index) {
              return CheckListComponent(checkCategory: _checkCategoryList[index]);
            })),
          ),
        ));
    /*return DefaultTabController(
      length: _checkCategoryList.length,
      child: PlatformScaffold(
        title: '체크리스트',
        body: CustomScrollView(
          slivers: [
            SliverPersistentHeader(
              pinned: true,
              delegate: CustomTabBarDelegate(
                onTap: (int index) {
                  if (_index == index) return;

                  setState(() {
                    _index = index;
                  });
                },
                tabs: tabs,
                isScrollable: true,
              ),
            ),
*/ /*            SliverPadding(
              padding: EdgeInsets.only(left: 16, right: 16, bottom: 20),
              sliver: SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Consumer<CheckProvider>(
                      builder: (context, checkProvider, _) {
                        final Map<int, List<Check>> checks =
                            checkProvider.checks;

                        if (checkProvider.isLoading) {
                          return CustomLoading(
                            margin: EdgeInsets.only(top: 20),
                          );
                        }

                        if (!checkProvider.isError) {
                          // TODO
                          return ErrorCard(
                            margin: EdgeInsets.only(top: 20),
                          );
                        }

                        if (checks[_index]!.isNotEmpty) {
                          return Column(
                            children: checks[_index]!.map(
                              (check) {
                                return CheckCard(
                                  onTap: () {
                                    // TODO
                                  },
                                  check: check,
                                );
                              },
                            ).toList(),
                          );
                        } else {
                          // TODO
                          return Container();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),*/ /*
          ],
        ),
      ),
    );*/
  }
}
