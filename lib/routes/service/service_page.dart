import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/faq_model.dart';
import 'package:onewed_app/providers/faq_provider.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ServicePage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ChangeNotifierProvider<FaqProvider>(
      create: (context) => FaqProvider(),
      child: _View(),
    );
  }

}

class _View extends BaseRoute {
  @override
  __ViewState createState() => __ViewState();
}

class __ViewState extends BaseRouteState {

  FaqProvider? _faqProvider;
  List<Faq> _faqList = [];

  RefreshController refreshController = new RefreshController(initialRefresh: false);

  int page = 0;
  bool hasMore = true;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    this.onLoading();
  }

  @override
  Widget build(BuildContext context) {
    _faqProvider = Provider.of<FaqProvider>(context);

     return PlatformScaffold(
      title: "자주 묻는 질문",
      body: SafeArea(
        bottom: false,
        child: RefreshConfiguration(
          child: SmartRefresher(
            onLoading: this.onLoading,
            footer: CustomSmartRefresher.customFooter(),
            enablePullUp: true,
            enablePullDown: true,
            controller: refreshController,
            child: ListView(
              children: [
                Consumer<FaqProvider>(builder: (context, faqModel, child){
                  _faqList = faqModel.getFaqList();
                  return Column(
                    children: _faqList.map((faq) => faqItemWidget(faq)).toList(),
                  );
                })
              ],
            ),
            /*child: Consumer<FaqProvider>(builder: (context, faqModel, child){
              _faqList = faqModel.getFaqList();
              return ListView(
                children: _faqList.map((faq) => faqItemWidget(faq)).toList(),
              );
            }),*/
          ),
        ),
      )
    );

  }

  onLoading(){
    if(hasMore){
      page = page+1;
      _faqProvider!.selectFaq(page).then((value){
        if(value!.length == 0){
          hasMore = false;
        }
      });

      refreshController.loadComplete();
    }else{
      refreshController.loadComplete();
    }
  }

  Widget faqItemWidget(Faq faq){
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(top: 28, left: 16, right: 16, bottom: 16),
          child: Column(
            children: [
              CustomContainer(
                margin: EdgeInsets.only(bottom: 14),
                onPressed: (){
                  faq.isOpen = !faq.isOpen!;
                  _faqProvider!.updateFaq(faq);
                },
                child: Row(
                  children: [
                    Text("Q.", style: TextStyleTheme.primaryS16W700,),
                    SizedBox(width: 4,),
                    Expanded(
                      child: Text("${faq.question}", style: TextStyleTheme.blackS16W500,),
                    ),
                    RotatedBox(
                      quarterTurns: faq.isOpen==true ? 2 : 0,
                      child: Image(image: AssetImage("assets/icons/icon_arrow_down_m.png"), width: 40, height: 40, fit: BoxFit.cover,),
                    )
                  ],
                ),
              ),
              if(faq.isOpen!)
                Container(
                  child: Text("A. ${faq.answer}", style: TextStyleTheme.darkDarkS16W400,),
                )
            ],
          ),
        ),
        Container(
          height: 8,
          decoration: BoxDecoration(
            color: ColorTheme.grayLevel1
          ),
        )
      ],
    );
  }
}
