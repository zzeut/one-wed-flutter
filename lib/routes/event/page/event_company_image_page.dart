import 'package:flutter/material.dart';
import 'package:onewed_app/models/event_model.dart';
import 'package:onewed_app/routes/event/widget/event_company_widget.dart';
import 'package:onewed_app/routes/event/widget/event_sheet.dart';

class EventCompanyImagePage extends StatefulWidget {
  final Event event;

  EventCompanyImagePage({
    Key? key,
    required this.event,
  }) : super(key: key);

  @override
  _EventCompanyImagePageState createState() => _EventCompanyImagePageState();
}

class _EventCompanyImagePageState extends State<EventCompanyImagePage> {
  Event _event = Event();
  String _imageType = 'all';

  @override
  void initState() {
    _event = widget.event;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(top: 10),
          height: 60,
          child: ListView(
            padding: EdgeInsets.only(left: 6, right: 16),
            scrollDirection: Axis.horizontal,
            children: [
              EventCompanyImageTypeCard(
                onTap: () {
                  // TODO
                  setState(() {
                    _imageType = 'all';
                  });
                },
                title: '전체',
                isActive: _imageType == 'all',
              ),
              EventCompanyImageTypeCard(
                onTap: () {
                  // TODO
                  setState(() {
                    _imageType = '2';
                  });
                },
                title: '신부대기실',
                isActive: _imageType == '2',
              ),
              EventCompanyImageTypeCard(
                onTap: () {
                  // TODO
                  setState(() {
                    _imageType = '3';
                  });
                },
                title: '연회장',
                isActive: _imageType == '3',
              ),
              EventCompanyImageTypeCard(
                onTap: () {
                  // TODO
                  setState(() {
                    _imageType = '4';
                  });
                },
                title: '폐백실',
                isActive: _imageType == '4',
              ),
              EventCompanyImageTypeCard(
                onTap: () {
                  // TODO
                  setState(() {
                    _imageType = '5';
                  });
                },
                title: '외관',
                isActive: _imageType == '5',
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 20, left: 16, right: 16),
          child: Wrap(
            // TODO
            spacing: 10,
            runSpacing: 10,
            children: [1, 2, 3, 4, 5].map(
              (e) {
                return EventCompanyImageCard(
                  onTap: () {
                    // TODO
                    eventCompanyImageBottomSheet(context);
                  },
                  event: _event,
                );
              },
            ).toList(),
          ),
        ),
      ],
    );
  }
}
