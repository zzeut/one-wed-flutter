import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/event_model.dart';
import 'package:onewed_app/providers/event_provider.dart';
import 'package:onewed_app/routes/event/page/event_detail_page.dart';
import 'package:onewed_app/routes/event/widget/event_widget.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/widgets/app_bar.dart';
import 'package:onewed_app/widgets/error.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:onewed_app/widgets/tab_bar.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class EventPage extends StatelessWidget {
  static const route = '';

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<EventProvider>(
            create: (context) => EventProvider()),
      ],
      child: _View(),
    );
  }
}

class _View extends BaseRoute {
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState with SingleTickerProviderStateMixin {
  EventProvider? _eventProvider;


  TabController? _tabController;

  RefreshController _doingRefreshController = new RefreshController(initialRefresh: false);
  RefreshController _endRefreshController = new RefreshController(initialRefresh: false);


  List<Event> doingEventList = [];
  int doingEventPage = 1;
  bool doingEventHasMore = true;

  List<Event> endEventList = [];
  int endEventPage = 1;
  bool endEventHasMore = true;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    this.refreshDoingEvents();
    this.refreshEndEvents();
  }

  @override
  void initState() {
    _tabController = TabController(
      initialIndex: 0,
      length: 2,
      vsync: this,
    );
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    _eventProvider = Provider.of<EventProvider>(context);

    return Scaffold(
      appBar: AppBar(

        title:Text('이벤트', style: TextStyle( color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold,),),
        leading: GestureDetector(
          onTap: () => Navigator.of(context).pop(),
          child: Image.asset(iconArrowLeft),
        ),
        centerTitle: true,
        /*bottom: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: Column(
            children: [
              Container(
                height: 1,
                color: Color(0xffECECEC),
              ),
              TabBar(

                controller: _tabController,
                indicatorColor: ColorTheme.primaryColor,
                indicator: UnderlineTabIndicator(
                  borderSide: BorderSide(width: 4,color: ColorTheme.primaryColor),
                  insets: EdgeInsets.symmetric(horizontal:16.0),
                ),
                labelColor: ColorTheme.primaryColor,
                unselectedLabelColor: Colors.black,
                labelStyle: TextStyle(
                  fontWeight: FontWeight.w700
                ),
                unselectedLabelStyle: TextStyle(
                    fontWeight: FontWeight.w400
                ),

                tabs: [
                  Container(
                    height: 58,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("진행중인 이벤트", style: TextStyle(fontSize: 18,),)
                      ],
                    ),
                  ),
                  Container(
                    height: 58,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("종료된 이벤트", style: TextStyle(
                            fontSize: 18,
                        ),)
                      ],
                    ),
                  )
                ]
              ),

              Container(
                height: 1,
                color: Color(0xffECECEC),
              ),
            ],
          ),
        ),*/

      ),
      body: Consumer<EventProvider>(builder: (context, eventModel, child){
        if (eventModel.isLoading) {
          return CustomLoading(
            margin: EdgeInsets.only(top: 20),
          );
        }
        if (!eventModel.isError) {
          // TODO
          return ErrorCard(
            margin: EdgeInsets.only(top: 20),
          );
        }

        return RefreshConfiguration(
            child: SmartRefresher(
              controller: _doingRefreshController,
              enablePullDown: false,
              enablePullUp: true,
              footer: CustomSmartRefresher.customFooter(),
              onLoading: this.loadingDoingEvents,
              child: ListView(
                children: doingEventList.map((event){
                  return EventCard(
                      onTap: (){
                        this.navigator!.pushRoute(EventDetailPage(eventNo: event.eventNo!.toInt(),),);
                      },
                      event: event
                  );
                }).toList(),
              ),
            )
        );
      }),
      /*body: SafeArea(
        child: TabBarView(
          controller: _tabController,
          children: [
            Consumer<EventProvider>(builder: (context, eventModel, child){
              if (eventModel.isLoading) {
                return CustomLoading(
                  margin: EdgeInsets.only(top: 20),
                );
              }
              if (!eventModel.isError) {
                // TODO
                return ErrorCard(
                  margin: EdgeInsets.only(top: 20),
                );
              }

              return RefreshConfiguration(
                  child: SmartRefresher(
                    controller: _doingRefreshController,
                    enablePullDown: false,
                    enablePullUp: true,
                    footer: CustomSmartRefresher.customFooter(),
                    onLoading: this.loadingDoingEvents,
                    child: ListView(
                      children: doingEventList.map((event){
                        return EventCard(
                            onTap: (){
                              this.navigator!.pushRoute(EventDetailPage(eventNo: event.eventNo!.toInt(),),);
                            },
                            event: event
                        );
                      }).toList(),
                    ),
                  )
              );
            }),

            Consumer<EventProvider>(builder: (context, eventModel, child){
              if (eventModel.isLoading) {
                return CustomLoading(
                  margin: EdgeInsets.only(top: 20),
                );
              }
              if (!eventModel.isError) {
                // TODO
                return ErrorCard(
                  margin: EdgeInsets.only(top: 20),
                );
              }
              return RefreshConfiguration(
                  child: SmartRefresher(
                    controller: _endRefreshController,
                    enablePullDown: false,
                    enablePullUp: true,
                    footer: CustomSmartRefresher.customFooter(),
                    onLoading: this.loadingEndEvents,
                    child: ListView(
                      children: endEventList.map((event){
                        return EventCard(
                            onTap: (){
                              this.navigator!.pushRoute(EventDetailPage(eventNo: event.eventNo!.toInt(),),);
                            },
                            event: event
                        );
                      }).toList(),
                    ),
                  )
              );
            })
          ],
        ),
      ),*/

    );

  }

  void refreshDoingEvents() {
    doingEventHasMore = true;
    doingEventPage = 0;
    this.loadingDoingEvents();
  }

  loadingDoingEvents() async {
    if (doingEventHasMore) {
      doingEventPage++;
      print(doingEventPage);
      await _eventProvider!.selectEventList({'is_doing': "true", "page":"$doingEventPage"}).then((response) {
        if (response!.length == 0) {
          doingEventHasMore = false;
        } else {
          if (doingEventPage == 1) {
            doingEventList = response;
          } else {
            doingEventList.addAll(response);
          }
        }
        setState(() {

        });
      });
    }
  }

  void refreshEndEvents() {
    endEventHasMore = true;
    endEventPage = 0;
    loadingEndEvents();
  }

  loadingEndEvents() async {
    if (endEventHasMore) {
      endEventPage++;
      await _eventProvider!.selectEventList({'is_end': "true", "page":"$endEventPage"}).then((response) {
        if (response!.length == 0) {
          endEventHasMore = false;
        } else {
          if (endEventPage == 1) {
            endEventList = response;
          } else {
            endEventList.addAll(response);
          }
          setState(() {

          });
        }
      });
    }
  }
}
