import 'package:flutter/material.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/company_review_model.dart';
import 'package:onewed_app/providers/company_review_provider.dart';
import 'package:onewed_app/routes/event/widget/event_company_widget.dart';
import 'package:onewed_app/routes/event/widget/event_sheet.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/widgets/error.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:provider/provider.dart';

class EventCompanyReviewPage extends StatefulWidget {
  EventCompanyReviewPage({Key? key}) : super(key: key);

  @override
  _EventCompanyReviewPageState createState() => _EventCompanyReviewPageState();
}

class _EventCompanyReviewPageState extends State<EventCompanyReviewPage> {
  bool _isPhotoReview = false;
  String _order = 'c';

  @override
  Widget build(BuildContext context) {
    return Consumer<CompanyReviewProvider>(
      builder: (context, companyReviewProvider, _) {
        /*List<CompanyReview> companyReviews =
            List.from(companyReviewProvider.companyReviews);*/

        if (companyReviewProvider.isLoading) {
          return CustomLoading(
            margin: EdgeInsets.only(top: 20),
          );
        }

        if (!companyReviewProvider.isError) {
          // TODO
          return ErrorCard(
            margin: EdgeInsets.only(top: 20),
          );
        }

        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(top: 20, left: 16, right: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      // TODO
                      setState(() {
                        _isPhotoReview = !_isPhotoReview;
                      });
                    },
                    child: Container(
                      color: Colors.transparent,
                      child: Row(
                        children: [
                          Container(
                            width: 20,
                            height: 20,
                            decoration: BoxDecoration(
                              color: _isPhotoReview
                                  ? ColorTheme.primaryColor
                                  : Color(0xffECECEC),
                              shape: BoxShape.circle,
                            ),
                            child: Center(
                              child: Image.asset(iconCheck),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text(
                              '사진리뷰',
                              style: TextStyle(
                                color: _isPhotoReview
                                    ? Color(0xff303030)
                                    : Color(0xffA7A7A7),
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      EventCompanyReviewTypeTitle(
                        onTap: () {
                          // TODO
                          setState(() {
                            _order = 'c';
                          });
                        },
                        title: '최신순',
                        isActive: _order == 'c',
                      ),
                      EventCompanyReviewTypeTitle(
                        onTap: () {
                          // TODO
                          setState(() {
                            _order = 'h';
                          });
                        },
                        margin: EdgeInsets.only(left: 16),
                        title: '별점 높은순',
                        isActive: _order == 'h',
                      ),
                      EventCompanyReviewTypeTitle(
                        onTap: () {
                          // TODO
                          setState(() {
                            _order = 'l';
                          });
                        },
                        margin: EdgeInsets.only(left: 16),
                        title: '별점 낮은순',
                        isActive: _order == 'l',
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 12),
              child: Column(
                // TODO
               /* children: companyReviews.map(
                  (companyReview) {
                    return EventCompanyReviewCard(
                      onTap: () {
                        // TODO
                        eventCompanyReviewImageBottomSheet(context);
                      },
                      companyReview: companyReview,
                    );
                  },
                ).toList(),*/
              ),
            ),
          ],
        );
      },
    );
  }
}
