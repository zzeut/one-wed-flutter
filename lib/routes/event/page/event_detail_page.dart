import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:onewed_app/base_navigator.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/event_model.dart';
import 'package:onewed_app/providers/company_review_provider.dart';
import 'package:onewed_app/providers/event_provider.dart';
import 'package:onewed_app/routes/event/page/event_company_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/utils/dynamic_link.dart';
import 'package:onewed_app/widgets/app_bar.dart';
import 'package:onewed_app/widgets/inkwell.dart';
import 'package:onewed_app/widgets/size.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

class EventDetailPage extends StatelessWidget {
  int eventNo;

  EventDetailPage({
    Key? key,
    required this.eventNo,
  }) : super(key: key);
  

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return ChangeNotifierProvider<EventProvider>(create: (context) => EventProvider(), child: _View(this.eventNo));
  }
}

class _View extends BaseRoute {
  int eventNo;
  _View(this.eventNo);

  @override
  _ViewState createState() => _ViewState(this.eventNo);
}

class _ViewState extends BaseRouteState {
  int eventNo;
  _ViewState(this.eventNo);

  EventProvider? _eventProvider;
  Event? event;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    _eventProvider!.selectEvent(eventNo).then((value){
      if(value!=null){
        setState(() {
          event = value;
        });
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    _eventProvider = Provider.of<EventProvider>(context);


    if(event == null){
      return Container();
    }
    CustomAppBar appBar = CustomAppBar(
      onTap: null,
      title: '${event!.eventTitle}',
      fullscreenDialog: false,
    );

    return Scaffold(
      appBar: appBar,
      body: SafeArea(
        bottom: false,
        left: false,
        right: false,
        child: Stack(
          children: [
            Container(
              // width: customWidth(context),
              width: double.infinity,
              // height: MediaQuery.of(context).size.height  - appBar.preferredSize.height,
              padding: EdgeInsets.zero,
              child: ListView(
                children: [
                  Html(
                    data: event!.content,
                  )
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              child: Container(
                margin: EdgeInsets.only(left: 16, right: 16, bottom: 20),
                child: Wrap(
                  spacing: 10,
                  children: [
                    Container(
                      width: ((customWidth(context) - 42) / 2).floorToDouble(),
                      height: 60,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Color(0xffF18F8F),
                        ),
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: CustomInkWell(
                        onTap: () async{
                          // TODO
                          String link  = await DynamicLink().buildDynamicLink('event?event_no=$eventNo');
                          Share.share(link);
                        },
                        borderRadius: BorderRadius.circular(12),
                        child: Center(
                          child: Text(
                            '공유하기',
                            style: TextStyle(
                              color: ColorTheme.primaryColor,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: ((customWidth(context) - 42) / 2).floorToDouble(),
                      height: 60,
                      decoration: BoxDecoration(
                        color: ColorTheme.primaryColor,
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: CustomInkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        borderRadius: BorderRadius.circular(12),
                        child: Center(
                          child: Text(
                            '목록',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


