import 'package:flutter/material.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/event_model.dart';
import 'package:onewed_app/providers/company_review_provider.dart';
import 'package:onewed_app/routes/event/page/event_company_image_page.dart';
import 'package:onewed_app/routes/event/page/event_company_info_page.dart';
import 'package:onewed_app/routes/event/page/event_company_review_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/style_theme.dart';
import 'package:onewed_app/widgets/image.dart';
import 'package:onewed_app/widgets/inkwell.dart';
import 'package:onewed_app/widgets/page_indicator.dart';
import 'package:onewed_app/widgets/size.dart';
import 'package:onewed_app/widgets/tab_bar.dart';
import 'package:provider/provider.dart';

class EventCompanyPage extends StatefulWidget {
  final Event event;

  EventCompanyPage({
    Key? key,
    required this.event,
  }) : super(key: key);

  @override
  _EventCompanyPageState createState() => _EventCompanyPageState();
}

class _EventCompanyPageState extends State<EventCompanyPage> {
  Event _event = Event();
  PageController _pageController = PageController();
  ScrollController _scrollController = ScrollController();
  int _index = 0;

  @override
  void initState() {
    _event = widget.event;

    _scrollController.addListener(() {
      setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  bool _isActive() {
    if (_scrollController.hasClients) {
      double currentScroll = _scrollController.position.pixels;

      if (currentScroll > customWidth(context) - 120) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  Widget _getTabScreen() {
    if (_index == 0) {
      return EventCompanyInfoPage(
        event: _event,
      );
    } else if (_index == 1) {
      return EventCompanyImagePage(
        event: _event,
      );
    } else {
      return EventCompanyReviewPage();
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: PlatformScaffold(
        body: Stack(
          children: [
            CustomScrollView(
              controller: _scrollController,
              physics: AlwaysScrollableScrollPhysics(),
              slivers: [
                SliverAppBar(
                  elevation: 0,
                  centerTitle: true,
                  pinned: true,
                  expandedHeight: customWidth(context) - 60,
                  leading: GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Image.asset(
                      iconArrowLeft,
                      color: _isActive() ? null : Colors.white,
                    ),
                  ),
                  title: Text(
                    '1234',
                    style: TextStyle(
                      color: _isActive() ? Colors.black : Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  actions: [
                    GestureDetector(
                      onTap: () {
                        // TODO
                      },
                      child: Image.asset(
                        iconShare,
                        color: _isActive() ? null : Colors.white,
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        // TODO
                      },
                      child: Image.asset(
                        iconHeart,
                        color:
                            // TODO
                            _isActive() ? Colors.grey : null,
                      ),
                    ),
                  ],
                  flexibleSpace: FlexibleSpaceBar(
                    background: Stack(
                      children: [
                        Container(
                          width: customWidth(context),
                          height: customWidth(context),
                          child: PageView.builder(
                            controller: _pageController,
                            physics: ClampingScrollPhysics(),
                            itemCount: 5,
                            itemBuilder: (context, index) {
                              return CustomImageCard(
                                width: customWidth(context),
                                height: customWidth(context),
                                imageUrl:
                                    'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg',
                              );
                            },
                          ),
                        ),
                        Positioned(
                          left: 0,
                          right: 0,
                          bottom: 16,
                          child: CustomPageIndicator(
                            pageController: _pageController,
                            count: 5,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SliverPersistentHeader(
                  pinned: true,
                  delegate: CustomTabBarDelegate(
                    onTap: (int index) {
                      if (_index == index) return;

                      setState(() {
                        _index = index;
                      });
                    },
                    tabs: [
                      Center(
                        child: Text('정보'),
                      ),
                      Center(
                        child: Text('이미지'),
                      ),
                      Center(
                        // TODO
                        child: Text(
                          '리뷰\n(1,000)',
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
                SliverPadding(
                  padding: EdgeInsets.only(
                    top: _index == 0 ? 20 : 0,
                    left: _index == 0 ? 16 : 0,
                    right: _index == 0 ? 16 : 0,
                    bottom: 20,
                  ),
                  sliver: SliverList(
                    delegate: SliverChildListDelegate(
                      [
                        _getTabScreen(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              right: 16,
              bottom: 32,
              child: Container(
                width: 60,
                height: 60,
                decoration: BoxDecoration(
                  color: ColorTheme.primaryColor,
                  shape: BoxShape.circle,
                  boxShadow: [StyleTheme.boxShadow],
                ),
                child: CustomInkWell(
                  onTap: () {
                    // TODO
                  },
                  borderRadius: BorderRadius.circular(200),
                  child: Center(
                    child: _index == 2
                        ? Image.asset(iconReviewAdd)
                        : Image.asset(iconTalkOn),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
