import 'package:flutter/material.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/event_model.dart';
import 'package:onewed_app/routes/event/widget/event_company_widget.dart';

class EventCompanyInfoPage extends StatefulWidget {
  final Event event;

  EventCompanyInfoPage({
    Key? key,
    required this.event,
  }) : super(key: key);

  @override
  _EventCompanyInfoPageState createState() => _EventCompanyInfoPageState();
}

class _EventCompanyInfoPageState extends State<EventCompanyInfoPage> {
  Event _event = Event();

  @override
  void initState() {
    _event = widget.event;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Container(
              width: 88,
              height: 88,
              decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(12),
              ),
            ),
            Flexible(
              child: Container(
                margin: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '경기도 부천시',
                      style: TextStyle(
                        color: Color(0xff303030),
                        fontSize: 14,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4),
                      child: Text(
                        'Sksdaljfaksjsdkjflasjefiasjflasdfjklsanvlkasdklfajsfiejsofjaselfjasdfkjasdlfjalsdjflaskdf',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 4),
                      child: Row(
                        children: [
                          Image.asset(iconStar),
                          Container(
                            margin: EdgeInsets.only(left: 3),
                            child: Text(
                              '(4.5)',
                              style: TextStyle(),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        Container(
          margin: EdgeInsets.only(top: 16),
          child: Column(
            children: [
              EventCompanyInfoInfoTitle(
                title: '수용인원',
                contents: '200명',
              ),
              EventCompanyInfoInfoTitle(
                title: '홀개수',
                contents: '2개',
              ),
              EventCompanyInfoInfoTitle(
                title: '주차',
                contents: '450대',
              ),
              EventCompanyInfoInfoTitle(
                title: '피로연',
                contents: '뷔폐식',
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 20),
          child: Text(
            'aasdfjasdkfhasdjkfhadskfljasdlkfjadslfjsadlkfjlsdafjklsadjflksadfjlkadsjflkdsajflkadsjflksdajflasdfjdsasdflksafjlsdjfladsfldkdsfjlasdf\nadsfjkasldfksadf\naksdjasdfksajdlfkjsadklfjasdlkfjasldkfjsdlakfjldsajfldsjfalsdfflajdsflsadf\nadsjkflasdf\n\nkasdjflkasdjflksadjflksadjflksadjflksdajflkasdjflkadsjflkasdjflkadsjflkasdaasdfjasdkfhasdjkfhadskfljasdlkfjadslfjsadlkfjlsdafjklsadjflksadfjlkadsjflkdsajflkadsjflksdajflasdfjdsasdflksafjlsdjfladsfldkdsfjlasdf\nadsfjkasldfksadf\naksdjasdfksajdlfkjsadklfjasdlkfjasldkfjsdlakfjldsajfldsjfalsdfflajdsflsadf\nadsjkflasdf\n\nkasdjflkasdjflksadjflksadjflksadjflksdajflkasdjflkadsjflkasdjflkadsjflkasdaasdfjasdkfhasdjkfhadskfljasdlkfjadslfjsadlkfjlsdafjklsadjflksadfjlkadsjflkdsajflkadsjflksdajflasdfjdsasdflksafjlsdjfladsfldkdsfjlasdf\nadsfjkasldfksadf\naksdjasdfksajdlfkjsadklfjasdlkfjasldkfjsdlakfjldsajfldsjfalsdfflajdsflsadf\nadsjkflasdf\n\nkasdjflkasdjflksadjflksadjflksadjflksdajflkasdjflkadsjflkasdjflkadsjflkasdaasdfjasdkfhasdjkfhadskfljasdlkfjadslfjsadlkfjlsdafjklsadjflksadfjlkadsjflkdsajflkadsjflksdajflasdfjdsasdflksafjlsdjfladsfldkdsfjlasdf\nadsfjkasldfksadf\naksdjasdfksajdlfkjsadklfjasdlkfjasldkfjsdlakfjldsajfldsjfalsdfflajdsflsadf\nadsjkflasdf\n\nkasdjflkasdjflksadjflksadjflksadjflksdajflkasdjflkadsjflkasdjflkadsjflkasd',
            style: TextStyle(
              color: Color(0xff303030),
              fontSize: 14,
            ),
          ),
        ),
      ],
    );
  }
}
