import 'package:flutter/material.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/company_review_model.dart';
import 'package:onewed_app/models/event_model.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/widgets/image.dart';
import 'package:onewed_app/widgets/inkwell.dart';
import 'package:onewed_app/widgets/size.dart';

class EventCompanyInfoInfoTitle extends StatelessWidget {
  final String title;
  final String contents;

  const EventCompanyInfoInfoTitle({
    Key? key,
    required this.title,
    required this.contents,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 4),
      child: Row(
        children: [
          Container(
            width: 88,
            child: Text(
              '$title',
              style: TextStyle(
                color: Color(0xff303030),
                fontSize: 16,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 10),
            child: Text(
              '$contents',
              style: TextStyle(
                // TODO
                color: Color(0xff303030),
                fontSize: 16,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class EventCompanyImageTypeCard extends StatelessWidget {
  final Function() onTap;
  final String title;
  final bool isActive;

  const EventCompanyImageTypeCard({
    Key? key,
    required this.onTap,
    required this.title,
    required this.isActive,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsets.only(left: 10),
        decoration: BoxDecoration(
          color: isActive ? ColorTheme.primaryColor : Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        child: CustomInkWell(
          onTap: onTap,
          padding: EdgeInsets.only(top: 8, left: 16, right: 16, bottom: 8),
          borderRadius: BorderRadius.circular(10),
          child: Text(
            '$title',
            style: TextStyle(
              color: isActive ? Colors.white : Color(0xff707070),
              fontSize: 16,
              fontWeight: isActive ? FontWeight.bold : FontWeight.normal,
            ),
          ),
        ),
      ),
    );
  }
}

class EventCompanyImageCard extends StatelessWidget {
  // TODO
  final Function() onTap;
  final Event event;

  const EventCompanyImageCard({
    Key? key,
    required this.onTap,
    required this.event,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Stack(
        children: [
          CustomImageCard(
            width: ((customWidth(context) - 42) / 2).floorToDouble(),
            height: ((customWidth(context) - 42) / 2).floorToDouble(),
            imageUrl:
                'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg',
            borderRadius: BorderRadius.circular(12),
          ),
          Positioned(
            left: 8,
            bottom: 8,
            child: Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.8),
                borderRadius: BorderRadius.circular(6),
              ),
              child: Text(
                '신부대기실',
                style: TextStyle(
                  color: Color(0xff303030),
                  fontSize: 14,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class EventCompanyReviewTypeTitle extends StatelessWidget {
  final Function() onTap;
  final EdgeInsetsGeometry? margin;
  final String title;
  final bool isActive;

  const EventCompanyReviewTypeTitle({
    Key? key,
    required this.onTap,
    this.margin,
    required this.title,
    required this.isActive,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: GestureDetector(
        onTap: onTap,
        child: Text(
          '$title',
          style: TextStyle(
            color: isActive ? ColorTheme.primaryColor : Color(0xffA7A7A7),
            fontSize: 16,
            fontWeight: isActive ? FontWeight.bold : FontWeight.normal,
          ),
        ),
      ),
    );
  }
}

class EventCompanyReviewCard extends StatelessWidget {
  // TODO
  final Function() onTap;
  final CompanyReview companyReview;

  const EventCompanyReviewCard({
    Key? key,
    required this.onTap,
    required this.companyReview,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double getReviewImageWidth() {
      // int length = companyReview.companyReviewPhotos!.length;
      int length = 2;

      if (length > 2) {
        return 160;
      } else if (length > 1) {
        return ((customWidth(context) - 42) / 2).floorToDouble();
      } else {
        return customWidth(context) - 32;
      }
    }

    List<Widget> getReviewRatings() {
      List<Widget> ratings = List.filled(5, Image.asset(iconStarGray));

      /*ratings.asMap().forEach((index, value) {
        if (index < companyReview.rating!) {
          ratings[index] = Image.asset(iconStarFill);
        }
      });*/

      return ratings;
    }

    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, right: 16),
            child: Row(
              children: [
                CustomImageCard(
                  width: 40,
                  height: 40,
                  imageUrl:
                      'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg',
                  borderRadius: BorderRadius.circular(10),
                ),
                Flexible(
                  child: Container(
                    margin: EdgeInsets.only(left: 8),
                    child: Column(
                      children: [
                        Row(
                          children: getReviewRatings(),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 4),
                          width: customWidth(context),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                '리뷰작성자',
                                style: TextStyle(
                                  color: Color(0xff707070),
                                  fontSize: 14,
                                ),
                              ),
                              Text(
                                '21.07.23',
                                style: TextStyle(
                                  color: Color(0xffA7A7A7),
                                  fontSize: 14,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 16, left: 16, right: 16),
            /*child: Text(
              '${companyReview.title}',
              style: TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
            ),*/
          ),
/*          if (companyReview.companyReviewPhotos!.isNotEmpty)
            Container(
              margin: EdgeInsets.only(top: 16),
              height: 160,
              child: ListView.builder(
                padding: EdgeInsets.only(left: 6, right: 16),
                scrollDirection: Axis.horizontal,
                itemCount: companyReview.companyReviewPhotos!.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.only(left: 10),
                    child: CustomImageCard(
                      onTap: onTap,
                      width: getReviewImageWidth(),
                      height: 160,
                      imageUrl:
                          '${companyReview.companyReviewPhotos![index].imageUrl}',
                      borderRadius: BorderRadius.circular(12),
                    ),
                  );
                },
              ),
            )
          else*/
            Container(),
          Container(
            margin: EdgeInsets.only(top: 20),
            height: 8,
            color: Color(0xffECECEC),
          ),
        ],
      ),
    );
  }
}
