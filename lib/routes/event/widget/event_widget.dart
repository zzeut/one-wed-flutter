import 'package:flutter/material.dart';
import 'package:onewed_app/components/dateTime.dart';
import 'package:onewed_app/models/event_model.dart';
import 'package:onewed_app/themes/style_theme.dart';
import 'package:onewed_app/widgets/image.dart';
import 'package:onewed_app/widgets/size.dart';

class EventCard extends StatelessWidget {
  // TODO
  final Function() onTap;
  final Event event;

  const EventCard({
    Key? key,
    required this.onTap,
    required this.event,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.only(top: 20,left: 16, right: 16),
        color: Colors.transparent,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomImageCard(
              width: customWidth(context),
              height: 120,
              imageUrl:'${event.src!.o}',
              boxShadow: [StyleTheme.boxShadow],
              borderRadius: BorderRadius.circular(12),
            ),
            Container(
              margin: EdgeInsets.only(top: 8),
              child: Text(
                '${Date.dateTime('${event.startDate}')} - ${Date.dateTime('${event.endDate}')}',
                style: TextStyle(
                  color: Color(0xff303030),
                  fontSize: 14,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 4),
              child: Text(
                '${event.eventTitle}',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.w500
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
