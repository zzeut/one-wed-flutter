import 'package:flutter/material.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/widgets/image.dart';
import 'package:onewed_app/widgets/page_indicator.dart';
import 'package:onewed_app/widgets/size.dart';

void eventCompanyImageBottomSheet(
  BuildContext context,
  // {
  // TODO
// }
) {
  PageController _pageController = PageController();

  showModalBottomSheet(
    isScrollControlled: true,
    backgroundColor: Colors.black,
    context: context,
    builder: (context) {
      return SafeArea(
        bottom: false,
        child: Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: customWidth(context),
                  height: customWidth(context),
                  child: PageView.builder(
                    controller: _pageController,
                    itemCount: 5,
                    itemBuilder: (context, index) {
                      return CustomImageCard(
                        width: customWidth(context),
                        height: customWidth(context),
                        imageUrl:
                            'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg',
                      );
                    },
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: 0,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    child: Text(
                      '신부대기실',
                      style: TextStyle(
                        color: Color(0xff222222),
                        fontSize: 16,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Stack(
                      children: [
                        Container(
                          width: customWidth(context),
                          height: 80,
                          color: Color(0xff303030),
                          child: CustomPageIndicator(
                            pageController: _pageController,
                            count: 5,
                          ),
                        ),
                        Positioned(
                          top: 0,
                          right: 32,
                          bottom: 0,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                            },
                            child: Container(
                              alignment: Alignment.center,
                              child: Image.asset(iconCancel),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    },
  );
}

void eventCompanyReviewImageBottomSheet(
  BuildContext context,
  // {
  // TODO
// }
) {
  PageController _pageController = PageController();

  showModalBottomSheet(
    isScrollControlled: true,
    backgroundColor: Colors.black,
    context: context,
    builder: (context) {
      return SafeArea(
        bottom: false,
        child: Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: customWidth(context),
                  height: customWidth(context),
                  child: PageView.builder(
                    controller: _pageController,
                    itemCount: 5,
                    itemBuilder: (context, index) {
                      return CustomImageCard(
                        width: customWidth(context),
                        height: customWidth(context),
                        imageUrl:
                            'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg',
                      );
                    },
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: 0,
              child: Stack(
                children: [
                  Container(
                    width: customWidth(context),
                    height: 80,
                    color: Color(0xff303030),
                    child: CustomPageIndicator(
                      pageController: _pageController,
                      count: 5,
                    ),
                  ),
                  Positioned(
                    top: 0,
                    right: 32,
                    bottom: 0,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        alignment: Alignment.center,
                        child: Image.asset(iconCancel),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    },
  );
}
