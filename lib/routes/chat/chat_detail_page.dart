import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_laravel_echo/flutter_laravel_echo.dart';
import 'package:image_picker/image_picker.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/message_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/broadcast_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/chat/chat_image_detail_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/utils/common.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:url_launcher/url_launcher.dart';

class ChatDetailPage extends StatelessWidget{
  final String threadId;
  final String userName;

  ChatDetailPage(this.threadId, this.userName, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View(this.threadId, this.userName);

  }
  
}

class _View extends BaseRoute {
  final String threadId;
  final String userName;

  _View(this.threadId, this.userName);

  @override
  __ViewState createState() => __ViewState(this.threadId, this.userName);
}

class __ViewState extends BaseRouteState {
  final String threadId;
  final String userName;

  __ViewState(this.threadId, this.userName);

  late Echo echo;

  late BroadCastProvider _broadCastProvider;
  RefreshController refreshController = new RefreshController(initialRefresh: false);

  bool _hasMore = true;
  int? lastMessageNo;

  TextEditingController _textEditingController = TextEditingController();

  ImagePicker picker = new ImagePicker();

  UserProvider? _userProvider;
  User? _me;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);
    this.onRefresh();
    _connectSocket();
  }

  void _connectSocket() async {

    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String? token = _prefs.getString('accessToken');

    Map<String, dynamic> options = {
      'broadcaster': 'socket.io',
      'client': IO.io,
      'auth': {
        'headers': {
          'Accept': 'application/json',
          'Authorization': 'Bearer $token'
        }
      },
      'authEndpoint': '/api/v1/broadcast/auth',
      'host': "${dotenv.env['SOCKET_BASE_URL']}",
    };
    echo = Echo(options);
    echo.join('chat.${threadId}').here((users) {
      print('here');
      print(users);
    }).joining((user) {
      print(user);
    }).leaving((user) {
      print('leaving tqtq');
      print(user);
    }).listen('SendMessage', (e) {
      print('sendMEssage');
      print(e['data']);
      e['data']['is_mine'] = _me!.userNo == e['data']['user_no'];
      Message message = Message.fromJson(e['data']);
      print('message :: ');
      print(message.toString());
      _broadCastProvider.addMessage(message);
    });

    // Accessing socket instance
    echo.socket.on('connect', (_) => print('connected'));
    echo.socket.on('disconnect', (_) {
      //chat disconnect function


    });
  }

  @override
  void dispose() async{
    // TODO: implement dispose
    echo.leave("chat.${threadId}");
    echo.leaveChannel("chat.${threadId}");
    echo.disconnect();
    await _broadCastProvider.leaveChannel(threadId);

    _broadCastProvider.resetMessageList();
    _broadCastProvider.selectChat(threadId).then((value){
      if(value!= null){
        _broadCastProvider.updateChatList(value);
      }
    });

    indicator.hide();

    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
    _broadCastProvider = Provider.of<BroadCastProvider>(context, listen: false);
    _userProvider = Provider.of<UserProvider>(context);
    _me = _userProvider!.getMe();

    return PlatformScaffold(
      resizeToAvoidBottomInset: true,
      title: "${this.userName}",
      body: SafeArea(
        bottom: false,
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 100),
              child: Consumer<BroadCastProvider>(
                builder: (context, broadcastModel, child) {

                  List<Message> _messageList = broadcastModel.getMessageList();
                  int prevUserNo = -1;
                  int prevMinute = -1;
                  Message? prevMessage;
                  if (_messageList.length > 0) {
                    lastMessageNo = _messageList.last.messageNo;
                  }


                  List<Widget> _widgetList = [];
                  _messageList.forEach((element) {

                    bool isFirst = false;
                    if (element.userNo != prevUserNo) {
                      isFirst = true;
                      prevUserNo = element.userNo!;
                    }
                    DateTime dateTime = DateTime.parse(element.createdAt!);
                    if (dateTime.minute != prevMinute) {
                      isFirst = true;
                      prevMinute = dateTime.minute;
                    }

                    _widgetList.add(chatItemListWidget(element, isFirst));

                    if (prevMessage != null) {
                      DateTime prevDateTime = DateTime.parse(prevMessage!.createdAt!);

                      if (dateTime.day != prevDateTime.day) {
                        String convertedDate = "${prevDateTime.year.toString()}.${prevDateTime.month.toString().padLeft(2,'0')}.${prevDateTime.day.toString().padLeft(2,'0')}";
                        _widgetList.add(chatDayItemWidget(convertedDate));
                        // prevDateTime = dateTime;
                      }
                    }

                    prevMessage = element;
                  });
                  return RefreshConfiguration(
                    // enableLoadingWhenNoData: false,
                    // footerTriggerDistance: 200,
                    child: SmartRefresher(
                      controller: refreshController,
                      onLoading: this.onLoading,
                      onRefresh: this.onRefresh,
                      enablePullDown: false,
                      enablePullUp: true,
                      footer: CustomSmartRefresher.customFooter(),
                      header: CustomSmartRefresher.customHeader(),
                      // reverse: false,

                      child: ListView(
                        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                        reverse: true,
                        shrinkWrap: true,
                        children: _widgetList
                      ),
                    ),
                  );
                },
              ),
            ),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                height: 100,
                child: Container(
                  padding: EdgeInsets.only(left: 14, right: 16, top: 10, bottom: 10),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                          top: BorderSide(
                              width: 1,
                              color: ColorTheme.grayLevel1
                          )
                      )
                  ),
                  child: Row(
                    children: [
                      CustomContainer(
                        onPressed: sendImageMessage,
                        child: Image.asset("assets/icons/icon-img_32.png"),
                      ),
                      SizedBox(width: 24,),
                      Expanded(child: Container(
                        child: TextField(
                          controller: _textEditingController,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: ColorTheme.darkDarkBlack
                                  )
                              ),
                              hintText: '메세지를 입력해주세요',
                              contentPadding: EdgeInsets.only(left: 16, right: 20, top: 20, bottom: 20),
                              suffixIcon: CustomContainer(
                                onPressed: sendMessage,
                                margin: EdgeInsets.only(right: 5),
                                child: Image(
                                  image: AssetImage('assets/icons/icon-send_24.png'),
                                ),
                              ),
                              enabledBorder:  new OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: ColorTheme.darkDarkBlack
                                  )
                              ),
                              focusedBorder:  new OutlineInputBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(12)),
                                  borderSide: new BorderSide(
                                      width: 1,
                                      color: ColorTheme.darkDarkBlack
                                  )
                              )
                          ),
                        ),
                      )
                      ),
                    ],
                  ),
                )
            )
          ],
        ),
      )
    );
  }
  Widget chatListWidget(){
    List<Widget> list = [];

     list.add(Container(
       margin: EdgeInsets.only(top: 20),
       decoration: BoxDecoration(
         border: Border(
           top: BorderSide(
             width: 1,
             color: Color.fromRGBO(239, 239, 239, 1.0)
           )
         )
       ),
       child: Center(child: Text("2021.07.23", style: TextStyleTheme.darkBlackS12W400,))
     ));


    return ListView(
      children: list,
    );
  }

  Widget chatDayItemWidget(String date) {
    return Container(
        margin: EdgeInsets.only(top: 20),
        padding: EdgeInsets.only(top: 10),
        decoration: BoxDecoration(
            border: Border(
                top: BorderSide(
                    width: 1,
                    color: Color.fromRGBO(239, 239, 239, 1.0)
                )
            )
        ),
        child: Center(child: Text("${date}", style: TextStyleTheme.darkBlackS12W400,))
    );
  }

  Widget chatItemListWidget(Message message, isFirst){
    bool isImage = false;
    if (message.body!['image'] != null) {
      isImage = true;
    }
    if (isImage) {
      List<Widget> columnList = [];
      message.body!['image'].forEach((element) {
        columnList.add(chatItemWidget(message, isFirst, element['src']['s']));
        if (isFirst) {
          isFirst = false;
        }
      });
      return Column(
        crossAxisAlignment: message.isMine == true ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: columnList,
      );
    } else {
      return chatItemWidget(message, isFirst, null);
    }

  }
  Widget chatItemWidget(Message message, isFirst, String? imageSrc){
    String? userProfileSrc;
    if (message.user!.profilePath != null) {
      userProfileSrc = message.user!.profilePath!.s;
    }

    return Container(
      margin: EdgeInsets.only(top: 20),
      width: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: message.isMine == true ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: [
          if(message.isMine == true && isFirst)
            Container(
              margin: EdgeInsets.only(right: 8),
              child: Text("${messageDateTime(message.createdAt!)}", style: TextStyleTheme.darkLightS12W400,),
            ),
          if(!(message.isMine == true) && isFirst)
           CustomContainer(
             onPressed: (){
               showProfile(message);
             },
             child:  Container(
               margin: EdgeInsets.only(right: 10),
               width: 40,
               height: 40,
               child: ClipRRect(
                 borderRadius: BorderRadius.circular(10),
                 child: profileWidget(message.user!),
               ),
             ),
           ),
          if(!(message.isMine == true) && !isFirst)
            Container(
              width: 40,
              margin: EdgeInsets.only(right: 10),
            ),
          imageSrc == null ? chatMessageWidget(message, isFirst) : chatImageMessageWidget(imageSrc),
          if(!(message.isMine == true))
            Container(
              margin: EdgeInsets.only(left: 8),
              child: Text("${messageDateTime(message.createdAt!)}", style: TextStyleTheme.darkLightS12W400,),
            ),

        ],
      ),
    );
  }

  Widget chatImageMessageWidget(String src) {
    return  CustomContainer(
      onPressed: (){
        this.navigator!.pushRoute(ChatImageDetailPage(src: src));
      },
      width: 200,
      height: 200,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(12),
        child: Image.network("${src}", fit: BoxFit.fill,),
      ),
    );
  }

  Widget chatMessageWidget(Message message, bool isFirst){
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 220),
      child: Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
            border: Border.all(
                color: ColorTheme.primaryColor,
                width: 1
            ),
            borderRadius: BorderRadius.only(
                topRight: (message.isMine == true) && isFirst ? Radius.circular(0) : Radius.circular(10),
                topLeft: !(message.isMine == true) && isFirst ? Radius.circular(0) : Radius.circular(10),
                bottomLeft:Radius.circular(10),
                bottomRight:Radius.circular(10)
            ),
            color: (message.isMine == true) ? ColorTheme.primaryColor :  Colors.white
        ),
        child: Text("${message.body!['message']}", style: (message.isMine == true) ? TextStyleTheme.whiteS16W400 : TextStyleTheme.blackS16W400,),
      ),
    );
  }

  sendImageMessage() async {
    await picker.pickMultiImage().then((response) async{
      if(response != null){
        List<File> imageFileList= [];
        response.forEach((element) {
          imageFileList.add(File(element.path));
        });
        _broadCastProvider.sendMessage(threadId, null, imageFileList);
      }
    }).catchError((onError){
      print(onError);
    });
  }

  sendMessage() {

    if (_textEditingController.text.isEmpty) {
      BotToast.showText(text: "메시지 내용을 입력해주세요.");
    } else {
      indicator.show();
      this._broadCastProvider.sendMessage(this.threadId, _textEditingController.text, null).then((value) {
        indicator.hide();
        _textEditingController.text = '';
      });
    }
  }


  onRefresh() async {
    _hasMore = true;

    await onLoading();
    refreshController.refreshCompleted();
  }

  onLoading() async{
    if(_hasMore){
      _broadCastProvider.selectMessageList(this.threadId, this.lastMessageNo).then((response){
        if(response!.length == 0){
          _hasMore = false;
        }
      }).catchError((e) {
        print(e);
      });

      refreshController.loadComplete();
    }
    refreshController.loadComplete();
  }


  void showProfile(Message message){
    print(message.user!.userNo);

    if(message.user != null){

      User user = message.user!;

      showDialog(
          context: context,
          builder: (context) => AlertDialog(
            insetPadding:
            EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
            clipBehavior: Clip.antiAliasWithSaveLayer,
            contentPadding:
            EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 16),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0))),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    CustomContainer(
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      child: Image.asset("assets/icons/icon_cancel_darkgray_32.png", width: 24,),
                    )
                  ],
                ),
                Container(
                  width: 80,
                  height: 80,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: profileWidget(user),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text("${user.name}", style: TextStyleTheme.blackS16W700),
                Text("${user.firstPhone}-${user.secondPhone}-${user.lastPhone}",style: TextStyleTheme.darkGrayS16W400,),
                SizedBox(
                  height: 20,
                ),
                CustomContainer(
                  width: double.infinity,
                  height: 60,
                  onPressed: () async {
                    launch("tel://${user.firstPhone}${user.secondPhone}${user.lastPhone}");
                  },
                  backgroundColor: ColorTheme.primaryColor,
                  borderRadius: [6, 6, 6, 6],
                  child: Center(
                    child: Text(
                      "전화걸기",
                      style: TextStyleTheme.whiteS16W700,
                    ),
                  ),
                )
              ],
            ),
          ));
    }
  }

  Widget profileWidget(User user) {
    if (user.profilePath == null) {
      if (user.gender == "1") {
        return Image(image: AssetImage("assets/images/profile_man.png"));
      } else if (user.gender == "2") {
        return Image(image: AssetImage("assets/images/profile_woman.png"));
      }
    } else {
      return Image.network(user.profilePath!.o, fit: BoxFit.cover,);
    }

    return Image(image: AssetImage("assets/images/profile_default.png"));
  }

}
