import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/themes/color_theme.dart';

class ChatImageDetailPage extends StatelessWidget{
  String src;
  ChatImageDetailPage({Key? key, required this.src}):super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return View(this.src);
  }


}

class View extends BaseRoute {

  String src;
  View(this.src);

  @override
  _ViewState createState() => _ViewState(this.src);
}

class _ViewState extends BaseRouteState {

  String src;
  _ViewState(this.src);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return PlatformScaffold(
      backgroundColor: Colors.black,
      body: SafeArea(

        child: Column(
          children: [
            Expanded(
              child: Container(
                width: double.infinity,
                child: FadeInImage.assetNetwork(
                  placeholder: 'assets/images/skeleton-loader.gif',
                  image: src,
                  fit: BoxFit.fill,
                ),
              )
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 28),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: ColorTheme.darkDark
              ),
              height: 80,
              child: Stack(
                alignment: Alignment.centerRight,
                children: [
                  // CustomPageIndicator(pageController: pageController, count: reviewImageList.length),
                  Positioned(
                      child: IconButton(
                          onPressed: (){
                            this.navigator!.popRoute(null);
                          },
                          icon: Image.asset("assets/icons/3.0x/icon_cancel.png", width: 24,)
                      )
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


