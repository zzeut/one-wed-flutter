import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/models/chat_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/broadcast_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/chat/chat_detail_page.dart';
import 'package:onewed_app/routes/setting/setting_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/utils/common.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';


class ChatPage extends StatelessWidget{
  bool? isBack;
  ChatPage({this.isBack});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View(isBack);
  }

}

class _View extends BaseRoute {
  bool? isBack;
  _View(this.isBack);

  @override
  __ViewState createState() => __ViewState(isBack);
}

class __ViewState extends BaseRouteState {
  bool? isBack;
  __ViewState(this.isBack);


  BroadCastProvider? _broadCastProvider;
  int _page = 0;
  bool _hasMore = true;
  RefreshController refreshController = new RefreshController(initialRefresh: false);

  UserProvider? _userProvider;
  User? me;


  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);
    this.onRefresh();
  }


  @override
  Widget build(BuildContext context) {
    _broadCastProvider = Provider.of<BroadCastProvider>(context, listen: false);


    _userProvider = Provider.of<UserProvider>(context, listen: false);
    me = _userProvider!.getMe();


    return Scaffold(
      appBar: AppBar(
        title: Text('웨딩챗', style: TextStyle( color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold,),),
        centerTitle: true,
        automaticallyImplyLeading:false,
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(1),
          child: Container(
            height: 1,
            color: Color(0xffECECEC),
          ),
        ),
        leading: isBack != null && isBack! ? GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Image.asset(
            iconArrowLeft,
          ),
        ): Container(),
      ),
      body: Builder(
        builder: (context){
          return WillPopScope(
            onWillPop: this.onWillPop,
            child: Consumer<BroadCastProvider>(builder: (context, chatModel, child) {
              List<Chat> list = chatModel.getChatList();

              List<Widget> widgetList = [];
              list.forEach((element) {
                widgetList.add(chatItemWidget(element));
              });
              return  SafeArea(
                child: RefreshConfiguration(
                  child: SmartRefresher(
                    controller: refreshController,
                    onLoading: this.onLoading,
                    onRefresh: this.onRefresh,
                    enablePullDown: true,
                    enablePullUp: true,
                    footer: CustomSmartRefresher.customFooter(),
                    header: CustomSmartRefresher.customHeader(),
                    child: ListView(
                      children: [
                        if(list.length ==0)
                          emptyChatListWidget()
                        else
                          Column(
                            children: widgetList,
                          )
                       /* Consumer<BroadCastProvider>(
                          builder: (context, chatModel, child) {
                            List<Chat> list = chatModel.getChatList();


                            if(_page == 1 && chatModel.isLoading()){
                              return Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CustomLoading(margin: EdgeInsets.zero)
                                ],
                              );
                            }


                            if (list.length == 0) {
                              return emptyChatListWidget();
                            }

                            return Column(
                              children: widgetList,
                            );
                          },
                        )*/
                      ],
                    ),
                  ),
                ),
              );
            },)
          );
        },
      )
    );
  }

  Widget emptyChatListWidget(){
    return Container(
      height: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
            child: Image.asset("assets/images/empty-chat.png"),
          ),
          SizedBox(height: 30,),
          Text("플래너와의 대화목록이 없습니다.", style: TextStyleTheme.darkDarkS18W700,),
          SizedBox(height: 8,),
          Text("빛나는 웨딩을 위해서", style: TextStyleTheme.darkGrayS16W400,),
          Text("1:1 맞춤 웨딩 플래닝을 받아보세요.", style: TextStyleTheme.darkGrayS16W400),
        ],
      ),
    );
  }

  Widget chatItemWidget(Chat chat){
    print(chat.users);
    String lastMessage = '';
    if (chat.lastMessage != null) {
      if (chat.lastMessage!.body != null) {
        if (chat.lastMessage!.body!['message'] == null) {
          lastMessage = '사진';
        } else {
          lastMessage = chat.lastMessage!.body!['message'];
        }
      }
    }
    String? userProfileSrc;

    if(chat.users!.length > 0){
      if (chat.users!.length > 0 && chat.users![0].profilePath != null) {
        userProfileSrc = chat.users![0].profilePath!.s;
      }
      return CustomContainer(
        onPressed: () async {
          await this.navigator!.pushRoute(ChatDetailPage(chat.threadId!, chat.users![0].name ?? ''));
        },
        padding: EdgeInsets.symmetric(vertical: 24, horizontal: 16),
        backgroundColor: chat.newMessageCount! > 0 ? Color.fromRGBO(254, 249, 249, 1.0) : Colors.white,
        child: Row(
          children: [
            Container(
              width: 48,
              height: 48,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: profileWidget(chat.users![0]),
              ),
            ),
            SizedBox(width: 8,),
            Expanded(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("${chat.users![0].name}", style: TextStyleTheme.blackS16W500,),
                        Text(chatDateTime(chat.createdAt!), style: TextStyleTheme.darkLightS12W400,)
                      ],
                    ),

                    SizedBox(height: 4,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                            child: Container(
                              child: Text(
                                lastMessage,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            )
                        ),
                        SizedBox(width: 7,),
                        chat.newMessageCount! > 0 ?
                        Container(
                          decoration: BoxDecoration(
                              color: ColorTheme.primaryColor,
                              borderRadius: BorderRadius.all(Radius.circular(4))
                          ),
                          width: 19,
                          height: 18,
                          child: Center(
                            child: Text("${chat.newMessageCount}", style: TextStyleTheme.whiteS14W700,),
                          ),
                        ): Container(
                          width: 19,
                          height: 18,
                        )

                      ],
                    )
                  ],
                )
            )
          ],
        ),
      );
    }else{
      return Container();
    }


  }


  onRefresh() async {
    _page = 0;
    _hasMore = true;

    await onLoading();
    refreshController.refreshCompleted();
  }

  onLoading() async{

    if(_hasMore){
      indicator.show();
      _page++;
      _broadCastProvider!.selectChatList(_page).then((response){
        if(response!.length == 0){
          _hasMore = false;
        }
        indicator.hide();
      });

      refreshController.loadComplete();
    }
    refreshController.loadComplete();
  }

  Widget profileWidget(User user) {
    if (user.profilePath == null) {
      if (user.gender == "1") {
        return Image(image: AssetImage("assets/images/profile_man.png"));
      } else if (user.gender == "2") {
        return Image(image: AssetImage("assets/images/profile_woman.png"));
      }
    } else {
      return Image.network(user.profilePath!.o, fit: BoxFit.cover,);
    }

    return Image(image: AssetImage("assets/images/profile_default.png"));
  }

}
