import 'package:flutter/cupertino.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/term_model.dart';
import 'package:onewed_app/providers/term_provider.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:provider/provider.dart';

class TermPage extends StatelessWidget{
  static const route='/term';


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ChangeNotifierProvider<TermProvider>(
      create: (context) => TermProvider(),
      child: _View(),
    );
  }
  
}

class _View extends BaseRoute {
  @override
  __ViewState createState() => __ViewState();
}

class __ViewState extends BaseRouteState {

  TermProvider? _termProvider;
  Term? term;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    _termProvider!.selectTerm("1");
  }

  @override
  Widget build(BuildContext context) {
    _termProvider = Provider.of<TermProvider>(context);
    term = _termProvider!.getTerm();

    if(term == null){
      return Container();
    }

    return PlatformScaffold(
      title: "이용약관",
      body: SafeArea(
        bottom: false,
        child: Container(
          padding: EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    Html(data: term!.termContent.toString())
                  ],
                )
              ),
              SizedBox(height: 8,),
              CustomContainer(
                onPressed: (){
                  this.navigator!.popRoute(null);
                },
                backgroundColor: ColorTheme.primaryColor,
                height: 60,
                borderRadius: [12, 12, 12, 12],
                child: Center(
                  child: Text("뒤로가기", style: TextStyleTheme.whiteS16W700,),
                ),
              )
            ],
          ),
        ),
      )
    );
  }
}
