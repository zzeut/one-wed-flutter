import 'package:flutter/cupertino.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/models/onewed_information_model.dart';
import 'package:onewed_app/providers/onewed_information_provider.dart';
import 'package:onewed_app/providers/onewed_television_provider.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class CallInquiryComponent extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ChangeNotifierProvider<OnewedInformationProvider>(
      create: (context) => OnewedInformationProvider(),
      child: _View(),
    );
  }
  
}

class _View extends BaseRoute {
  @override
  __ViewState createState() => __ViewState();
}

class __ViewState extends BaseRouteState {
  OnewedInformationProvider? _onewedTelevisionProvider;
  OnewedInformation? _onewedInformation;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    _onewedTelevisionProvider!.selectOnewedInformation();
  }

  @override
  Widget build(BuildContext context) {
    _onewedTelevisionProvider = Provider.of<OnewedInformationProvider>(context);
    _onewedInformation = _onewedTelevisionProvider!.getOnewedInformation();


    if(_onewedInformation == null){
      return Container();
    }

    return Column(

      children: [
        Expanded(
          child:  Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 80,
                child: Image.asset("assets/images/call_inquiry.png"),
              ),
              SizedBox(height: 30,),
              Text("고객센터", style: TextStyleTheme.darkDarkS18W700,),
              SizedBox(height: 4,),
              Text("대표번호 : ${_onewedInformation!.phoneNumber}", style: TextStyleTheme.darkGrayS16W400,),
              Text("${_onewedInformation!.businessDay} : ${_onewedInformation!.businessTime}", style: TextStyleTheme.darkGrayS16W400,),
            ],
          )
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: CustomContainer(
            onPressed: (){
              String tel = _onewedInformation!.phoneNumber!.replaceAll(RegExp('-'), '');
              launch("tel://$tel");
            },
            height: 60,
            backgroundColor: ColorTheme.primaryColor,
            borderRadius: [12, 12, 12, 12],
            child: Center(
              child: Text("전화 하기", style: TextStyleTheme.whiteS16W700,),
            ),
          ),
        )

      ],
    );
  }
}
