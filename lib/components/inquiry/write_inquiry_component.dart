import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/platform_scaffold.dart';
import 'package:onewed_app/models/category_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/category_provider.dart';
import 'package:onewed_app/providers/inquiry_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';

class WriteInquiryComponent extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return MultiProvider(
        providers: [
          ChangeNotifierProvider<InquiryProvider>(
            create: (context) => InquiryProvider(),
          ),
        ],
      child: _View(),
    );
    return ChangeNotifierProvider<InquiryProvider>(
      create: (context) => InquiryProvider(),
      child: _View(),
    );
    return _View();
  }

}

class _View extends BaseRoute {
  @override
  __ViewState createState() => __ViewState();
}

class __ViewState extends BaseRouteState {
  CategoryProvider? _categoryProvider;
  List<Categories> _categoryList = [];

  Categories? _selectCategory;

  ImagePicker picker = new ImagePicker();
  File? imageFile;

  TextEditingController textEditingController = new TextEditingController();

  InquiryProvider? _inquiryProvider;

  UserProvider? _userProvider;
  User? me;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

  }

  @override
  Widget build(BuildContext context) {

    _categoryProvider = Provider.of<CategoryProvider>(context);
    _categoryList = _categoryProvider!.getTypeCategoryList(1);

    _inquiryProvider = Provider.of<InquiryProvider>(context);

    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();

    return PlatformScaffold(
      resizeToAvoidBottomInset: true,
      body: SafeArea(
          child: ListView(
            padding: EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 16),
            children: [
              Text("카테고리", style: TextStyleTheme.darkDarkS16W700,),
              SizedBox(height: 4,),
              CustomContainer(
                onPressed: (){
                  categoryBottomSheet();
                },
                padding: EdgeInsets.only(left: 16, top: 18, bottom: 18),
                height: 60,
                borderRadius: [12, 12, 12, 12],
                borderColor: ColorTheme.darkDarkBlack,
                borderWidth: 1,
                child: Row(
                  children: [
                    Expanded(
                        child: _selectCategory == null ?  Text("문의하실 카테고리를 선택해주세요", style: TextStyleTheme.darkLightS16W400,) : Text("${_selectCategory!.categoryName}", style: TextStyleTheme.blackS16W400,)
                    ),
                    Image(image: AssetImage("assets/icons/icon_arrow_down_m.png"), width: 60, height: 60, fit: BoxFit.cover,)
                  ],
                ),
              ),
              SizedBox(height: 20,),
              TextField(
                maxLines: 13,
                maxLength: 700,
                controller: textEditingController,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        borderSide: new BorderSide(
                            width: 1,
                            color: ColorTheme.darkDarkBlack
                        )
                    ),

                    contentPadding: EdgeInsets.only(left: 16, right: 16, top: 12, bottom: 12),
                    enabledBorder:  new OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        borderSide: new BorderSide(
                            width: 1,
                            color: ColorTheme.darkDarkBlack
                        )
                    ),
                    focusedBorder:  new OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        borderSide: new BorderSide(
                            width: 1,
                            color: ColorTheme.darkDarkBlack
                        )
                    ),
                    hintText: "문의 내용을 작성해주세요. (최소 10자 이상)",
                    hintStyle: TextStyleTheme.darkLightS16W400
                ),
              ),
              SizedBox(height: 10,),
              Text("답변이 완료되기 전에는 수정, 삭제가 가능합니다.", style: TextStyleTheme.darkGrayS14W400,),
              Text("사진은 1장까지 첨부가 가능합니다.", style: TextStyleTheme.darkGrayS14W400,),
              SizedBox(height: 20,),
              Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: CustomContainer(
                      onPressed: (){
                        imagePicker();
                      },
                      width: 88,
                      height: 88,
                      backgroundColor: ColorTheme.lightLight,
                      borderColor: ColorTheme.grayLevel1,
                      borderWidth: 1,
                      child: Image(
                        image: AssetImage("assets/icons/icon_camera_40.png"),
                      ),

                    ),
                  ),
                  if(imageFile != null)
                    Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Stack(
                          alignment: Alignment.topRight,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(12),
                              child: Container(
                                width: 88,
                                height: 88,
                                decoration: BoxDecoration(
                                    color: ColorTheme.lightLight,
                                    border: Border.all(
                                        color:  ColorTheme.grayLevel1,
                                        width: 1
                                    )
                                ),
                                child: Image.file(imageFile!, fit: BoxFit.cover),
                              ),
                            ),
                            CustomContainer(
                              onPressed: (){
                                setState(() {
                                  imageFile = null;
                                });
                              },
                              margin: EdgeInsets.only(left: 4, right: 4, top: 4),
                              backgroundColor: Colors.black.withOpacity(0.7),
                              borderRadius: [6, 6, 6, 6],
                              width: 32,
                              height: 32,
                              child: Center(
                                child: Image.asset("assets/icons/3.0x/icon_cancel.png", width: 24, height: 24,),
                              ),
                            ),

                          ],
                        )
                    ),

                ],
              ),
              SizedBox(height: 60,),
              CustomContainer(
                  onPressed: (){
                    saveInquiry();
                  },
                  height: 60,
                  backgroundColor: ColorTheme.primaryColor,
                  borderRadius: [12, 12, 12, 12],
                  child: Center(
                    child: Text("문의 하기", style: TextStyleTheme.whiteS16W700,),
                  )
              )
            ],
          )
      )
    );
  }


  void categoryBottomSheet(){
    Categories? selectCategory = _selectCategory;

    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
        ),
        builder: (BuildContext context){
          return StatefulBuilder(
            builder: (BuildContext context, setState){
              return SafeArea(
                  child: Container(
                    height: 700,
                    padding: EdgeInsets.only(top: 16, left: 16, right: 16),
                    child: Column(
                      children: [
                        Container(
                          height: 60,
                          child: Stack(
                            children: [
                              Align(
                                alignment: Alignment.center,
                                child: Text("카테고리 선택", style: TextStyleTheme.darkDarkS18W700,),
                              ),
                              Align(
                                alignment: Alignment.centerRight,
                                child: IconButton(onPressed: (){
                                  Navigator.pop(context);
                                }, icon: Image(image: AssetImage('assets/icons/icon_cancel_gy.png'),)),
                              )
                            ],
                          ),
                        ),
                        Expanded(child: ListView(
                          children: _categoryList.map((e) => CustomContainer(
                            onPressed: (){
                              setState(() {
                                selectCategory = e;
                              });


                            },
                            margin: EdgeInsets.only(top: 16),
                            height: 60,
                            width: double.infinity,
                            borderWidth: 1,
                            borderRadius: [6, 6, 6, 6],
                            borderColor: selectCategory == e ? ColorTheme.primaryColor : ColorTheme.darkDarkBlack,
                            child: Center(
                              child: Text("${e.categoryName}", style: selectCategory == e ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkDarkS16W400,),
                            ),
                          )).toList(),
                        )),
                        Container(
                          padding: EdgeInsets.only(top: 20,),
                          child: CustomContainer(
                            onPressed: (){
                              setSelectCategory(selectCategory!);

                              Navigator.pop(context);
                            },
                            height: 60,
                            borderRadius: [6, 6, 6, 6],
                            backgroundColor: ColorTheme.primaryColor,
                            child: Align(
                              alignment: Alignment.center,
                              child: Text("필터적용", style: TextStyleTheme.whiteS16W700,),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
              );
            }
          );

    });
  }

  void setSelectCategory(Categories selectCategory){
    setState(() {
      _selectCategory = selectCategory;
    });

  }

  void imagePicker() async{

    await picker.pickImage(source: ImageSource.gallery).then((response) async{
      if(response != null){
        imageFile = File(response.path);
        setState(() {

        });
      }

    }).catchError((onError){
      print(onError);
    });

  }

  void saveInquiry(){
    if(_selectCategory == null){
      BotToast.showText(text: "문의하실 카테고리를 선택해주세요");
    } else if(textEditingController.text.length <10){
      BotToast.showText(text: "10자 이상의 문의 내용을 작성해주세요.");
    }else{
      indicator.show();
      Map map = {
        "category_no": _selectCategory!.categoryNo!.toString(),
        "question_content": textEditingController.text
      };

      _inquiryProvider!.insertInquiry(map, imageFile).then((response){
        indicator.hide();
        if(response!=null){

          Map<String, String> map = {
            "user_no": "${me!.userNo}",
            "more_field": "*",
            "sort[by]":"created_at",
            "sort[order]":"desc",
            "page": "1"
          };

          _inquiryProvider!.selectInquiryList(1, map).then((value) {
            BotToast.showText(text: "문의사항 작성이 완료되었습니다.");
            this.navigator!.popRoute(null);
          });

        }else{
          BotToast.showText(text: "문의사항 작성중 오류가 발생했습니다. 잠시 후 시도해주세요");
        }

      }).catchError((onError){
        print(onError);
        BotToast.showText(text: "문의사항 작성중 오류가 발생했습니다. 잠시 후 시도해주세요");
      });

    }
  }
}
