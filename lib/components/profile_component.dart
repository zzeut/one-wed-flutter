import 'package:flutter/material.dart';
import 'package:onewed_app/models/user_model.dart';

// ignore: must_be_immutable
class ProfileComponent extends StatelessWidget{
  User user;
  ProfileComponent({required this.user});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    if(user.profilePath == null){
      if(user.gender == "1"){
        return Image(
            image: AssetImage("assets/images/profile_man.png")
        );
      }else if(user.gender == "2"){
        return Image(
            image: AssetImage("assets/images/profile_woman.png")
        );
      }
    }else{
      return Image.network(user.profilePath!.s, fit: BoxFit.fill,);
    }


    return Image(
        image: AssetImage("assets/images/profile_default.png")
    );
  }
}