import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/widgets/app_bar.dart';

class PlatformScaffold extends StatelessWidget {
  final Future<bool> Function()? onWillPop;
  final Function()? onPressed;
  final Function()? onTap;
  final Color? backgroundColor;
  final bool fullscreenDialog;
  final String? title;
  final PreferredSizeWidget? appBar;
  final Widget body;
  final bool resizeToAvoidBottomInset;
  final Widget? bottomNavigationBar;
  final bool? automaticallyImplyLeading;
  final Widget? leading;

  const PlatformScaffold(
      {Key? key,
      this.onWillPop,
      this.onPressed,
      this.onTap,
      this.backgroundColor,
      this.fullscreenDialog = false,
      this.title,
      this.appBar,
      required this.body,
      this.resizeToAvoidBottomInset = false,
      this.bottomNavigationBar,
      this.automaticallyImplyLeading,
      this.leading})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (kIsWeb) {
      // TODO
      return Scaffold(
        backgroundColor: backgroundColor ?? ColorTheme.backgroundColor,
        resizeToAvoidBottomInset: resizeToAvoidBottomInset,
        appBar: appBar != null
            ? appBar
            : CustomAppBar(
                onTap: onTap,
                title: title,
                fullscreenDialog: fullscreenDialog,
                automaticallyImplyLeading: automaticallyImplyLeading,
                leading: leading,
              ),
        body: GestureDetector(
          onTap: onPressed,
          child: body,
        ),
        bottomNavigationBar: bottomNavigationBar,
      );
    } else {
      return WillPopScope(
        onWillPop: onWillPop,
        child: GestureDetector(
          onTap: onPressed,
          child: Scaffold(
            backgroundColor: backgroundColor ?? ColorTheme.backgroundColor,
            resizeToAvoidBottomInset: resizeToAvoidBottomInset,
            appBar: appBar != null
                ? appBar
                : title != null
                    ? CustomAppBar(
                        onTap: onTap,
                        title: title,
                        fullscreenDialog: fullscreenDialog,
                        automaticallyImplyLeading: automaticallyImplyLeading,
                        leading: leading,
                      )
                    : null,
            body: body,
            bottomNavigationBar: bottomNavigationBar,
          ),
        ),
      );
    }
  }
}
