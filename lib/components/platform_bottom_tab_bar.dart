import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/application.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/icons.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:provider/provider.dart';

class PlatformBottomTabBar extends BaseRoute {
  final List<Widget> children;
  final List<String> pathList;
  int? currentIndex;

  PlatformBottomTabBar({
    Key? key,
    required this.children,
    required this.pathList,
    this.currentIndex
  });

  @override
  _PlatformBottomTabBarState createState() => _PlatformBottomTabBarState(this.children, this.pathList, this.currentIndex);
}

// MARK: 탭바 UI 설정

class _PlatformBottomTabBarState extends BaseRouteState {
  int _currentIndex = 0;

  final List<Widget> children;
  final List<String> pathList;
  int? currentIndex;

  _PlatformBottomTabBarState(this.children, this.pathList, this.currentIndex);

  UserProvider? _userProvider;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    if(currentIndex != null){
      setState(() {
        _currentIndex = currentIndex!;
      });
    }

  }

  @override
  Widget build(BuildContext context) {
    _userProvider = Provider.of<UserProvider>(context);

    return Scaffold(
      body: IndexedStack(
        index: _currentIndex,
        children: children,
      ),
      bottomNavigationBar: Theme(
        data: ThemeData(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
        ),
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              top: BorderSide(
                color: Color(0xffE6E6E6),
              ),
            ),
          ),
          child: BottomNavigationBar(
            currentIndex: _currentIndex,
            elevation: 0,
            backgroundColor: Colors.white,
            type: BottomNavigationBarType.fixed,
            selectedItemColor: Color(0xff444444),
            onTap: (int index) {
              if (kIsWeb) {
                // TODO
                Application.router!
                    .navigateTo(context, "${pathList[index]}");
              }

              if (_currentIndex == index) return;

              setState(() {
                _currentIndex = index;
              });
            },
            items: [
              //홈
              BottomNavigationBarItem(
                title: Container(),
                icon: Column(
                  children: [
                    Image.asset(firstTap, width: 26, height: 26),
                    Container(
                      margin: EdgeInsets.only(top: 3),
                    ),
                    Text("홈")
                  ],
                ),
                activeIcon: Column(
                  children: [
                    Image.asset(firstTapHover, width: 26, height: 26),
                    Container(
                      margin: EdgeInsets.only(top: 3),
                    ),
                    Text("홈")
                  ],
                ),
              ),
              //검색
              BottomNavigationBarItem(
                title: Container(),
                icon: Column(
                  children: [
                    Image.asset(secondTap, width: 26, height: 26),
                    Container(
                      margin: EdgeInsets.only(top: 3),
                    ),
                    Text("검색")
                  ],
                ),
                activeIcon: Column(
                  children: [
                    Image.asset(secondTapHover, width: 26, height: 26),
                    Container(
                      margin: EdgeInsets.only(top: 3),
                    ),
                    Text("검색")
                  ],
                ),
              ),
              //웨딩챗
              BottomNavigationBarItem(
                title: Container(),
                icon: Column(
                  children: [
                    Image.asset(thirdTap, width: 26, height: 26),
                    Container(
                      margin: EdgeInsets.only(top: 3),
                    ),
                    Text("웨딩챗")
                  ],
                ),
                activeIcon: Column(
                  children: [
                    Image.asset(thirdTapHover, width: 26, height: 26),
                    Container(
                      margin: EdgeInsets.only(top: 3),
                    ),
                    Text("웨딩챗")
                  ],
                ),
              ),
              //스케줄
              BottomNavigationBarItem(
                title: Container(),
                icon: Column(
                  children: [
                    Image.asset(firthTap, width: 26, height: 26),
                    Container(
                      margin: EdgeInsets.only(top: 3),
                    ),
                    Text("스케쥴")
                  ],
                ),
                activeIcon: Column(
                  children: [
                    Image.asset(firthTapHover, width: 26, height: 26),
                    Container(
                      margin: EdgeInsets.only(top: 3),
                    ),
                    Text("스케쥴")
                  ],
                ),
              ),
              //마이페이지
              BottomNavigationBarItem(
                title: Container(),
                icon: Column(
                  children: [
                    Image.asset(fifthTap, width: 26, height: 26),
                    Container(
                      margin: EdgeInsets.only(top: 3),
                    ),
                    Text("마이페이지")
                  ],
                ),
                activeIcon: Column(
                  children: [
                    Image.asset(fifthTapHover, width: 26, height: 26),
                    Container(
                      margin: EdgeInsets.only(top: 3),
                    ),
                    Text("마이페이지")
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
