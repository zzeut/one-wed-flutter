import 'package:intl/intl.dart';

class Date{
  static String date(String dateString, {bool numericDates = true}) {
    DateTime notificationDate = DateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString);

    String month = notificationDate.month < 10 ? '0${notificationDate.month}' : "${notificationDate.month}";
    String date = notificationDate.day < 10 ? '0${notificationDate.day}' : "${notificationDate.day}";

    return '${notificationDate.year}.${month}.${date}';
  }
  static String dateTime(String dateString, {bool numericDates = true}) {
    // DateTime notificationDate = DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(dateString);
    DateTime notificationDate = DateFormat("yyyy-MM-dd HH:mm:ss").parse(dateString);

    String month = notificationDate.month < 10 ? '0${notificationDate.month}' : "${notificationDate.month}";
    String date = notificationDate.day < 10 ? '0${notificationDate.day}' : "${notificationDate.month}";

    var hour = notificationDate.hour;
    String criteria = '오전';
    if(hour > 12){
      criteria = '오후';
    }

    hour = hour % 12 == 0 ? 12 : hour % 12;



    return '${notificationDate.year}.${month}.${date} ${criteria} ${hour}시';
  }


  static String weddingCeremonyDay(String weddingCeremonyYear, String weddingCeremonyMonth, String weddingCeremonyDate, {bool numericDates = true}) {
    // DateTime notificationDate = DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(dateString);
    DateTime notificationDate = DateFormat("yyyy-MM-dd").parse('$weddingCeremonyYear-$weddingCeremonyMonth-$weddingCeremonyDate');


    List<String> week = ['월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일'];
    String weekOfDay = week[notificationDate.weekday];

    print(notificationDate.weekday);

    return '$weddingCeremonyYear년 $weddingCeremonyYear월 $weddingCeremonyYear일 $weekOfDay';
  }

  static String weddingCeremonyDate(String weddingCeremonyDate, {bool numericDates = true}) {
    // DateTime notificationDate = DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(dateString);
    DateTime notificationDate = DateFormat("yyyy-MM-dd").parse('$weddingCeremonyDate');


    String year = notificationDate.year.toString();
    String month = notificationDate.month < 10 ? '0${notificationDate.month}' : "${notificationDate.month}";
    String date = notificationDate.day < 10 ? '0${notificationDate.day}' : "${notificationDate.day}";

    List<String> week = ['월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일'];
    String weekOfDay = week[notificationDate.weekday-1];

    print(notificationDate.weekday);

    return '$year년 $month월 $date일 $weekOfDay';
  }


  static String scheduleYearMonthDate(String dateString, {bool numericDates = true}) {
    DateTime notificationDate = DateFormat("yyyy-MM-dd").parse(dateString);

    String year = notificationDate.year.toString().substring(2, 4);
    String month = notificationDate.month < 10 ? '0${notificationDate.month}' : "${notificationDate.month}";
    String date = notificationDate.day < 10 ? '0${notificationDate.day}' : "${notificationDate.day}";


    return '$year.$month.$date';
  }
  static String scheduleMonthDateDay(String dateString, {bool numericDates = true}) {
    DateTime notificationDate = DateFormat("yyyy-MM-dd").parse(dateString);

    String month = notificationDate.month < 10 ? '0${notificationDate.month}' : "${notificationDate.month}";
    String date = notificationDate.day < 10 ? '0${notificationDate.day}' : "${notificationDate.day}";

    List<String> week = ['월', '화', '수', '목', '금', '토', '일'];
    String weekOfDay = week[notificationDate.weekday-1];

    return '$month.$date($weekOfDay)';
  }

  static String scheduleTime(String dateString, {bool numericDates = true}) {
    DateTime notificationDate = DateFormat("HH:mm:ss").parse(dateString);

    var hour = notificationDate.hour;
    String criteria = '오전';
    if(hour > 12){
      criteria = '오후';
    }

    hour = hour % 12 == 0 ? 12 : hour % 12;
    var min = notificationDate.minute < 10 ? "0${notificationDate.minute}" : notificationDate.minute;

    return '${criteria} ${hour}:${min}';
  }

  static String timeHourMinCol(String dateString, {bool numericDates = true}) {
    DateTime notificationDate = DateFormat("HH:mm:ss").parse(dateString);

    var hour = notificationDate.hour < 10 ? "0${notificationDate.hour}" : notificationDate.hour;

    var min = notificationDate.minute < 10 ? "0${notificationDate.minute}" : notificationDate.minute;

    return '${hour}:${min}';
  }



}