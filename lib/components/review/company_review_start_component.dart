import 'package:flutter/material.dart';
import 'package:onewed_app/models/company_review_model.dart';

class CompanyReviewStarComponent extends StatelessWidget{
  CompanyReview companyReview;
  CompanyReviewStarComponent({required this.companyReview});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    List<Widget> list = [];
    for(int i=1; i<=companyReview.score!.toInt(); i++){
      list.add(Image(
        image: AssetImage("assets/icons/3.0x/icon_star_fill.png"),
        width: 20,
        height: 20,
      ));
    }
    for(int i=1; i<=5-companyReview.score!.toInt(); i++){
      list.add(Image(
        image: AssetImage("assets/icons/3.0x/icon_star_gray.png"),
        width: 20,
        height: 20,
      ));
    }

    return Row(
      children: list,
    );
  }

}