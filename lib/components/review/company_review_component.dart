import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/components/review/company_reveiw_item_component.dart';
import 'package:onewed_app/const.dart';
import 'package:onewed_app/models/category_model.dart';
import 'package:onewed_app/models/company_review_model.dart';
import 'package:onewed_app/providers/company_review_provider.dart';
import 'package:onewed_app/routes/review/review_detail_page.dart';
import 'package:onewed_app/routes/review/review_image_detail_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CompanyReviewComponent extends StatelessWidget{
  Categories category;
  CompanyReviewComponent({Key? key,required this.category}):super(key: key);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return View(this.category);
  }

}

class View extends BaseRoute {
  Categories category;
  View(this.category);

  @override
  _ViewState createState() => _ViewState(this.category);
}

class _ViewState extends BaseRouteState {
  Categories category;
  _ViewState(this.category);


  CompanyReviewProvider? _companyReviewProvider;
  List<CompanyReview> _companyReviewList = [];

  int page = 0;
  bool hasMore = true;


  bool isOnlyPhoto = false;
  REVIEW_SORT reviewSort = REVIEW_SORT.create_desc;

  RefreshController refreshController = new RefreshController(initialRefresh: false);

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);
    onRefresh();
  }



  @override
  Widget build(BuildContext context) {
    _companyReviewProvider = Provider.of<CompanyReviewProvider>(context);
    return RefreshConfiguration(
      child: SmartRefresher(
        controller: refreshController,
        enablePullUp: true,
        enablePullDown: false,
        footer: CustomSmartRefresher.customFooter(),
        onLoading: this.onLoading,
        child: ListView(
          children: [
            SizedBox(height: 10,),
            Padding(
              padding: EdgeInsets.only(left: 18, right: 8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomContainer(
                    onPressed: (){
                      setState(() {
                        isOnlyPhoto = !isOnlyPhoto;
                      });

                      onRefresh();
                    },
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: 24,
                          height: 24,
                          decoration: BoxDecoration(
                              color: isOnlyPhoto ? ColorTheme.primaryColor : ColorTheme.grayLevel1,
                              shape: BoxShape.circle
                          ),
                          child: Center(
                            child: Image.asset("assets/icons/icon_check.png"),
                          ),
                        ),
                        SizedBox(width: 8,),
                        Text("사진리뷰", style: isOnlyPhoto ? TextStyleTheme.darkDarkS14W400 : TextStyleTheme.darkLightS14W400 ,)
                      ],
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CustomContainer(
                        onPressed: (){
                          setState(() {
                            reviewSort = REVIEW_SORT.create_desc;
                            onRefresh();
                          });
                        },
                        padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                        child: Text("최신순", style: reviewSort == REVIEW_SORT.create_desc ? TextStyleTheme.primaryS14W700 : TextStyleTheme.darkLightS14W400 ,),
                      ),
                      CustomContainer(
                        onPressed: (){
                          setState(() {
                            reviewSort = REVIEW_SORT.score_desc;
                            onRefresh();
                          });
                        },
                        padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),

                        child: Text("별점 높은순", style: reviewSort == REVIEW_SORT.score_desc ? TextStyleTheme.primaryS14W700 : TextStyleTheme.darkLightS14W400 ,),
                      ),
                      CustomContainer(
                        onPressed: (){
                          setState(() {
                            reviewSort = REVIEW_SORT.score_asc;
                            onRefresh();
                          });
                        },
                        padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),

                        child: Text("별점 낮은순", style: reviewSort == REVIEW_SORT.score_asc ? TextStyleTheme.primaryS14W700 : TextStyleTheme.darkLightS14W400 ,),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Consumer<CompanyReviewProvider>(builder: (context, companyReviewModel, child){
              _companyReviewList = companyReviewModel.getCategoryCompanyReviewList(category.categoryNo!.toInt());

              if(_companyReviewList.length == 0){
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 100,),
                    Image.asset("assets/images/gray-logo.png", width: 235, height: 60, fit: BoxFit.fill,),
                    SizedBox(height: 30,),
                    Text("리뷰가 없습니다.", style: TextStyleTheme.darkDarkS18W700,),

                  ],
                );
              }

              return Column(
                children: _companyReviewList.map((e) => CompanyReviewItemComponent(
                  companyReview: e,
                  reviewImageDetail: (index){
                    this.navigator!.pushRoute(ReviewImageDetailPage(reviewImageList: e.companyReviewImageList!, index: index,));
                  },
                  reviewDetail: (){
                    this.navigator!.pushRoute(ReviewDetailPage(companyReviewNo: e.companyReviewNo!.toInt(),));
                  },
                )).toList(),
              );

            })

          ],
        ),
      )
    );
  }

  onRefresh(){
    page = 0;
    hasMore = true;

    this.onLoading();
  }

  onLoading(){
    if(hasMore){
      page = page +1;
      Map<String, String> data = {
        "page": "$page",
        "more_field":"*",
        "parent_category_no": "${category.categoryNo}"
      };


      if(isOnlyPhoto){
        data.addAll({"company_review_images_exists":"1"});
      }

      switch(reviewSort){
        case REVIEW_SORT.create_desc:
          data.addAll({"sort[by]":"created_at", "sort[order]":"desc"});
          break;
        case REVIEW_SORT.score_desc:
          data.addAll({"sort[by]":"score", "sort[order]":"desc"});
          break;
        case REVIEW_SORT.score_asc:
          data.addAll({"sort[by]":"score", "sort[order]":"asc"});
          break;
      }

      _companyReviewProvider!.selectCategoryCompanyReviewList(category.categoryNo!.toInt(), page, data).then((value){

        if(value!.length == 0){
          hasMore = false;
        }
        refreshController.loadComplete();
      });
    }else{
      refreshController.loadComplete();
    }
  }
}
