import 'package:flutter/material.dart';
import 'package:onewed_app/components/dateTime.dart';
import 'package:onewed_app/components/making_name.dart';
import 'package:onewed_app/components/profile_component.dart';
import 'package:onewed_app/components/review/company_review_start_component.dart';
import 'package:onewed_app/models/company_review_model.dart';
import 'package:onewed_app/themes/text_style_theme.dart';

class CompanyReviewProfileComponent extends StatelessWidget{
  CompanyReview companyReview;
  CompanyReviewProfileComponent({required this.companyReview});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Container(
          height: 40,
          width: 40,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: ProfileComponent(user: companyReview.user!,),
          ),
        ),
        SizedBox(width: 8,),
        Expanded(child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CompanyReviewStarComponent(companyReview: companyReview,),
            Text("${MaskingName.changeMaskingName(companyReview.user!.name!)}", style: TextStyleTheme.darkGrayS14W400,)
          ],
        )),
        Container(
          child: Text("${Date.date(companyReview.createdAt.toString())}", style: TextStyleTheme.darkLightS14W400,),
        )

      ],
    );
  }

}