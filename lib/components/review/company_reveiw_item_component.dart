import 'package:flutter/cupertino.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/dateTime.dart';
import 'package:onewed_app/components/review/company_review_profile_component.dart';
import 'package:onewed_app/models/company_review_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';

class CompanyReviewItemComponent extends StatelessWidget{

  CompanyReview companyReview;
  Function(int)? reviewImageDetail;
  Function? reviewDetail;

  CompanyReviewItemComponent({required this.companyReview, this.reviewDetail, this.reviewImageDetail});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: 16, right: 16, top: 20, bottom: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                      decoration: BoxDecoration(
                          color: ColorTheme.primaryColor,
                          borderRadius: BorderRadius.all(Radius.circular(10))
                      ),
                      child: Text("${companyReview.company!.companyName}", style: TextStyleTheme.whiteS14W700,),

                    ),
                  ),
                  SizedBox(height: 16,),
                  CustomContainer(
                    onPressed: (){
                      if(this.reviewDetail != null){
                        this.reviewDetail!();
                      }
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CompanyReviewProfileComponent(companyReview: companyReview),
                        SizedBox(height: 16,),
                        if(companyReview.content != null)
                          CustomContainer(
                            margin: EdgeInsets.only(bottom: 16),
                            child: Text(
                              "${companyReview.content}",
                              style: TextStyleTheme.blackS16W400,
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                      ],
                    ),
                  ),
                  if(companyReview.companyReviewImageList!.length > 0)
                    reviewImageWidget(companyReview),

                ],
              ),
            ),
            Container(
              height: 8,
              decoration: BoxDecoration(
                  color: ColorTheme.grayLevel1
              ),
            )
          ],
        )
      ],
    );
  }

  Widget profileWidget(User user){
    if(user.profilePath == null){
      if(user.gender == "1"){
        return Image(
            image: AssetImage("assets/images/profile_man.png")
        );
      }else if(user.gender == "2"){
        return Image(
            image: AssetImage("assets/images/profile_woman.png")
        );
      }
    }else{
      return Image.network(user.profilePath!.o);
    }


    return Image(
        image: AssetImage("assets/images/profile_default.png")
    );
  }

  Widget reviewStarsWidget(CompanyReview companyReview){
    List<Widget> list = [];
    for(int i=1; i<=companyReview.score!.toInt(); i++){
      list.add(Image(
        image: AssetImage("assets/icons/3.0x/icon_star_fill.png"),
        width: 20,
        height: 20,
      ));
    }
    for(int i=1; i<=5-companyReview.score!.toInt(); i++){
      list.add(Image(
        image: AssetImage("assets/icons/3.0x/icon_star_gray.png"),
        width: 20,
        height: 20,
      ));
    }

    return Row(
      children: list,
    );
  }


  Widget reviewImageWidget(CompanyReview companyReview){
    if(companyReview.companyReviewImageList!.length == 1){
      return CustomContainer(
        onPressed: (){
          if(reviewImageDetail != null){
            this.reviewImageDetail!(0);
          }
        },
        width: double.infinity,
        height: 160,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(12),
          child: FadeInImage.assetNetwork(
            placeholder: 'assets/images/skeleton-loader.gif',
            image: companyReview.companyReviewImageList![0].src!.m,
            fit: BoxFit.fill,
          ),
        ),
      );
    }else if(companyReview.companyReviewImageList!.length ==2){

      List<Widget> list = [];

      for(int i=0; i<companyReview.companyReviewImageList!.length; i++){
        list.add(Expanded(
          child: CustomContainer(
            onPressed: (){
              if(reviewImageDetail != null){
                this.reviewImageDetail!(i);
              }
            },
            height: 160,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12),

              child: FadeInImage.assetNetwork(
                placeholder: 'assets/images/skeleton-loader.gif',
                image: companyReview.companyReviewImageList![i].src!.m,
                fit: BoxFit.fill,
              ),
            ),
          ),
        ));

        if(i < companyReview.companyReviewImageList!.length-1 ){
          list.add(SizedBox(width: 10,));
        }
      }


      return Row(
        children: list,
      );
    }else{
      return Container(
        height: 160,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Expanded(
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: companyReview.companyReviewImageList!.length,
                  itemBuilder: (BuildContext context, int index) => CustomContainer(
                      onPressed: (){
                        print(index);
                        if(reviewImageDetail != null){
                          this.reviewImageDetail!(index);
                        }
                      },
                      height: 160,
                      width: 160,
                      margin: EdgeInsets.only(right: index == companyReview.companyReviewImageList!.length ? 0: 10),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: FadeInImage.assetNetwork(
                          placeholder: 'assets/images/skeleton-loader.gif',
                          image: companyReview.companyReviewImageList![index].src!.m,
                          fit: BoxFit.fill,
                        ),
                        /*child: Image(
                          image: NetworkImage("${companyReview.companyReviewImageList![index].src!.o}"),
                          fit: BoxFit.cover,
                        ),*/
                      )

                  ),
                ))
          ],
        ),
      );
    }
  }
}