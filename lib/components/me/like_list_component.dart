import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/company/company_image_component.dart';
import 'package:onewed_app/components/company/company_item_component.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/models/category_model.dart';
import 'package:onewed_app/models/company_like_model.dart';
import 'package:onewed_app/models/company_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/company_like_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/company/company_detail_page.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class LikeListComponent extends StatelessWidget{
  Categories category;
  LikeListComponent({Key? key, required this.category}):super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return View(this.category);
  }

}

class View extends BaseRoute {
  Categories category;
  View(this.category);

  @override
  _ViewState createState() => _ViewState(this.category);
}

class _ViewState extends BaseRouteState with AutomaticKeepAliveClientMixin {
  Categories category;
  _ViewState(this.category);

  RefreshController refreshController = new RefreshController(initialRefresh: false);

  CompanyLikeProvider? _companyLikeProvider;
  List<CompanyLike> _companyLikeList = [];

  UserProvider? _userProvider;
  User? me;

  int page = 0;
  bool hasMore = true;


  @override
  bool get wantKeepAlive => true;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    onRefresh();
  }

  @override
  Widget build(BuildContext context) {
    _companyLikeProvider = Provider.of<CompanyLikeProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();

    var columns = 2;




    return Container(

      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
      child: RefreshConfiguration(
          key: PageStorageKey("${category.categoryNo}"),
        child: SmartRefresher(

          controller: refreshController,
          onLoading: this.onLoading,
          onRefresh: this.onRefresh,
          enablePullDown: true,
          enablePullUp: true,
          header: CustomSmartRefresher.customHeader(),
          footer: CustomSmartRefresher.customFooter(),
          child: Consumer<CompanyLikeProvider>(builder: (context, companyLikeModel, child){
            _companyLikeList = companyLikeModel.getCompanyLikeList(category.categoryNo!.toInt());

            if(_companyLikeList.length == 0){
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("assets/images/gray-logo.png", width: 235, height: 60, fit: BoxFit.fill,),
                  SizedBox(height: 30,),
                  Text("찜한 업체가 없습니다.", style: TextStyleTheme.darkDarkS18W700,),
                  SizedBox(height: 8,),
                  Text("마음에 드는 업체를 찜해주세요", style: TextStyleTheme.darkGrayS16W400,),
                  SizedBox(height: 120,)
                ],
              );
            }

            return StaggeredGridView.countBuilder(
                crossAxisCount: columns,
                itemCount: _companyLikeList.length,
                crossAxisSpacing: 10,
                mainAxisSpacing: 20,
                itemBuilder: (BuildContext context, int index) => CompanyItemComponent(
                  company: _companyLikeList[index].company!,
                  companyDetail: (){
                    this.navigator!.pushRoute(CompanyDetailPage(companyNo: _companyLikeList[index].companyNo!.toInt()));
                  },
                  companyLike: (){
                    companyLike(_companyLikeList[index]);
                  },
                ),
                staggeredTileBuilder: (int index) {
                  return StaggeredTile.fit(1);
                }
            );
          }),
        )
      ),
    );
  }

  onRefresh(){
    page = 0;
    hasMore = true;
    onLoading();
    refreshController.refreshCompleted();
  }

  onLoading(){
    if(hasMore){
      page = page + 1;

      Map<String, String> data = {
        "category_no": "${category.categoryNo}",
        "user_no": "${me!.userNo}",
        "page": "$page",
      };
      _companyLikeProvider!.selectCompanyLike(category.categoryNo!.toInt(), page, data).then((response){

        if(response!.length == 0){
          hasMore = false;
        }
        refreshController.loadComplete();

      });
    }else{
      refreshController.loadComplete();
    }
  }

  void companyLike(CompanyLike companyLike){
    _companyLikeProvider!.companyLike(companyLike.companyNo!.toInt()).then((response){
      if(response!.deletedAt != null){
        companyLike.company!.companyLikeExists = false;
      }else{
        companyLike.company!.companyLikeExists = true;
      }

      if(companyLike.company!.category!.parentCategoryNo == null){
        _companyLikeProvider!.updateCompanyLike(companyLike.company!.categoryNo!.toInt(), companyLike);
      }else{
        _companyLikeProvider!.updateCompanyLike(companyLike.company!.category!.parentCategoryNo!.toInt(), companyLike);
      }


      _userProvider!.selectMe('*');
      setState(() {

      });
    });
  }
}
