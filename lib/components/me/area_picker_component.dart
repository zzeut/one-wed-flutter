import 'package:flutter/material.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';

class AreaPickerComponent extends StatelessWidget{
  String? areaCode;
  AreaPickerComponent({this.areaCode});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(

        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Stack(
            children: [
              Text("지역선택", style: TextStyleTheme.darkBlackS18W700,),
            ],
          ),


          SizedBox(height: 20,),
          Column(
            children: [
              Row(
                children: [
                  Expanded(
                      child: CustomContainer(
                        height: 60,
                        borderWidth: 1,
                        borderColor: areaCode == null ? ColorTheme.primaryColor : ColorTheme.darkDark,
                        borderRadius: [6, 6, 6, 6],
                        child: Center(
                          child: Text("전체", style: areaCode == null ?  TextStyleTheme.primaryS16W700 : TextStyleTheme.darkDarkS16W400,),
                        ),
                      )
                  ),
                  SizedBox(width: 10,),
                  Expanded(
                      child: CustomContainer(
                        height: 60,
                        borderWidth: 1,
                        borderColor: areaCode == "1100000000" ? ColorTheme.primaryColor : ColorTheme.darkDark,
                        borderRadius: [6, 6, 6, 6],
                        child: Center(
                          child: Text("서울", style: areaCode == null ?  TextStyleTheme.primaryS16W700 : TextStyleTheme.darkDarkS16W400,),
                        ),
                      )
                  ),

                ],
              ),

            ],
          ),
          SizedBox(height: 20,),
          CustomContainer(
            onPressed: (){
            },
            backgroundColor: ColorTheme.primaryColor,
            height: 60,
            borderRadius: [16, 16, 16, 16],
            child: Center(
              child: Text("선택 완료", style: TextStyleTheme.whiteS16W700,),
            ),
          )
        ],
      ),
    );
  }

}