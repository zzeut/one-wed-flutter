import 'package:flutter/cupertino.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/models/check_category_model.dart';
import 'package:onewed_app/models/check_model.dart';
import 'package:onewed_app/providers/check_provider.dart';
import 'package:onewed_app/routes/check/widget/check_widget.dart';
import 'package:onewed_app/widgets/error.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

// ignore: must_be_immutable
class CheckListComponent extends StatelessWidget{
  CheckCategory checkCategory;
  CheckListComponent({required this.checkCategory});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return _View(this.checkCategory);
  }
}

class _View extends BaseRoute {
  CheckCategory checkCategory;
  _View(this.checkCategory);

  @override
  _ViewState createState() => _ViewState(this.checkCategory);
}

class _ViewState extends BaseRouteState {
  CheckCategory checkCategory;
  _ViewState(this.checkCategory);

  CheckProvider? _checkProvider;
  bool hasMore = true;
  int page = 0;

  List<Check> _checkList = [];

  RefreshController _refreshController = new RefreshController(initialRefresh: false);

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    hasMore = true;
    page = 0;
    onLoading();

  }

  @override
  Widget build(BuildContext context) {
    _checkProvider = Provider.of<CheckProvider>(context);
    return RefreshConfiguration(
      child: SmartRefresher(
        controller: _refreshController,
        enablePullDown: false,
        enablePullUp: true,
        footer: CustomSmartRefresher.customFooter(),
        onLoading: this.onLoading,
        child: Consumer<CheckProvider>(builder: (context, checkModel, child){
          if (checkModel.isLoading) {
            return CustomLoading(
              margin: EdgeInsets.only(top: 20),
            );
          }

          if (!checkModel.isError) {
            // TODO
            return ErrorCard(
              margin: EdgeInsets.only(top: 20),
            );
          }

          if (_checkList.length > 0) {
            return ListView(
              children: List<Widget>.generate(_checkList.length, (index){
                return CheckCard(
                  onTap: () {
                    // TODO
                    _checkProvider!.userCheck(_checkList[index].checkNo!.toInt()).then((response){
                      if(response != null){
                        int index = _checkList.indexWhere((element) => element.checkNo == response.checkNo);
                        _checkList[index] = response;
                        setState(() {

                        });
                      }
                    }).catchError((e){

                    });
                  },
                  check: _checkList[index],
                );
              }),
            );
          } else {
            // TODO
            return Container();
          }
        }),
      ),
    );
  }

  onLoading(){
    print('onloading');
    if(hasMore){
      page++;
      _checkProvider!.selectCheckList({
        "check_category_no": "${checkCategory.checkCategoryNo}",
        "page": "$page"
      }).then((response){
        if(response!.length == 0){
          hasMore = false;
        }else{
          if(page == 1){
            _checkList = response;
          }else{
            _checkList.addAll(response);
          }
        }

        setState(() {

        });
      });
    }
  }
}
