import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/schedule/schedule_item_component.dart';
import 'package:onewed_app/models/event_model.dart';
import 'package:onewed_app/models/schedule_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/schdule_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/schedule/schedule_detail_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';

class CalendarScheduleListComponent extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return View();
  }

}

class View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState with AutomaticKeepAliveClientMixin{


  @override
  bool get wantKeepAlive => true;

  DateTime? selectDate;
  DateTime? focusedDate;

  Map<DateTime, List<Schedule>> _schedules = {};

  ScheduleProvider? _scheduleProvider;
  List<Schedule> _selectSchedules = [];

  UserProvider? _userProvider;
  User? me;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);


    Map<String, String> data = {
      "schedule_date": "${DateTime.now()}"
    };

    if(me!.level == 1){
      data['user_no'] = "${me!.userNo}";
    }else{
      data['writer_no'] = "${me!.userNo}";
    }

    _scheduleProvider!.selectCalendarScheduleList(data).then((value){
      if(value != null){
        setState(() {
          _selectSchedules = value[DateFormat("yyyy-MM-dd").parse('${DateTime.now()}')] ?? [];
        });
      }
    });




  }


  @override
  Widget build(BuildContext context) {
    _scheduleProvider = Provider.of<ScheduleProvider>(context);
    _schedules = _scheduleProvider!.getCalendarScheduleList();

    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();

    return ListView(
      children: [
        Container(
          height: 350,
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              Expanded(
                child: TableCalendar(
                  shouldFillViewport: true,
                  eventLoader: (day) {

                    return _schedules[DateFormat("yyyy-MM-dd").parse('$day')] ?? [];
                  },
                  // calendarFormat: CalendarFormat.twoWeeks,
                  focusedDay: focusedDate?? DateTime.now(),
                  firstDay: DateTime.utc(DateTime.now().year-10),
                  lastDay: DateTime.utc(DateTime.now().year+10),
                  locale: 'ko',
                  daysOfWeekStyle: daysOfWeekStyle(),
                  daysOfWeekHeight: 40,
                  headerStyle: headerStyle(),

                  onDaySelected: (selectedDay, focusedDay) {
                    setState(() {
                      selectDate = selectedDay;
                      _selectSchedules =  _schedules[DateFormat("yyyy-MM-dd").parse('$selectedDay')] ?? [];
                    });
                  },
                  rangeStartDay: selectDate,
                  calendarBuilders: calendarBuilders(),
                  calendarStyle: CalendarStyle(
                      weekendTextStyle: const TextStyle(
                          fontSize: 14,
                          color: Color(0xffFEC1B8),
                          fontWeight: FontWeight.w400
                      ),
                      defaultTextStyle: const TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                          fontWeight: FontWeight.w400
                      )
                  ),
                  onPageChanged: (focusedDay) {

                    Map<String, String> data = {
                      "schedule_date": "$focusedDay",
                    };

                    if(me!.level == 1){
                      data['user_no'] = "${me!.userNo}";
                    }else{
                      data['writer_no'] = "${me!.userNo}";
                    }

                    _scheduleProvider!.selectCalendarScheduleList({
                      "schedule_date": "$focusedDay",
                      "user_no": "${me!.userNo}"
                    });
                    setState(() {
                      focusedDate = focusedDay;
                      _selectSchedules =  _schedules[DateFormat("yyyy-MM-dd").parse('$focusedDay')] ?? [];
                    });
                  },
                )
              )
            ],
          ),
        ),
        SizedBox(height: 20,),
        Container(
          height: 8,
          decoration: BoxDecoration(
            color: ColorTheme.grayLevel1
          ),
        ),
        SizedBox(height: 20,),
        _selectSchedules.length == 0 ?
        Column(
          children: [
            SizedBox(height: 30,),
            Image.asset("assets/images/empty-schedule.png", width: 60, height: 60,),
            SizedBox(height: 30,),
            Text("등록된 웨딩 스케줄이 없습니다.", style: TextStyleTheme.darkDarkS16W400,)

          ],
        ) :
        Column(
          children: _selectSchedules.map((schedule) => ScheduleItemComponent(
              schedule: schedule,
              scheduleDetail: (){
                this.navigator!.pushRoute(ScheduleDetailPage(scheduleNo: schedule.scheduleNo!.toInt()));
              }
            )).toList(),
        )
      ],
    );
  }

  CalendarBuilders calendarBuilders(){
    return CalendarBuilders(
      todayBuilder: (context, day, focusedDay) {
        return Container(
          height: 40,
          width: 40,
          decoration: BoxDecoration(
            color: ColorTheme.primaryColor,
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: Column(
            children: [
              Text("${day.day}", style: TextStyleTheme.whiteS14W700,),
            ],
          ),
        );
      },

      selectedBuilder: (context, day, focusedDay) {
        return Container(
          height: 40,
          width: 40,
          decoration: BoxDecoration(
            border: Border.all(
              color: ColorTheme.primaryColor,
              width: 1,
            ),
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: Column(
            children: [
              Text("${day.day}", style: TextStyleTheme.blackS14W400,),
            ],
          ),
        );
      },
      rangeStartBuilder: (context, day, focusedDay) {

        return Container(
          height: 40,
          width: 40,
          decoration: BoxDecoration(
            border: Border.all(
              color: ColorTheme.primaryColor,
              width: 1,
            ),
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: Column(
            children: [
              Text("${day.day}", style: TextStyleTheme.blackS14W400,),
            ],
          ),
        );
      },
      holidayBuilder: (context, day, focusedDay) {
        return Container();
      },

      defaultBuilder: (context, day, focusedDay) {
        return Container(
          height: 40,
          width: 40,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text("${day.day}", style: day.weekday == 6 || day.weekday == 7 ? TextStyleTheme.primaryS14W400 :  TextStyleTheme.blackS14W400,),
            ],
          ),
        );
      },

      disabledBuilder: (context, day, focusedDay) {
        return Container(
          height: 40,
          width: 40,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text("${day.day}", style: day.weekday == 6 || day.weekday == 7 ? TextStyleTheme.primaryS14W400 :  TextStyleTheme.blackS14W400,),
            ],
          ),
        );
      },
      outsideBuilder: (context, day, focusedDay) {
        return Container(
          height: 40,
          width: 40,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text("${day.day}",
                style: day.weekday == 6 || day.weekday == 7 ? TextStyle(
                  color: Color(0xffFEC1B8),
                  fontSize:14
                ) :  TextStyle(
                    color: Color(0xffA7A7A7),
                    fontSize:14
                ),),
            ],
          ),
        );
      },
      markerBuilder: (context, day, events) {

        if(events.isNotEmpty){
          DateTime now = DateTime.now();

          return Positioned(
            bottom: 10,
            child: Container(
              width: 8,
              height: 8,
              decoration: BoxDecoration(
                color: isSameDay(now, day) ? Colors.white : ColorTheme.primaryColor,
                shape: BoxShape.circle
              ),
          ));
        }

      },
      headerTitleBuilder: (context, day) {
        print(day);
        return Center(
          child: Text("${day.month}월", style: TextStyleTheme.primaryS18W700,),
        );
      },

    );
  }


  DaysOfWeekStyle daysOfWeekStyle(){
    return DaysOfWeekStyle(
        weekendStyle: const TextStyle(
            fontSize: 16,
            color: Color(0xffFEC1B8),
            fontWeight: FontWeight.w700
        ),
        weekdayStyle: const TextStyle(
            fontSize: 16,
            color: Color(0xffA7A7A7),
            fontWeight: FontWeight.w700
        )
    );
  }



  HeaderStyle headerStyle(){
    return HeaderStyle(
      formatButtonShowsNext: false,
      formatButtonVisible: false,
      titleCentered: true,
      headerMargin: EdgeInsets.only(top: 20, )
    );
  }


}
