import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/components/schedule/schedule_item_component.dart';
import 'package:onewed_app/const.dart';
import 'package:onewed_app/models/schedule_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/schdule_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/schedule/schedule_detail_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ScheduleListComponent extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return View();
  }

}

class View extends BaseRoute {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends BaseRouteState with AutomaticKeepAliveClientMixin{
  ScheduleProvider? _scheduleProvider;
  List<Schedule> _scheduleList = [];

  UserProvider? _userProvider;
  User? me;

  int page = 0;
  bool hasMore = true;

  RefreshController refreshController = new RefreshController(initialRefresh: false);

  SCHEDULE_FILTER scheduleFilter = SCHEDULE_FILTER.all;


  @override
  bool get wantKeepAlive => true;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    onRefresh();
  }

  @override
  Widget build(BuildContext context) {
    _scheduleProvider = Provider.of<ScheduleProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();

    return RefreshConfiguration(
      child: SmartRefresher(
        controller: refreshController,
        onRefresh: this.onRefresh,
        onLoading: this.onLoading,
        header: CustomSmartRefresher.customHeader(),
        footer: CustomSmartRefresher.customFooter(),
        // enablePullDown: true,

        child: ListView(
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
              child: Row(
                children: [
                  CustomContainer(
                    onPressed: (){
                      scheduleFilter = SCHEDULE_FILTER.all;
                      onRefresh();
                    },
                    backgroundColor: scheduleFilter == SCHEDULE_FILTER.all ? ColorTheme.primaryColor : Colors.white,
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                    borderRadius: [10, 10 , 10 ,10],
                    child: Text("전체", style: scheduleFilter == SCHEDULE_FILTER.all ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
                  ),
                  CustomContainer(
                    onPressed: (){
                      scheduleFilter = SCHEDULE_FILTER.doing;
                      onRefresh();
                    },
                    backgroundColor: scheduleFilter == SCHEDULE_FILTER.doing ? ColorTheme.primaryColor : Colors.white,
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                    borderRadius: [10, 10 , 10 ,10],
                    child: Text("D-Day", style: scheduleFilter == SCHEDULE_FILTER.doing ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
                  ),
                  CustomContainer(
                    onPressed: (){
                      scheduleFilter = SCHEDULE_FILTER.done;
                      onRefresh();
                    },
                    backgroundColor: scheduleFilter == SCHEDULE_FILTER.done ? ColorTheme.primaryColor : Colors.white,
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                    borderRadius: [10, 10 , 10 ,10],
                    child: Text("완료", style: scheduleFilter == SCHEDULE_FILTER.done ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
                  ),
                  CustomContainer(
                    onPressed: (){
                      scheduleFilter = SCHEDULE_FILTER.pending;
                      onRefresh();
                    },
                    backgroundColor: scheduleFilter == SCHEDULE_FILTER.pending ? ColorTheme.primaryColor : Colors.white,
                    padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                    borderRadius: [10, 10 , 10 ,10],
                    child: Text("진행 대기", style: scheduleFilter == SCHEDULE_FILTER.pending ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
                  )
                ],
              ),
            ),
            Consumer<ScheduleProvider>(builder: (context, scheduleModel, child){
              _scheduleList = scheduleModel.getScheduleList();
              if(_scheduleList.length == 0){
                return Column(
                  children: [
                    SizedBox(height: 100,),
                    Image.asset("assets/images/empty-schedule.png", width: 60, height: 60,),
                    SizedBox(height: 30,),
                    Text("등록된 웨딩 스케줄이 없습니다.", style: TextStyleTheme.darkDarkS16W400,)

                  ],
                );
              }

              return Column(
                children: _scheduleList.map((e) =>
                    ScheduleItemComponent(
                      schedule: e,
                      scheduleDetail: (){
                        this.navigator!.pushRoute(ScheduleDetailPage(scheduleNo: e.scheduleNo!.toInt()));
                      },
                    )
                ).toList(),
              );
            })
          ],
        ),
      )
    );
  }

  onRefresh(){
    page = 0;
    hasMore = true;
    this.onLoading();
    refreshController.refreshCompleted();
  }

  onLoading(){
    if(hasMore){
      page++;
      Map<String, String> data = {
        "page": "$page",
        // "user_no": "${me!.userNo}",
        "more_field": "*",
        "sort[by]": "schedule_date",
        "sort[order]": "desc"
      };

      if(me!.level == 1){
        data['user_no'] = "${me!.userNo}";
      }else{
        data['writer_no'] = "${me!.userNo}";
      }

      switch(scheduleFilter){
        case SCHEDULE_FILTER.pending:
          data.addAll({"state":"pending"});
          break;
        case SCHEDULE_FILTER.doing:
          data.addAll({"state":"doing"});
          break;
        case SCHEDULE_FILTER.done:
          data.addAll({"state":"done"});
          break;
      }
      _scheduleProvider!.selectScheduleList(page, data).then((response){
        if(response==null && response!.length == 0){
          setState(() {
            hasMore = false;
          });

          refreshController.loadComplete();
        }
        refreshController.loadComplete();

      });
    }else{
      refreshController.loadComplete();
    }

  }

}
