import 'package:flutter/material.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/dateTime.dart';
import 'package:onewed_app/models/schedule_model.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';

// ignore: must_be_immutable
class ScheduleItemComponent extends StatelessWidget{
  Schedule schedule;
  Function scheduleDetail;
  ScheduleItemComponent({required this.schedule, required this.scheduleDetail});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    if(schedule.state == 1){
      return pendingSchedule();
    }else if(schedule.state ==2){
      return  doingSchedule();
    }else if(schedule.state == 3){
      return doneSchedule();
    }else{
       return Container();
    }
  }

  Widget pendingSchedule(){
    return CustomContainer(
      onPressed: (){
        this.scheduleDetail();
      },
      margin: EdgeInsets.only(bottom: 20, left: 16, right: 16),
      padding: EdgeInsets.all(16),
      borderRadius: [12, 12, 12, 12],
      borderColor: ColorTheme.primaryColor,
      borderWidth: 1,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 70,
                height: 24,
                decoration: BoxDecoration(
                  color: ColorTheme.primaryColor,
                  borderRadius: BorderRadius.all(Radius.circular(10))
                ),
                child: Center(child: Text("진행대기", style: TextStyleTheme.whiteS14W700,),),
              ),
              Container(
                width: 50,
                height: 24,
                decoration: BoxDecoration(
                    color: ColorTheme.primaryColor,
                    borderRadius: BorderRadius.all(Radius.circular(10))
                ),
                child: Center(child: Text("D - ${schedule.startTimeDuration!.toInt().abs()}", style: TextStyleTheme.whiteS14W700,),),
              ),
            ],
          ),
          SizedBox(height: 16,),
          Row(
            children: [
              Text("${Date.scheduleMonthDateDay(schedule.scheduleDate.toString())}", style: TextStyleTheme.darkDarkS14W400,),
              SizedBox(width: 8,),
              Text("${schedule.company!.areaCode!.region1DepthName} ${schedule.company!.areaCode!.region2DepthName}", style: TextStyleTheme.darkDarkS14W400,)
            ],
          ),
          SizedBox(height: 16,),
          Text("${schedule.scheduleName}", style: TextStyleTheme.darkDarkS18W700,),
          SizedBox(height: 4,),
          Text("${Date.scheduleTime(schedule.startTime.toString())} - ${Date.scheduleTime(schedule.endTime.toString())}", style: TextStyleTheme.darkGrayS14W400,),
        ],
      ),
    );
  }

  Widget doingSchedule(){

    return CustomContainer(
      onPressed: (){
        this.scheduleDetail();
      },
      margin: EdgeInsets.only(bottom: 20, left: 16, right: 16),
      padding: EdgeInsets.all(16),
      backgroundColor: ColorTheme.primaryColor,
      borderRadius: [12, 12, 12, 12],
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 55,
                height: 24,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10))
                ),
                child: Center(child: Text("진행중", style: TextStyleTheme.primaryS14W700,),),
              ),
              Container(
                width: 70,
                height: 24,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10))
                ),
                child: Center(child: Text("D - Day", style: TextStyleTheme.primaryS14W700,),),
              ),
            ],
          ),
          SizedBox(height: 16,),
          Row(
            children: [
              Text("${Date.scheduleMonthDateDay(schedule.scheduleDate.toString())}", style: TextStyleTheme.whiteS14W400,),
              SizedBox(width: 8,),
              Text("${schedule.company!.areaCode!.region1DepthName} ${schedule.company!.areaCode!.region2DepthName}", style: TextStyleTheme.whiteS14W400,)
            ],
          ),
          SizedBox(height: 16,),
          Text("${schedule.scheduleName}", style: TextStyleTheme.whiteS18W700,),
          SizedBox(height: 4,),
          Text("${Date.scheduleTime(schedule.startTime.toString())} - ${Date.scheduleTime(schedule.endTime.toString())}", style: TextStyleTheme.whiteS14W400,),
        ],
      ),
    );
  }

  Widget doneSchedule(){
    return CustomContainer(
      onPressed: (){
        this.scheduleDetail();
      },
      margin: EdgeInsets.only(bottom: 20, left: 16, right: 16),
      padding: EdgeInsets.all(16),
      backgroundColor: Colors.white,
      borderWidth: 1,
      borderColor: ColorTheme.lightGray,
      borderRadius: [12, 12, 12, 12],
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 55,
                height: 24,
                decoration: BoxDecoration(
                    color: ColorTheme.lightGray,
                    borderRadius: BorderRadius.all(Radius.circular(10))
                ),
                child: Center(child: Text("완료", style: TextStyleTheme.whiteS14W700,),),
              ),
              Container(
                width: 70,
                height: 24,
                decoration: BoxDecoration(
                    color: ColorTheme.lightGray,
                    borderRadius: BorderRadius.all(Radius.circular(10))
                ),
                child: Center(child: Text("D + ${schedule.endTimeDuration!.toInt().abs()}", style: TextStyleTheme.whiteS14W700,),),
              ),
            ],
          ),
          SizedBox(height: 16,),
          Row(
            children: [
              Text("${Date.scheduleMonthDateDay(schedule.scheduleDate.toString())}", style: TextStyleTheme.darkLightS14W400,),
              SizedBox(width: 8,),
              Text("${schedule.company!.areaCode!.region1DepthName ?? ''} ${schedule.company!.areaCode!.region2DepthName ?? ''}", style: TextStyleTheme.darkLightS14W400)
            ],
          ),
          SizedBox(height: 16,),
          Text("${schedule.scheduleName}", style: TextStyleTheme.lightDarkS18W700),
          SizedBox(height: 4,),
          Text("${Date.scheduleTime(schedule.startTime.toString())} - ${Date.scheduleTime(schedule.endTime.toString())}", style: TextStyleTheme.darkLightS14W400,),
        ],
      ),
    );
  }



}