import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/company/company_item_component.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/const.dart';
import 'package:onewed_app/models/company_model.dart';
import 'package:onewed_app/providers/company_like_provider.dart';
import 'package:onewed_app/providers/company_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/company/company_detail_page.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/widgets/error.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ReturnGiftComponent extends StatelessWidget{
  int categoryNo;
  ReturnGiftComponent({Key? key, required this.categoryNo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return ChangeNotifierProvider<CompanyLikeProvider>(
      create: (context) => CompanyLikeProvider(),
      child: View(this.categoryNo),
    );

  }

}

class View extends BaseRoute {
  int categoryNo;
  View(this.categoryNo);

  @override
  _ViewState createState() => _ViewState(this.categoryNo);
}

class _ViewState extends BaseRouteState   with AutomaticKeepAliveClientMixin{


  @override
  bool get wantKeepAlive => true;

  int categoryNo;
  _ViewState(this.categoryNo);

  CompanyProvider? _companyProvider;
  List<Company> _companyList = [];

  RefreshController refreshController = new RefreshController(initialRefresh: false);

  int page = 0;
  bool hasMore = true;

  COMPANY_SORT companySort = COMPANY_SORT.all;

  CompanyLikeProvider? _companyLikeProvider;
  UserProvider? _userProvider;


  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);


    onRefresh();
  }

  @override
  Widget build(BuildContext context) {
    _companyProvider = Provider.of<CompanyProvider>(context);
    _companyLikeProvider = Provider.of<CompanyLikeProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);

    return RefreshConfiguration(
      child: SmartRefresher(
        controller: refreshController,
        onRefresh: this.onRefresh,
        onLoading: this.onLoading,
        header: CustomSmartRefresher.customHeader(),
        footer: CustomSmartRefresher.customFooter(),
        enablePullUp: true,
        enablePullDown: true,

        child: CustomScrollView(
          slivers: [
            SliverList(delegate: SliverChildListDelegate([
              Container(
                padding: EdgeInsets.only(left: 8, top: 10),
                child: Row(
                  children: [
                    CustomContainer(
                      onPressed: (){
                        companySort = COMPANY_SORT.all;
                        onRefresh();
                        setState(() {

                        });
                      },
                      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                      child: Text("기본순", style: companySort == COMPANY_SORT.all ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                    ),
                    CustomContainer(
                      onPressed: (){
                        companySort = COMPANY_SORT.score_asc;
                        onRefresh();
                        setState(() {

                        });
                      },
                      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                      child: Text("찜 적은 순", style: companySort == COMPANY_SORT.score_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                    ),
                    CustomContainer(
                      onPressed: (){
                        companySort = COMPANY_SORT.score_desc;
                        onRefresh();
                        setState(() {
 
                        });
                      },
                      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                      child: Text("찜 많은 순", style: companySort == COMPANY_SORT.score_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                    ),
                  ],
                ),
              )
            ])),
            SliverPadding(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 30),
              sliver: Consumer<CompanyProvider>(builder: (context, companyModel, child){
                _companyList = _companyProvider!.getCompanyList(categoryNo);
                if(companyModel.isError){
                  return SliverList(delegate: SliverChildListDelegate([
                    ErrorCard(margin: EdgeInsets.only(top: 20))
                  ]));
                }
                
                if(page == 1 && companyModel.isLoading){
                  return SliverFillRemaining(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CustomLoading(margin: EdgeInsets.zero)
                        ],
                      )
                  );
                }

                if(_companyList.length == 0){
                  return SliverFillRemaining(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(height: 100,),
                          Image.asset("assets/images/gray-logo.png", width: 235, height: 60, fit: BoxFit.fill,),
                          SizedBox(height: 30,),
                          Text("해당 검색결과가 없습니다.", style: TextStyleTheme.darkDarkS18W700,),
                        ],
                      )
                  );
                }
                
                return SliverGrid.count(
                  crossAxisCount: 2,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 26,
                  childAspectRatio: ((MediaQuery.of(context).size.width - 42) / 2) / 210,
                  children: _companyList.map((e) => CompanyItemComponent(
                      company: e,
                      companyDetail: () async{
                        Company? company = await this.navigator!.pushRoute(CompanyDetailPage(companyNo: e.companyNo!.toInt()));
                        if(company!=null){
                          int index = _companyList.indexWhere((element) => element.companyNo == company.companyNo);
                          if(index > -1){
                            _companyList[index] = company;
                            setState(() {

                            });
                          }
                        }

                      },
                      companyLike: (){
                        companyLike(e);
                      }
                  )).toList(),
                );
              }),
            )
          ],
        ),
      )
    );
    return Container();
  }

  onRefresh(){
    page = 0;
    hasMore = true;

    onLoading();
    refreshController.refreshCompleted();
  }

  onLoading(){

    if(hasMore){
      page = page+1;

      Map<String, String> data = {
        "category_no":"$categoryNo",
        "page":"$page",
        "more_field": "*"
      };

      switch(companySort){
        case COMPANY_SORT.score_desc:
          data['sort[by]'] = "review_score";
          data['sort[order]'] = "desc";
          break;
        case COMPANY_SORT.score_asc:
          data['sort[by]'] = "review_score";
          data['sort[order]'] = "asc";
          break;
      }

      _companyProvider!.selectCompany(categoryNo, page, data).then((value) {
        if(value!.length ==0){
          hasMore = false;
        }



        refreshController.loadComplete();
      });
      refreshController.loadComplete();
    }else{
      refreshController.loadComplete();
    }

  }

  void companyLike(Company company){

    _companyLikeProvider!.companyLike(company.companyNo!.toInt()).then((response){
      if(response == null || response.deletedAt != null){
        company.companyLikeExists = false;
      }else{
        company.companyLikeExists = true;
      }
      // 내정보 새로 가져오기
      _userProvider!.selectMe('*');

      _companyProvider!.companyListUpdate(categoryNo, company);

    });

  }


}
