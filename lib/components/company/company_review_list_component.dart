import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/components/review/reveiw_item_component.dart';
import 'package:onewed_app/const.dart';
import 'package:onewed_app/models/company_review_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/providers/company_review_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/review/review_detail_page.dart';
import 'package:onewed_app/routes/review/review_image_detail_page.dart';
import 'package:onewed_app/routes/review/review_write_list_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/widgets/error.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CompanyReviewListComponent extends StatelessWidget{
  int companyNo;
  CompanyReviewListComponent({Key? key, required this.companyNo}) :super(key:key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return View(this.companyNo);
  }

}

class View extends BaseRoute {
  int companyNo;
  View(this.companyNo);

  @override
  _ViewState createState() => _ViewState(this.companyNo);
}

class _ViewState extends BaseRouteState {
  int companyNo;
  _ViewState(this.companyNo);

  CompanyReviewProvider? _companyReviewProvider;
  List<CompanyReview> companyReviewList =[];

  RefreshController refreshController = new RefreshController(initialRefresh: false);

  bool isOnlyPhoto = false;
  REVIEW_SORT reviewSort = REVIEW_SORT.create_desc;

  int page = 0;
  bool hasMore = true;

  UserProvider? _userProvider;
  User? me;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);
    onRefresh();
  }

  @override
  Widget build(BuildContext context) {
    _companyReviewProvider = Provider.of<CompanyReviewProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);
    me = _userProvider!.getMe();

    return Stack(
      children: [
        RefreshConfiguration(
            child: SmartRefresher(
              controller: refreshController,
              onLoading: this.onLoading,
              onRefresh: this.onRefresh,
              enablePullDown: true,
              enablePullUp: true,
              footer: CustomSmartRefresher.customFooter(),
              header: CustomSmartRefresher.customHeader(),
              child: CustomScrollView(
                slivers: [
                  SliverList(delegate: SliverChildListDelegate([
                    Container(
                      padding: EdgeInsets.only(top: 24, bottom: 14, left: 18, right: 8),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CustomContainer(
                            onPressed: (){
                              isOnlyPhoto = !isOnlyPhoto;
                              onRefresh();
                              setState(() {

                              });
                            },
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  width: 24,
                                  height: 24,
                                  decoration: BoxDecoration(
                                      color: isOnlyPhoto ? ColorTheme.primaryColor : ColorTheme.grayLevel1,
                                      shape: BoxShape.circle
                                  ),
                                  child: Center(
                                    child: Image.asset("assets/icons/icon_check.png"),
                                  ),
                                ),
                                SizedBox(width: 8,),
                                Text("사진리뷰", style: isOnlyPhoto ? TextStyleTheme.darkDarkS14W400 : TextStyleTheme.darkLightS14W400 ,)
                              ],
                            ),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              CustomContainer(
                                onPressed: (){
                                  reviewSort = REVIEW_SORT.create_desc;
                                  onRefresh();
                                  setState(() {

                                  });
                                },
                                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                                child: Text("최신순", style: reviewSort == REVIEW_SORT.create_desc ? TextStyleTheme.primaryS14W700 : TextStyleTheme.darkLightS14W400 ,),
                              ),
                              CustomContainer(
                                onPressed: (){
                                  reviewSort = REVIEW_SORT.score_desc;
                                  onRefresh();
                                  setState(() {

                                  });
                                },
                                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),

                                child: Text("별점 높은순", style: reviewSort == REVIEW_SORT.score_desc ? TextStyleTheme.primaryS14W700 : TextStyleTheme.darkLightS14W400 ,),
                              ),
                              CustomContainer(
                                onPressed: (){
                                  reviewSort = REVIEW_SORT.score_asc;
                                  onRefresh();
                                  setState(() {

                                  });
                                },
                                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),

                                child: Text("별점 낮은순", style: reviewSort == REVIEW_SORT.score_asc ? TextStyleTheme.primaryS14W700 : TextStyleTheme.darkLightS14W400 ,),
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  ]),),
                  SliverPadding(
                    padding: EdgeInsets.symmetric(),

                    sliver: Consumer<CompanyReviewProvider>(builder: (context, companyReviewModel, child){
                      companyReviewList = companyReviewModel.getCompanyReviewList();

                      if(companyReviewModel.isError){
                        return SliverList(delegate: SliverChildListDelegate([
                          ErrorCard(
                            margin: EdgeInsets.only(top: 20),
                          )
                        ]));
                      }



                      if(companyReviewList.length == 0){
                        return SliverFillRemaining(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(height: 30,),
                                Image.asset("assets/images/gray-logo.png", width: 235, height: 60, fit: BoxFit.fill,),
                                SizedBox(height: 30,),
                                Text("리뷰가 없습니다.", style: TextStyleTheme.darkDarkS18W700,),

                              ],
                            )
                        );
                      }


                      return SliverList(
                        delegate: SliverChildListDelegate([
                          Column(
                              children: companyReviewList.map((e) => ReviewItemComponent(
                                companyReview: e,
                                reviewImageDetail: (index){
                                  this.navigator!.pushRoute(ReviewImageDetailPage(reviewImageList: e.companyReviewImageList!, index: index,));
                                },
                                reviewDetail: (){
                                  this.navigator!.pushRoute(ReviewDetailPage(companyReviewNo: e.companyReviewNo!.toInt()));
                                },
                              )).toList()
                          )
                        ])

                      );
                    }),
                  )
                ],
              ),

            )
        ),
        if(me!.level == 1)
          Positioned(
            right: 16,
            bottom: 32,
            child: GestureDetector(
              onTap: (){
                this.navigator!.pushRoute(ReviewWriteListPage());
              },
              child: Container(
                width: 60,
                height: 60,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: ColorTheme.primaryColor
                ),
                child: Center(
                  child: Image.asset("assets/icons/icon_review_32.png", fit: BoxFit.fill, width: 32,)
                ),
              ),
            )
          )
      ],
    );
  }

  onRefresh(){
    page = 0;
    hasMore = true;

    this.onLoading();
    refreshController.refreshCompleted();
  }

  onLoading(){
    if(hasMore){
      page = page +1;
      Map<String, String> data = {
        "page": "$page",
        "more_field":"*",
        "company_no":"$companyNo"
      };

      if(isOnlyPhoto){
        data.addAll({"company_review_images_exists":"true"});
      }

      switch(reviewSort){
        case REVIEW_SORT.create_desc:
          data.addAll({"sort[by]":"created_at", "sort[order]":"desc"});
          break;
        case REVIEW_SORT.score_desc:
          data.addAll({"sort[by]":"score", "sort[order]":"desc"});
          break;
        case REVIEW_SORT.score_asc:
          data.addAll({"sort[by]":"score", "sort[order]":"asc"});
          break;
      }

      _companyReviewProvider!.selectCompanyReviewList(page, data).then((value){
        if(page == 1){
          companyReviewList = value!;
        }else{
          companyReviewList.addAll(value!);
        }



        if(value.length == 0){
          hasMore = false;
        }
        refreshController.loadComplete();
      });
    }else{
      refreshController.loadComplete();
    }
  }
}
