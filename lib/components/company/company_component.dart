import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/company/company_item_component.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/const.dart';
import 'package:onewed_app/models/category_model.dart';
import 'package:onewed_app/models/company_model.dart';
import 'package:onewed_app/providers/company_like_provider.dart';
import 'package:onewed_app/providers/company_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/company/company_detail_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/widgets/error.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CompanyComponent extends StatelessWidget {
  Categories category;

  CompanyComponent({Key? key, required this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return ChangeNotifierProvider<CompanyLikeProvider>(
      create: (context) => CompanyLikeProvider(),
      child:  _View(this.category),
    );

  }
}

class _View extends BaseRoute {
  Categories category;
  _View(this.category);

  @override
  __ViewState createState() => __ViewState(this.category);
}

class __ViewState extends BaseRouteState  with AutomaticKeepAliveClientMixin{
  Categories category;
  __ViewState(this.category);

  int? selectCategoryNo;

  COMPANY_SORT companySort = COMPANY_SORT.all;

  RefreshController refreshController = new RefreshController(initialRefresh: false);

  CompanyProvider? _companyProvider;
  List<Company> _companyList = [];

  CompanyLikeProvider? _companyLikeProvider;
  UserProvider? _userProvider;

  int page = 0;
  bool hasMore = true;


  @override
  bool get wantKeepAlive => true;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);


    if(category.childCategories!.length > 0){
      setState(() {
        selectCategoryNo = category.childCategories![0].categoryNo;
      });

    }

    page = 0;
    hasMore = true;
    this.onRefresh();

  }

  @override
  Widget build(BuildContext context) {
    _companyProvider = Provider.of<CompanyProvider>(context);
    _companyLikeProvider = Provider.of<CompanyLikeProvider>(context);

    _userProvider = Provider.of<UserProvider>(context);

    var columns = 2;


    return  RefreshConfiguration(
      child: SmartRefresher(
        controller: refreshController,
        enablePullDown: true,
        enablePullUp: true,
        onLoading: this.onLoading,
        onRefresh: this.onRefresh,
        footer: CustomSmartRefresher.customFooter(),
        header: CustomSmartRefresher.customHeader(),
        child: ListView(
          children: [
            if(category.childCategories!.length > 0 )
              Container(
                height: 40,
                margin: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                        child: ListView(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          children: category.childCategories!.map((element) => CustomContainer(
                              onPressed: (){
                                setState(() {
                                  selectCategoryNo = element.categoryNo;
                                });
                                onRefresh();
                              },
                              padding: EdgeInsets.symmetric(horizontal: 16),
                              backgroundColor: selectCategoryNo == element.categoryNo ? ColorTheme.primaryColor : Colors.white,
                              borderRadius: [10, 10, 10, 10],
                              child: Center(
                                child: Text("${element.categoryName}", style: selectCategoryNo == element.categoryNo ? TextStyleTheme.whiteS16W700: TextStyleTheme.darkGrayS16W400,),
                              )
                          )).toList(),

                        ))
                  ],
                ),
              ),
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(
                          color: ColorTheme.grayLevel1,
                          width: 1
                      )
                  )
              ),
              padding: EdgeInsets.only(left: 16),
              child: Stack(
                alignment: Alignment.centerRight,
                children: [
                  Container(
                    width: 152,
                    height: 80,
                    child: Image.network("${category.backgroundSrc!.o}", fit: BoxFit.cover,),
                  ),
                  Container(
                    width: double.infinity,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("${category.categoryName}", style: TextStyleTheme.darkDarkS16W700,),
                        SizedBox(height: 4,),
                        Text("${category.categoryContent}", style: TextStyleTheme.darkDarkS14W400,),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
                height: 48,
                margin: EdgeInsets.symmetric(horizontal: 8),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        child: ListView(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,

                          children: <Widget>[
                            CustomContainer(
                                height: 48,
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                onPressed: (){
                                  companySort = COMPANY_SORT.all;
                                  onRefresh();
                                },
                                child: Center(
                                  child: Text("기본순", style: companySort == COMPANY_SORT.all ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                )
                            ),
                            CustomContainer(
                                height: 48,
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                onPressed: (){
                                  companySort = COMPANY_SORT.like_desc;
                                  onRefresh();
                                },
                                child: Center(
                                  child: Text("찜 많은 순", style: companySort == COMPANY_SORT.like_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                )
                            ),
                            CustomContainer(
                                height: 48,
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                onPressed: (){
                                  companySort = COMPANY_SORT.like_asc;
                                  onRefresh();
                                },
                                child: Center(
                                  child: Text("찜 적은 순", style: companySort == COMPANY_SORT.like_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                )
                            ),
                            CustomContainer(
                                height: 48,
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                onPressed: (){
                                  companySort = COMPANY_SORT.score_desc;
                                  onRefresh();
                                },
                                child: Center(
                                  child: Text("별점 높은 순", style: companySort == COMPANY_SORT.score_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                )
                            ),
                            CustomContainer(
                                height: 48,
                                padding: EdgeInsets.symmetric(horizontal: 8),
                                onPressed: (){
                                  companySort = COMPANY_SORT.score_asc;
                                  onRefresh();
                                },
                                child: Center(
                                  child: Text("별점 낮은 순", style: companySort == COMPANY_SORT.score_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                )
                            ),
                          ],
                        )
                    )
                  ],
                )
            ),
            Consumer<CompanyProvider>(builder: (context, companyModel, child){
              _companyList = companyModel.getCompanyList(category.categoryNo!.toInt());
              if(page == 1){
                if(companyModel.isLoading){
                  return Padding (
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomLoading(margin: EdgeInsets.zero)
                      ],
                    ),
                  );
                }
              }


              if(_companyList.length == 0){
                return Padding (
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset("assets/images/gray-logo.png", width: 235, height: 60, fit: BoxFit.fill,),
                        SizedBox(height: 30,),
                        Text("해당 검색결과가 없습니다.", style: TextStyleTheme.darkDarkS18W700,),
                        SizedBox(height: 8,),
                        Text("다양한 웨딩 검색어를 입력해주세요", style: TextStyleTheme.darkGrayS16W400,),
                        SizedBox(height: 120,)
                      ],
                    )

                );
              }else{
                return Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Wrap(
                    children: _companyList.asMap().map((index, company) => MapEntry(index, Container(
                      margin: EdgeInsets.only(left: index%2==1 ? 10 : 0, bottom: 20),
                      child: CompanyItemComponent(
                        company: company,
                        companyDetail: (){
                          this.navigator!.pushRoute(CompanyDetailPage(companyNo: company.companyNo!.toInt()));
                        },
                        companyLike: (){
                          companyLike(company);
                        },
                      ),
                    ))).values.toList(),
                  ),
                );

                return Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: StaggeredGridView.countBuilder(
                    // physics: NeverScrollableScrollPhysics(),
                    physics: BouncingScrollPhysics(),
                    crossAxisCount: columns,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 20,
                    itemCount: _companyList.length,
                    itemBuilder: (BuildContext context, int index) => CompanyItemComponent(
                      company: _companyList[index],
                      companyDetail: (){
                        this.navigator!.pushRoute(CompanyDetailPage(companyNo: _companyList[index].companyNo!.toInt()));
                      },
                      companyLike: (){
                        companyLike(_companyList[index]);
                      },
                    ),
                    staggeredTileBuilder: (int index) {
                      return StaggeredTile.fit(1);
                    },
                  ),
                );
              }
            }),
          ],
        ),
      )
    );
    return ListView(
      children: [
        if(category.childCategories!.length > 0 )
          Container(
            height: 40,
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                    child: ListView(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children: category.childCategories!.map((element) => CustomContainer(
                          onPressed: (){
                            setState(() {
                              selectCategoryNo = element.categoryNo;
                            });
                            onRefresh();
                          },
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          backgroundColor: selectCategoryNo == element.categoryNo ? ColorTheme.primaryColor : Colors.white,
                          borderRadius: [10, 10, 10, 10],
                          child: Center(
                            child: Text("${element.categoryName}", style: selectCategoryNo == element.categoryNo ? TextStyleTheme.whiteS16W700: TextStyleTheme.darkGrayS16W400,),
                          )
                      )).toList(),

                    ))
              ],
            ),
          ),
        Container(
          width: double.infinity,
          decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(
                      color: ColorTheme.grayLevel1,
                      width: 1
                  )
              )
          ),
          padding: EdgeInsets.only(left: 16),
          child: Stack(
            alignment: Alignment.centerRight,
            children: [
              Container(
                width: 152,
                height: 80,
                child: Image.network("${category.backgroundSrc!.o}", fit: BoxFit.cover,),
              ),
              Container(
                width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("${category.categoryName}", style: TextStyleTheme.darkDarkS16W700,),
                    SizedBox(height: 4,),
                    Text("${category.categoryContent}", style: TextStyleTheme.darkDarkS14W400,),
                  ],
                ),
              )
            ],
          ),
        ),
        Container(
            height: 48,
            margin: EdgeInsets.symmetric(horizontal: 8),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                    child: ListView(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,

                      children: <Widget>[
                        CustomContainer(
                            height: 48,
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            onPressed: (){
                              companySort = COMPANY_SORT.all;
                              onRefresh();
                            },
                            child: Center(
                              child: Text("기본순", style: companySort == COMPANY_SORT.all ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                            )
                        ),
                        CustomContainer(
                            height: 48,
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            onPressed: (){
                              companySort = COMPANY_SORT.like_desc;
                              onRefresh();
                            },
                            child: Center(
                              child: Text("찜 많은 순", style: companySort == COMPANY_SORT.like_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                            )
                        ),
                        CustomContainer(
                            height: 48,
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            onPressed: (){
                              companySort = COMPANY_SORT.like_asc;
                              onRefresh();
                            },
                            child: Center(
                              child: Text("찜 적은 순", style: companySort == COMPANY_SORT.like_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                            )
                        ),
                        CustomContainer(
                            height: 48,
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            onPressed: (){
                              companySort = COMPANY_SORT.score_desc;
                              onRefresh();
                            },
                            child: Center(
                              child: Text("별점 높은 순", style: companySort == COMPANY_SORT.score_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                            )
                        ),
                        CustomContainer(
                            height: 48,
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            onPressed: (){
                              companySort = COMPANY_SORT.score_asc;
                              onRefresh();
                            },
                            child: Center(
                              child: Text("별점 낮은 순", style: companySort == COMPANY_SORT.score_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                            )
                        ),
                      ],
                    )
                )
              ],
            )
        ),
        Consumer<CompanyProvider>(builder: (context, companyModel, child){
          _companyList = companyModel.getCompanyList(category.categoryNo!.toInt());
          if(page == 1){
            if(companyModel.isLoading){
              return Padding (
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomLoading(margin: EdgeInsets.zero)
                  ],
                ),
              );
            }
          }


          if(_companyList.length == 0){
            return Padding (
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("assets/images/gray-logo.png", width: 235, height: 60, fit: BoxFit.fill,),
                    SizedBox(height: 30,),
                    Text("해당 검색결과가 없습니다.", style: TextStyleTheme.darkDarkS18W700,),
                    SizedBox(height: 8,),
                    Text("다양한 웨딩 검색어를 입력해주세요", style: TextStyleTheme.darkGrayS16W400,),
                    SizedBox(height: 120,)
                  ],
                )

            );
          }else{
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Wrap(
                children: _companyList.map((company) => Container(
                  margin: EdgeInsets.only(right: 10, bottom: 20),
                  child: CompanyItemComponent(
                    company: company,
                    companyDetail: (){
                      this.navigator!.pushRoute(CompanyDetailPage(companyNo: company.companyNo!.toInt()));
                    },
                    companyLike: (){
                      companyLike(company);
                    },
                  ),
                )).toList(),
              ),
            );

            return Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: StaggeredGridView.countBuilder(
                // physics: NeverScrollableScrollPhysics(),
                physics: BouncingScrollPhysics(),
                crossAxisCount: columns,
                crossAxisSpacing: 10,
                mainAxisSpacing: 20,
                itemCount: _companyList.length,
                itemBuilder: (BuildContext context, int index) => CompanyItemComponent(
                  company: _companyList[index],
                  companyDetail: (){
                    this.navigator!.pushRoute(CompanyDetailPage(companyNo: _companyList[index].companyNo!.toInt()));
                  },
                  companyLike: (){
                    companyLike(_companyList[index]);
                  },
                ),
                staggeredTileBuilder: (int index) {
                  return StaggeredTile.fit(1);
                },
              ),
            );
          }
        }),
      ],
    );

    return CustomScrollView(
      slivers: [
        SliverAppBar(
          pinned: true,
          automaticallyImplyLeading: false,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(category.childCategories!.length > 0 ? 170 : 130),
            child: Column(
              children: [
                if(category.childCategories!.length > 0 )
                  Container(
                    height: 40,
                    margin: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                            child: ListView(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,
                              children: category.childCategories!.map((element) => CustomContainer(
                                  onPressed: (){
                                    setState(() {
                                      selectCategoryNo = element.categoryNo;
                                    });
                                    onRefresh();
                                  },
                                  padding: EdgeInsets.symmetric(horizontal: 16),
                                  backgroundColor: selectCategoryNo == element.categoryNo ? ColorTheme.primaryColor : Colors.white,
                                  borderRadius: [10, 10, 10, 10],
                                  child: Center(
                                    child: Text("${element.categoryName}", style: selectCategoryNo == element.categoryNo ? TextStyleTheme.whiteS16W700: TextStyleTheme.darkGrayS16W400,),
                                  )
                              )).toList(),

                            ))
                      ],
                    ),
                  ),
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: ColorTheme.grayLevel1,
                              width: 1
                          )
                      )
                  ),
                  padding: EdgeInsets.only(left: 16),
                  child: Stack(
                    alignment: Alignment.centerRight,
                    children: [
                      Container(
                        width: 152,
                        height: 80,
                        child: Image.network("${category.backgroundSrc!.o}", fit: BoxFit.cover,),
                      ),
                      Container(
                        width: double.infinity,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("${category.categoryName}", style: TextStyleTheme.darkDarkS16W700,),
                            SizedBox(height: 4,),
                            Text("${category.categoryContent}", style: TextStyleTheme.darkDarkS14W400,),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                    height: 48,
                    margin: EdgeInsets.symmetric(horizontal: 8),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: ListView(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,

                              children: <Widget>[
                                CustomContainer(
                                    height: 48,
                                    padding: EdgeInsets.symmetric(horizontal: 8),
                                    onPressed: (){
                                      companySort = COMPANY_SORT.all;
                                      onRefresh();
                                    },
                                    child: Center(
                                      child: Text("기본순", style: companySort == COMPANY_SORT.all ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                    )
                                ),
                                CustomContainer(
                                    height: 48,
                                    padding: EdgeInsets.symmetric(horizontal: 8),
                                    onPressed: (){
                                      companySort = COMPANY_SORT.like_desc;
                                      onRefresh();
                                    },
                                    child: Center(
                                      child: Text("찜 많은 순", style: companySort == COMPANY_SORT.like_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                    )
                                ),
                                CustomContainer(
                                    height: 48,
                                    padding: EdgeInsets.symmetric(horizontal: 8),
                                    onPressed: (){
                                      companySort = COMPANY_SORT.like_asc;
                                      onRefresh();
                                    },
                                    child: Center(
                                      child: Text("찜 적은 순", style: companySort == COMPANY_SORT.like_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                    )
                                ),
                                CustomContainer(
                                    height: 48,
                                    padding: EdgeInsets.symmetric(horizontal: 8),
                                    onPressed: (){
                                      companySort = COMPANY_SORT.score_desc;
                                      onRefresh();
                                    },
                                    child: Center(
                                      child: Text("별점 높은 순", style: companySort == COMPANY_SORT.score_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                    )
                                ),
                                CustomContainer(
                                    height: 48,
                                    padding: EdgeInsets.symmetric(horizontal: 8),
                                    onPressed: (){
                                      companySort = COMPANY_SORT.score_asc;
                                      onRefresh();
                                    },
                                    child: Center(
                                      child: Text("별점 낮은 순", style: companySort == COMPANY_SORT.score_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                    )
                                ),
                              ],
                            )
                        )
                      ],
                    )
                ),
              ],
            ),
          ),
        ),
        SliverFillRemaining(
          child: RefreshConfiguration(
              child: SmartRefresher(
                controller: refreshController,
                enablePullDown: true,
                enablePullUp: true,
                onLoading: this.onLoading,
                onRefresh: this.onRefresh,
                footer: CustomSmartRefresher.customFooter(),
                header: CustomSmartRefresher.customHeader(),
                child: Consumer<CompanyProvider>(builder: (context, companyModel, child){
                  _companyList = companyModel.getCompanyList(category.categoryNo!.toInt());
                  if(page == 1){
                    if(companyModel.isLoading){
                      return Padding (
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CustomLoading(margin: EdgeInsets.zero)
                          ],
                        ),
                      );
                    }
                  }


                  if(_companyList.length == 0){
                    return Padding (
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset("assets/images/gray-logo.png", width: 235, height: 60, fit: BoxFit.fill,),
                            SizedBox(height: 30,),
                            Text("해당 검색결과가 없습니다.", style: TextStyleTheme.darkDarkS18W700,),
                            SizedBox(height: 8,),
                            Text("다양한 웨딩 검색어를 입력해주세요", style: TextStyleTheme.darkGrayS16W400,),
                            SizedBox(height: 120,)
                          ],
                        )

                    );
                  }else{


                    return Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: StaggeredGridView.countBuilder(
                        // physics: NeverScrollableScrollPhysics(),
                        physics: BouncingScrollPhysics(),
                        crossAxisCount: columns,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 20,
                        itemCount: _companyList.length,
                        itemBuilder: (BuildContext context, int index) => CompanyItemComponent(
                          company: _companyList[index],
                          companyDetail: (){
                            this.navigator!.pushRoute(CompanyDetailPage(companyNo: _companyList[index].companyNo!.toInt()));
                          },
                          companyLike: (){
                            companyLike(_companyList[index]);
                          },
                        ),
                        staggeredTileBuilder: (int index) {
                          return StaggeredTile.fit(1);
                        },
                      ),
                    );
                  }
                }),
              )
          ),
        )

      ],
    );

    return NestedScrollView(
        headerSliverBuilder: (context, innerBoxIsScrolled) {
          return[
            SliverToBoxAdapter(
              child: Column(
                children: [
                  if(category.childCategories!.length > 0 )
                    Container(
                      height: 40,
                      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                              child: ListView(
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                children: category.childCategories!.map((element) => CustomContainer(
                                    onPressed: (){
                                      setState(() {
                                        selectCategoryNo = element.categoryNo;
                                      });
                                      onRefresh();
                                    },
                                    padding: EdgeInsets.symmetric(horizontal: 16),
                                    backgroundColor: selectCategoryNo == element.categoryNo ? ColorTheme.primaryColor : Colors.white,
                                    borderRadius: [10, 10, 10, 10],
                                    child: Center(
                                      child: Text("${element.categoryName}", style: selectCategoryNo == element.categoryNo ? TextStyleTheme.whiteS16W700: TextStyleTheme.darkGrayS16W400,),
                                    )
                                )).toList(),

                              ))
                        ],
                      ),
                    ),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: ColorTheme.grayLevel1,
                                width: 1
                            )
                        )
                    ),
                    padding: EdgeInsets.only(left: 16),
                    child: Stack(
                      alignment: Alignment.centerRight,
                      children: [
                        Container(
                          width: 152,
                          height: 80,
                          child: Image.network("${category.backgroundSrc!.o}", fit: BoxFit.cover,),
                        ),
                        Container(
                          width: double.infinity,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("${category.categoryName}", style: TextStyleTheme.darkDarkS16W700,),
                              SizedBox(height: 4,),
                              Text("${category.categoryContent}", style: TextStyleTheme.darkDarkS14W400,),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                      height: 48,
                      margin: EdgeInsets.symmetric(horizontal: 8),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child: ListView(
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,

                                children: <Widget>[
                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.all;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("기본순", style: companySort == COMPANY_SORT.all ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.like_desc;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("찜 많은 순", style: companySort == COMPANY_SORT.like_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.like_asc;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("찜 적은 순", style: companySort == COMPANY_SORT.like_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.score_desc;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("별점 높은 순", style: companySort == COMPANY_SORT.score_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.score_asc;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("별점 낮은 순", style: companySort == COMPANY_SORT.score_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                ],
                              )
                          )
                        ],
                      )
                  ),
                ],
              ),
            )
          ];
          return [
            SliverList(
                delegate: SliverChildListDelegate([
                  if(category.childCategories!.length > 0 )
                    Container(
                      height: 40,
                      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                              child: ListView(
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                children: category.childCategories!.map((element) => CustomContainer(
                                    onPressed: (){
                                      setState(() {
                                        selectCategoryNo = element.categoryNo;
                                      });
                                      onRefresh();
                                    },
                                    padding: EdgeInsets.symmetric(horizontal: 16),
                                    backgroundColor: selectCategoryNo == element.categoryNo ? ColorTheme.primaryColor : Colors.white,
                                    borderRadius: [10, 10, 10, 10],
                                    child: Center(
                                      child: Text("${element.categoryName}", style: selectCategoryNo == element.categoryNo ? TextStyleTheme.whiteS16W700: TextStyleTheme.darkGrayS16W400,),
                                    )
                                )).toList(),

                              ))
                        ],
                      ),
                    ),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(
                                color: ColorTheme.grayLevel1,
                                width: 1
                            )
                        )
                    ),
                    padding: EdgeInsets.only(left: 16),
                    child: Stack(
                      alignment: Alignment.centerRight,
                      children: [
                        Container(
                          width: 152,
                          height: 80,
                          child: Image.network("${category.backgroundSrc!.o}", fit: BoxFit.cover,),
                        ),
                        Container(
                          width: double.infinity,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("${category.categoryName}", style: TextStyleTheme.darkDarkS16W700,),
                              SizedBox(height: 4,),
                              Text("${category.categoryContent}", style: TextStyleTheme.darkDarkS14W400,),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                      height: 48,
                      margin: EdgeInsets.symmetric(horizontal: 8),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child: ListView(
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,

                                children: <Widget>[
                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.all;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("기본순", style: companySort == COMPANY_SORT.all ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.like_desc;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("찜 많은 순", style: companySort == COMPANY_SORT.like_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.like_asc;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("찜 적은 순", style: companySort == COMPANY_SORT.like_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.score_desc;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("별점 높은 순", style: companySort == COMPANY_SORT.score_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                  CustomContainer(
                                      height: 48,
                                      padding: EdgeInsets.symmetric(horizontal: 8),
                                      onPressed: (){
                                        companySort = COMPANY_SORT.score_asc;
                                        onRefresh();
                                      },
                                      child: Center(
                                        child: Text("별점 낮은 순", style: companySort == COMPANY_SORT.score_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                      )
                                  ),
                                ],
                              )
                          )
                        ],
                      )
                  ),
                ])
            )
          ];
        },
        body:  RefreshConfiguration(
            child: SmartRefresher(
              controller: refreshController,
              enablePullDown: true,
              enablePullUp: true,
              onLoading: this.onLoading,
              onRefresh: this.onRefresh,
              footer: CustomSmartRefresher.customFooter(),
              header: CustomSmartRefresher.customHeader(),
              child: Consumer<CompanyProvider>(builder: (context, companyModel, child){
                _companyList = companyModel.getCompanyList(category.categoryNo!.toInt());
                if(page == 1){
                  if(companyModel.isLoading){
                    return Padding (
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CustomLoading(margin: EdgeInsets.zero)
                          ],
                        ),
                    );
                  }
                }


                if(_companyList.length == 0){
                  return Padding (
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset("assets/images/gray-logo.png", width: 235, height: 60, fit: BoxFit.fill,),
                          SizedBox(height: 30,),
                          Text("해당 검색결과가 없습니다.", style: TextStyleTheme.darkDarkS18W700,),
                          SizedBox(height: 8,),
                          Text("다양한 웨딩 검색어를 입력해주세요", style: TextStyleTheme.darkGrayS16W400,),
                          SizedBox(height: 120,)
                        ],
                      )

                  );
                }else{


                  return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: StaggeredGridView.countBuilder(
                      // physics: NeverScrollableScrollPhysics(),
                      physics: BouncingScrollPhysics(),
                      crossAxisCount: columns,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 20,
                      itemCount: _companyList.length,
                      itemBuilder: (BuildContext context, int index) => CompanyItemComponent(
                        company: _companyList[index],
                        companyDetail: (){
                          this.navigator!.pushRoute(CompanyDetailPage(companyNo: _companyList[index].companyNo!.toInt()));
                        },
                        companyLike: (){
                          companyLike(_companyList[index]);
                        },
                      ),
                      staggeredTileBuilder: (int index) {
                        return StaggeredTile.fit(1);
                      },
                    ),
                  );
                }
              }),
            )
        )
    );

    return RefreshConfiguration(
      child: SmartRefresher(
        controller: refreshController,
        enablePullDown: true,
        enablePullUp: true,
        onLoading: this.onLoading,
        onRefresh: this.onRefresh,
        footer: CustomSmartRefresher.customFooter(),
        header: CustomSmartRefresher.customHeader(),
        child: CustomScrollView(
          slivers: [
            SliverList(
              delegate: SliverChildListDelegate([
                if(category.childCategories!.length > 0 )
                Container(
                    height: 40,
                    margin: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: ListView(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            children: category.childCategories!.map((element) => CustomContainer(
                                onPressed: (){
                                  setState(() {
                                    selectCategoryNo = element.categoryNo;
                                  });
                                  onRefresh();
                                },
                                padding: EdgeInsets.symmetric(horizontal: 16),
                                backgroundColor: selectCategoryNo == element.categoryNo ? ColorTheme.primaryColor : Colors.white,
                                borderRadius: [10, 10, 10, 10],
                                child: Center(
                                  child: Text("${element.categoryName}", style: selectCategoryNo == element.categoryNo ? TextStyleTheme.whiteS16W700: TextStyleTheme.darkGrayS16W400,),
                                )
                            )).toList(),

                        ))
                      ],
                    ),
                ),
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: ColorTheme.grayLevel1,
                              width: 1
                          )
                      )
                  ),
                  padding: EdgeInsets.only(left: 16),
                  child: Stack(
                    alignment: Alignment.centerRight,
                    children: [
                      Container(
                        width: 152,
                        height: 80,
                        child: Image.network("${category.backgroundSrc!.o}", fit: BoxFit.cover,),
                      ),
                      Container(
                        width: double.infinity,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("${category.categoryName}", style: TextStyleTheme.darkDarkS16W700,),
                            SizedBox(height: 4,),
                            Text("${category.categoryContent}", style: TextStyleTheme.darkDarkS14W400,),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                    height: 48,
                    margin: EdgeInsets.symmetric(horizontal: 8),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                            child: ListView(
                              shrinkWrap: true,
                              scrollDirection: Axis.horizontal,

                              children: <Widget>[
                                CustomContainer(
                                    height: 48,
                                    padding: EdgeInsets.symmetric(horizontal: 8),
                                    onPressed: (){
                                      companySort = COMPANY_SORT.all;
                                      onRefresh();
                                    },
                                    child: Center(
                                      child: Text("기본순", style: companySort == COMPANY_SORT.all ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                    )
                                ),
                                CustomContainer(
                                    height: 48,
                                    padding: EdgeInsets.symmetric(horizontal: 8),
                                    onPressed: (){
                                      companySort = COMPANY_SORT.like_desc;
                                      onRefresh();
                                    },
                                    child: Center(
                                      child: Text("찜 많은 순", style: companySort == COMPANY_SORT.like_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                    )
                                ),
                                CustomContainer(
                                    height: 48,
                                    padding: EdgeInsets.symmetric(horizontal: 8),
                                    onPressed: (){
                                      companySort = COMPANY_SORT.like_asc;
                                      onRefresh();
                                    },
                                    child: Center(
                                      child: Text("찜 적은 순", style: companySort == COMPANY_SORT.like_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                    )
                                ),
                                CustomContainer(
                                    height: 48,
                                    padding: EdgeInsets.symmetric(horizontal: 8),
                                    onPressed: (){
                                      companySort = COMPANY_SORT.score_desc;
                                      onRefresh();
                                    },
                                    child: Center(
                                      child: Text("별점 높은 순", style: companySort == COMPANY_SORT.score_desc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                    )
                                ),
                                CustomContainer(
                                    height: 48,
                                    padding: EdgeInsets.symmetric(horizontal: 8),
                                    onPressed: (){
                                      companySort = COMPANY_SORT.score_asc;
                                      onRefresh();
                                    },
                                    child: Center(
                                      child: Text("별점 낮은 순", style: companySort == COMPANY_SORT.score_asc ? TextStyleTheme.primaryS16W700 : TextStyleTheme.darkLightS16W400,),
                                    )
                                ),
                              ],
                            )
                        )
                      ],
                    )
                ),
              ])
            ),

            Consumer<CompanyProvider>(builder: (context, companyModel, child){
              _companyList = companyModel.getCompanyList(category.categoryNo!.toInt());
              if(page == 1){
                if(companyModel.isLoading){
                  return SliverPadding (
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      sliver: SliverFillRemaining(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CustomLoading(margin: EdgeInsets.zero)
                            ],
                          )
                      )
                  );
                }
              }
                
              
              if(_companyList.length == 0){
                return SliverPadding (
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    sliver: SliverFillRemaining(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset("assets/images/gray-logo.png", width: 235, height: 60, fit: BoxFit.fill,),
                            SizedBox(height: 30,),
                            Text("해당 검색결과가 없습니다.", style: TextStyleTheme.darkDarkS18W700,),
                            SizedBox(height: 8,),
                            Text("다양한 웨딩 검색어를 입력해주세요", style: TextStyleTheme.darkGrayS16W400,),
                            SizedBox(height: 120,)
                          ],
                        )
                    )

                );
              }else{
                return SliverPadding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    sliver: SliverFillRemaining(
                      child: StaggeredGridView.countBuilder(
                        crossAxisCount: columns,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 20,
                        itemCount: _companyList.length,
                        itemBuilder: (BuildContext context, int index) => CompanyItemComponent(
                          company: _companyList[index],
                          companyDetail: (){
                            this.navigator!.pushRoute(CompanyDetailPage(companyNo: _companyList[index].companyNo!.toInt()));
                          },
                          companyLike: (){
                            companyLike(_companyList[index]);
                          },
                        ),
                        staggeredTileBuilder: (int index) {
                          return StaggeredTile.fit(1);
                        },
                      ),
                    ),
                  );
              }
            }),
          ],
        ),
      )
    );

  }

  Widget companyItemWidget(Company company){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Stack(
          children: [
            CustomContainer(
              width: double.infinity,
              height: 160,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: company.representativeImageSrc == null ?
                  Image.asset("assets/images/default-company.jpeg", fit: BoxFit.cover,)  :
                  FadeInImage.assetNetwork(
                    placeholder: 'assets/images/skeleton-loader.gif',
                    image: company.representativeImageSrc!.o
                ),
              ),
            ),
            CustomContainer(
              onPressed: (){
                this.navigator!.pushRoute(CompanyDetailPage(companyNo: company.companyNo!.toInt(),));
              },
              height: 160,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      CustomContainer(

                        onPressed: (){
                          companyLike(company);
                        },
                        child: company.companyLikeExists==true ? Image.asset("assets/icons/icon_haert_fill_40.png", width: 40) : Image.asset("assets/icons/icon_heart_40.png", width: 40),
                      ),
                    ],
                  ),
                  Container(
                    height: 48,
                    padding: EdgeInsets.only(left: 14, right: 8),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(12),
                        bottomRight: Radius.circular(12),
                      ),
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Color.fromRGBO(146, 146, 146, 0),
                          Color.fromRGBO(19, 19, 19, 0.55)
                        ]
                      )
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          children: [
                            Image.asset("assets/icons/3.0x/icon_star.png", width: 24),
                            SizedBox(width: 6,),
                            Text("${company.reviewScore}", style: TextStyleTheme.whiteS14W400,)
                          ],
                        ),
                        Row(
                          children: [
                            Image.asset("assets/icons/3.0x/icon_comment.png", width:15),
                            SizedBox(width: 6,),
                            Text("${company.reviewCount}", style: TextStyleTheme.whiteS14W400,)
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        SizedBox(height: 8,),
        Text("${company.areaCode!.region1DepthName} ${company.areaCode!.region2DepthName}", style: TextStyleTheme.darkDarkS14W400,),
        SizedBox(height: 4,),
        Text("${company.companyName}", style: TextStyleTheme.blackS16W400, maxLines: 1, overflow: TextOverflow.ellipsis,),
      ],
    );
  }

  onRefresh() async{
    page = 0;
    hasMore = true;
    await onLoading();
    refreshController.refreshCompleted();

  }

  onLoading(){
    if(hasMore){
      page= page +1;

      Map<String, String> map = {
        "page": "$page",
        "category_no": "${selectCategoryNo ?? category.categoryNo}",
        "more_field": "company_like"
      };

      switch(companySort){
        case COMPANY_SORT.like_desc:
          map['sort[by]'] = "like_count";
          map['sort[order]'] = "desc";
          break;
        case COMPANY_SORT.like_asc:
          map['sort[by]'] = "like_count";
          map['sort[order]'] = "asc";
          break;

        case COMPANY_SORT.score_desc:
          map['sort[by]'] = "review_score";
          map['sort[order]'] = "desc";
          break;
        case COMPANY_SORT.score_asc:
          map['sort[by]'] = "review_score";
          map['sort[order]'] = "asc";
          break;
      }

      print(map);

      _companyProvider!.selectCompany(category.categoryNo!.toInt(), page, map).then((response){
        print("_companyProvider ::: ");
        print(response!.length);
        if(response.length == 0 ){
          hasMore = false;
        }

      });

      refreshController.loadComplete();
    }else{
      refreshController.loadComplete();
    }
  }

  void companyLike(Company company){
    _companyLikeProvider!.companyLike(company.companyNo!.toInt()).then((response){
      if(response == null || response.deletedAt != null){
        company.companyLikeExists = false;
      }else{
        company.companyLikeExists = true;
      }

      _userProvider!.selectMe('*');

      _companyProvider!.companyListUpdate(category.categoryNo!.toInt(), company);

    });

  }
}
