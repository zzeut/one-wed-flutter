import 'package:flutter/cupertino.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/models/company_model.dart';
import 'package:onewed_app/themes/text_style_theme.dart';

class CompanyItemComponent extends StatelessWidget{
  Company company;
  Function() companyDetail;
  Function() companyLike;

  CompanyItemComponent({
    Key? key,
    required this.company,
    required this.companyDetail,
    required this.companyLike
  }) :super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Stack(
          children: [
            CustomContainer(
              // width: double.infinity,
              width: (MediaQuery.of(context).size.width / 2) - 21,
              // width: 160,
              height: 160,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: company.representativeImageSrc == null ?
                Image.asset("assets/images/default-company.jpeg", fit: BoxFit.cover,)  :
                FadeInImage.assetNetwork(
                  placeholder: 'assets/images/skeleton-loader.gif',
                  image: company.representativeImageSrc!.o,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              child: CustomContainer(
              onPressed: (){
                companyDetail();
              },
                width: (MediaQuery.of(context).size.width / 2) - 21,
              height: 160,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      CustomContainer(

                        onPressed: (){
                          companyLike();

                        },
                        child: company.companyLikeExists==true ? Image.asset("assets/icons/icon_haert_fill_40.png", width: 40) : Image.asset("assets/icons/icon_heart_40.png", width: 40),
                      ),
                    ],
                  ),
                  if(company.category!.type != 2)
                    Container(
                      height: 48,
                      padding: EdgeInsets.only(left: 14, right: 8),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(12),
                            bottomRight: Radius.circular(12),
                          ),
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Color.fromRGBO(146, 146, 146, 0),
                                Color.fromRGBO(19, 19, 19, 0.55)
                              ]
                          )
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              Image.asset("assets/icons/3.0x/icon_star.png", width: 24),
                              SizedBox(width: 6,),
                              Text("${company.reviewScore}", style: TextStyleTheme.whiteS14W400,)
                            ],
                          ),
                          Row(
                            children: [
                              Image.asset("assets/icons/3.0x/icon_comment.png", width:15),
                              SizedBox(width: 6,),
                              Text("${company.reviewCount}", style: TextStyleTheme.whiteS14W400,)
                            ],
                          ),
                        ],
                      ),
                    )
                ],
              ),
            )
            )
          ],
        ),
        SizedBox(height: 8,),
        Container(
          width: (MediaQuery.of(context).size.width / 2) - 21,
          height: 18,
          child: Text("${company.areaCode!.region1DepthName} ${company.areaCode!.region2DepthName}", style: TextStyleTheme.darkDarkS14W400,),
        ),
        SizedBox(height: 4,),
        Container(
          width: (MediaQuery.of(context).size.width / 2) - 21,
          height: 20,
          child: Text("${company.companyName}", style: TextStyleTheme.blackS16W400, maxLines: 1, overflow: TextOverflow.ellipsis,),
        )

      ],
    );
  }

}