import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/models/company_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/themes/text_style_theme.dart';

// ignore: must_be_immutable
class CompanyInformationComponent extends StatelessWidget{
  Company company;
  Function? matching;
  User me;

  CompanyInformationComponent({
    required this.company,
    this.matching,
    required this.me
  });

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  Stack(
      children: [
        ListView(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(right: 10),
                  width: 88,
                  height: 88,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: company.companyRepresentativeImage == null ? Image.asset("assets/images/company_default_logo.png") : Image.network(company.companyRepresentativeImage!.src!.o, fit: BoxFit.cover,),
                  ),
                ),
                Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(height: 7,),
                        Text("${company.areaCode!.region1DepthName} ${company.areaCode!.region2DepthName}", style: TextStyleTheme.darkDarkS14W400,),
                        SizedBox(height: 4,),
                        Text("${company.companyName}", style: TextStyleTheme.blackS16W700,),
                        if(company.category!.type != 2)
                        Container(
                          margin: EdgeInsets.only(top: 4),
                          child: Row(
                            children: [
                              Image.asset("assets/icons/3.0x/icon_star.png", width: 24,),
                              Text("(${company.reviewScore})", style: TextStyleTheme.darkDarkS14W400,)

                            ],
                          ),
                        )
                      ],
                    )
                )
              ],
            ),
            SizedBox(height: 20,),
            if(company.companyInformation!= null && company.companyInformation!.numberOfCapacity!=null)
              Container(
                margin: EdgeInsets.only(bottom: 4),
                child: Row(
                  children: [
                    Container(
                      width: 88,
                      child: Text("수용인원", style: TextStyleTheme.darkDarkS16W400,),
                    ),
                    Expanded(
                        child: Text("${company.companyInformation!.numberOfCapacity}명", style: TextStyleTheme.darkDarkS16W400,)
                    )
                  ],
                ),
              ),
            if(company.companyInformation!= null && company.companyInformation!.holeCount!=null)
              Container(
                margin: EdgeInsets.only(bottom: 4),
                child: Row(
                  children: [
                    Container(
                      width: 88,
                      child: Text("홀개수", style: TextStyleTheme.darkDarkS16W400,),
                    ),
                    Expanded(
                        child: Text("${company.companyInformation!.holeCount}개", style: TextStyleTheme.darkDarkS16W400,)
                    )
                  ],
                ),
              ),
            if(company.companyInformation!= null && company.companyInformation!.numberOfParkingLots!=null)
              Container(
                margin: EdgeInsets.only(bottom: 4),
                child: Row(
                  children: [
                    Container(
                      width: 88,
                      child: Text("주차", style: TextStyleTheme.darkDarkS16W400,),
                    ),
                    Expanded(
                        child: Text("${company.companyInformation!.numberOfParkingLots}개", style: TextStyleTheme.darkDarkS16W400,)
                    )
                  ],
                ),
              ),
            if(company.companyInformation!= null && company.companyInformation!.reception!=null)
              Container(
                margin: EdgeInsets.only(bottom: 4),
                child: Row(
                  children: [
                    Container(
                      width: 88,
                      child: Text("피로연", style: TextStyleTheme.darkDarkS16W400,),
                    ),
                    Expanded(
                        child: Text("${company.companyInformation!.reception}", style: TextStyleTheme.darkDarkS16W400,)
                    )
                  ],
                ),
              ),
            if(company.companyInformation!= null && company.companyInformation!.honeymoonCountry!=null)
              Container(
                margin: EdgeInsets.only(bottom: 4),
                child: Row(
                  children: [
                    Container(
                      width: 88,
                      child: Text("국가 및 지역", style: TextStyleTheme.darkDarkS16W400,),
                    ),
                    Expanded(
                        child: Text("${company.companyInformation!.honeymoonCountry}", style: TextStyleTheme.darkDarkS16W400,)
                    )
                  ],
                ),
              ),
            SizedBox(height: 20,),
            if(company.description != null)
              Container(
                width: double.infinity,
                child: Text("${company.description}"),
              )
          ],
        ),

        if(me.level == 1)
          Positioned(
              bottom: 32,
              right: 16,
              child: CustomContainer(
                onPressed: (){
                  if(matching!=null){
                    matching!();
                  }
                },
                child: Image.asset("assets/icons/icon-talk.png"),
              )
          )
      ],
    );
  }



}