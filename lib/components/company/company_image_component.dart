import 'package:flutter/material.dart';
import 'package:onewed_app/base_route.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/components/custom_smart_refresher.dart';
import 'package:onewed_app/models/company_image_category_model.dart';
import 'package:onewed_app/models/company_image_model.dart';
import 'package:onewed_app/models/company_model.dart';
import 'package:onewed_app/providers/company_image_provider.dart';
import 'package:onewed_app/providers/company_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:onewed_app/routes/company/company_image_detail_page.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/widgets/error.dart';
import 'package:onewed_app/widgets/loading.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class  CompanyImageComponent extends StatelessWidget{

  Company company;
  Function()? matching;
  CompanyImageComponent({Key? key, required this.company, this.matching}): super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ChangeNotifierProvider<CompanyImageProvider>(
      create: (context) => CompanyImageProvider(),
      child: View(this.company, this.matching),
    );
  }
}

class View extends BaseRoute {

  Company company;
  Function? matching;

  View(this.company, this.matching);

  @override
  _ViewState createState() => _ViewState(this.company, this.matching);
}

class _ViewState extends BaseRouteState {


  Company company;
  Function? matching;

  _ViewState(this.company, this.matching);

  CompanyProvider? _companyProvider;

  RefreshController _refreshController = new RefreshController(initialRefresh: false);

  CompanyImageCategory? companyImageCategory;

  CompanyImageProvider? _companyImageProvider;
  List<CompanyImage> _companyImageList = [];

  UserProvider? _userProvider;

  int page = 0;
  bool hasMore = true;

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    super.afterFirstLayout(context);

    onRefresh();
  }


  @override
  Widget build(BuildContext context) {
    _companyProvider = Provider.of<CompanyProvider>(context);
    _companyImageProvider = Provider.of<CompanyImageProvider>(context);
    _userProvider = Provider.of<UserProvider>(context);

    return Stack(
      children: [
        RefreshConfiguration(
            child: SmartRefresher(
              onLoading: this.onLoading,
              onRefresh: this.onRefresh,
              enablePullDown: true,
              enablePullUp: true,
              footer: CustomSmartRefresher.customFooter(),
              header: CustomSmartRefresher.customHeader(),
              controller: _refreshController,
              child: CustomScrollView(
                slivers: [
                  SliverList(delegate: SliverChildListDelegate([
                    Container(
                      height: 60,
                      padding: EdgeInsets.only(left: 16, top: 10, bottom: 10),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                              child: companyImageCategoryListWidget()
                          )
                        ],
                      ),
                    )
                  ])),
                  SliverPadding(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                    sliver:  Consumer<CompanyImageProvider>(builder: (context, companyImageModel, child){
                      _companyImageList = companyImageModel.getCompanyImageList();

                      if(companyImageModel.isLoading){
                        return SliverList(delegate: SliverChildListDelegate([
                          CustomLoading(
                            margin: EdgeInsets.only(top: 20),
                          )
                        ]));
                      }

                      if(companyImageModel.isError){
                        return SliverList(delegate: SliverChildListDelegate([
                          ErrorCard(
                            margin: EdgeInsets.only(top: 20),
                          )
                        ]));
                      }

                      if(_companyImageList.length == 0){
                        return SliverFillRemaining(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                SizedBox(height: 30,),
                                Image.asset("assets/images/gray-logo.png", width: 235, height: 60, fit: BoxFit.fill,),
                                SizedBox(height: 30,),
                                Text("이미지가 없습니다.", style: TextStyleTheme.darkDarkS18W700,),

                              ],
                            )
                        );
                      }

                      return SliverGrid.count(
                        crossAxisCount: 2,
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                        children: _companyImageList.map((e) => companyImageWidget(e)).toList(),
                      );
                    }),
                  )
                ],
              ),
            )
        ),
        if(_userProvider!.getMe()!.level == 1)
          Positioned(
            bottom: 32,
            right: 16,
            child: CustomContainer(
              onPressed: (){
                if(matching!=null){
                  matching!();
                }
              },
              child: Image.asset("assets/icons/icon-talk.png"),
            )
          )
      ],
    );
  }

  Widget companyImageWidget(CompanyImage companyImage){
    return CustomContainer(
      onPressed: (){
        int index = _companyImageList.indexWhere((element) => element ==companyImage);
        this.navigator!.pushRoute(CompanyImageDetailPage(companyImageList: _companyImageList, index: index));
      },
      height: 160, 
      child: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(12),
            child: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage("${companyImage.src!.o}"),
                      fit: BoxFit.cover
                  )
              ),
            ),
          ),
/*          if(companyImage.companyImageCategory!=null)
            Positioned(
              bottom: 8,
              left: 8,
              child: Container(
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.8),
                  borderRadius: BorderRadius.all(Radius.circular(6))
                ),
                child: Center(
                  child: Text("${companyImage.companyImageCategory!.categoryName}", style: TextStyleTheme.darkDarkS14W400,),
                ),
              )
            )*/
        ],
      ),
    );

  }

  Widget companyImageCategoryListWidget(){
    List<Widget> list = [];
    list.add(CustomContainer(
      onPressed: (){
        companyImageCategory = null;
        setState(() {

        });
        onRefresh();
      },
      height: 40,
      padding: EdgeInsets.symmetric(horizontal: 16),
      borderRadius: [10, 10, 10, 10],
      backgroundColor: companyImageCategory == null ? ColorTheme.primaryColor : Colors.transparent,
      child: Center(
        child: Text("전체", style: companyImageCategory == null ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
      ),
    ));

    company.imageCategoryList!.forEach((element) {list.add(
        CustomContainer(
          onPressed: (){
            companyImageCategory = element;
            setState(() {

            });
            onRefresh();
          },
          height: 40,
          padding: EdgeInsets.symmetric(horizontal: 16),
          borderRadius: [10, 10, 10, 10],
          backgroundColor: companyImageCategory == element ? ColorTheme.primaryColor : Colors.transparent,
          child: Center(
            child: Text("${element.categoryName}", style: companyImageCategory == element ? TextStyleTheme.whiteS16W700 : TextStyleTheme.darkGrayS16W400,),
          ),
        )
    );});

    return ListView(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      children: list,
    );
  }

  onRefresh(){
    page = 0;
    hasMore = true;

    onLoading();
    _refreshController.refreshCompleted();
  }

  onLoading(){
    if(hasMore){
      page = page+1;

      Map<String, String> data = {
        "company_no": "${company.companyNo}",
        "more_field": "*",
        "page":"$page"
      };
      if(companyImageCategory!=null){
        data.addAll({"company_image_category_no":"${companyImageCategory!.companyImageCategoryNo}"});
      }

      _companyImageProvider!.selectCompanyImageList(page, data).then((value){
        if(value!.length == 0){
          hasMore = false;
        }
        _refreshController.loadComplete();
      });
    }else{
      _refreshController.loadComplete();
    }
  }
}
