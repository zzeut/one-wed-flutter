import 'package:fluro/fluro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/application.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/utils/web_router.dart';

class PlatformApp extends StatelessWidget {
  final List<NavigatorObserver>? navigatorObservers;
  final Iterable<LocalizationsDelegate<dynamic>>? localizationDelegates;
  final Iterable<Locale>? supportedLocales;
  final Locale? locale;
  final Widget? home;
  final Widget Function(BuildContext, Widget?)? builder;

  const PlatformApp({
    Key? key,
    this.navigatorObservers,
    this.localizationDelegates,
    this.supportedLocales,
    this.locale,
    this.home,
    this.builder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final FluroRouter router = FluroRouter();

    Routes.configureRoutes(router);
    Application.router = router;

    return MaterialApp(
      navigatorObservers: navigatorObservers!,
      localizationsDelegates: localizationDelegates,
      supportedLocales: supportedLocales!,
      locale: locale,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        backgroundColor: Colors.white,
        scaffoldBackgroundColor: Colors.white,
        splashColor: Colors.transparent,
        primaryColorBrightness: Brightness.light,
        appBarTheme: AppBarTheme(
          elevation: 0,
          color: Colors.white,
          brightness: Brightness.light,
        ),
        cupertinoOverrideTheme: CupertinoThemeData(
            textTheme: CupertinoTextThemeData(
              dateTimePickerTextStyle: TextStyle(
                  fontSize: 16,
              ),
            )
        )
      ),
      home: home,
      builder: builder!,
      onGenerateRoute: Application.router!.generator,
    );
  }
}
