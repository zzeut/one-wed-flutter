class MaskingName{
  static String changeMaskingName(String name){
    String makingName = '';
    for(int i=0; i<name.length; i++){
      if(i==0 || i == name.length-1){
        makingName += name[i];
      }else{
        makingName += '*';
      }
    }

    return makingName;
  }
}