import 'package:bot_toast/bot_toast.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:kakao_flutter_sdk/all.dart';
import 'package:onewed_app/models/on_boarding_model.dart';
import 'package:onewed_app/routes/onboard/onboarding_page.dart';
import 'package:onewed_app/routes/splash/splash_page.dart';
import 'package:onewed_app/components/platform_app.dart';
import 'package:shared_preferences/shared_preferences.dart';

class App extends StatelessWidget {
  bool isFirst;
  List<onboarding> onboardingList;

  App({Key? key, required this.isFirst, required this.onboardingList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    KakaoContext.clientId = "${dotenv.env['KAKAO_NATIVE_APP_KEY']}";
    final botToastBuilder = BotToastInit();


    return PlatformApp(
      localizationDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ],
      builder: (BuildContext context, Widget? child) {
        child = botToastBuilder(context, child);
        child = EasyLoading.init()(context, child);
        return child;
      },
        // home: SplashPage()
      home: (isFirst && onboardingList.length > 0)
          ? OnboardingPage(
        setFirst: (){
          setFirst();
          },
        onboardingList: onboardingList,
      ) : SplashPage()
      ,
    );
  }


  setFirst() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('isFirst', false);
  }

}
