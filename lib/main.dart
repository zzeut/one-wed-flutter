import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:onewed_app/providers/broadcast_provider.dart';
import 'package:onewed_app/models/on_boarding_model.dart';
import 'package:onewed_app/providers/category_provider.dart';
import 'package:onewed_app/providers/company_provider.dart';
import 'package:onewed_app/providers/company_review_provider.dart';
import 'package:onewed_app/providers/inquiry_provider.dart';
import 'package:onewed_app/providers/schdule_provider.dart';
import 'package:provider/provider.dart';
import 'package:onewed_app/app.dart';
import 'package:onewed_app/providers/auth_provider.dart';
import 'package:onewed_app/providers/user_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';


bool _isFirst = true;

/// Define a top-level named handler which background/terminated messages will
/// call.
///
/// To verify things are working, check out the native platform logs.
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');

}



void main() async {

  WidgetsFlutterBinding.ensureInitialized();

  print(2222);

  await EasyLocalization.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await dotenv.load(fileName: ".env");
  await Firebase.initializeApp();

  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool isFirst = prefs.getBool('isFirst') ?? true;
  _isFirst = isFirst;
  // _isFirst = true;

  await Firebase.initializeApp();


  // Set the background messaging handler early on, as a named top-level function
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  
  List<onboarding> onboardingList = [];
  if (_isFirst) {
    DatabaseReference db = FirebaseDatabase.instance.reference().child('onboard');
    DataSnapshot dataSnapshot = await db.once();

    if (dataSnapshot.value != null) {
      dataSnapshot.value.forEach((element){
        onboardingList.add(onboarding.fromJson(element));
      });
    }

  }

  runApp(
    EasyLocalization(
      supportedLocales: [Locale('ko')],
      path: 'assets/translations',
      fallbackLocale: Locale('ko'),
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider<AuthProvider>(
            create: (context) => AuthProvider()..getOauth(),
          ),
          ChangeNotifierProvider<UserProvider>(
            create: (context) => UserProvider(),
          ),
          ChangeNotifierProvider<CompanyProvider>(
            create: (context) => CompanyProvider(),
          ),
          ChangeNotifierProvider<ScheduleProvider>(
            create: (context) => ScheduleProvider(),
          ),
          ChangeNotifierProvider<BroadCastProvider>(
            create: (context) => BroadCastProvider(),
          ),
          ChangeNotifierProvider<CompanyReviewProvider>(
            create: (context) => CompanyReviewProvider(),
          ),
          ChangeNotifierProvider<InquiryProvider>(
            create: (context) => InquiryProvider(),
          ),
          ChangeNotifierProvider<CategoryProvider>(
            create: (context) => CategoryProvider(),
          ),
        ],
        child: App(isFirst: _isFirst, onboardingList: onboardingList,),
      ),
    ),
  );
}
