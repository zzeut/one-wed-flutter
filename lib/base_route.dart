import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:onewed_app/base_navigator.dart';
import 'package:onewed_app/components/custom_button.dart';
import 'package:onewed_app/components/custom_container.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/themes/text_style_theme.dart';
import 'package:onewed_app/utils/indicator_client.dart';

class BaseRoute extends StatefulWidget {
  @override
  BaseRouteState createState() => BaseRouteState();
}

class BaseRouteState<T extends BaseRoute> extends State<T>
    with WidgetsBindingObserver {
  BaseNavigator? navigator;
  IndicatorClient indicator = IndicatorClient.shared;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    WidgetsBinding.instance!
        .addPostFrameCallback((_) => afterFirstLayout(context));
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      this.changeStatusBarColor();
    }
  }

  void changeStatusBarColor() {
    if (kIsWeb) {
    } else {
      //Switcher 설명
      SystemChrome.setApplicationSwitcherDescription(
          ApplicationSwitcherDescription(
              primaryColor: ColorTheme.primaryBlue.hashCode, label: 'dal'));

      // 상태바 색상 조절
      SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle.dark.copyWith(
          statusBarColor: Colors.white,
          statusBarBrightness: Brightness.light,
          systemNavigationBarColor: Colors.white,
          systemNavigationBarIconBrightness: Brightness.dark,
        ),
      );
    }
  }

  void afterFirstLayout(BuildContext context) {
    this.navigator = BaseNavigator(context);
    this.changeStatusBarColor();
    this.checkSystem();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }

  void checkSystem() {}

  Future<bool> onWillPop() {
    return this.showConfirm(
        title: '종료 안내',
        description: '정말로 종료하시겠습니까?',
        positiveButtonText: '종료',
        negativeButtonText: '취소');
  }

  Future<bool> showConfirm(
      {String? title,
      String? description,
      String? positiveButtonText,
      String? negativeButtonText}) async {
    return await showModalBottomSheet(
      useRootNavigator: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30.0), topRight: Radius.circular(30.0)),
      ),
      context: context,
      builder: (context) {
        return SafeArea(
          top: false,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 30),
              CustomContainer(
                padding: EdgeInsets.symmetric(horizontal: 25),
                child:
                    Center(child: Text(title!, style: TextStyleTheme.subtitle)),
              ),
              SizedBox(height: 10),
              CustomContainer(
                padding: EdgeInsets.symmetric(horizontal: 25),
                child: Center(
                    child: Text(description ?? '',
                        style: TextStyleTheme.common
                            .copyWith(fontSize: 15, color: ColorTheme.black))),
              ),
              SizedBox(height: 30),
              Row(
                children: [
                  SizedBox(width: 25),
                  Expanded(
                    child: CustomContainer(
                      child: CustomRouteButton(
                        text: Text(negativeButtonText ?? ''),
                        padding: EdgeInsets.all(16),
                        borderRadius: 18,
                        backgroundColor: ColorTheme.grayPlaceholder,
                        onPressed: () {
                          Navigator.of(context).pop(false);
                        },
                      ),
                    ),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: CustomContainer(
                      child: CustomRouteButton(
                        text: Text(positiveButtonText ?? ''),
                        padding: EdgeInsets.all(16),
                        borderRadius: 18,
                        backgroundColor: ColorTheme.primaryColor,
                        onPressed: () {
                          SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                          // Navigator.of(context).pop(true);
                        },
                      ),
                    ),
                  ),
                  SizedBox(width: 25),
                ],
              ),
              SizedBox(height: 20),
            ],
          ),
        );
      },
    );
  }

  void showPlaceMoreSheetMe({VoidCallback? onDelete, VoidCallback? onUpdate}) {
    showCupertinoModalPopup(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        actions: <Widget>[
          if (onUpdate != null)
            CupertinoActionSheetAction(
              child: Text('수정'),
              onPressed: () {
                Navigator.pop(context);
                onUpdate();
              },
            ),
          if (onDelete != null)
            CupertinoActionSheetAction(
              isDestructiveAction: true,
              child: Text('삭제'),
              onPressed: () {
                Navigator.pop(context);
                this
                    .showConfirm(
                        title: '정말 삭제하시겠습니까?',
                        description: '삭제하신 장소는 복구되지 않아요',
                        positiveButtonText: '삭제',
                        negativeButtonText: '취소')
                    .then((value) {
                  if (value == true) {
                    onDelete();
                  }
                });
              },
            ),
        ],
        cancelButton: CupertinoActionSheetAction(
          child: Text('닫기'),
          isDefaultAction: true,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
