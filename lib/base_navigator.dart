import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:onewed_app/application.dart';

class BaseNavigator {
  final BuildContext context;

  BaseNavigator(this.context);

  Future pushRoute(
    Widget route, {
    String? path,
    Map<String, dynamic>? data,
    bool fullscreenDialog = false,
  }) {
    if (kIsWeb) {
      // TODO
      String url = '';

      if (data != null) {
        var query = Uri(queryParameters: data).query;

        url = Uri.parse('$path?$query').toString();
      } else {
        url = path!;
      }

      return Application.router!.navigateTo(context, '$url');
    } else {
      if (Platform.isIOS) {
        return Navigator.of(context).push(
          CupertinoPageRoute(
            builder: (context) => route,
            fullscreenDialog: fullscreenDialog,
          ),
        );
      } else {
        return Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => route,
          ),
        );
      }
    }
  }

  Future resetRoute(
    Widget route, {
    bool isAll = false,
  }) {
    if (Platform.isIOS) {
      return Navigator.of(context).pushAndRemoveUntil(
        CupertinoPageRoute(
          builder: (context) => route,
        ),
        isAll ? (Route<dynamic> route) => false : ModalRoute.withName('/'),
      );
    } else {
      return Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
          builder: (context) => route,
        ),
        isAll ? (Route<dynamic> route) => false : ModalRoute.withName('/'),
      );
    }
  }

  Future presentRoute(Widget route) {
    return Navigator.of(context, rootNavigator: true).push(
      CupertinoPageRoute(fullscreenDialog: true, builder: (context) => route),
    );
  }

  Future replaceRoute(Widget route) {
    return Navigator.of(context).pushReplacement(
      PageRouteBuilder(
        opaque: false,
        pageBuilder: (context, animation, secondaryAnimation) {
          return route;
        },
        transitionDuration: Duration(milliseconds: 100),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return FadeTransition(
            opacity: animation,
            child: child,
          );
        },
      ),
    );
  }

  Future fadePresentRoute(Widget route) {
    return Navigator.of(context, rootNavigator: true).push(
      PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) {
          return route;
        },
        transitionDuration: Duration(milliseconds: 100),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return FadeTransition(
            opacity: animation,
            child: child,
          );
        },
      ),
    );
  }

  Future modalRoute(Widget route) {
    return Navigator.of(context).push(
      PageRouteBuilder(
          opaque: false,
          pageBuilder: (context, animation1, animation2) => route),
    );
  }

  void popRoute(var result) {
    Navigator.of(context).pop(result);
  }

  void popToRootRoute() {
    Navigator.popUntil(context, (route) => route.isFirst);
  }
}
