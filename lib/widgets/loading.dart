import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:loading_overlay/loading_overlay.dart';

class CustomLoading extends StatelessWidget {
  final EdgeInsetsGeometry margin;

  const CustomLoading({
    Key? key,
    required this.margin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: margin,
          child: SpinKitRing(
            color: Colors.grey,
            size: 25,
            lineWidth: 2.5,
          ),
        ),
      ],
    );
  }
}

class CustomModal extends StatelessWidget {
  final bool isLoading;
  final Widget child;

  const CustomModal({
    Key? key,
    required this.isLoading,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: isLoading
          ? () async {
              return false;
            }
          : null,
      child: LoadingOverlay(
        isLoading: isLoading,
        color: Colors.black,
        progressIndicator: Center(
          child: SpinKitRing(
            color: Colors.white,
            size: 25,
            lineWidth: 2.5,
          ),
        ),
        child: child,
      ),
    );
  }
}
