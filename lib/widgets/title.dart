import 'package:flutter/material.dart';

class CompanyInfoCard extends StatelessWidget {
  const CompanyInfoCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20, left: 16, right: 16, bottom: 20),
      color: Color(0xff424242),
      child: RichText(
        text: TextSpan(
          text: '주식회사 온디맨드 | 대표 박석규\n',
          style: TextStyle(
            color: Color(0xffA6A6A6),
            fontSize: 12,
          ),
          children: [
            TextSpan(
              text: '서울시 광진구 가상텍스트 가상텍스트\n',
            ),
            TextSpan(
              text: '사업자 등록번호 | 111-11-11111\n',
            ),
            TextSpan(
              text: '통신판매업 신고번호 | 2019-서울강남-11111\n\n',
            ),
            TextSpan(
              text: '대표번호 | 070-8860-9201\n',
            ),
            TextSpan(
              text: '고객문의 | contact@11111.com',
            ),
          ],
        ),
      ),
    );
  }
}
