import 'package:flutter/material.dart';
import 'package:onewed_app/icons.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Function()? onTap;
  final String? title;
  final bool fullscreenDialog;
  final bool? automaticallyImplyLeading;
  final Widget? leading;

  const CustomAppBar({
    Key? key,
    this.onTap,
    this.title,
    required this.fullscreenDialog,
    this.automaticallyImplyLeading,
    this.leading
  }) : super(key: key);

  Size get preferredSize => AppBar().preferredSize;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: automaticallyImplyLeading ?? false,
      centerTitle: true,
      leading: leading ?? GestureDetector(
        onTap: onTap ?? () => Navigator.of(context).pop(),
        child: fullscreenDialog ? Text('x') : Image.asset(iconArrowLeft),
      ),
      title: title != null
          ? Text(
              '$title',
              style: TextStyle(
                color: Colors.black,
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            )
          : Container(),
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(1),
        child: Container(
          height: 1,
          color: Color(0xffECECEC),
        ),
      ),
    );
  }
}
