import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class CustomImageCard extends StatelessWidget {
  final Function()? onTap;
  final double width;
  final double height;
  final String imageUrl;
  final BoxFit? boxFit;
  final List<BoxShadow>? boxShadow;
  final BorderRadiusGeometry? borderRadius;

  const CustomImageCard({
    Key? key,
    this.onTap,
    required this.width,
    required this.height,
    required this.imageUrl,
    this.boxFit,
    this.boxShadow,
    this.borderRadius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      imageBuilder: (context, imageProvider) {
        return GestureDetector(
          onTap: onTap,
          child: Container(
            width: width,
            height: height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: imageProvider,
                fit: boxFit ?? BoxFit.cover,
              ),
              boxShadow: boxShadow != null ? boxShadow : [],
              borderRadius: borderRadius ?? BorderRadius.circular(0),
            ),
          ),
        );
      },
      placeholder: (context, value) {
        return Container(
          width: width,
          height: height,
          child: Center(
            child: SpinKitRing(
              color: Colors.grey,
              size: 25,
              lineWidth: 2.5,
            ),
          ),
        );
      },
      errorWidget: (context, value, error) {
        return Container(
          width: width,
          height: height,
        );
      },
    );
  }
}
