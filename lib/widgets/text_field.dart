import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextField extends StatelessWidget {
  final EdgeInsetsGeometry? margin;
  final double? height;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final Color textColor;
  final String hintText;
  final List<TextInputFormatter>? inputFormatters;
  final int? maxLines;
  final bool isError;

  const CustomTextField({
    Key? key,
    this.margin,
    this.height,
    required this.controller,
    required this.keyboardType,
    required this.textColor,
    required this.hintText,
    this.inputFormatters,
    this.maxLines,
    this.isError = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      height: height,
      child: TextField(
        controller: controller,
        style: TextStyle(
          color: textColor,
          fontSize: 16,
        ),
        cursorColor: textColor,
        keyboardType: keyboardType,
        inputFormatters: inputFormatters ?? [],
        maxLines: maxLines,
        decoration: InputDecoration(
          contentPadding:
              EdgeInsets.only(top: 20, left: 10, right: 10, bottom: 20),
          isDense: true,
          hintText: hintText,
          hintStyle: TextStyle(
            color: Color(0xffA6A6A6),
            fontSize: 16,
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: isError ? Color(0xffD1504B) : Color(0xffA6A6A6),
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: isError ? Color(0xffD1504B) : Color(0xffA6A6A6),
            ),
            borderRadius: BorderRadius.circular(5),
          ),
        ),
      ),
    );
  }
}
