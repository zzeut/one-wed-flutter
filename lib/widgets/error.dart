import 'package:flutter/material.dart';

class ErrorCard extends StatelessWidget {
  // TODO
  final EdgeInsetsGeometry margin;

  const ErrorCard({
    Key? key,
    required this.margin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          margin: margin,
          child: Text(
            '서버 오류',
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ],
    );
  }
}
