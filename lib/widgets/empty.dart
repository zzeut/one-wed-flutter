import 'package:flutter/material.dart';
import 'package:onewed_app/icons.dart';

class EmptyCard extends StatelessWidget {
  final String title;
  final String contents;

  const EmptyCard({
    Key? key,
    required this.title,
    required this.contents,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 120),
          child: Image.asset(iconWeddingshop),
        ),
        Container(
          margin: EdgeInsets.only(top: 20),
          child: Text(
            '$title',
            style: TextStyle(
              color: Color(0xff303030),
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Text(
            '$contents',
            style: TextStyle(
              color: Color(0xff656565),
              fontSize: 16,
            ),
          ),
        ),
      ],
    );
  }
}
