import 'package:flutter/material.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/widgets/inkwell.dart';
import 'package:onewed_app/widgets/size.dart';

void customAlert(
  BuildContext context, {
  required Function() onTap,
  required Function() onTapSub,
  required String title,
  required String contents,
  required String buttonTitle,
  required String buttonSubTitle,
}) {
  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        insetPadding: EdgeInsets.only(left: 16, right: 16),
        titlePadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        title: Container(
          width: customWidth(context),
          child: Column(
            children: <Widget>[
              Text(
                '$title',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Text(
                  '$contents',
                  style: TextStyle(
                    color: Color(0xff303030),
                    fontSize: 16,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 40),
                child: Row(
                  children: [
                    Flexible(
                      flex: 5,
                      child: Container(
                        margin: EdgeInsets.only(right: 5),
                        height: 52,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: Color(0xff656565),
                          ),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: CustomInkWell(
                          onTap: () {
                            onTap();

                            Navigator.of(context).pop();
                          },
                          borderRadius: BorderRadius.circular(5),
                          child: Center(
                            child: Text(
                              '$buttonTitle',
                              style: TextStyle(
                                color: Color(0xff424242),
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 5,
                      child: Container(
                        margin: EdgeInsets.only(left: 5),
                        height: 52,
                        decoration: BoxDecoration(
                          color: ColorTheme.primaryColor,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: CustomInkWell(
                          onTap: () {
                            onTapSub();

                            Navigator.of(context).pop();
                          },
                          borderRadius: BorderRadius.circular(5),
                          child: Center(
                            child: Text(
                              '$buttonSubTitle',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}
