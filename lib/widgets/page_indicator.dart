import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class CustomPageIndicator extends StatelessWidget {
  final PageController pageController;
  final int count;
  Color? dotColor;

  CustomPageIndicator({
    Key? key,
    required this.pageController,
    required this.count,
    this.dotColor
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SmoothPageIndicator(
        controller: pageController,
        count: count,
        effect: ExpandingDotsEffect(
          dotWidth: 8,
          dotHeight: 8,
          dotColor: dotColor ??  Color(0xffECECEC),
          activeDotColor: Color(0xffEA9589),
        ),
      ),
    );
  }
}
