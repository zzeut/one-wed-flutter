import 'package:flutter/material.dart';

class CustomInkWell extends StatelessWidget {
  final Function() onTap;
  final EdgeInsetsGeometry? padding;
  final Widget child;
  final BorderRadius? borderRadius;
  final Color testColor;

  const CustomInkWell({
    Key? key,
    required this.onTap,
    this.padding,
    required this.child,
    this.borderRadius,
    this.testColor = Colors.transparent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: testColor,
      borderRadius: borderRadius ?? BorderRadius.circular(0),
      child: InkWell(
        onTap: onTap,
        borderRadius: borderRadius ?? BorderRadius.circular(0),
        child: Container(
          padding: padding ?? EdgeInsets.all(0),
          child: child,
        ),
      ),
    );
  }
}
