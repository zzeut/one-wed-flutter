import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/widgets/size.dart';

void customToast(
  BuildContext context, {
  required String title,
}) {
  BotToast.showCustomNotification(
    align: Alignment(0, 0.99),
    dismissDirections: [
      DismissDirection.horizontal,
      DismissDirection.down,
    ],
    toastBuilder: (_) {
      return Container(
        padding: EdgeInsets.only(top: 10, left: 16, right: 16, bottom: 10),
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(200),
        ),
        child: Text(
          '$title',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16,
          ),
        ),
      );
    },
    wrapToastAnimation: (controller, cancel, child) {
      return CustomAnimationWidget(
        controller: controller,
        child: child,
      );
    },
  );
}

void errorToast(
  BuildContext context, {
  String? title,
}) {
  BotToast.showCustomNotification(
    align: Alignment(0, 0.99),
    dismissDirections: [
      DismissDirection.horizontal,
      DismissDirection.down,
    ],
    toastBuilder: (_) {
      return Container(
        margin: EdgeInsets.only(left: 20, right: 20),
        padding: EdgeInsets.all(20),
        width: customWidth(context),
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Text(
          title ?? '서버 오류',
          style: TextStyle(
            color: Colors.white,
          ),
          textAlign: TextAlign.center,
        ),
      );
    },
    wrapToastAnimation: (controller, cancel, child) {
      return CustomAnimationWidget(
        controller: controller,
        child: child,
      );
    },
  );
}

class CustomAnimationWidget extends StatefulWidget {
  final AnimationController controller;
  final Widget child;

  const CustomAnimationWidget({
    Key? key,
    required this.controller,
    required this.child,
  }) : super(key: key);

  @override
  _CustomAnimationWidgetState createState() => _CustomAnimationWidgetState();
}

class _CustomAnimationWidgetState extends State<CustomAnimationWidget> {
  static final Tween<Offset> tweenOffset = Tween<Offset>(
    begin: const Offset(0, 40),
    end: const Offset(0, 0),
  );

  Animation<double>? animation;

  @override
  void initState() {
    animation = CurvedAnimation(
      parent: widget.controller,
      curve: Curves.decelerate,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animation!,
      builder: (BuildContext context, Widget? child) {
        return Transform.translate(
          offset: tweenOffset.evaluate(animation!),
          child: Opacity(
            child: child,
            opacity: animation!.value,
          ),
        );
      },
      child: widget.child,
    );
  }
}
