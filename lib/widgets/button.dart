import 'package:flutter/material.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/widgets/inkwell.dart';
import 'package:onewed_app/widgets/size.dart';

class CustomButton extends StatelessWidget {
  final Function() onTap;
  final EdgeInsetsGeometry? margin;
  final String title;
  final bool isActive;

  const CustomButton({
    Key? key,
    required this.onTap,
    required this.margin,
    required this.title,
    this.isActive = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      width: customWidth(context),
      height: 52,
      decoration: BoxDecoration(
        color: isActive ? ColorTheme.primaryColor : Color(0xffA6A6A6),
        borderRadius: BorderRadius.circular(10),
      ),
      child: CustomInkWell(
        onTap: onTap,
        borderRadius: BorderRadius.circular(10),
        child: Center(
          child: Text(
            '$title',
            style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
