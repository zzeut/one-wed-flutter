import 'package:flutter/material.dart';

double customWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}
