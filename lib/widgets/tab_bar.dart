import 'package:flutter/material.dart';
import 'package:onewed_app/themes/color_theme.dart';
import 'package:onewed_app/widgets/inkwell.dart';

class CustomTabBarDelegate extends SliverPersistentHeaderDelegate {
  final Function(int) onTap;
  final List<Widget> tabs;
  final bool isScrollable;

  CustomTabBarDelegate({
    required this.onTap,
    required this.tabs,
    this.isScrollable = false,
  });

  @override
  double get minExtent => 60;
  @override
  double get maxExtent => 60;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      height: 60,
      color: Colors.white,
      child: CustomInkWell(
        onTap: () {},
        child: TabBar(
          isScrollable: isScrollable,
          indicatorWeight: 4,
          indicatorColor: ColorTheme.primaryColor,
          labelColor: ColorTheme.primaryColor,
          labelStyle: TextStyle(
            color: ColorTheme.primaryColor,
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
          unselectedLabelStyle: TextStyle(
            color: Colors.black,
            fontSize: 18,
          ),
          unselectedLabelColor: Colors.black,
          onTap: onTap,
          tabs: tabs,
        ),
      ),
    );
  }

  @override
  bool shouldRebuild(CustomTabBarDelegate oldDelegate) {
    return true;
  }
}
