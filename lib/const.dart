enum COMPANY_SORT {
  all,
  like_desc,
  like_asc,
  score_desc,
  score_asc
}

enum SCHEDULE_FILTER {
  all,
  pending,
  doing,
  done
}

enum REVIEW_SORT {
  create_desc,
  score_desc,
  score_asc
}

enum SCHEDULE_SORT {
  created_desc,
  created_asc
}

enum SPENDING_FILTER {
  week,
  month,
  three_month,
  half_year,
  all
}