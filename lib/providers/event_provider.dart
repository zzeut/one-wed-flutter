import 'package:flutter/material.dart';
import 'package:onewed_app/models/event_model.dart';
import 'package:onewed_app/models/hot_event_model.dart';
import 'package:onewed_app/models/oauth_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class EventProvider with ChangeNotifier {

  bool _isLoading = true;
  bool _isError = false;
  Oauth? _oauth;
  NetworkComponent _networkComponent = new NetworkComponent();

  List<Event> _eventList = [];

  bool get isLoading => _isLoading;
  bool get isError => _isError;

  HotEvent? _hotEvent;

  Future<List<Event>?> selectEvents(String moreField) async {

    setLoading(true);

    Map<String, String> params = {
      "more_field": moreField
    };

    Response response = await _networkComponent.shared.request('get', '/event', params);

    if(response.success){
      setLoading(false);
      List<Event> eventList = (response.data['data'] as List).map((record) => Event.fromJson(record)).toList();
      setEventList(eventList);
      setError(true);
      return eventList;
    }else{
      setLoading(false);
      setError(false);
      return null;
    }
  }

  void setEventList(List<Event> events) {
    _eventList = events;

    notifyListeners();
  }

  List<Event> getEventList(){
    return _eventList;
  }

  void setLoading(bool isLoading) {
    _isLoading = isLoading;
    notifyListeners();
  }

  void setError(bool isError) {
    _isError = isError;

    notifyListeners();
  }


  Future<List<Event>?> selectEventList(Map<String, dynamic> map) async{
    setLoading(true);
    Response response =  await _networkComponent.shared.request('get', '/event', map);
    if(response.success){
      setLoading(false);
      setError(true);
      List<Event> eventList = (response.data['data'] as List).map((record) => Event.fromJson(record)).toList();
      return eventList;
    }else{
      setLoading(false);
      setError(false);
      return null;
    }
  }

  Future <HotEvent?> selectHotEvent() async {
    setLoading(true);
    Response response = await _networkComponent.shared.request('get', '/event/hotEvent', null);
    if(response.success){

      HotEvent hotEvent = HotEvent.fromJson(response.data);
      setHotEvent(hotEvent);
      setLoading(false);
      setError(false);
      return hotEvent;
    }else{
      setLoading(false);
      setError(true);
      return null;
    }
  }

  void setHotEvent(HotEvent hotEvent){
    _hotEvent = hotEvent;
    notifyListeners();
  }

  HotEvent? getHotEvent(){
    return _hotEvent;
  }

  Future<Event?> selectEvent(int eventNo) async{
    setLoading(true);
    Response response =  await _networkComponent.shared.request('get', '/event', {"event_no":"$eventNo"});
    if(response.success){
      setLoading(false);
      setError(true);
      List<Event> eventList = (response.data['data'] as List).map((record) => Event.fromJson(record)).toList();
      return eventList[0];
    }else{
      setLoading(false);
      setError(false);
      return null;
    }
  }
}
