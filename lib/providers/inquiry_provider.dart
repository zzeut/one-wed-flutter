import 'dart:ffi';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/inquiry_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';
import 'package:onewed_app/utils/resize_client.dart';

class InquiryProvider with ChangeNotifier{

  NetworkComponent _networkComponent = new NetworkComponent();

  List<Inquiry> _inquiryList = [];
  List<Inquiry> get getInquiryList  => _inquiryList;

  Inquiry? _inquiry;

  Future<Inquiry?> insertInquiry(Map map, File? image) async{

    Map<String, Uint8List> files = {};
    return Future.value().then((value){
      if(image != null){
        return resizeFuture(image.readAsBytesSync()).then((value){
          files = {"one_on_one_question_image[]": value};
          return Future.value();
        });
      }else{
        return Future.value();
      }
    }).then((value){
      return _networkComponent.shared.requestMultipart('post', '/oneOnOneQuestion', map, files).then((response){
        if(response.success){
          Inquiry inquiry = Inquiry.fromJson(response.data);
          return inquiry;
        }else{
          return null;
        }
      });
    }).catchError((onError){
      print(onError);
      return null;
    });
  }

  Future <List<Inquiry>?> selectInquiryList(int page, Map<String, String> map) async{
    Response response = await _networkComponent.shared.request('get', '/oneOnOneQuestion', map);
    if(response.success){
      List<Inquiry> inquiryList = (response.data['data'] as List).map((json) => Inquiry.fromJson(json)).toList();
      setInquiryList(page, inquiryList);
      return inquiryList;
    }else{
      return null;
    }
  }

  void setInquiryList(int page, List<Inquiry> inquiryList){
    if(page == 1){
      _inquiryList = inquiryList;
    }else{
      _inquiryList.addAll(inquiryList);
    }

    notifyListeners();
  }

  void updateInquiryList(Inquiry inquiry){
    int index = _inquiryList.indexWhere((element) => element.questionNo == inquiry.questionNo);
    if(index > -1){
      _inquiryList[index] = inquiry;
      notifyListeners();
    }
  }


  Future<Inquiry?> deleteInquiry(int inquiryNo) async{

    Response response = await _networkComponent.shared.request('delete', '/oneOnOneQuestion/$inquiryNo', null);
    if(response.success){
      Inquiry inquiry = Inquiry.fromJson(response.data);
      return inquiry;
    }else{
      return null;
    }
  }

  void deleteUpdateInquiryList(Inquiry inquiry){
    _inquiryList.removeWhere((element) => element.questionNo == inquiry.questionNo);
    notifyListeners();
  }

  Future<Inquiry?> selectInquiry(int inquiryNo) async{

    Response response = await _networkComponent.shared.request('get', '/oneOnOneQuestion', {"question_no":"$inquiryNo"});
    if(response.success){
      List<Inquiry> inquiryList = (response.data['data'] as List).map((json) => Inquiry.fromJson(json)).toList();
      setInquiry(inquiryList[0]);
      return inquiryList[0];
    }else{
      return null;
    }
  }

  void setInquiry(Inquiry inquiry){
    _inquiry = inquiry;
    notifyListeners();
  }

  Inquiry? getInquiry(){
    return _inquiry;
  }


  Future<Inquiry?> modifyInquiry(int inquiryNo, Map map, File? image) async{

    Map<String, Uint8List> files = {};
    return Future.value().then((value){
      if(image != null){
        return resizeFuture(image.readAsBytesSync()).then((value){
          files = {"one_on_one_question_image[]": value};
          return Future.value();
        });
      }else{
        return Future.value();
      }
    }).then((value){
      return _networkComponent.shared.requestMultipart('post', '/oneOnOneQuestion/$inquiryNo', map, files).then((response){
        if(response.success){
          Inquiry inquiry = Inquiry.fromJson(response.data);
          return inquiry;
        }else{
          return null;
        }
      });
    }).catchError((onError){
      print(onError);
      return null;
    });
  }

}