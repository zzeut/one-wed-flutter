import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/spending_model.dart';
import 'package:onewed_app/models/total_spending_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class SpendingProvider with ChangeNotifier{
  NetworkComponent _networkComponent = new NetworkComponent();

  TotalSpending? _totalSpending;
  List<Spending> _spendingList = [];


  Future <TotalSpending?> selectTotalSpending(int userNo) async{
    Response response = await _networkComponent.shared.request('get', '/spending/sumSpendingMoney', {"user_no":"$userNo"});

    if(response.success){
      TotalSpending totalSpending = TotalSpending.fromJson(response.data);
      setTotalSpending(totalSpending);
      return totalSpending;
    }else{
      return null;
    }
  }

  void setTotalSpending(TotalSpending totalSpending){
    _totalSpending = totalSpending;
    notifyListeners();
  }

  TotalSpending? getTotalSpending(){
    return _totalSpending;
  }

  Future <List<Spending>?> selectSpendingList(int page, Map<String, String> data) async{
    Response response = await _networkComponent.shared.request('get', '/spending', data);
    if(response.success){
      List<Spending> spendingList = (response.data['data'] as List).map((json) => Spending.fromJson(json)).toList();
      setSpendingList(page, spendingList);
      return spendingList;
    }else{
      return null;
    }
  }

  void setSpendingList(int page, List<Spending> spendingList){
    if(page == 1){
      _spendingList = spendingList;
    }else{
      _spendingList.addAll(spendingList);
    }

    notifyListeners();
  }

  List<Spending> getSpendingList(){
    return _spendingList;
  }

}