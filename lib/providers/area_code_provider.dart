import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/area_code_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class AreaCodeProvider with ChangeNotifier{
  NetworkComponent _networkComponent = new NetworkComponent();


  List<AreaCode> _areaCodeList = [];

  Future <List<AreaCode>?> selectAreaCodeList(Map<String, String> data) async{
    Response response = await _networkComponent.shared.request('get', '/areaCode', data);
    if(response.success){
      List<AreaCode> areaCodeList = (response.data['data'] as List).map((json) => AreaCode.fromJson(json)).toList();
      setAreaCodeList(areaCodeList);
      return areaCodeList;

    }else{
      return null;
    }
  }

  void setAreaCodeList(List<AreaCode> areaCodeList){
    _areaCodeList = areaCodeList;
    notifyListeners();
  }

  List<AreaCode> getAreaCodeList(){
    return _areaCodeList;
  }
}