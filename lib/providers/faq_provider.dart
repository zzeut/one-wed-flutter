import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/faq_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class FaqProvider with ChangeNotifier{
  NetworkComponent _networkComponent = new NetworkComponent();

  List<Faq> _faqList = [];

  Future <List<Faq>?> selectFaq(int page) async {
    Response response =  await _networkComponent.shared.request('get', '/faq', {"page":"$page"});
    if(response.success){
      List<Faq> faqList = (response.data['data'] as List).map((json) => Faq.fromJson(json)).toList();
      setFaqList(faqList);
      return faqList;
    }else{
      return null;
    }
  }

  void setFaqList(List<Faq> faqList){
    _faqList.addAll(faqList);
    notifyListeners();
  }

  List<Faq> getFaqList(){
    return _faqList;
  }

  void updateFaq(Faq faq){
    int index = _faqList.indexWhere((element) => element == faq);
    if(index > -1){
      _faqList[index] = faq;
      notifyListeners();
    }
  }

}