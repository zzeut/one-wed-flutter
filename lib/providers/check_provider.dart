import 'package:flutter/material.dart';
import 'package:onewed_app/models/check_category_model.dart';
import 'package:onewed_app/models/check_model.dart';
import 'package:onewed_app/models/oauth_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class CheckProvider with ChangeNotifier {
  Map<int, List<Check>> _checks = {};
  bool _isLoading = true;
  bool _isError = false;
  Oauth? _oauth;
  NetworkComponent _networkComponent = NetworkComponent();

  List<CheckCategory> _checkCategoryList = [];

  Map<int, List<Check>> get checks => _checks;
  bool get isLoading => _isLoading;
  bool get isError => _isError;

  Future<List<Check>?> selectCheckList(Map<String, dynamic> map) async{
    setLoading(true);
    Map<String, dynamic> params = {
      'more_field': '*'
    };
    params.addAll(map);

    Response response = await _networkComponent.shared.request('get', '/check/check', params);
    if(response.success){
      setLoading(false);
      setError(true);

      List<Check> checkList = (response.data['data'] as List).map((json) => Check.fromJson(json)).toList();
      return checkList;
    }else{
      setLoading(false);
      setError(false);
      return null;
    }
  }

  void setChecks(Map<int, List<Check>> checks) {
    _checks = checks;

    notifyListeners();
  }

  void setLoading(bool isLoading) {
    _isLoading = isLoading;

    notifyListeners();
  }

  void setError(bool isError) {
    _isError = isError;

    notifyListeners();
  }

  Future<List<CheckCategory>?> selectCheckCategoryList() async{
    setLoading(true);
    Response response = await _networkComponent.shared.request('get', '/check', null);

    if(response.success){
      setLoading(false);
      setError(true);

      List<CheckCategory> checkCategoryList = (response.data as List).map((json) => CheckCategory.fromJson(json)).toList();
      setCheckCategoryList(checkCategoryList);
      return checkCategoryList;

    }else{
      setLoading(false);
      setError(false);
      return null;
    }
  }

  void setCheckCategoryList(List<CheckCategory> checkCategoryList){
    _checkCategoryList = checkCategoryList;
    notifyListeners();
  }

  List<CheckCategory> getCheckCategoryList(){
    return _checkCategoryList;
  }
  
  Future<Check?> userCheck(int checkNo) async{
    Response response = await _networkComponent.shared.request('post', '/check/userCheck/$checkNo', null);

    if(response.success){
      Check _check = Check.fromJson(response.data);
      return _check;
    }else{
      return null;
    }
  }
}
