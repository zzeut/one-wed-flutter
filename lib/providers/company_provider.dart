import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/company_like_model.dart';
import 'package:onewed_app/models/company_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class CompanyProvider with ChangeNotifier{
  NetworkComponent _networkComponent = new NetworkComponent();


  bool _isLoading = true;
  bool _isError = false;


  bool get isLoading => _isLoading;
  bool get isError => _isError;


  List<Company> _searchCompanyList = [];
  Map<int, List<Company>> _companyList = {};
  Company? _company;

  Future<List<Company>?> selectCompany(int categoryNo, int page, Map<String, String> map) async{
    setLoading(true);
    Map<String, String> params = {

    };

    params.addAll(map);

    Response response = await _networkComponent.shared.request('get', '/company', params);

    if(response.success){

      setLoading(false);
      setError(false);
      List<Company> companyList = (response.data['data'] as List).map((json) => Company.fromJson(json)).toList();
      addCompanyList(categoryNo, page, companyList);
      return companyList;
    }else{
      setLoading(false);
      setError(true);
      return null;
    }
  }


  void addCompanyList(int categoryNo, int page, List<Company> companyList){
    if(_companyList[categoryNo] == null){
      _companyList[categoryNo] = [];
    }

    if(page == 1){
      _companyList[categoryNo] = companyList;
    }else{
      _companyList[categoryNo]!.addAll(companyList);
    }

    notifyListeners();
  }

  List<Company> getCompanyList(int categoryNo){
    return _companyList[categoryNo] ?? [];
  }


  void setLoading(bool isLoading) {
    _isLoading = isLoading;

    notifyListeners();
  }

  void setError(bool isError) {
    _isError = isError;

    notifyListeners();
  }




  void companyListUpdate(int categoryNo, Company company){
    if(_companyList[categoryNo] != null){
      int index = _companyList[categoryNo]!.indexWhere((element) => element.companyNo == company.companyNo);
      print("index ::::: $index");
      if(index > -1){
        _companyList[categoryNo]![index] = company;
        notifyListeners();
      }
    }

  }

  Future<Company?> selectCompanyDetail(int companyNo) async{
    Response response = await _networkComponent.shared.request('get', '/company', {"more_field":"*", "company_no":"$companyNo"});

    if(response.success){
      setLoading(false);
      setError(false);
      List<Company> company = (response.data['data'] as List).map((json) => Company.fromJson(json)).toList();
      setCompanyDetail(company[0]);
      return company[0];
    }else{
      setLoading(false);
      setError(true);
      return null;
    }
  }

  void setCompanyDetail(Company? company){
    _company = company;
    notifyListeners();
  }

  Company? getCompany(){
    return _company;
  }


  Future<List<Company>?> searchCompany(int page, Map<String, String> params) async{
    setLoading(true);

    Response response = await _networkComponent.shared.request('get', '/company', params);

    if(response.success){
      setLoading(false);
      setError(false);
      List<Company> companyList = (response.data['data'] as List).map((json) => Company.fromJson(json)).toList();
      setSearchCompanyList(page, companyList);
      return companyList;
    }else{
      setLoading(false);
      setError(true);
      return null;
    }
  }

  void setSearchCompanyList(int page, List<Company> companyList){
    if(page == 1){
      _searchCompanyList = companyList;
    }else{
      _searchCompanyList.addAll(companyList);
    }

    notifyListeners();
  }

  List<Company> getSearchCompanyList(){
    return _searchCompanyList;
  }

  void searchCompanyListUpdate(Company company){
    int index = _searchCompanyList.indexWhere((element) => element.companyNo == company.companyNo);
    if(index > -1){
      _searchCompanyList[index] = company;
      notifyListeners();
    }

  }
}
