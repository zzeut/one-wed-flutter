import 'dart:ffi';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/chat_model.dart';
import 'package:onewed_app/models/matching_model.dart';
import 'package:onewed_app/models/message_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';
import 'package:onewed_app/utils/resize_client.dart';

class BroadCastProvider with ChangeNotifier{
  bool _isLoading = true;
  bool _isError = false;
  NetworkComponent _networkComponent = NetworkComponent();
  List<Chat> _chatList = [];
  List<Message> _messageList = [];

  Future<List<Message>?> selectMessageList(String threadId, int? lastMessageNo) async {
    setLoading(true);
    print('lastMessageNo :: ${lastMessageNo}');
    Map<String, dynamic>? data;
    if (lastMessageNo == null) {
      resetMessageList();
    } else {
      print('tqtqtqtq');
      data = {
        'message_no': lastMessageNo.toString()
      };
    }
    Response response = await _networkComponent.shared.request('get', '/broadcast/${threadId}', data);

    if (response.success) {
      List<Message> messageList = (response.data['data'] as List).map((record) => Message.fromJson(record)).toList();
      // List<Message> reversedMessageList = List.from(messageList.reversed);
      addMessageList(messageList);
      setLoading(false);
      return messageList;
    } else {
      setError(true);
      return null;
    }
  }

  Future<List<Chat>?> selectChatList(int page) async {
    setLoading(true);

    Response response = await _networkComponent.shared.request('get', '/broadcast', {
      "page": page.toString(),
      "more_field": "*"
    });

    print("response.data['data] :::::::::::::::::::::::::::::::::::::::::::::");
    print(response.data['data']);

    if (response.success) {
      print(response.data);
      List<Chat> chatList = (response.data['data'] as List).map((record) => Chat.fromJson(record)).toList();
      print("response.data['data] :::::::::::::::::::::::::::::::::::::::::::::");
      print(response.data['data']);
      addChatList(page, chatList);
      setLoading(false);
      return chatList;
    } else {
      setLoading(false);
      setError(true);
      return null;
    }
  }

  Future<void> sendMessage(String threadId, String? message, List<File>? images) async {
    Map<String, Uint8List> files = {};
    return Future.value().then((value){
      if(images != null){
        List<Future> futureList = [];
        images.asMap().forEach((index, image) {
          futureList.add(resizeFuture(image.readAsBytesSync()).then((value) {
            files["image[${index}]"] = value;
          }));
        });
        return Future.wait(futureList).then((value) {
          return Future.value();
        });
      }else{
        return Future.value();
      }
    }).then((value){
      Map data = {
        "thread_id": threadId,
      };
      if (message != null) {
        data["message"] = message;
      }
      return _networkComponent.shared.requestMultipart('post', '/broadcast/chat', data, files).then((response){

      });
    }).catchError((onError){
      print(onError);
      return null;
    });
  }
  Future<Response> leaveChannel(String threadId) async {
    Response response = await _networkComponent.shared.request('delete', '/broadcast/${threadId}',null);
    return response;
  }

  void addChatList(page, value) {
    if(page == 1){
      _chatList = value;
    }else{
      _chatList.addAll(value);
    }

    notifyListeners();
  }

  void resetChatList() {
    _chatList = [];
  }
  List<Chat> getChatList() {
    return _chatList;
  }

  void addMessageList(value) {
    _messageList.addAll(value);
    // _messageList.insertAll(0, value);
    notifyListeners();
  }
  void addMessage(Message message) {
    _messageList.insert(0, message);
    notifyListeners();
  }
  void resetMessageList() {
    _messageList = [];
  }

  List<Message> getMessageList() {
    return _messageList;
  }

  void setLoading(bool isLoading) {
    _isLoading = isLoading;

    notifyListeners();
  }

  bool isLoading() {
    return _isLoading;
  }

  void setError(bool isError) {
    _isError = isError;

    notifyListeners();
  }

  Future<Chat?> selectChat(String threadId) async{
    setLoading(true);
    Response response = await _networkComponent.shared.request('get', '/broadcast', {"thread_id": threadId});
    if(response.success){
      if(response.data != null){
        List<Chat> chat = (response.data['data'] as List).map((json) => Chat.fromJson(json)).toList();
        setLoading(false);
        return chat[0];
      }else{
        setLoading(false);
        return null;
      }

    }else{
      return null;
    }
  }

  void updateChatList(Chat chat){

    int index = _chatList.indexWhere((element) => element.participantNo == chat.participantNo);
    if(index > -1){
      _chatList[index] =chat;
      notifyListeners();
    }
  }



}