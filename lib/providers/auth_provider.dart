import 'dart:convert';
import 'dart:core';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:onewed_app/models/oauth_model.dart';

class
AuthProvider with ChangeNotifier {
  Oauth _oauth = Oauth();
  SharedPreferences? _sharedPreferences;

  Oauth get oauth => _oauth;

  Future<void> getOauth() async {
    try {
      _sharedPreferences = await SharedPreferences.getInstance();

      String? auth = _sharedPreferences!.getString('auth');

      if (auth != null) {
        setOauth(Oauth.fromJson(jsonDecode(auth)));
      } else {
        setOauth(Oauth());
      }

      setOauth(Oauth(accessToken: 'test'));
    } catch (e) {
      print(['getOauth', e]);
    }
  }

  Future<void> login() async {
    try {
      _sharedPreferences = await SharedPreferences.getInstance();

      _sharedPreferences!.setString(
        'auth',
        jsonEncode(
          Oauth(accessToken: 'test'),
        ),
      );

      setOauth(Oauth(accessToken: 'test'));
    } catch (e) {
      print(['login', e]);
    }
  }

  Future<void> logout() async {
    try {
      setOauth(Oauth());
    } catch (e) {
      print(['logout', e]);
    }
  }

  void setOauth(Oauth oauth) {
    _oauth = oauth;

    notifyListeners();
  }
}
