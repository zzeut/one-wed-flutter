import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/company_image_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class CompanyImageProvider extends ChangeNotifier{
  NetworkComponent _networkComponent = new NetworkComponent();

  bool _isLoading = true;
  bool _isError = false;

  bool get isLoading => _isLoading;
  bool get isError => _isError;

  List<CompanyImage> _companyImageList = [];


  Future <List<CompanyImage>?> selectCompanyImageList(int page, Map<String, String> data) async{

    setLoading(true);
    Response response = await _networkComponent.shared.request('get', '/company/image', data);
    if(response.success){
      setLoading(false);
      setError(false);
      List<CompanyImage> companyImageList = (response.data['data'] as List).map((e) => CompanyImage.fromJson(e)).toList();
      setCompanyImage(page, companyImageList);
      return companyImageList;
    }else{
      setLoading(false);
      setError(true);
      return null;
    }
  }

  void setCompanyImage(int page, List<CompanyImage> companyImageList){
    if(page == 1){
      _companyImageList = companyImageList;
    }else{
      _companyImageList.addAll(companyImageList);
    }

    notifyListeners();
  }

  List<CompanyImage> getCompanyImageList(){
    return _companyImageList;
  }

  void setLoading(bool loading){
    _isLoading = loading;
    notifyListeners();
  }

  void setError (bool error){
    _isError = error;
    notifyListeners();
  }

}