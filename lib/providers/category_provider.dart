import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/category_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class CategoryProvider with ChangeNotifier{
  NetworkComponent _networkComponent = NetworkComponent();

  bool _isLoading = true;
  bool _isError = false;
  bool get isLoading => _isLoading;
  bool get isError => _isError;


  Map<int, List<Categories>> _typeCategoryList = {};
  List<Categories> _categoryList = [];

  Categories? _category;


  Future<List<Categories>?> selectTypeCategoryList(int type, Map<String, String> params) async{
    setLoading(true);

    print("selectTypeCategoryList type ::::: $type");
    print(params);

    Response response = await _networkComponent.shared.request('get', '/category', params);
    if(response.success){
      setLoading(false);
      setError(true);
      List<Categories> categoryList = (response.data as List).map((json) => Categories.fromJson(json)).toList();
      setTypeCategoryList(type, categoryList);
      return categoryList;
    }else{
      setLoading(false);
      setError(false);
      return null;
    }
  }

  void setTypeCategoryList(int type, List<Categories> categoryList){
    _typeCategoryList[type] = categoryList;
    notifyListeners();
  }

  List<Categories> getTypeCategoryList(int type){
    print(_typeCategoryList);
    return _typeCategoryList[type] ?? [];

  }

  Future<Categories?> selectCategory(Map<String, String> data) async{

    setLoading(true);

    Response response = await _networkComponent.shared.request('get', '/category', data);
    if(response.success){
      setLoading(false);
      setError(true);
      Categories category = Categories.fromJson(response.data[0]);
      setCategory(category);
      return category;
    }else{
      setLoading(false);
      setError(false);
      return null;
    }

  }

  void setCategory(Categories category){
    _category = category;
    notifyListeners();
  }

  Categories? getCategory(){
    return _category;
  }




  void setLoading(bool isLoading) {
    _isLoading = isLoading;

    notifyListeners();
  }

  void setError(bool isError) {
    _isError = isError;

    notifyListeners();
  }

  Future<List<Categories>?> selectCategoryList(Map<String, String> params) async{
    setLoading(true);

    Response response = await _networkComponent.shared.request('get', '/category', params);
    if(response.success){
      setLoading(false);
      setError(true);
      List<Categories> categoryList = (response.data as List).map((json) => Categories.fromJson(json)).toList();
      setCategoryList(categoryList);
      return categoryList;
    }else{
      setLoading(false);
      setError(false);
      return null;
    }
  }

  void setCategoryList(List<Categories> categoryList){
    _categoryList = categoryList;
    notifyListeners();
  }

  List<Categories> getCategoryList(){
    return _categoryList;

  }


}