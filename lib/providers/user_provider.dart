
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:onewed_app/models/oauth_model.dart';
import 'package:onewed_app/models/user_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';
import 'package:onewed_app/utils/resize_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserProvider with ChangeNotifier {
  User _user = User();
  bool _isLoading = true;
  bool _isError = false;
  Oauth? _oauth;
  NetworkComponent _networkComponent = NetworkComponent();


  SharedPreferences? prefs;

  User get user => _user;
  bool get isLoading => _isLoading;
  bool get isError => _isError;
  
  User? _me;

  Future<void> getUser(String moreField) async {
    try {
      setLoading(true);

      setUser(User(name: 'test'));

      Response response = await _networkComponent.shared
          .request('get', '/user/me', {"more_field": moreField});

      if (response.success) {
        final User user = User.fromJson(response.data);

        setUser(user);
      } else {
        setError(true);
        print(['getUser', response.error]);
      }
    } catch (e) {
      setError(true);
      print(['getUser', e]);
    } finally {
      setLoading(false);
    }
  }

  void setUser(User user) {
    _user = user;

    notifyListeners();
  }

  void setLoading(bool isLoading) {
    _isLoading = isLoading;

    notifyListeners();
  }

  void setError(bool isError) {
    _isError = isError;

    notifyListeners();
  }

  Future<User?> selectMe(String moreField) async {
    setLoading(true);
    Response response = await _networkComponent.shared.request('get', '/user/me', {"more_field": moreField});
    if (response.success) {
      print(response.data);
      User user = User.fromJson(response.data);
      this.setMe(user);
      setLoading(false);
      return user;
    } else {
      setLoading(false);
      return null;
    }
  }
  
  void setMe(User user){
    _me = user;
    notifyListeners();
  }
  
  User? getMe(){
   return _me; 
  }


  Future<Response?> socialLogin(String type, Map map) async{
    setLoading(true);
    Response response = await _networkComponent.shared.request('post', '/user/login/$type', map);
    if(response.success){
      setLoading(false);
      Oauth responseOauth = Oauth.fromJson(response.data);
      setOauth(responseOauth);
      return response;
    }else{
      print('==================e==================');
      setLoading(false);
      return null ;
    }
  }
  void setOauth(Oauth oauth) async {
    _oauth = oauth;

    prefs = await SharedPreferences.getInstance();
    prefs!.setString("accessToken", _oauth!.accessToken!);
    prefs!.setString("refreshToken", _oauth!.refreshToken!);

    notifyListeners();
  }

  Future<User?> modifyMe(Map data, File? image) async{
    setLoading(true);

    Map<String, Uint8List> files = {};
    return Future.value().then((value) {
      if(image != null){
        return resizeFuture(image.readAsBytesSync()).then((value){
          files = {"user_profile_image": value};
          return Future.value();
        });
      }else{
        return Future.value();
      }
    }).then((value) async {
      Response response = await _networkComponent.shared.requestMultipart('post', '/user/me', data, files);
      print(response.data);
      if(response.success){
        User user = User.fromJson(response.data);
        return user;
      }else{
        return null;
      }
    });
  }

  Future<User?> withdrawalMe() async{
    setLoading(true);
    Response response = await _networkComponent.shared.request('delete', '/user/me', null);
    if(response.success){
      User user = User.fromJson(response.data);
      return user;
    }else{
      return null;
    }
  }
}
