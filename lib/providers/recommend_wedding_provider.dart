import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/recommend_wedding_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class RecommendWeddingProvider with ChangeNotifier{
  
  NetworkComponent _networkComponent = new NetworkComponent();
  
  RecommendWedding? _recommendWedding;
  
  Future<RecommendWedding?> selectRecommendWedding() async{
    Response response = await _networkComponent.shared.request('get', '/recommended-wedding-information', {"more_field": "*"});
    if(response.success){
      RecommendWedding recommendWedding = RecommendWedding.fromJson(response.data);
      setRecommendWedding(recommendWedding);
      return recommendWedding;
    }else{
      return null;
    }
  }

  void setRecommendWedding(RecommendWedding recommendWedding){
    _recommendWedding = recommendWedding;
    notifyListeners();
  }

  RecommendWedding? getRecommendWedding(){
    return _recommendWedding;
  }
  
}