import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/schedule_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class ScheduleProvider with ChangeNotifier{
  NetworkComponent _networkComponent = new NetworkComponent();


  bool _isLoading = true;
  bool _isError = false;


  bool get isLoading => _isLoading;
  bool get isError => _isError;

  List<Schedule> _scheduleList = [];
  Schedule? _schedule;
  List<Schedule> _reviewScheduleList = [];

  Map<DateTime, List<Schedule>> _calendarScheduleList = {};

  Future <List<Schedule>?> selectScheduleList(int page, Map<String, String> params) async {
    setLoading(true);
    Response response = await _networkComponent.shared.request('get', '/schedule', params);
    if(response.success){
      setLoading(false);
      setError(false);
      List<Schedule> scheduleList = (response.data['data'] as List).map((e) => Schedule.fromJson(e)).toList();
      setScheduleList(page, scheduleList);
      return scheduleList;
    }else{
      setLoading(false);
      setError(true);
      return null;
    }
  }

  void setLoading(bool isLoading) {
    _isLoading = isLoading;

    notifyListeners();
  }

  void setError(bool isError) {
    _isError = isError;

    notifyListeners();
  }

  void setScheduleList(int page, List<Schedule> scheduleList){
    if(page == 1){
      _scheduleList = scheduleList;
    }else{
      _scheduleList.addAll(scheduleList);
    }

    notifyListeners();
  }

  List<Schedule> getScheduleList(){
    return _scheduleList;
  }

  Future <Schedule?> selectScheduleDetail(int scheduleNo) async{
    Map<String, String> params = {
      "schedule_no":"$scheduleNo",
      "more_field": "*"
    };
    Response response = await _networkComponent.shared.request('get', '/schedule', params);
    if(response.success){
      Schedule schedule = Schedule.fromJson(response.data['data'][0]);
      setSchedule(schedule);
      return schedule;
    }else{
      return null;
    }
  }

  void setSchedule(Schedule schedule){
    _schedule = schedule;
    notifyListeners();
  }

  Schedule? getSchedule(){
    return _schedule;
  }


  Future <List<Schedule>?> selectReviewScheduleList(int page, Map<String, String> params) async {
    setLoading(true);
    Response response = await _networkComponent.shared.request('get', '/schedule', params);
    if(response.success){
      setLoading(false);
      setError(false);
      List<Schedule> scheduleList = (response.data['data'] as List).map((e) => Schedule.fromJson(e)).toList();
      setReviewScheduleList(page, scheduleList);
      return scheduleList;
    }else{
      setLoading(false);
      setError(true);
      return null;
    }
  }

  void setReviewScheduleList(int page, List<Schedule> scheduleList){
    if(page == 1){
      _reviewScheduleList = scheduleList;
    }else{
      _reviewScheduleList.addAll(scheduleList);
    }

    notifyListeners();
  }

  List<Schedule> getReviewScheduleList(){
    return _reviewScheduleList;
  }

  void updateReviewScheduleList(Schedule schedule){
    int index = _reviewScheduleList.indexWhere((element) => element.scheduleNo == schedule.scheduleNo);
    if(index > -1){
      _reviewScheduleList[index] = schedule;
      notifyListeners();
    }
  }

  void removeReviewSchedule(Schedule schedule){
    _reviewScheduleList.removeWhere((element) => element.scheduleNo == schedule.scheduleNo);

    notifyListeners();
  }
  
  Future<Map<DateTime, List<Schedule>>?> selectCalendarScheduleList(Map<String, String> data) async{
    Response response = await _networkComponent.shared.request('get', '/schedule/date', data);
    if(response.success){
      Map<DateTime, List<Schedule>> calendarScheduleList = {};
      if(response.data.length >0){

        response.data.forEach((key, value){
          List<Schedule> scheduleList = (value as List).map((json) => Schedule.fromJson(json)).toList();
          calendarScheduleList.addAll({DateFormat("yyyy-MM-dd").parse('$key'): scheduleList});
        });
      }

      setCalendarScheduleList(calendarScheduleList);

      return calendarScheduleList;

    }else{
      return null;
    }
  }

  void setCalendarScheduleList(Map<DateTime, List<Schedule>> calendarScheduleList){
    _calendarScheduleList = calendarScheduleList;
    notifyListeners();
  }

  Map<DateTime, List<Schedule>> getCalendarScheduleList(){
    return _calendarScheduleList;
  }
  
}