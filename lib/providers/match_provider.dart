import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:onewed_app/models/matching_model.dart';
import 'package:onewed_app/models/thread_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class MatchProvider with ChangeNotifier{
  NetworkComponent _networkComponent = new NetworkComponent();

  Future<Thread?> checkMatching(int userNo, int plannerNo) async{
    return await _networkComponent.shared.request('get', '/matching/check', {
      "user_no": "$userNo",
      "planner_no": "$plannerNo"
    }).then((response) {
      if(response == null){
        return null;
      }else{
        Thread thread = Thread.fromJson(response.data);
        return thread;
      }


    }).catchError((onError){
      return null;
    });

  }



  Future<Thread?> checkCompanyMatching(int userNo, int companyNo) async{
    return await _networkComponent.shared.request('get', '/matching/check/company', {
      "user_no": "$userNo",
      "company_no": "$companyNo"
    }).then((response) {
      Thread thread = Thread.fromJson(response.data);
      return thread;

    }).catchError((onError){
      return null;
    });

  }



  Future<Matching?> insertMatching(int userNo, int? plannerNo) async{
    Map<String, String> data = {
      "user_no": "$userNo",
      "status": "0"
    };

    if(plannerNo != null){
      data['planner_no'] = "$plannerNo";
    }

    Response response = await _networkComponent.shared.request('post', '/matching', data);

    if(response.success){
      Matching matching = Matching.fromJson(response.data);
      return matching;
    }else{
      return null;
    }

  }

  Future<List<Matching>?> selectSelectMating(Map<String, String> data) async{

    Response response = await _networkComponent.shared.request('get', '/matching', data);

    if(response.success){
      List<Matching> matchingList = (response.data['data'] as List).map((json) => Matching.fromJson(json)).toList();
      return matchingList;
    }else{
      return null;
    }
  }

}