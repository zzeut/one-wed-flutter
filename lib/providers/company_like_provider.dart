import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/company_like_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class CompanyLikeProvider with ChangeNotifier{

  NetworkComponent _networkComponent = new NetworkComponent();

  Map<int, List<CompanyLike>> _companyLikeList = {};

  Future<List<CompanyLike>?> selectCompanyLike(int categoryNo,int page, Map<String, String> data) async{

    Response response = await _networkComponent.shared.request('get', '/company/like', data);
    if(response.success){
      List<CompanyLike> companyLikeList = (response.data['data'] as List).map((e) => CompanyLike.fromJson(e)).toList();
      addCompanyLikeList(categoryNo, page, companyLikeList);
      return companyLikeList;
    }else{
      return null;
    }
  }

  void addCompanyLikeList(int categoryNo, int page, List<CompanyLike> companyLikeList){
    if(page == 1){
      _companyLikeList[categoryNo] = companyLikeList;
    }else{
      _companyLikeList[categoryNo]!.addAll(companyLikeList);
    }
    notifyListeners();
  }

  List<CompanyLike> getCompanyLikeList(int categoryNo){
    return _companyLikeList[categoryNo] ?? [];
  }

  Future<CompanyLike?> companyLike(int companyNo) async{

    print("data :::: $companyNo");

    Response response = await _networkComponent.shared.request('post', '/company/like/$companyNo', null);
    if(response.success){
      CompanyLike companyLike = CompanyLike.fromJson(response.data);
      return companyLike;
    }else{
      return null;
    }
  }

  void updateCompanyLike(int categoryNo, CompanyLike companyLike){
    if(_companyLikeList[categoryNo] != null){
      int index = _companyLikeList[categoryNo]!.indexWhere((element) => element.companyNo == companyLike.companyNo);
      if(index > -1){
        _companyLikeList[categoryNo]![index] = companyLike;
        notifyListeners();

      }
    }


  }

}