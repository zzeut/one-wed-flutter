import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/term_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class TermProvider with ChangeNotifier{
  NetworkComponent _networkComponent = new NetworkComponent();

  Term? _term;

  Future <Term?> selectTerm(String type) async{
    Response response = await _networkComponent.shared.request('get', '/term', {"type":"$type"});

    if(response.success){
      List<Term> terms = (response.data as List).map((json) => Term.fromJson(json)).toList();
      setTerm(terms[0]);
      return terms[0];
    }else{
      return null;
    }
  }

  void setTerm(Term term){
    _term = term;
    notifyListeners();
  }

  Term? getTerm(){
    return _term;
  }
}