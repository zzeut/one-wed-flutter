import 'dart:ffi';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:onewed_app/models/company_review_model.dart';
import 'package:onewed_app/models/oauth_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';
import 'package:onewed_app/utils/resize_client.dart';

class CompanyReviewProvider with ChangeNotifier {
  bool _isLoading = true;
  bool _isError = false;

  bool get isLoading => _isLoading;
  bool get isError => _isError;

  NetworkComponent _networkComponent = NetworkComponent();

  Map<int, List<CompanyReview>> _companyCategoryReviewList  = {};

  CompanyReview? _companyReview;

  List<CompanyReview> _companyReviewList = [];



  Future <List<CompanyReview>?> selectCategoryCompanyReviewList(int categoryNo, int page, Map<String, String> params) async{

    setLoading(true);
    Response response = await _networkComponent.shared.request('get', '/company/review', params);
    if(response.success){
      setLoading(false);
      setError(false);

      List<CompanyReview> companyReviewList = (response.data['data'] as List).map((json) => CompanyReview.fromJson(json)).toList();
      setCategoryCompanyReviewList(categoryNo, page, companyReviewList);
      return companyReviewList;
    }else{
      setLoading(false);
      setError(true);

      return null;
    }
  }

  void setCategoryCompanyReviewList(int categoryNo, int page, List<CompanyReview> companyReviewList) {
    if(page == 1){
      _companyCategoryReviewList[categoryNo] = companyReviewList;
    }else{
      _companyCategoryReviewList[categoryNo]!.addAll(companyReviewList);
    }

    notifyListeners();
  }

  List<CompanyReview> getCategoryCompanyReviewList(int categoryNo){

    return _companyCategoryReviewList[categoryNo] ?? [];
  }

  void setLoading(bool isLoading) {
    _isLoading = isLoading;

    notifyListeners();
  }

  void setError(bool isError) {
    _isError = isError;

    notifyListeners();
  }

  Future<CompanyReview?> insertCompanyReview(int scheduleNo, Map data, List<File>? images) async{

    setLoading(true);

    Map<String, Uint8List> files = {};
    List<Future> list = [];

    print(images!.length);

    if(images != null){
      images.forEach((image) {
        int index = images.indexOf(image);
        list.add(
            resizeFuture(image.readAsBytesSync()).then((value){
              files["company_review_image[$index]"] = value;
            })
        );

      });
    }


    return Future.wait(list).then((value) async{
      Response response = await _networkComponent.shared.requestMultipart('post', '/company/review/$scheduleNo', data, files);
      if(response.success){
        setLoading(false);
        setError(false);
        CompanyReview companyReview = CompanyReview.fromJson(response.data);
        return companyReview;
      }else{
        setLoading(false);
        setError(true);
        return null;
      }

    });

  }

  Future<CompanyReview?> selectCompanyReview(int companyReviewNo) async{
    Map<String, String> params = {
      "more_field": "*",
      "company_review_no": "$companyReviewNo"
    };

    setLoading(true);

    Response response = await _networkComponent.shared.request('get', '/company/review', params);
    if(response.success){
      setLoading(false);
      setError(false);

      List<CompanyReview> companyReviewList = (response.data['data'] as List).map((json) => CompanyReview.fromJson(json)).toList();
      setCompanyReview(companyReviewList[0]);
      return companyReviewList[0];
    }else{
      setLoading(false);
      setError(true);

      return null;
    }
  }

  void setCompanyReview(CompanyReview companyReview){
    _companyReview = companyReview;
    notifyListeners();
  }

  CompanyReview? getCompanyReview(){
    return _companyReview;
  }


  Future<CompanyReview?> deleteCompanyReview(int companyReviewNo) async{
    Response response = await _networkComponent.shared.request('delete', '/company/review/$companyReviewNo', null);
    if(response.success){
      CompanyReview companyReview = CompanyReview.fromJson(response.data);
      return companyReview;
    }else{
      return null;
    }
  }

  void deleteCategoryCompanyReviewUpdate(int categoryNo, CompanyReview companyReview){
    if(_companyCategoryReviewList[categoryNo] != null){
      _companyCategoryReviewList[categoryNo]!.removeWhere((element) => element.companyReviewNo == companyReview.companyReviewNo);
      notifyListeners();
    }

  }


  Future <List<CompanyReview>?> selectCompanyReviewList(int page, Map<String, String> params) async{

    setLoading(true);

    Response response = await _networkComponent.shared.request('get', '/company/review', params);
    if(response.success){
      setLoading(false);
      setError(false);

      List<CompanyReview> companyReviewList = (response.data['data'] as List).map((json) => CompanyReview.fromJson(json)).toList();
      setCompanyReviewList(page, companyReviewList);
      return companyReviewList;
    }else{
      setLoading(false);
      setError(true);

      return null;
    }
  }

  void setCompanyReviewList(int page, List<CompanyReview> companyReviewList){
    if(page == 1){
      _companyReviewList = companyReviewList;
    }else{
      _companyReviewList.addAll(companyReviewList);
    }

    notifyListeners();
  }

  List<CompanyReview> getCompanyReviewList(){
    return _companyReviewList;
  }

  Future<CompanyReview?> modifyCompanyReview(int companyReviewNo, Map data, List<File>? images) async{
    setLoading(true);

    Map<String, Uint8List> files = {};
    List<Future> list = [];

    if(images != null){
      images.forEach((image) {
        list.add(
            resizeFuture(image.readAsBytesSync()).then((value){
              files["company_review_image[]"] = value;
            })
        );

      });
    }

    return Future.wait(list).then((value) async{
      Response response = await _networkComponent.shared.requestMultipart('post', '/company/review/$companyReviewNo', data, files);
      if(response.success){
        setLoading(false);
        setError(false);
        CompanyReview companyReview = CompanyReview.fromJson(response.data);
        return companyReview;
      }else{
        setLoading(false);
        setError(true);
        return null;
      }
    });
  }

  int? categoryCompanyReviewListUpdate(int categoryNo, CompanyReview companyReview){
    if(_companyCategoryReviewList[categoryNo] != null){
      int index = _companyCategoryReviewList[categoryNo]!.indexWhere((element) => element.companyReviewNo == companyReview.companyReviewNo);

      if(index > -1){
        print(3333333);
        _companyCategoryReviewList[categoryNo]![index] = companyReview;
        notifyListeners();
      }


      return index;
    }

  }

  int companyReviewListUpdate(CompanyReview companyReview){
    int index = _companyReviewList.indexWhere((element) => element.companyReviewNo == companyReview.companyReviewNo);
    if(index > -1){
      _companyReviewList[index] = companyReview;
      notifyListeners();
    }

    return index;
  }

  void deleteCompanyReviewUpdate(CompanyReview companyReview){
    print('_companyReviewList :::: ${_companyReviewList}');
    _companyReviewList.removeWhere((element) => element.companyReviewNo == companyReview.companyReviewNo);
    notifyListeners();
  }


}
