import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/onewed_television_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class OnewedTelevisionProvider with ChangeNotifier{
  NetworkComponent _networkComponent = new NetworkComponent();
  
  OnewedTelevision? _onewedTelevision;
  
  Future<OnewedTelevision?> selectOnewedTelevision() async{
    Response response = await _networkComponent.shared.request('get', '/onewed-television', null);
    if(response.success){
      OnewedTelevision onewedTelevision = OnewedTelevision.fromJson(response.data);
      setOnewedTelevision(onewedTelevision);
      return onewedTelevision;
    }else{
      return null;
    }
  }

  void setOnewedTelevision(OnewedTelevision onewedTelevision){
    _onewedTelevision = onewedTelevision;
    notifyListeners();
  }

  OnewedTelevision? getOnewedTelevision(){
    return _onewedTelevision;
  }
}