import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/fcm_token_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class PushProvider with ChangeNotifier{
  NetworkComponent _networkComponent = new NetworkComponent();

  Future<FcmToken?> insertFcmToken(String token, String type) async{
    Response response = await _networkComponent.shared.request('post', '/push', {
      "fcm_token": token,
      "machine_type": type
    });

    if(response.success){
      FcmToken token = FcmToken.fromJson(response.data);
      return token;
    }else{
      return null;
    }
  }

  Future<Response?> deleteUserFcmToken() async{
    Response response = await _networkComponent.shared.request('delete', '/push', null);

    if(response.success){
      return response;
    }else{
      return null;
    }
  }


}