import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/onewed_information_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class OnewedInformationProvider with ChangeNotifier{

  NetworkComponent _networkComponent = new NetworkComponent();

  OnewedInformation? _onewedInformation;

  Future<OnewedInformation?> selectOnewedInformation() async{
    Response response = await _networkComponent.shared.request('get', '/onewed-information', null);
    if(response.success){
      OnewedInformation onewedInformation = OnewedInformation.fromJson(response.data);
      setOnewedInformation(onewedInformation);
      return onewedInformation;
    }else{
      return null;
    }
  }

  void setOnewedInformation(OnewedInformation onewedInformation){
    _onewedInformation = onewedInformation;
    notifyListeners();
  }

  OnewedInformation? getOnewedInformation(){
    return _onewedInformation;
  }

}