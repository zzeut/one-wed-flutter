import 'package:flutter/foundation.dart';
import 'package:onewed_app/models/notice_model.dart';
import 'package:onewed_app/networking/network_service.dart';
import 'package:onewed_app/networking/response.dart';

class NoticeProvider with ChangeNotifier{
  NetworkComponent _networkComponent = new NetworkComponent();


  bool _isLoading = true;
  bool _isError = false;

  bool get isLoading => _isLoading;
  bool get isError => _isError;

  List<Notice> _noticeList = [];
  List<Notice> get noticeList => _noticeList;


  Future<List<Notice>?> selectNotice(int page) async{

    setLoading(true);
    Response response = await _networkComponent.shared.request('get', '/notice', {"sort[by]":"created_at", "sort[order]":"desc", "page":"$page"});

    if(response.success){
      setLoading(false);
      setError(false);
      List<Notice> noticeList = (response.data['data'] as List).map((json) => Notice.fromJson(json)).toList();
      setNoticeList(page, noticeList);

      return noticeList;

    }else{
      setLoading(false);
      setError(true);
      return null;
    }
  }

  void setNoticeList(int page, List<Notice> noticeList){
    if(page == 1){
      _noticeList = noticeList;
    }else{
      _noticeList.addAll(noticeList);
    }

    notifyListeners();

  }

  void setLoading(bool isLoading) {
    _isLoading = isLoading;

    notifyListeners();
  }

  void setError(bool isError) {
    _isError = isError;

    notifyListeners();
  }

  void updateNoticeList(Notice notice){
    int index = this._noticeList.indexWhere((element) => element.noticeNo == notice.noticeNo);
    if (index > -1) {
      noticeList[index] = notice;
      notifyListeners();
    }
  }

}